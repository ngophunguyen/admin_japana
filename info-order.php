<style>
	.scroll-order{
		position: fixed;
		width: 94%;
		right: 0;
		top: 48px;
		height: 98px;
		padding: 15px 21px 0;
		border-bottom: 1px solid #e3e3e3;
		background: #fff;
		z-index: 1;
		display: grid;
		grid-template-columns: repeat(3,1fr);
	}
	.scroll-order .item{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	.scroll-order .item p{
		font-weight: 500;
    	font-size: 14px;
	}
	.scroll-order .custom-dropdown{
		width: 180px;
		margin-left: 5px;
	}
	.custom-dropdown:after{
		padding: 12px 15px;
	}
	.box-button-info-order{
  		display: none;
  		width: 44px;
  		height: 44px;
  		text-align: center;
  	}
  	.box-button-info-order a:hover{
  		color: #fff;
  	}
  	.show-info-order{
		color: #fff;
		z-index: 999;
	}
	#info-kh, #note-kh{
		width: 49%;
		float: left;
		display: inline-block;
		margin-top: 98px;
	}
	#note-kh{
		margin-left: 2%;
	}
	@media (max-width: 575.98px) {
	  	.entry-header ul li:first-child{
	  		display: none;
	  	}
	  	.box-button-info-order{
	  		display: block;
	  	}
	  	.scroll-order{
	  		height: auto;
	  		width: 100%;
	  		padding: 10px;
	  		display: none;
	  		top: 92px; /*****Test******/
	  		border-bottom: 2px solid #eee;
	  		z-index: 99;
	  	}
	  	.scroll-order .item{
	  		display: inline-block;
	  		width: 100%;
	  		margin-bottom: 10px;
	  	}
	  	.scroll-order .item:last-child{
	  		margin-bottom: 0;
	  	}
	  	.scroll-order .custom-dropdown{
	  		margin-left: 0;
	  		width: 100%;
	  	}
	  	.scroll-order .item p{
	  		margin-bottom: 5px;
	  	}
		#info-kh, #note-kh, #products-kh, #ship-kh, .customer-dropdown{
			display: none;
			width: 100%;
		}
		#info-kh, #note-kh{
			margin-top: 0;
		}
		#products-kh{
			padding-top: 15px;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	  	.scroll-order{
	  		width: 90%;
		    top: 104px;
		    padding-top: 0;
		    height: auto;

	  	}
	  	.scroll-order .item {
		    width: 50%;
		    height: 38px;
		    margin-bottom: 5px;
		}
		#info-kh, #note-kh{
	  		width: 100%;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.scroll-order{
	  		width: 90%;
		    top: 104px;
		    padding-top: 0;
		    height: auto;
	  	}
	  	.scroll-order .item {
		    width: 50%;
		    height: 38px;
		    margin-bottom: 5px;
		}
		#info-kh{
			margin-top: 130px;
		}
		#info-kh, #note-kh{
	  		width: 100%;
	  	}
	  	#note-kh{
	  		margin: 0 0 15px 0;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
		.scroll-order{
	  		width: 90%;
		    padding-top: 0;
		    height: auto;

	  	}
	  	.scroll-order .item {
		    height: 38px;
		    margin-bottom: 5px;
		}
		#info-kh, #note-kh{
	  		width: 100%;
	  	}
	  	#note-kh{
	  		margin-left: 0;
	  	}
	}
	@media (min-width: 1200px) {	
	}
</style>
<main class="info-order content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Thông tin đơn hàng</h1>
			<ul>
				<li>
					<a href="?action=order.php" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Trở về</label>
					</a>
				</li>
				<li>
                    <a href="javascript:void(0);" class="link-custom black-custom" title="Lịch sử thay đổi trạng thái" data-toggle="modal" data-target="#log-cart-modal"><i class="fa fa-history" aria-hidden="true"></i> Lịch sử</a>
                </li>
				<li>
					<button onclick="tinhtrang()" type="button" class="button button-header link-custom black-custom">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
                    </button>
				</li>
				<li>
                    <button type="button" onclick="window.location.href = 'include/order/print.php';" class="button button-header link-custom black-custom">
                        <i class="fa fa-print" aria-hidden="true"></i> <label>In tem</label>
                    </button>
                </li>
				<li>
					<button type="button" data-toggle="modal" data-target="#huydon-modal" class="button button-header link-custom red-custom">
                        <i class="ti-close" aria-hidden="true"></i> <label>Hủy đơn</label>
                    </button>
				</li>
				<li>
					<button type="button" data-toggle="modal" data-target="#trahang-modal" class="button button-header link-custom red-custom">
                        <i class="fa fa-minus-circle" aria-hidden="true"></i> <label>Trả hàng</label>
                    </button>
				</li>
				<li class="box-button-info-order">
					<a class="show-info-order link-custom black-custom" href="javascript:void(0);">
						<i class="fa fa-eye" aria-hidden="true"></i> <label>Xem</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="tab-custom bg-black">
				<div class="item active">
					<a href="javascript:void(0)" data-id="info-kh" title="Khách hàng">
						<i class="fa fa-user" aria-hidden="true"></i>
						<label>Khách hàng</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="note-kh" title="Ghi chú">
						<i class="fa fa-sticky-note" aria-hidden="true"></i>
						<label>Ghi chú</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="products-kh" title="Sản phẩm">
						<i class="fa fa-list" aria-hidden="true"></i>
						<label>Sản phẩm</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="ship-kh" data-id="Vận chuyển">
						<i class="fa fa-truck" aria-hidden="true"></i>
						<label>Vận chuyển</label>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<div class="scroll-order">
					<div class="item">
						<p class="black-custom">
							Mã đơn hàng: <span>JP01234</span>
						</p>
					</div>
					<div class="item">
						<p class="black-custom">
							Khách hàng: <span>Nguyễn Văn A</span>
						</p>
					</div>
					<div class="item">
						<p class="black-custom">
							Tư vấn viên: <span class="red-custom">Chưa có</span>
						</p>
					</div>
					<div class="item">
						<p class="black-custom">
							Trạng thái: <span>Mới đặt</span>
						</p>
						<div class="custom-dropdown">
							<select class="form-control" id="tinhtrang_cart">
					    		<option value="-1">Chọn trạng thái</option>
							  	<option value="0">Mới đặt</option>
							  	<option value="1">Đã xác nhận</option>
							</select>
						</div>
					</div>
					<div class="item">
						<p class="black-custom">
							Nguồn: 
						</p>
						<div class="custom-dropdown">
							<select class="form-control" id="nguonkh">
					    		<option value="-1">Chọn nguồn</option>
							  	<option value="0">Facebook</option>
							  	<option value="1">Zalo</option>
							  	<option value="2">Khách cũ</option>
							  	<option value="3">Điện thoại</option>
							  	<option value="4">Văn phòng</option>
							</select>
						</div>
					</div>
					<div class="item">
						<p class="black-custom">
							Nhân viên: 
						</p>
						<div class="custom-dropdown">
							<select class="form-control" name="username" id="nhanvien">
					    		<option value="-1">Chọn nhân viên</option>
							  	<option value="0">Đặng Tuyết Nhung</option>
							  	<option value="1">Trương Thị Thanh Hằng</option>
							  	<option value="2">Nguyễn Hà My</option>
							  	<option value="3">Nguyễn Quang Minh</option>
							  	<option value="4">Trần Thị Hường</option>
							</select>
						</div>
					</div>
				</div>
				<form name="frmPro" id="frmPro" class="formOrder" method="POST" enctype="multipart/form-data">
					<div id="info-kh" class="tab-content item show-inline">
						<?php include('include/order/thongtinkhachhang.php'); ?>
					</div>
					<div id="note-kh" class="tab-content item">
						<?php include('include/order/ghichu.php'); ?>
					</div>
					<div id="products-kh" class="tab-content item">
						<?php include('include/order/danhsachsanpham.php'); ?>
					</div>
					<div id="ship-kh" class="tab-content item">
						<?php include('include/order/vanchuyen.php'); ?>
					</div>
				</form>
			</div>
		</div>
	</article>
</main>
<?php include('include/order/huydon.php'); ?>
<?php include('include/order/trahang.php'); ?>
<?php include('include/order/themsanpham.php'); ?>
<?php include('include/order/logcart.php'); ?>
<?php include('include/order/lichsusanpham.php'); ?>
<script>
	jQuery(function(){
		var heightScrollOrder = jQuery('.scroll-order').outerHeight();
		if(window.innerWidth > 767 && window.innerWidth < 1024) {
			jQuery('.box-info-cart').css('margin-top',heightScrollOrder);
	    }
		jQuery('.show-info-order').click(function(){
	    	if(jQuery('.scroll-order').css('display')=='none'){
	    		jQuery('.scroll-order').css('display','block');
	    		showBackgroundPopup();
	    		jQuery(this).text("");
				jQuery(this).append("<i class='fa fa-eye-slash'></i> Đóng");
	    	}
	    	else{
	    		jQuery('.scroll-order').css('display','none');
	    		deleteBackgroundPopup();
	    		jQuery(this).text("");
	    		jQuery(this).append("<i class='fa fa-eye'></i> Xem");
	    	}
	    });
	    jQuery('.tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.tab-content').not('#' + data).removeClass('show-inline');
	    	jQuery(this).parent().addClass('active');
	    	jQuery('.tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    });
	})
</script>