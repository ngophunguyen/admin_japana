<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item button{
		float: left;
		margin-left: 15px;
	}
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item{
			width: 100%;
		}
		.refund .table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="partner content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách nhà cung cấp</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#addncc-modal" class="link-custom black-custom open-receipt" title="Tạo nhà cung cấp">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Tạo nhà cung cấp</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Nhập thông tin cần tìm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Mã NCC</th>
					            <th class="bg-black">Tên NCC</th>
					            <th class="bg-black">Người liên hệ</th>
					            <th class="bg-black">Số điện thoại</th>
					            <th class="bg-black right-custom">Email</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Mã NCC">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editncc-modal" title="NCC_001-1">NCC_001-1</a>
					            </td>
					            <td data-title="Tên NCC">SBS (Anh Phi)</td>
					            <td data-title="Người liên hệ">Anh Phi</td>
					            <td data-title="Số điện thoại">0904963666</td>
					            <td data-title="Email" class="right-custom"></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Mã NCC">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editncc-modal" title="NCC_001-10">NCC_001-10</a>
					            </td>
					            <td data-title="Tên NCC">JP Shop Nhật (Chị Thu)</td>
					            <td data-title="Người liên hệ">Chị Thu</td>
					            <td data-title="Số điện thoại">09363653635</td>
					            <td data-title="Email" class="right-custom"></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Mã NCC">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editncc-modal" title="NCC_001-11">NCC_001-11</a>
					            </td>
					            <td data-title="Tên NCC">Chị Châu SK-II	</td>
					            <td data-title="Người liên hệ">Chị Châu</td>
					            <td data-title="Số điện thoại">0909594757</td>
					            <td data-title="Email" class="right-custom"></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Mã NCC">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editncc-modal" title="NCC_001-12">NCC_001-12</a>
					            </td>
					            <td data-title="Tên NCC">Jenny Food</td>
					            <td data-title="Người liên hệ">Anh Tuấn</td>
					            <td data-title="Số điện thoại">0903737470</td>
					            <td data-title="Email" class="right-custom"></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Mã NCC">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editncc-modal" title="NCC_001-13">NCC_001-13</a>
					            </td>
					            <td data-title="Tên NCC">Cty Vương Dũng</td>
					            <td data-title="Người liên hệ">Chị Hiền</td>
					            <td data-title="Số điện thoại">0907321228</td>
					            <td data-title="Email" class="right-custom"></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Mã NCC">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editncc-modal" title="NCC_001-14">NCC_001-14</a>
					            </td>
					            <td data-title="Tên NCC">Chị Hà (mỹ phẩm SK-II)</td>
					            <td data-title="Người liên hệ">Châu</td>
					            <td data-title="Số điện thoại">0964888866</td>
					            <td data-title="Email" class="right-custom"></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Mã NCC">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editncc-modal" title="NCC_001-15">NCC_001-15</a>
					            </td>
					            <td data-title="Tên NCC">Bubas House</td>
					            <td data-title="Người liên hệ">Bubas</td>
					            <td data-title="Số điện thoại">0947917632</td>
					            <td data-title="Email" class="right-custom"></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Mã NCC">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editncc-modal" title="NCC_001-1">NCC_001-16</a>
					            </td>
					            <td data-title="Tên NCC">Hàng Nhật Minh Nguyên</td>
					            <td data-title="Người liên hệ">Chị Duyên</td>
					            <td data-title="Số điện thoại">0376515515</td>
					            <td data-title="Email" class="right-custom"></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Mã NCC">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editncc-modal" title="NCC_001-17">NCC_001-17</a>
					            </td>
					            <td data-title="Tên NCC">Hàng nhật HaLi (Chị Diệu Linh)</td>
					            <td data-title="Người liên hệ">Chị Linh</td>
					            <td data-title="Số điện thoại">0975452505</td>
					            <td data-title="Email" class="right-custom"></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Mã NCC">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editncc-modal" title="NCC_001-18">NCC_001-18</a>
					            </td>
					            <td data-title="Tên NCC">Nobita Shop (Orihiro)</td>
					            <td data-title="Người liên hệ">Chị Hoàn</td>
					            <td data-title="Số điện thoại">0912995463</td>
					            <td data-title="Email" class="right-custom"></td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/partner/add.php'); ?>
<?php include('include/partner/edit.php'); ?>
<script>
	jQuery(function(){
		
	})
</script>