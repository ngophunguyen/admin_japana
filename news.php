<style>
	.filter-pc{
		margin-top: 15px;
	}
	.filter-mobile{
		display: none;
	}
	.custom-collapse{
		display: none;
		width: 100%;
		padding: 0 15px;
	}
	.custom-collapse .item{
		display: inline-block;
		width: 24%;
		margin: 10px 0;
		float: left;
	}
	.custom-collapse .item:nth-child(2), 
	.custom-collapse .item:nth-child(3), 
	.custom-collapse .item:nth-child(4),
	.custom-collapse .item:nth-child(6), 
	.custom-collapse .item:nth-child(7), 
	.custom-collapse .item:nth-child(8){
		margin-left: 1%;
	}
	.custom-collapse .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.show-filter-order{
		color: #fff;
	    font-size: 14px;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	    border: 1px solid #222D32;
	    height: 38px;
	    width: 38px;
	    position: fixed;
	    right: 0;
		top: 214px;
		z-index: 99;
	}
	.show-filter-order:focus, .show-filter-order:hover{
		color: #fff;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin: 15px 0;
	    position: relative;
	}
	.table-custom{
		margin-top: 0;
		white-space: nowrap;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		height: auto;
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	img.gift-order{
		height: 20px!important; 
		bottom: 5px; 
		left: 0;  
		position: absolute;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		padding: 15px 0;
	}
	.box-table tr td:nth-child(8) input{
		width: 100px;
	}
	.box-table tr td:nth-child(10) .box-time{
		width: 150px;
	}
	.box-table tr td .checkbox-custom{
		padding: 0;
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:last-child{
		width: 55%;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	}
	.box-quick-search .item:first-child input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .box-time{
		width: 40%;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
		float: right;
	}
	.search2, .search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.search1{
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.box-button-filter{
  		display: none;
  	}
  	.fixed-button{
  		position: fixed; 
  		bottom: 0;
  		right: 0;
	    width: calc(100% - 38px);
	    border-radius: 0;
	    z-index: 9999;
  	}
	@media (max-width: 575.98px) {
	  	.group-category{
	  		display: none;
	  	}
	  	.dropdown-collapse{
	  		border-radius: 0;
	  		-webkit-box-pack: center;
	  		    -ms-flex-pack: center;
	  		        justify-content: center;
	  		display: none;
	  	}
	  	.dropdown-collapse i{
	  		display: none;
	  	}
	  	.filter-pc{
	  		display: block;
	  		position: fixed;
		    top: 0;
		    right: 0;
		    right: 0;
		    width: calc(100% - 38px);
		    height: 100%;
		    z-index: -1;
		    margin-top: 0!important;
	  	}
	  	.custom-collapse{
	  		height: 100%;
		    width: 100%;
		    padding: 0!important;
		    float: right;
		    overflow-x: hidden;
		    position: relative;
		    z-index: 999;
		    background: #fff;
		    border-left: 1px solid #eee;
	  	}
	  	.custom-collapse .item{
	  		width: 100%;
	  		margin: 10px 0 0!important;
	  	}
	  	.custom-collapse .item:last-child{
	  		margin-bottom: 53px!important;

	  	}
	  	.custom-collapse .item:last-child label{
	  		display: none;
	  	}
	  	.custom-collapse .item:last-child button{
	  		position: fixed;
	  		bottom: 0;
	  		right: 0;
	  		width: calc(100% - 38px);
	  		border-radius: 0;
	  	}
	  	.box-button-filter.active{
	  		background: rgba(0,0,0,.3);
		    width: 38px;
		    height: 100%;
		    position: fixed;
		    left: 0;
		    top: 0;
    		z-index: 999;
	  	}
	  	#advance_search{
	  		display: inline-block;
	  		width: 100%;
	  		height: calc(100% - 38px);
		    background: #fff;
		    z-index: 999;
		    top: 0;
		    overflow-y: scroll;
		    overflow-x: hidden;
		    padding: 0 15px;
	  	}
	  	.custom-dropdown:after{
	  		width: 10%;
	  	}
	  	.custom-collapse .title{
	  		padding: 10px 12px;
	  		margin-bottom: 0;
	  		border-bottom: 1px solid #ccc;
	  		text-align: center;
	  	}
		.box-quick-search .item, .box-quick-search .item:first-child input, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child,.box-quick-search .item:last-child button{
	  		margin-top: 15px;
	  	}
	  	.box-quick-search .item:last-child,.search2{
	  		display: inline-block;
	  	}
	  	.box-quick-search .item:last-child .custom-dropdown{
	  		width: 40%;
	  	}
	  	.box-quick-search .item:last-child .box-time{
	  		width: 55%;
	  		margin-left: 5%;
	  	}
	  	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
	  		float: left;
	  	}
	  	.box-quick-search .item:last-child button{
	  		margin-left: 0;
	  		clear: both;
	  	}
		.box-button-filter{
	  		display: block;
	  	}
	  	.custom-collapse .item:nth-child(9){
	  		margin-bottom: 53px!important;
	  	}
	  	.table-custom > tbody > tr > td{
			min-height: 40px;
		}
		.table-responsive > tbody > tr td:first-child {
		    display: none;
		}
		.box-table tr td:last-child{
			padding: 0;
		}
		.table-custom > tbody > tr > td a{
			white-space: normal;
		}
		.box-table tr td:nth-child(8) input, .box-table tr td:nth-child(10) .box-time{
			width: 100%;
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	  	.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.box-quick-search .item, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child{
	  	 	margin-top: 15px;
	  	}
	  	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 80%;
	  	}
	  	.box-quick-search .item:last-child .box-time{
	  		width: 54%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
	  	.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.box-quick-search .item, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child{
	  	 	margin-top: 15px;
	  	}
	  	.custom-collapse .item:nth-child(7){
	  		width: 49%;
	  	}
	  	.custom-collapse .item:last-child{
	  		margin-left: 0;
	  	}
	  	.custom-collapse .item:last-child label{
	  		display: none;
	  	}
	  	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 80%;
	  	}
	  	.box-quick-search .item:last-child .box-time{
	  		width: 54%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
	  	.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 1200px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
</style>
<main class="news content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách tin tức</h1>
			<ul>
				<li>
					<a href="?action=include/news/add.php" class="link-custom black-custom" title="Thêm bài viết">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="filter-pc">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">Tìm kiếm nâng cao <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div id="filter-order" class="custom-collapse">
						<form id="advance_search" name="frm advance_search" method="post">
						    <div class="item">
						    	<label for="cbo_username">Nhân viên</label>
						    	<select class="form-control" name="id_user" id="cbo_username">
						    		<option value="-1">Chọn nhân viên</option>
								  	<option value="0">Nghi</option>
								  	<option value="1">Thu Sương</option>
								</select>
						    </div>
						    <div class="item">
						    	<label for="cbo_catenews">Danh mục</label>
					    		<select class="form-control" name="id_category" id="cbo_catenews">
						    		<option value="-1">Chọn danh mục</option>
								  	<option value="0">Tin tức</option>
								  	<option value="1">Tin tức tổng hợp</option>
								  	<option value="2">Tin tức công ty</option>
								</select>
						    </div>
						    <div class="item">
						    	<label for="cbo_status">Trạng thái</label>
						    	<div class="custom-dropdown">
						    		<select class="form-control" name="showview" id="cbo_status">
							    		<option value="-1">Chọn trạng thái</option>
									  	<option value="0">Publish</option>
									  	<option value="1">Unpublish</option>
									</select>
						    	</div>
						    </div>
						    <div class="item">
						    	<label for="cbo_views">Lượt xem</label>
						    	<div class="custom-dropdown">
						    		<select class="form-control" name="visit" id="cbo_views">
							    		<option value="-1">Chọn</option>
									  	<option value="0">Cao nhất</option>
									  	<option value="1">Thấp nhất</option>
									</select>
						    	</div>
						    </div>
						    <div class="item">
						    	<label for="idnews">ID bài viết</label>
						    	<input autocomplete="off" type="text" name="id_search" id="idnews" class="form-control" placeholder="Nhập ID bài viết">
						    </div>
						    <div class="item">
						    	<label for="name">Tên bài viết</label>
						    	<input autocomplete="off" type="text" name="name_search_advance" id="name" class="form-control" placeholder="Nhập tên bài viết">
						    </div>
						    <div class="item">
						    	<label for="date-order">Khoảng thời gian</label>
						    	<div class="box-time">
						    		<input type="hidden" id="start-date3" name="from_date_ad" value="">
						    		<input type="hidden" id="end-date3" name="to_date_ad" value="">
		                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày...">
		                            <i class="fa fa-calendar icon-time"></i>
		                        </div>
						    </div>
						    <div class="item">
						    	<label class="visible-hidden">Tìm kiếm</label>
						    	<input type="hidden" name="method" value="3">
						    	<button type="submit" class="button bg-black">Tìm kiếm</button>
						    </div>
						</form>
					</div>
				</div>
				<div class="box-button-filter">
					<a class="show-filter-order bg-black" href="javascript:void(0);"><i class="ti-search" aria-hidden="true"></i></a>
				</div>
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="keyword" value="" type="text" class="form-control custom-ipt" placeholder="Tìm kiếm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<form name="status_search" id="frm" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="status_cart" name="status_cart">
						    		<option value="-1">Chọn trạng thái</option>
								  	<option value="0">Xem nhiều nhất</option>
								  	<option value="1">Xem ít nhất</option>
								</select>
							</div>
							<div class="box-time">
								<input autocomplete="off" type="hidden" value="" id="start-date2" name="from_date_ad">
								<input autocomplete="off" type="hidden" id="end-date2" value="" name="to_date_ad">
	                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news-fast" id="date-news-fast" class="form-control ipt-date" placeholder="Chọn ngày...">
	                            <i class="fa fa-calendar icon-time"></i>
	                        </div>
	                        <button type="submit" class="button bg-black">Tìm kiếm</button>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black center-custom">ID</th>
					            <th class="bg-black">Tên bài viết</th>
					            <th class="bg-black center-custom">Hình ảnh</th>
					            <th class="bg-black">Ngày Publish</th>
					            <th class="bg-black">Publish</th>
					            <th class="bg-black center-custom">Lượt xem</th>
					            <th class="bg-black center-custom">Vị trí</th>
					            <th class="bg-black center-custom">Hot</th>
					            <th class="bg-black center-custom">Time Limit</th>
					            <th class="bg-black">Danh mục</th>
					            <th class="bg-black center-custom">Xem trước</th>
					            <th class="bg-black">Người tạo</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="ID" class="center-custom">65</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/news/edit.php" title="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            		Chương trình tập huấn PCCC dành cho cán bộ công nhân viên
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/news/1556005364-nhung-thuong-hieu-my-pham-cao-cap-nhat-ban-dang-duoc-uu-ai-nhat-hien-nay-11-min.jpeg" alt="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            </td>
					            <td data-title="Ngày Publish">21-02-2019</td>
					            <td data-title="Publish"><input type="checkbox" class="checkbox-ios"/></td>
					            <td data-title="Lượt xem" class="center-custom">1</td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" class="form-control" value="1" id="sort_cate_news_1">
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Time Limit" class="center-custom">
					            	<div class="box-time">
							    		<input autocomplete="off" onkeypress="return false;" type="text" id="time_limit_1" name="date-news-table" class="form-control" value="">
				                        <i class="fa fa-calendar icon-time"></i>
				                    </div>
					            </td>
					            <td data-title="Danh mục">Tin tức</td>
					            <td data-title="Xem trước" class="center-custom">
					            	<a href="#" title="Xem trước">Xem</a>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="ID" class="center-custom">65</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/news/edit.php" title="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            		Chương trình tập huấn PCCC dành cho cán bộ công nhân viên
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/news/1556005364-nhung-thuong-hieu-my-pham-cao-cap-nhat-ban-dang-duoc-uu-ai-nhat-hien-nay-11-min.jpeg" alt="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            </td>
					            <td data-title="Ngày Publish">21-02-2019</td>
					            <td data-title="Publish"><input type="checkbox" class="checkbox-ios"/></td>
					            <td data-title="Lượt xem" class="center-custom">1</td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" class="form-control" value="2" id="sort_cate_news_2">
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Time Limit" class="center-custom">
					            	<div class="box-time">
							    		<input autocomplete="off" onkeypress="return false;" type="text" id="time_limit_1" name="date-news-table" class="form-control" value="">
				                        <i class="fa fa-calendar icon-time"></i>
				                    </div>
					            </td>
					            <td data-title="Danh mục">Tin tức</td>
					            <td data-title="Xem trước" class="center-custom">
					            	<a href="#" title="Xem trước">Xem</a>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="ID" class="center-custom">65</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/news/edit.php" title="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            		Chương trình tập huấn PCCC dành cho cán bộ công nhân viên
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/news/1556005364-nhung-thuong-hieu-my-pham-cao-cap-nhat-ban-dang-duoc-uu-ai-nhat-hien-nay-11-min.jpeg" alt="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            </td>
					            <td data-title="Ngày Publish">21-02-2019</td>
					            <td data-title="Publish"><input type="checkbox" class="checkbox-ios"/></td>
					            <td data-title="Lượt xem" class="center-custom">1</td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" class="form-control" value="3" id="sort_cate_news_3">
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Time Limit" class="center-custom">
					            	<div class="box-time">
							    		<input autocomplete="off" onkeypress="return false;" type="text" id="time_limit_1" name="date-news-table" class="form-control" value="">
				                        <i class="fa fa-calendar icon-time"></i>
				                    </div>
					            </td>
					            <td data-title="Danh mục">Tin tức</td>
					            <td data-title="Xem trước" class="center-custom">
					            	<a href="#" title="Xem trước">Xem</a>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="ID" class="center-custom">65</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/news/edit.php" title="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            		Chương trình tập huấn PCCC dành cho cán bộ công nhân viên
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/news/1556005364-nhung-thuong-hieu-my-pham-cao-cap-nhat-ban-dang-duoc-uu-ai-nhat-hien-nay-11-min.jpeg" alt="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            </td>
					            <td data-title="Ngày Publish">21-02-2019</td>
					            <td data-title="Publish"><input type="checkbox" class="checkbox-ios"/></td>
					            <td data-title="Lượt xem" class="center-custom">1</td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" class="form-control" value="4" id="sort_cate_news_4">
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Time Limit" class="center-custom">
					            	<div class="box-time">
							    		<input autocomplete="off" onkeypress="return false;" type="text" id="time_limit_1" name="date-news-table" class="form-control" value="">
				                        <i class="fa fa-calendar icon-time"></i>
				                    </div>
					            </td>
					            <td data-title="Danh mục">Tin tức</td>
					            <td data-title="Xem trước" class="center-custom">
					            	<a href="#" title="Xem trước">Xem</a>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="ID" class="center-custom">65</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/news/edit.php" title="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            		Chương trình tập huấn PCCC dành cho cán bộ công nhân viên
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/news/1556005364-nhung-thuong-hieu-my-pham-cao-cap-nhat-ban-dang-duoc-uu-ai-nhat-hien-nay-11-min.jpeg" alt="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            </td>
					            <td data-title="Ngày Publish">21-02-2019</td>
					            <td data-title="Publish"><input type="checkbox" class="checkbox-ios"/></td>
					            <td data-title="Lượt xem" class="center-custom">1</td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" class="form-control" value="5" id="sort_cate_news_5">
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Time Limit" class="center-custom">
					            	<div class="box-time">
							    		<input autocomplete="off" onkeypress="return false;" type="text" id="time_limit_1" name="date-news-table" class="form-control" value="">
				                        <i class="fa fa-calendar icon-time"></i>
				                    </div>
					            </td>
					            <td data-title="Danh mục">Tin tức</td>
					            <td data-title="Xem trước" class="center-custom">
					            	<a href="#" title="Xem trước">Xem</a>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="ID" class="center-custom">65</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/news/edit.php" title="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            		Chương trình tập huấn PCCC dành cho cán bộ công nhân viên
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/news/1556005364-nhung-thuong-hieu-my-pham-cao-cap-nhat-ban-dang-duoc-uu-ai-nhat-hien-nay-11-min.jpeg" alt="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            </td>
					            <td data-title="Ngày Publish">21-02-2019</td>
					            <td data-title="Publish"><input type="checkbox" class="checkbox-ios"/></td>
					            <td data-title="Lượt xem" class="center-custom">1</td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" class="form-control" value="6" id="sort_cate_news_6">
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Time Limit" class="center-custom">
					            	<div class="box-time">
							    		<input autocomplete="off" onkeypress="return false;" type="text" id="time_limit_1" name="date-news-table" class="form-control" value="">
				                        <i class="fa fa-calendar icon-time"></i>
				                    </div>
					            </td>
					            <td data-title="Danh mục">Tin tức</td>
					            <td data-title="Xem trước" class="center-custom">
					            	<a href="#" title="Xem trước">Xem</a>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="ID" class="center-custom">65</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/news/edit.php" title="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            		Chương trình tập huấn PCCC dành cho cán bộ công nhân viên
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/news/1556005364-nhung-thuong-hieu-my-pham-cao-cap-nhat-ban-dang-duoc-uu-ai-nhat-hien-nay-11-min.jpeg" alt="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            </td>
					            <td data-title="Ngày Publish">21-02-2019</td>
					            <td data-title="Publish"><input type="checkbox" class="checkbox-ios"/></td>
					            <td data-title="Lượt xem" class="center-custom">1</td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" class="form-control" value="7" id="sort_cate_news_7">
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Time Limit" class="center-custom">
					            	<div class="box-time">
							    		<input autocomplete="off" onkeypress="return false;" type="text" id="time_limit_1" name="date-news-table" class="form-control" value="">
				                        <i class="fa fa-calendar icon-time"></i>
				                    </div>
					            </td>
					            <td data-title="Danh mục">Tin tức</td>
					            <td data-title="Xem trước" class="center-custom">
					            	<a href="#" title="Xem trước">Xem</a>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="ID" class="center-custom">65</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/news/edit.php" title="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            		Chương trình tập huấn PCCC dành cho cán bộ công nhân viên
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/news/1556005364-nhung-thuong-hieu-my-pham-cao-cap-nhat-ban-dang-duoc-uu-ai-nhat-hien-nay-11-min.jpeg" alt="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            </td>
					            <td data-title="Ngày Publish">21-02-2019</td>
					            <td data-title="Publish"><input type="checkbox" class="checkbox-ios"/></td>
					            <td data-title="Lượt xem" class="center-custom">1</td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" class="form-control" value="8" id="sort_cate_news_8">
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Time Limit" class="center-custom">
					            	<div class="box-time">
							    		<input autocomplete="off" onkeypress="return false;" type="text" id="time_limit_1" name="date-news-table" class="form-control" value="">
				                        <i class="fa fa-calendar icon-time"></i>
				                    </div>
					            </td>
					            <td data-title="Danh mục">Tin tức</td>
					            <td data-title="Xem trước" class="center-custom">
					            	<a href="#" title="Xem trước">Xem</a>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="ID" class="center-custom">65</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/news/edit.php" title="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            		Chương trình tập huấn PCCC dành cho cán bộ công nhân viên
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/news/1556005364-nhung-thuong-hieu-my-pham-cao-cap-nhat-ban-dang-duoc-uu-ai-nhat-hien-nay-11-min.jpeg" alt="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            </td>
					            <td data-title="Ngày Publish">21-02-2019</td>
					            <td data-title="Publish"><input type="checkbox" class="checkbox-ios"/></td>
					            <td data-title="Lượt xem" class="center-custom">1</td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" class="form-control" value="9" id="sort_cate_news_9">
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Time Limit" class="center-custom">
					            	<div class="box-time">
							    		<input autocomplete="off" onkeypress="return false;" type="text" id="time_limit_1" name="date-news-table" class="form-control" value="">
				                        <i class="fa fa-calendar icon-time"></i>
				                    </div>
					            </td>
					            <td data-title="Danh mục">Tin tức</td>
					            <td data-title="Xem trước" class="center-custom">
					            	<a href="#" title="Xem trước">Xem</a>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="ID" class="center-custom">65</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/news/edit.php" title="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            		Chương trình tập huấn PCCC dành cho cán bộ công nhân viên
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/news/1556005364-nhung-thuong-hieu-my-pham-cao-cap-nhat-ban-dang-duoc-uu-ai-nhat-hien-nay-11-min.jpeg" alt="Những thương hiệu mỹ phẩm cao cấp Nhật Bản đang được ưu ái nhất hiện nay">
					            </td>
					            <td data-title="Ngày Publish">21-02-2019</td>
					            <td data-title="Publish"><input type="checkbox" class="checkbox-ios"/></td>
					            <td data-title="Lượt xem" class="center-custom">1</td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" class="form-control" value="10" id="sort_cate_news_10">
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Time Limit" class="center-custom">
					            	<div class="box-time">
							    		<input autocomplete="off" onkeypress="return false;" type="text" id="time_limit_1" name="date-news-table" class="form-control" value="">
				                        <i class="fa fa-calendar icon-time"></i>
				                    </div>
					            </td>
					            <td data-title="Danh mục">Tin tức</td>
					            <td data-title="Xem trước" class="center-custom">
					            	<a href="#" title="Xem trước">Xem</a>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		var heightBottomTotal = jQuery('.total-order').outerHeight();
		var heightBottomPagination = jQuery('.pagination-custom').outerHeight();
		if(window.innerWidth < 576){
			jQuery('.box-table').css('margin-bottom',heightBottomPagination+heightBottomTotal);
		}
		if(window.innerWidth > 576){
		    jQuery('.dropdown-collapse').click(function(){
		    	if(jQuery('.custom-collapse').css('display')=='none'){
		    		jQuery('.custom-collapse').css('display','inline-block');
		    		jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
		    	}
		    	else{
		    		jQuery('.custom-collapse').css('display','none');
		    		jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
		    	}
		    });
		}else{
			jQuery('.show-filter-order').click(function(){
		    	if(jQuery('.custom-collapse').css('display')=='none'){
		    		jQuery('.custom-collapse').css('display','inline-block');
		    		jQuery('.dropdown-collapse').css('display','flex');
		    		jQuery(this).css('left',0);
		    		jQuery(this).css('right','auto');
		    		jQuery(this).css('top',38);
		    		jQuery(this).css('z-index','99');
		    		jQuery(this).parent().addClass('active');
		    		jQuery(this).find('i').removeClass('ti-search').addClass('ti-arrow-right');
		    		jQuery('body').css('overflow','hidden');
		    		jQuery('.filter-pc').css('z-index','999');

		    	}
		    	else{
		    		jQuery('.custom-collapse').css('display','none');
		    		jQuery('.dropdown-collapse').css('display','none');
		    		jQuery(this).css('left','auto');
		    		jQuery(this).css('right',0);
		    		jQuery(this).css('top',214);
		    		jQuery(this).parent().removeClass('active');
		    		jQuery(this).find('i').removeClass('ti-arrow-right').addClass('ti-search');
		    		jQuery('body').css('overflow','inherit');
		    		jQuery('.filter-pc').css('z-index','-1');
		    	}
		    });
		}
	    
	    var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    jQuery('#cbo_username,#cbo_catenews').select2();
	    if(window.innerWidth > 576) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
	    jQuery('input[name="date-news"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date3').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date3').val(end.format('DD-MM-YYYY'));
		});
		jQuery('input[name="date-news-fast"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date2').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date2').val(end.format('DD-MM-YYYY'));
		});
		jQuery('input[name="date-news-table"]').daterangepicker({
			singleDatePicker: true,
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		});
	});
</script>