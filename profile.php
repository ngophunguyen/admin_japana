<style>
	#info-user{
		display: inline-block;
		width: 20%;
		float: left;
		margin-top: 15px;
    	border: 1px solid #eee;
    	padding: 10px;
    	padding-bottom: 0;
    	border-radius: 4px;
	}
	#edit-user{
		width: 78%;
		float: left;
		margin-left: 2%;
	}
	.box-img{
		width: 100px;
		height: 100px;
		border-radius: 100%;
		border: 1px solid #ccc;
		padding: 5px;
		text-align: center;
		margin: 0 auto;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	}
	.box-img img{
		width: 90px;
		height: 90px;
	    -o-object-fit: cover;
	       object-fit: cover;
	    border-radius: 100%;
	}
	.title{
		font-size: 18px;
	    margin-top: 5px;
	    font-weight: 400;
	    text-align: center;
	}
	.job{
		color: #777;
		text-align: center;
		margin-bottom: 15px;
	}
	.about-me .item{
		padding: 10px 0;
		border-top: 1px solid #eee;
	}
	.about-me .item .title{
		font-weight: 500;
		font-size: 14px;
   	 	text-align: left;
   	 	display: -webkit-box;
   	 	display: -ms-flexbox;
   	 	display: flex;
	}
	.about-me .item p{
		line-height: 1.5;
		word-break: break-word;
	}
	.about-me .item .title i{
		margin-right: 5px;
	}
	#edit-user{
		margin-top: 15px;
		border: 1px solid #eee;
	    border-radius: 2px;
	    padding: 10px;
	}
	#edit-user .item>label {
	    font-size: 14px;
	    line-height: 1.5;
	    font-weight: 500;
	    margin-bottom: 5px;
	}
	#edit-user .item{
		width: 32%;
		float: left;
		margin-bottom: 15px;
	}
	#edit-user .item:nth-child(2),
	#edit-user .item:nth-child(5){
		margin: 0 2%;
	}
	#edit-user .item:nth-child(8){
		width: 66%;
	}
	#edit-user .item:nth-child(7){
		margin-right: 2%;
		width: 32%;
	}
	#edit-user .item:nth-child(9){
		width: 100%;
	}
	@media (max-width: 575.98px) {
		#info-user, #edit-user{
			width: 100%;
			display: none;
		}
		#edit-user .item{
			width: 100%!important;
			float: left;
			margin: 0!important;
			margin-bottom: 10px!important;
		}
		#edit-user{
			margin-left: 0;

		}
		#edit-user .item:last-child{
			display: block;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		#info-user{
	  		width: 34%;
	  	}
	  	#edit-user{
	  		width: 64%;
	  	}
	  	#edit-user .item{
	  		width: 100%!important;
	  	}
	  	#edit-user .item:nth-child(2), #edit-user .item:nth-child(5){
	  		margin: 0 0 15px;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
	  	#info-user{
	  		width: 34%;
	  	}
	  	#edit-user{
	  		width: 64%;
	  	}
	  	#edit-user .item{
	  		width: 100%!important;
	  	}
	  	#edit-user .item:nth-child(2), #edit-user .item:nth-child(5){
	  		margin: 0 0 15px;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
	  	#info-user{
	  		width: 34%;
	  	}
	  	#edit-user{
	  		width: 64%;
	  	}
	  	#edit-user .item{
	  		width: 100%!important;
	  	}
	  	#edit-user .item:nth-child(2), #edit-user .item:nth-child(5){
	  		margin: 0 0 15px;
	  	}
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="profile content">
	<article class="entry">
		<form name="frm" id="frm" method="post" action="" enctype="multipart/form-data">
			<header class="entry-header">
				<h1 class="entry-title">Tài khoản</h1>
				<ul>
					<li>
						<button type="submit" class="button button-header link-custom black-custom">
							<i class="fa fa-save"></i> <label>Lưu lại</label>
						</button>
					</li>
					<li>
						<a href="?action=home.php" class="link-custom red-custom" title="Thoát">
							<i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
						</a>
					</li>
					<?php include('include/pc-user.php'); ?>
				</ul>
			</header>
			<div class="entry-content">
				<div class="tab-custom bg-black">
					<div class="item active">
						<a href="javascript:void(0)" data-id="info-user" title="Thông tin user">
							<i class="fa fa-user" aria-hidden="true"></i>
							<label>Thông tin</label>
						</a>
					</div>
					<div class="item">
						<a href="javascript:void(0)" data-id="edit-user" title="Cập nhật user">
							<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							<label>Cập nhật</label>
						</a>
					</div>
				</div>
				<div class="container-fluid">
					<div id="info-user" class="tab-content item show-inline">
						<div class="box-img">
							<img src="assets/images/user.jpg" alt="Thuỳ Vân Nguyễn" id="photo">
						</div>
						<h3 class="title">Thuỳ Vân Nguyễn</h3>
						<p class="job">Admin</p>
						<div class="about-me">
							<div class="item">
								<h3 class="title"><i class="fa fa-envelope" aria-hidden="true"></i> Email</h3>
								<p>trongnghia2401@gmail.com</p>
							</div>
							<div class="item">
								<h3 class="title"><i class="fa fa-map-marker" aria-hidden="true"></i> Địa chỉ</h3>
								<p>76 Nguyễn Háo Vĩnh, P. Tân Quý, Q. Tân Phú, TP. HCM</p>
							</div>
							<div class="item">
								<h3 class="title"><i class="fa fa-phone" aria-hidden="true"></i> Mobile</h3>
								<p>0975.800.600</p>
							</div>
							<div class="item">
								<h3 class="title"><i class="fa fa-sticky-note" aria-hidden="true"></i> Ghi chú</h3>
								<p>Dev là để yêu thương</p>
							</div>
						</div>
					</div>
					<div id="edit-user" class="tab-content item">
						<div class="item">
							<label>Avatar</label>
							<div class="upload-img">
	                            <div class="input-group up-img">
	                                <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
	                                <label class="button bg-green custom-upload">
	                                    <input type="file" id="ipt-img" class="form-control" name="imgs" onchange="readURL(this);" accept="image/*">Upload
	                                </label>
	                                <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
	                            </div>
	                        </div>
						</div>
						<div class="item">
							<label for="fullname">Họ và tên</label>
							<input type="text" value="Deverloper" id="fullname" name="fullname" class="form-control">
						</div>
						<div class="item">
							<label for="mobile">Số điện thoại</label>
							<input type="number" value="0975800600" id="mobile" name="mobile" class="form-control">
						</div>
						<div class="item">
							<label for="inputPassCur" class="tooltip-custom">
								Mật khẩu hiện tại
							</label>
							<input type="text" name="cPwd" id="inputPassCur" class="form-control" placeholder="Nhập mật khẩu hiện tại">
						</div>
						<div class="item">
							<label for="inputPassword3" class="tooltip-custom">
								Mật khẩu mới
							</label>
							<input type="password" name="nPwd" id="inputPassword3" class="form-control" placeholder="Nhập mật khẩu mới">
						</div>
						<div class="item">
							<label for="inputPassword4" class="tooltip-custom">
								Xác nhận mật khẩu mới
							</label>
							<input type="password" name="cfPwd" id="inputPassword4" placeholder="Xác nhận mật khẩu mới" class="form-control">
						</div>
						<div class="item">
							<label for="email">Email</label>
							<input type="email" value="trongnghia2401@gmail.com" id="email" name="email" class="form-control">
						</div>
						<div class="item">
							<label for="address">Địa chỉ</label>
							<input type="text" value="76 Nguyễn Háo Vĩnh, P. Tân Quý, Q. Tân Phú, TP. HCM" id="address" name="address" class="form-control">
						</div>
						<div class="item">
							<label for="notes">Ghi chú</label>
							<input type="text" value="Dev là để yêu thương" id="notes" name="notes" class="form-control">
						</div>
					</div>
				</div>
			</div>
		</form>
		
	</article>
</main>
<script>
	jQuery(function(){
		jQuery(document).on('change', ':file', function() {
		    var input = jQuery(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		});
		jQuery(':file').on('fileselect', function(event, numFiles, label) {
          	var input = jQuery(this).parents('.up-img').find(':text'),
              	log = numFiles > 1 ? numFiles + ' files selected' : label;
          	if( input.length ) {
              	input.val(log);
              	if(input.val(log)){
                	jQuery('.custom-upload').css('display','none');
                	jQuery('.delete-img').css('display','block');
              	}
          	} else {
              	if(log) ;
          	}
      	});
      	jQuery('.delete-img').on('click', function(e){
		    jQuery('#ipt-img').replaceWith(jQuery('#ipt-img').val('').clone( true ));
		    jQuery('.custom-upload').css('display','flex');
		    jQuery('.delete-img').css('display','none');
		});
		jQuery('.tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.tab-content').not('#' + data).removeClass('show-inline');
	    	jQuery(this).parent().addClass('active');
	    	jQuery('.tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    });
	})
	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	          jQuery('#photo').attr('src', e.target.result);
	          jQuery('#photo').parent().css('display','block'); 
	        };
	        reader.readAsDataURL(input.files[0]);
	    }
	}
</script>