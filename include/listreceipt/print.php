<!DOCTYPE html>
<html lang="vi">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="title" content="Admin Ver2">
		<meta name="description" content="Admin Ver2">
		<meta name="keywords" content="Admin Ver2">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui" />
		<title>Phiếu nhập hàng</title>
		<link rel="shortcut icon" href="assets/images/favicon.webp">

		<!--include CSS-->
		<link rel="stylesheet" href="../../assets/plugins/jquery-ui/jquery-ui.min.css">
		<link rel="stylesheet" href="../../assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="../../assets/plugins/themify-icons/themify-icons.css">
		<link rel="stylesheet" href="../../assets/plugins/normalize/normalize.css">
		<link rel="stylesheet" href="../../assets/plugins/normalize/libs.css">
		<link rel="stylesheet" href="../../assets/css/japana.css">
		<!--/include CSS-->

		<!--include JS-->
		<script src="../../assets/plugins/jquery/jquery-3.3.1.min.js"></script>
		<script src="../../assets/plugins/jquery-ui/jquery-ui.min.js"></script>
		<!--/include JS-->

		<!--include Font-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=vietnamese" rel="stylesheet">
        <!--/.include Font-->
        <style>
	        html, body {
                margin: 0 auto;
                -webkit-print-color-adjust: exact;
            }
        	.content-sidebar-wrap{
	    		width: 100%;
	    	}
	    	.dropdown-collapse{
	    		border-radius: 0;
	    	}
	    	.info-ncc{
				display: inline-block;
				width: 100%;
				margin-top: 15px;
			}
			.info-ncc .item{
				width: 50%;
				float: left;
				margin: 10px 0;
			}
			.info-ncc .item label, .total-price label, .sign-receipt ul li{
				font-size: 12pt;
				line-height: 1.5;
				font-weight: 500;
			}
			.total-price{
				margin-top: 15px;
			}
			.total-price .item{
				display: -webkit-box;
				display: -ms-flexbox;
				display: flex;
				-webkit-box-align: baseline;
				    -ms-flex-align: baseline;
				        align-items: baseline;
				-webkit-box-pack: justify;
				    -ms-flex-pack: justify;
				        justify-content: space-between;
				border: 1px solid #ccc;
				padding: 10px;
			}
			.total-price .item:last-child{
				border-top: 0;
			}
			.total-price .item label{
				width: 150px;
				border-right: 1px solid #ccc;
			}
			.total-price span{
				font-size: 12pt;
				font-weight: 500;
				margin-left: 15px;
				float: right;
			}
			.list-tb-table, .head-table, .body-table{
			    display: inline-block;
			    float: left;
			    width: 100%;
			}
			.head-table, .body-table{
				display: -webkit-box;
				display: -ms-flexbox;
				display: flex;
				-webkit-box-align: center;
				    -ms-flex-align: center;
				        align-items: center;
				-webkit-box-pack: justify;
				    -ms-flex-pack: justify;
				        justify-content: space-between;
				border: 1px solid #ccc;
			}
			.body-table{
				border-top: 0;
			}
			.head-table li, .body-table li{
				float: left;
				display: inline-block;
				padding: 5px;
				line-height: 1.5;
			}
			.head-table li:first-child, 
			.head-table li:nth-child(4), 
			.head-table li:last-child, 
			.body-table li:first-child, 
			.body-table li:nth-child(4), 
			.body-table li:last-child{
				width: 90px;
			}
			.head-table li:nth-child(3), .body-table li:nth-child(3){
				width: 300px;
			}
			.head-table li:nth-child(5), 
			.body-table li:nth-child(5),
			.head-table li:nth-child(6),
			.body-table li:nth-child(6),
			.head-table li:last-child,
			.body-table li:last-child{
				width: 150px;
			}
			.head-table li:nth-child(2), 
			.head-table li:nth-child(7), 
			.head-table li:nth-child(8), 
			.head-table li:nth-child(9), 
			.body-table li:nth-child(2), 
			.body-table li:nth-child(7), 
			.body-table li:nth-child(8), 
			.body-table li:nth-child(9){
				width: 100px;
			}
			.info-header{
				display: inline-block;
				width: 100%;
				border-bottom: 1px solid #ccc;
			}
			.info-header .item{
				width: 47%;
				float: left;
				padding: 15px 0;
				line-height: 1.5;
			}
			.info-header .item img{
				width: auto;
				height: 90px;
			}
			.info-header .item:first-child{
				text-align: right;
				margin-right: 6%;
			}
			.info-header .item p{
				font-size: 12pt;
				font-weight: 500;
			}
			.print .title{
				font-size: 20pt; 
				text-align: center; 
				margin: 20px 0 0;
			}
			.sign-receipt{
				display: inline-block;
				width: 100%;
				margin-top: 15px;
			}
			.sign-receipt ul{
				display: -webkit-box;
				display: -ms-flexbox;
				display: flex;
				-webkit-box-align: center;
				    -ms-flex-align: center;
				        align-items: center;
				-ms-flex-pack: distribute;
				    justify-content: space-around;
			}
        </style>
        <style type="text/css" media="print">
		    @page {
		        margin: 0;
		        size: auto;
		    }
		</style>
	</head>

	<body>
		<div class="site-container">
			<div class="site-inner">
				<div class="content-sidebar-wrap">
					<main class="print content">
						<div class="info-header">
							<div class="item">
								<img src="../../assets/images/logo-tem.png" alt="logo">
							</div>
							<div class="item">
								<p>CÔNG TY CỔ PHẦN JAPANA VIỆT NAM</p>
								<p>76 Nguyễn Háo Vĩnh, P.Tân Quý, Q.Tân Phú, TP.Hồ Chí Minh</p>
								<p>Tel: (028) 5408 6666 - Fax: (028) 5434 2039</p>
								<p>Website: https://japana.vn</p>
							</div>
						</div>
						<h3 class="title">TẠO PHIẾU NHẬP</h3>
						<div class="info-ncc">
							<div class="item">
				        		<label>Ngày:</label>
				        		<span>12-06-2019</span>
				        	</div>
				        	<div class="item">
				        		<label>Mã nhà cung cấp:</label>
				        		<span>NCC_001-1</span>
				        	</div>
				        	<div class="item">
				        		<label>Mã phiếu nhập:</label>
				        		<span>PN-935</span>
				        	</div>
				        	<div class="item">
				        		<label>Tên nhà cung cấp:</label>
				        		<span>SBS (Anh Phi)</span>
				        	</div>
				        </div>
			            <div class="list-tb-table">
			              <ul class="head-table">
			                  <li class="bold center-custom black-custom">STT</li>
			                  <li class="bold black-custom">SKU</li>
			                  <li class="bold black-custom">Tên sản phẩm</li>
			                  <li class="bold center-custom black-custom">Quy cách</li>
			                  <li class="bold right-custom black-custom">Giá bán</li>
			                  <li class="bold right-custom black-custom">Giá mua</li>
			                  <li class="bold center-custom black-custom">Chiết khấu</li>
			                  <li class="bold center-custom black-custom">Loại</li>
			                  <li class="bold center-custom black-custom">Số lượng</li>
			                  <li class="bold right-custom black-custom">Thành tiền</li>
			              </ul>
			              <ul class="body-table">
			                  <li class="center-custom">1</li>
			                  <li>087AA9</li>
			                  <li>Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</li>
			                  <li class="center-custom">Hộp</li>
			                  <li class="right-custom">420.000 đ</li>
			                  <li class="right-custom">270.000 đ</li>
			                  <li class="center-custom">35.7%</li>
			                  <li class="center-custom">Còn lại</li>
			                  <li class="center-custom">1</li>
			                  <li class="right-custom">270.000 đ</li>
			              </ul>
			              <ul class="body-table">
			                  <li class="center-custom">2</li>
			                  <li>087AA9</li>
			                  <li>Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</li>
			                  <li class="center-custom">Hộp</li>
			                  <li class="right-custom">420.000 đ</li>
			                  <li class="right-custom">270.000 đ</li>
			                  <li class="center-custom">35.7%</li>
			                  <li class="center-custom">Còn lại</li>
			                  <li class="center-custom">1</li>
			                  <li class="right-custom">270.000 đ</li>
			              </ul>
			            </div>
				        <div class="total-price custom-fright">
				        	<div class="item">
				        		<label>Tổng cộng:</label> <span>540.000 đ</span>
				        	</div>
				        	<div class="item">
				        		<label>Phí vận chuyển:</label> <span>0 đ</span>
				        	</div>
				        </div>
				        <div class="sign-receipt">
				        	<ul>
				        		<li>Người lập phiếu</li>
				        		<li>Kho</li>
				        		<li>Phó giám đốc</li>
				        	</ul>
				        </div>
					</main>
				</div>
			</div>
		</div>
		<script src="../../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
	            window.print();
	        });
		    (function () {
		        var beforePrint = function () {
		            console.log('Đang chuẩn bị in.');
		        };
		        var afterPrint = function () {
		            window.close();
		        };

		        if (window.matchMedia) {
		            var mediaQueryList = window.matchMedia('print');
		            mediaQueryList.addListener(function (mql) {
		                if (mql.matches) {
		                    beforePrint();
		                } else {
		                    afterPrint();
		                }
		            });
		        }

		        window.onbeforeprint = beforePrint;
		        window.onafterprint = afterPrint;
		    }());
		</script>
	</body>
</html>