<style>
	#info-pn .item{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		float: left;
		width: 25%;
		margin-top: 20px;
	}
	#info-pn .item:last-child{
		width: 50%;
	}
	#info-pn .item:last-child span, #info-pn .item:last-child .custom-dropdown{
		margin-right: 5px;
	}
	#info-pn .item label{
		font-weight: 500;
		font-size: 14px;
		color: #000;
		margin-right: 5px;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    position: relative;
	}
	.table-custom{
		margin-bottom: 15px;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		padding: 15px 0;
	}
	.link-show{
		display: none;
	}
	.total-order-box{
		width: 20%;
	}
	.total-order-box p {
	    font-weight: 500;
	    font-size: 14px;
	    margin: 15px 0;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-pack: justify;
	    -ms-flex-pack: justify;
	    justify-content: space-between;
	}
	.title-check, .detail-check, .number-list li, .title-list li, .listpro-slider li{
	    display: inline-block;
	    width: 100%;
	}
	.title-check{
	    border-bottom: 1px solid #ccc;
	}
	.check-kcs{
	    display: inline;
	}
	.check-kcs .checkmark{
	    left: 50%;
	    -webkit-transform: translateX(-50%);
	        -ms-transform: translateX(-50%);
	            transform: translateX(-50%);
	}
	.title-check li, .detail-check li{
	    float: left;
	}
	.title-check li{
	    padding: 5px;
	}
	.title-check li:first-child, .detail-check>li:first-child{
	    width: 5%;
	}
	.title-check li:nth-child(2), .detail-check>li:nth-child(2){
	    width: 50%;
	}
	.title-check li:last-child, .detail-check>li:last-child{
	    width: 45%;
	}
	.number-list li:first-child, .title-list li:first-child{
	    visibility: hidden;
	}
	.number-list li, .title-list li, .listpro-slider .item li{
	    padding: 5px;
	}
	.listpro-slider .item li .custom-dropdown{
	    display: inline-block;
	}
	.listpro-slider .item li .custom-dropdown:after{
	    padding: 12px;
	}
	.listpro-slider .item li:last-child{
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	}
	.slick-arrow {
		position: absolute;
		top: 30%;
		font-size: 20px;
	}
	.listpro-slider .slick-prev, .listpro-slider .slick-next {
	    color: #ccc;
	    cursor: pointer;
	    top: -5px;
	    width: 30px;
	    height: 30px;
	    display: -webkit-box !important;
	    display: -ms-flexbox !important;
	    display: flex !important;
	    -webkit-box-align: center;
	    -ms-flex-align: center;
	    align-items: center;
	    -webkit-box-pack: center;
	    -ms-flex-pack: center;
	    justify-content: center;
	    z-index: 99;
	}
	.listpro-slider .slick-next {
	    right: 0;
	}
	.listpro-slider .slick-prev {
	    left: 0;
	}
	.listpro-slider .slick-next:hover, .listpro-slider .slick-prev:hover{
	    color: #333;
	}
	.box-cmt{
	    padding: 10px 5px;
	}
	.box-cmt textarea, .upload-img{
	    margin-bottom: 15px;
	}
	.box-cmt button{
	    float: right;
	    margin-left: 10px;
	}
	.box-cmt .item{
		display: inline-block;
		width: 100%;
		float: left;
		line-height: 1.5;
		margin-bottom: 15px;
	}
	.box-cmt .item p:first-child span:last-child{
		font-size: 13px;
	}
	.box-cmt .item p:last-child{
		font-size: 14px;
	}
	#comment_kh{
	    border: 1px solid #cbcbcb;
	    padding: 10px;
	    border-radius: 5px;
	    margin-bottom: 12px;
	    max-height: 164px;
	    overflow-y: scroll;
	    overflow-x: hidden;
	    background: #fff;
	    position: relative;
	  }
	.box-input-cmt{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
	}
	.box-input-cmt .input-cmt{
		width: 84%;
	    float: left;
	    resize: none;
	}
	.box-input-cmt button{
		float: right;
		margin-left: 2%;
		height: 35px;
	}
	.cmt-staff{
		display: inline-block;
		width: 100%;
	}
	.cmt-staff .item:nth-child(even){
		text-align: right;
	}
	#info-pn, #products-pn, #check-pn , #note-pn{
		display: inline-block;
		width: 100%;
	}
	#note-pn{
	    float: left;
	    width: 35%
	}
	#check-pn{
	    width: 65%;
	    float: left;
	}
	.show-coupon-box{
		display: none;
	}
	.barcode-check{
		display: flex;
	}
	.barcode-check input{
		margin: 10px 15px 0 0;
	}
	.barcode-check input:last-child{
		width: 80px;
	}
	@media (max-width: 575.98px) {
		#info-pn, #check-pn, #products-pn, #note-pn{
			display: none;
			width: 100%;
		}
		#products-pn button{
			margin-bottom: 30px;
		}
		#info-pn .item:last-child{
			display: initial;
		}
		#info-pn .item, #info-pn .item:last-child{
			width: 100%;
		}
		#info-pn .item:last-child .custom-dropdown{
			margin: 10px 0;
		}
		.box-coupon-total{
			display: none;
			width: 100%;
			background: #fff;
			position: fixed;
		    left: 0;
		    right: 0;
		    padding: 0 15px;
		    overflow: scroll;
		    bottom: 35px;
		    border-top: 1px solid #eee;
		    z-index: 2;
		}
		.show-coupon-box{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		    -webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		    -webkit-box-pack: center;
		        -ms-flex-pack: center;
		            justify-content: center;
		    height: 35px;
			width: 100%;
			position: fixed;
			left: 0;
			right: 0;
			bottom: 0;
			z-index: 2;
		}
		.show-coupon-box a{
			color: #fff;
    		line-height: 1.5;
		}
		.slide-top{
			-webkit-animation: slide-top 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) infinite both;
			        animation: slide-top 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) infinite both;
		}
		@-webkit-keyframes slide-top {
		  0% {
		    -webkit-transform: translateY(10px);
		            transform: translateY(10px);
		  }
		  100% {
		    -webkit-transform: translateY(-10px);
		            transform: translateY(-10px);
		  }
		}
		@keyframes slide-top {
		  0% {
		    -webkit-transform: translateY(10px);
		            transform: translateY(10px);
		  }
		  100% {
		    -webkit-transform: translateY(-10px);
		            transform: translateY(-10px);
		  }
		}
		.total-order-box{
			width: 100%;
		}
		.title-check{
			margin-top: 15px;
		}
		.title-check li:first-child, .detail-check>li:first-child{
      		display: none;
	    }
	    .title-list li{
	      	white-space: nowrap;
	      	overflow: scroll;
	    }
	    #note-pn .item .title-check li{
	    	display: block;
	    }
	    #products-pn{
	    	padding-top: 15px;
	    	height: 100%;
	    }
	    #products-pn .table-custom{
	    	margin-top: 0;
	    }
	    #products-pn .table-custom > tbody > tr > td:nth-child(12) a, #products-pn .table-custom > tbody > tr > td:nth-child(14) a{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
		}
		#products-pn .table-custom > tbody > tr > td:nth-child(12) a i, #products-pn .table-custom > tbody > tr > td:nth-child(14) a i{
			margin-bottom: 0;
		}
		#check-pn .title-check li:last-child{
	      	text-align: center;
	    }
		.table-custom tr td:first-child{
			display: none;
		}
		.table-custom > tbody > tr > td{
			white-space: normal;
		}
		.table-custom > tbody > tr > td:last-child{
			padding: 0;
		}
		.title-check li:last-child, .detail-check>li:last-child{
			width: 50%;
		}
		.barcode-check{
			margin-top: 15px;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		#info-pn .item{
			width: 32%;
		}
		#info-pn .item:last-child{
			width: 100%;
		}
		.total-order-box{
			width: 50%;
		}
		#note-pn, #check-pn{
			width: 100%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		#info-pn .item{
			width: 32%;
		}
		#info-pn .item:last-child{
			width: 100%;
		}
		.total-order-box{
			width: 50%;
		}
		#note-pn, #check-pn{
			width: 100%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
		.title-check li:nth-child(2), .detail-check>li:nth-child(2){
			width: 55%;
		}
		.title-check li:last-child, .detail-check>li:last-child{
			width: 40%;
		}
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="listreceipt-detail content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chi tiết phiếu nhập</h1>
			<ul>
				<li>
					<a href="?action=listreceipt.php" class="link-custom black-custom" title="Trở về">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Trở về</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" onclick="window.location.href = 'include/listreceipt/print.php';" target="_blank" class="link-custom black-custom" title="In phiếu">
						<i class="fa fa-print" aria-hidden="true"></i> <label>In phiếu</label>
					</a>
	            </li>
	            <li>
					<a href="javascript:void(0);" onclick="printBarCode(1);" target="_blank" class="link-custom black-custom" title="In mã vạch">
						<i class="fa fa-barcode" aria-hidden="true"></i> <label>In mã vạch</label>
					</a>
	            </li>
	            <li>
					<button type="button" class="button button-header link-custom black-custom">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
                    </button>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="tab-custom bg-black">
				<div class="item active">
					<a href="javascript:void(0)" data-id="info-pn" title="Thông tin">
						<i class="fa fa-user" aria-hidden="true"></i>
						<label>Thông tin</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="products-pn" title="Sản phẩm">
						<i class="fa fa-list" aria-hidden="true"></i>
						<label>Sản phẩm</label>
					</a>
				</div>
				<div class="item bg-black">
					<a href="javascript:void(0)" data-id="check-pn" data-id="Kiểm duyệt">
						<i class="fa fa-check-square-o" aria-hidden="true"></i>
						<label>Kiểm duyệt</label>
					</a>
				</div>
				<div class="item bg-black">
					<a href="javascript:void(0)" data-id="note-pn" title="Ghi chú">
						<i class="fa fa-sticky-note" aria-hidden="true"></i>
						<label>Ghi chú</label>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<div id="info-pn" class="tab-content item show-inline">
					<div class="item">
						<label>Mã phiếu nhập:</label>
						<span>PN-935</span>
					</div>
					<div class="item">
						<label>Vận chuyển:</label>
						<span>Nhà cung cấp</span>
					</div>
					<div class="item">
						<label>Người tạo:</label>
						<span>Thới Thị Bích Khiêm</span>
					</div>
					<div class="item">
						<label>Mã NCC:</label>
						<span>NCC_069</span>
					</div>
					<div class="item">
						<label>Kho hàng:</label>
						<span>Kho ký gửi</span>
					</div>
					<div class="item">
						<label>Phí vận chuyển:</label>
						<span>0 đ</span>
					</div>
					<div class="item">
						<label>Ngày tạo:</label>
						<span>22-05-2019 11:49:35</span>
					</div>
					<div class="item">
						<label>Tên NCC:</label>
						<span>Cty Happy Life Tea</span>
					</div>
					<div class="item">
						<label>Trạng thái:</label>
						<span>Yêu cầu đàm phán</span>
						<div class="custom-dropdown">
				    		<select class="form-control" name="id_status">
					    		<option value="-1">Chọn trạng thái</option>
							</select>
				    	</div>
				    	<button type="submit" class="button bg-green">Lưu lại</button>
					</div>
				</div>
				<div id="products-pn" class="tab-content item">
					<div class="box-table">
						<table class="table table-custom table-striped table-responsive">
						    <thead class="bg-black">
						        <tr class="bg-black">
						            <th class="bg-black center-custom">STT</th>
						            <th class="bg-black">SKU</th>
						            <th class="bg-black">Tên sản phẩm</th>
						            <th class="bg-black center-custom">Quy cách</th>
						            <th class="bg-black right-custom">Giá mua</th>
						            <th class="bg-black right-custom">Giá bán</th>
						            <th class="bg-black center-custom">Chiết khấu</th>
						            <th class="bg-black center-custom">Loại</th>
						            <th class="bg-black center-custom">Hạn sử dụng</th>
						            <th class="bg-black center-custom">Mã vạch</th>
						            <th class="bg-black center-custom">SL quét</th>
						            <th class="bg-black center-custom">Số lượng</th>
						            <th class="bg-black right-custom">Thành tiền</th>
						            <th class="bg-black center-custom">Ghi chú</th>
						            <th class="bg-black right-custom">Trạng thái</th>
						            <th class="bg-black">Tác vụ</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td data-title="STT" class="center-custom">1</td>
						            <td data-title="SKU">069IB05</td>
						            <td data-title="Tên sản phẩm">Bột trà xanh Uji Matcha Yano Nhật Bản 100g</td>
						            <td data-title="Quy cách" class="center-custom">Túi</td>
						            <td data-title="Giá mua" class="right-custom">
						            	<input type="text" data-type="currency" id="price_buy1" name="price_buy[1]" value="129800" class="form-control link-show">
						            	<span>129,800 đ</span>
						            </td>
						            <td data-title="Giá bán" class="right-custom">249.000 đ</td>
						            <td data-title="Chiết khấu" class="center-custom">47.9%</td>
						            <td data-title="Loại" class="center-custom">Còn lại</td>
						            <td data-title="Hạn sử dụng" class="center-custom">20-04-2020</td>
						            <td data-title="Mã vạch" class="center-custom"><span class="red-custom">3450-3</span></td>
						            <td data-title="SL quét" class="center-custom"><span class="red-custom">0</span></td>
						            <td data-title="Số lượng" class="center-custom">3</td>
						            <td data-title="Thành tiền" class="right-custom">389.400 đ</td>
						            <td data-title="Ghi chú" class="center-custom">
						            	<a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#ghichu-modal" class="link-custom black-custom" title="Ghi chú">
					                        <i class="fa fa-file-text"></i>
					                    </a>
						            </td>
						            <td data-title="Trạng thái" class="right-custom">
						            	<div class="custom-dropdown">
								    		<select class="form-control" name="id_status[1]">
									    		<option value="-1">Chọn trạng thái</option>
									    		<option value="1">Yêu cầu đàm phán</option>
									    		<option value="2">Yêu cầu trả hàng</option>
									    		<option value="3">Đã đàm phán</option>
											</select>
								    	</div>
						            </td>
						            <td data-title="Tác vụ">
						            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editPrice(1)" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu lại">
						            		<i class="fa fa-floppy-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">2</td>
						            <td data-title="SKU">069IB05</td>
						            <td data-title="Tên sản phẩm">Bột trà xanh Uji Matcha Yano Nhật Bản 100g</td>
						            <td data-title="Quy cách" class="center-custom">Túi</td>
						            <td data-title="Giá mua" class="right-custom">
						            	<input type="text" data-type="currency" id="price_buy2" name="price_buy[2]" value="129800" class="form-control link-show">
						            	<span>129,800 đ</span>
						            </td>
						            <td data-title="Giá bán" class="right-custom">249.000 đ</td>
						            <td data-title="Chiết khấu" class="center-custom">47.9%</td>
						            <td data-title="Loại" class="center-custom">Còn lại</td>
						            <td data-title="Hạn sử dụng" class="center-custom">20-04-2020</td>
						            <td data-title="Mã vạch" class="center-custom"><span class="red-custom">3450-3</span></td>
						            <td data-title="SL quét" class="center-custom"><span class="red-custom">0</span></td>
						            <td data-title="Số lượng" class="center-custom">3</td>
						            <td data-title="Thành tiền" class="right-custom">389.400 đ</td>
						            <td data-title="Ghi chú" class="center-custom">
						            	<a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#ghichu-modal" class="link-custom black-custom" title="Ghi chú">
					                        <i class="fa fa-file-text"></i>
					                    </a>
						            </td>
						            <td data-title="Trạng thái" class="right-custom">
						            	<div class="custom-dropdown">
								    		<select class="form-control" name="id_status[1]">
									    		<option value="-1">Chọn trạng thái</option>
									    		<option value="1">Yêu cầu đàm phán</option>
									    		<option value="2">Yêu cầu trả hàng</option>
									    		<option value="3">Đã đàm phán</option>
											</select>
								    	</div>
						            </td>
						            <td data-title="Tác vụ">
						            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editPrice(2)" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu lại">
						            		<i class="fa fa-floppy-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">3</td>
						            <td data-title="SKU">069IB05</td>
						            <td data-title="Tên sản phẩm">Bột trà xanh Uji Matcha Yano Nhật Bản 100g</td>
						            <td data-title="Quy cách" class="center-custom">Túi</td>
						            <td data-title="Giá mua" class="right-custom">
						            	<input type="text" data-type="currency" id="price_buy3" name="price_buy[3]" value="129800" class="form-control link-show">
						            	<span>129,800 đ</span>
						            </td>
						            <td data-title="Giá bán" class="right-custom">249.000 đ</td>
						            <td data-title="Chiết khấu" class="center-custom">47.9%</td>
						            <td data-title="Loại" class="center-custom">Còn lại</td>
						            <td data-title="Hạn sử dụng" class="center-custom">20-04-2020</td>
						            <td data-title="Mã vạch" class="center-custom"><span class="red-custom">3450-3</span></td>
						            <td data-title="SL quét" class="center-custom"><span class="red-custom">0</span></td>
						            <td data-title="Số lượng" class="center-custom">3</td>
						            <td data-title="Thành tiền" class="right-custom">389.400 đ</td>
						            <td data-title="Ghi chú" class="center-custom">
						            	<a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#ghichu-modal" class="link-custom black-custom" title="Ghi chú">
					                        <i class="fa fa-file-text"></i>
					                    </a>
						            </td>
						            <td data-title="Trạng thái" class="right-custom">
						            	<div class="custom-dropdown">
								    		<select class="form-control" name="id_status[1]">
									    		<option value="-1">Chọn trạng thái</option>
									    		<option value="1">Yêu cầu đàm phán</option>
									    		<option value="2">Yêu cầu trả hàng</option>
									    		<option value="3">Đã đàm phán</option>
											</select>
								    	</div>
						            </td>
						            <td data-title="Tác vụ">
						            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editPrice(3)" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu lại">
						            		<i class="fa fa-floppy-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">4</td>
						            <td data-title="SKU">069IB05</td>
						            <td data-title="Tên sản phẩm">Bột trà xanh Uji Matcha Yano Nhật Bản 100g</td>
						            <td data-title="Quy cách" class="center-custom">Túi</td>
						            <td data-title="Giá mua" class="right-custom">
						            	<input type="text" data-type="currency" id="price_buy4" name="price_buy[4]" value="129800" class="form-control link-show">
						            	<span>129,800 đ</span>
						            </td>
						            <td data-title="Giá bán" class="right-custom">249.000 đ</td>
						            <td data-title="Chiết khấu" class="center-custom">47.9%</td>
						            <td data-title="Loại" class="center-custom">Còn lại</td>
						            <td data-title="Hạn sử dụng" class="center-custom">20-04-2020</td>
						            <td data-title="Mã vạch" class="center-custom"><span class="red-custom">3450-3</span></td>
						            <td data-title="SL quét" class="center-custom"><span class="red-custom">0</span></td>
						            <td data-title="Số lượng" class="center-custom">3</td>
						            <td data-title="Thành tiền" class="right-custom">389.400 đ</td>
						            <td data-title="Ghi chú" class="center-custom">
						            	<a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#ghichu-modal" class="link-custom black-custom" title="Ghi chú">
					                        <i class="fa fa-file-text"></i>
					                    </a>
						            </td>
						            <td data-title="Trạng thái" class="right-custom">
						            	<div class="custom-dropdown">
								    		<select class="form-control" name="id_status[1]">
									    		<option value="-1">Chọn trạng thái</option>
									    		<option value="1">Yêu cầu đàm phán</option>
									    		<option value="2">Yêu cầu trả hàng</option>
									    		<option value="3">Đã đàm phán</option>
											</select>
								    	</div>
						            </td>
						            <td data-title="Tác vụ">
						            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editPrice(4)" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu lại">
						            		<i class="fa fa-floppy-o"></i>
						            	</a>
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
					<div class="box-coupon-total">
						<div class="barcode-check custom-fleft">
							<input type="text" name="" onkeypress="checkMaVach(event);" id="code_mavach" placeholder="Nhập mã sản phẩm" class="form-control">
							<input type="text" name="" id="code_sl" class="form-control" value="1" placeholder="Nhập số">
						</div>
						<div class="total-order-box custom-fright">
							<p class="black-custom">
								Tổng đơn: <span>1,557,600‬ đ</span>
							</p>
							<p class="black-custom">
								Phí vận chuyển: <span>+ 0 đ</span>
							</p>
							<hr>
							<p class="black-custom">
								Tổng cộng: <span class="bold blue-custom">1,557,600‬ đ</span>
							</p>
						</div>
					</div>
					<div class="show-coupon-box bg-black">
						<a href="javascript:void(0);" title="Show">
							Mở thông tin <i class="fa fa-angle-double-up slide-top"></i>
						</a>
					</div>
				</div>
	          	<div class="box-check">
	          		<div id="check-pn" class="tab-content item">
		            	<div class="item">
							<ul class="title-check">
								<li class="center-custom bold">STT</li>
								<li class="bold">Tiêu chí</li>
								<li class="bold">KCS kiểm duyệt</li>
							</ul>
		              		<ul class="detail-check">
			                	<li>
									<ul class="number-list">
										<li class="center-custom">0</li>
										<li class="center-custom">1</li>
										<li class="center-custom">2</li>
										<li class="center-custom">3</li>
										<li class="center-custom">4</li>
										<li class="center-custom">5</li>
										<li class="center-custom">6</li>
										<li class="center-custom">7</li>
										<li class="center-custom">8</li>
										<li class="center-custom">9</li>
									</ul>
								</li>
								<li>
									<ul class="title-list">
										<li>0</li>
										<li>All</li>
										<li>Tên sản phẩm</li>
										<li>Hình ảnh</li>
										<li>Xuất xứ</li>
										<li>Dung tích</li>
										<li>Giấy CBSP checkcosmetic.net</li>
										<li>SP chính hãng có tem nhãn phụ</li>
										<li>Trên 50% hoặc ít nhất trước 8 tháng khi hết HSD</li>
										<li>Ngoại quang</li>
									</ul>
								</li>
			                	<li class="listpro-slider" id="kcs_listproslider">
			                  		<ul class="item">
					                    <li class="center-custom">SP1</li>
					                    <li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" id="checkAll1" onclick="checkall(1)" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
					                    </li>
					                    <li class="center-custom">
					                      	<label class="checkbox-custom check-kcs">
					                        	<input value="" type="checkbox" name="check[0][kcs][properties][]" class="chk1" disabled="" checked="">
					                        	<span class="checkmark"></span>
					                      	</label>
					                    </li>
					                    <li class="center-custom">
					                      	<label class="checkbox-custom check-kcs">
					                        	<input value="" type="checkbox" name="check[1][kcs][properties][]" class="chk1" disabled="" checked="">
					                        	<span class="checkmark"></span>
					                      	</label>
					                    </li>
					                    <li class="center-custom">
					                      	<label class="checkbox-custom check-kcs">
					                        	<input value="" type="checkbox" name="check[2][kcs][properties][]" class="chk1" disabled="" checked="">
					                        	<span class="checkmark"></span>
					                      	</label>
					                    </li>
					                    <li class="center-custom">
					                      	<label class="checkbox-custom check-kcs">
					                        	<input value="" type="checkbox" name="check[3][kcs][properties][]" class="chk1" disabled="" checked="">
					                        	<span class="checkmark"></span>
					                      	</label>
					                    </li>
					                    <li class="center-custom">
					                      	<label class="checkbox-custom check-kcs">
					                        	<input value="" type="checkbox" name="check[4][kcs][properties][]" class="chk1" disabled="" checked="">
					                        	<span class="checkmark"></span>
					                      	</label>
					                    </li>
					                    <li class="center-custom">
					                      	<label class="checkbox-custom check-kcs">
					                        	<input value="" type="checkbox" name="check[5][kcs][properties][]" class="chk1" disabled="" checked="">
					                        	<span class="checkmark"></span>
					                      	</label>
					                    </li>
					                    <li class="center-custom">
					                      	<label class="checkbox-custom check-kcs">
					                        	<input value="" type="checkbox" name="check[6][kcs][properties][]" class="chk1" disabled="" checked="">
					                        	<span class="checkmark"></span>
					                      	</label>
					                    </li>
					                    <li>
					                      	<div class="custom-dropdown">
					                        	<select class="form-control" name="check[0][kcs][quality]" disabled="">
					                          		<option value="0">0%</option>
													<option value="1">10%</option>
													<option value="2">20%</option>
													<option value="3">30%</option>
													<option value="4">40%</option>
													<option value="5">50%</option>
													<option value="6" selected="">60%</option>
													<option value="7">70%</option>
													<option value="8">80%</option>
													<option value="9">90%</option>
													<option value="10">100%</option>
					                        	</select>
					                      	</div>
					                    </li>
				                  	</ul>
			                  		<ul class="item">
					                    <li class="center-custom">SP2</li>
					                    <li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" id="checkAll2" onclick="checkall(2)" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
					                    </li>
					                    <li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[0][kcs][properties][]" class="chk2" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
					                    </li>
					                    <li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[1][kcs][properties][]" class="chk2" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
					                    </li>
					                    <li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[2][kcs][properties][]" class="chk2" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
					                    </li>
					                    <li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[3][kcs][properties][]" class="chk2" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
					                    </li>
					                    <li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[4][kcs][properties][]" class="chk2" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
					                    </li>
					                    <li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[5][kcs][properties][]" class="chk2" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
					                    </li>
					                    <li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[6][kcs][properties][]" class="chk2" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
					                    </li>
					                    <li>
					                      	<div class="custom-dropdown">
						                        <select class="form-control" name="check[0][kcs][quality]" disabled="">
													<option value="0">0%</option>
													<option value="1">10%</option>
													<option value="2">20%</option>
													<option value="3">30%</option>
													<option value="4">40%</option>
													<option value="5">50%</option>
													<option value="6">60%</option>
													<option value="7">70%</option>
													<option value="8">80%</option>
													<option value="9">90%</option>
													<option value="10" selected="">100%</option>
						                        </select>
					                      	</div>
					                    </li>
			                  		</ul>
									<ul class="item">
										<li class="center-custom">SP3</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" id="checkAll3" onclick="checkall(3)" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[0][kcs][properties][]" class="chk3" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[1][kcs][properties][]" class="chk3" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[2][kcs][properties][]" class="chk3" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[3][kcs][properties][]" class="chk3" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[4][kcs][properties][]" class="chk3" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[5][kcs][properties][]" class="chk3" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[6][kcs][properties][]" class="chk3" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li>
											<div class="custom-dropdown">
												<select class="form-control" name="check[0][kcs][quality]" disabled="">
													<option value="0">0%</option>
													<option value="1">10%</option>
													<option value="2">20%</option>
													<option value="3">30%</option>
													<option value="4">40%</option>
													<option value="5">50%</option>
													<option value="6">60%</option>
													<option value="7">70%</option>
													<option value="8">80%</option>
													<option value="9" selected="">90%</option>
													<option value="10">100%</option>
												</select>
											</div>
										</li>
									</ul>
							      	<ul class="item">
										<li class="center-custom">SP4</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" id="checkAll4" onclick="checkall(4)" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[0][kcs][properties][]" class="chk4" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[1][kcs][properties][]" class="chk4" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[2][kcs][properties][]" class="chk4" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[3][kcs][properties][]" class="chk4" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[4][kcs][properties][]" class="chk4" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[5][kcs][properties][]" class="chk4" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li class="center-custom">
											<label class="checkbox-custom check-kcs">
												<input value="" type="checkbox" name="check[6][kcs][properties][]" class="chk4" disabled="" checked="">
												<span class="checkmark"></span>
											</label>
										</li>
										<li>
											<div class="custom-dropdown">
												<select class="form-control" name="check[0][kcs][quality]" disabled="">
													<option value="0">0%</option>
													<option value="1">10%</option>
													<option value="2">20%</option>
													<option value="3">30%</option>
													<option value="4">40%</option>
													<option value="5">50%</option>
													<option value="6">60%</option>
													<option value="7">70%</option>
													<option value="8" selected="">80%</option>
													<option value="9">90%</option>
													<option value="10">100%</option>
												</select>
											</div>
										</li>
							      	</ul>
							    </li>
							</ul>
		            	</div>
		            </div>
		            <div id="note-pn" class="tab-content item">
		            	<div class="item">
		              		<ul class="title-check">
		                		<li class="bold">Ghi chú</li>
		              		</ul>
		              		<div class="box-cmt">
		              		<div id="comment_kh">
			                  	<div class="item cmt-customer">
				                    <p class="red-custom bold">Quản lý TVBH <span class="black-custom">- Nguyễn Thu Hà</span> <span class="gray-custom normal">(27/05/2019 10:40:33)</span></p>
				                    <p class="detail-cmt">Khách mua loại khác dành cho công nghiệp</p>
			                  	</div>
			                  	<div class="cmt-staff">
									<div class="item">
										<p class="blue-custom bold">Thu mua <span class="black-custom">- Hương</span> <span class="gray-custom normal">(27-12-2018 | 10:45)</span></p>
										<p class="detail-cmt">Giao hàng nhanh, trước 14h hôm nay</p>
									</div>
									<div class="item">
										<p class="blue-custom bold">Tư vấn bán hàng <span class="black-custom">- Hằng</span> <span class="gray-custom normal">(27-12-2018 | 10:45)</span></p>
										<p class="detail-cmt">Khách yêu cầu không giao nhanh</p>
									</div>
									<div class="item">
										<p class="blue-custom bold">Xử lý đơn hàng <span class="black-custom">- Lam</span> <span class="gray-custom normal">(27-12-2018 | 10:45)</span></p>
										<p class="detail-cmt">Giao nhanh cho chị</p>
									</div>
								</div>
			                </div>
			                <textarea id="comment" rows="2" class="form-control input-cmt"></textarea>
			                <button type="button" data-dismiss="modal" class="button bg-red">Hủy</button>
			                <button type="button" class="button bg-green">Lưu</button>
			              </div>
		            	</div>
		            </div>
	          	</div>
		        </div>  
			</div>
		</div>
	</article>
</main>
<?php include('include/listreceipt/note.php')?>
<script>
	function editPrice(id){
		jQuery('#price_buy'+id).css('display','block');
		jQuery('#price_buy'+id).next().css('display','none');
	}
	jQuery(document).one('focus.input-cmt', 'textarea.input-cmt', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    }).on('input.input-cmt', 'textarea.input-cmt', function(){
        var minRows = this.getAttribute('data-min-rows')|0, rows;
        this.rows = minRows;
        rows = Math.floor((this.scrollHeight - this.baseScrollHeight)/16);
        this.rows = minRows + rows;
    });
	jQuery(function(){
		jQuery('.show-coupon-box a').click(function(){
			if(jQuery('.box-coupon-total').css('display')=='none'){
				jQuery('.box-coupon-total').css('display','inline-block');
				jQuery(this).text("");
				jQuery(this).append("Đóng thông tin <i class='fa fa-angle-double-down slide-top'></i>");
				showBackgroundPopup();
			}
			else{
				jQuery('.box-coupon-total').css('display','none');
				deleteBackgroundPopup();
				jQuery(this).text("");
				jQuery(this).append("Mở thông tin <i class='fa fa-angle-double-up slide-top'></i>");
			}
		});
		new PerfectScrollbar('#comment_kh');
		jQuery('.listpro-slider').slick({
	        autoplay: false,
	        dots: false,
	        infinite: false,
	        centerMode: false,
	        speed: 300,
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        variableWidth: false,
	        autoplaySpeed: 2000,
	        nextArrow: '<span class="slick-next"><i class="ti-angle-right" aria-hidden="true"></i></span>',
	        prevArrow: '<span class="slick-prev"><i class="ti-angle-left" aria-hidden="true"></i></span>',
	        responsive: [
	        {
	            breakpoint: 1024,
	            settings: {
	            slidesToShow: 2,
	            slidesToScroll: 2,
	            infinite: true,
	            dots: false
	          }
	        },
	        {
	            breakpoint: 600,
	            settings: {
	            slidesToShow: 2,
	            slidesToScroll: 2
	          }
	        },
	        {
	          breakpoint: 480,
	          settings: {
	            slidesToShow: 1,
	            slidesToScroll: 1
	          }
	        }
	      ]
	      });
		jQuery('.tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.tab-content').not('#' + data).removeClass('show-inline');
	    	jQuery(this).parent().addClass('active');
	    	jQuery('.tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    	jQuery('.listpro-slider').slick('setPosition'); 
	    });
	})
</script>