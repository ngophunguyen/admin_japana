<style>
  #addtag-modal .box-input-tag input{
    width: 49%;
    float: left;
  }
  #addtag-modal .box-input-tag input:nth-child(even){
    margin-left: 2%;
  }
  @media (max-width: 575.98px) {
    #addtag-modal .box-input-tag input{
      width: 100%;
      margin-left: 0;
      margin-bottom: 15px;
    }
    #addtag-modal .box-input-tag input:last-child{
      margin-bottom: 0;
    }
    #addtag-modal .box-input-tag input:nth-child(even){
      margin-left: 0;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {  
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal large-modal" id="addtag-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Thêm tag
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
      	<div class="box-input-tag">
      		<input type="text" id="name" name="name" class="form-control" placeholder="Nhập tags" autocomplete="on">
			     <input type="text" id="slug_vi" name="slug_vi" placeholder="Nhập slug" class="form-control" autocomplete="on">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="button bg-green">Thêm</button>
      </div>
    </div>
  </div>
</div>
<script>
  
</script>