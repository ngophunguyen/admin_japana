<style>
	.group-custom-cate, .group-box-seo{
		display: inline-block;
		float: left;
		width: 49%;
		margin-top: 15px;
		margin-right: 2%;
		margin-bottom: 15px;
	}
	.group-box-seo{
		margin-right: 0;
	}
	.box-cate-custom .item, .box-seo .item{
		display: inline-block;
		width: 48%;
		margin: 10px 0;
		float: left;
	}
	.box-cate-custom .item:nth-child(odd) {
	    margin-left: 4%;
	}
	.box-cate-custom .item:first-child{
		margin-left: 0;
	}
	.box-cate-custom .item:last-child, .box-seo .item .seo-input:last-child{
		margin-bottom: 0;
	}

	.box-cate-custom .item:first-child{
		width: 100%;
	}

	.input-group label.custom-upload{
		width: 100px!important;
	}
	.box-cate-custom .item label, .box-seo .item label, .box-ckeditor .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.box-ckeditor .item:last-child{
		margin-top: 15px;
	}
	.custom-dropdown:after{
		padding-right: 15px;
	}
	.box-img-meta{
		max-height: 180px;
		height: 180px;
		display: none;
		margin-top: 15px;
	}
	.box-img-meta img{
		height: auto;
		width: 100%;
	}
	.dropdown-collapse{
		float: left;
	}
	.box-seo{
		display: inline-block;
		width: 100%;
	}
	.box-seo .item{
		width: 100%;
	}
	.box-seo .item:last-child{
		margin-bottom: 0;
	}
	.box-seo .item .seo-input{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: baseline;
		    -ms-flex-align: baseline;
		        align-items: baseline;
		-webkit-box-pack: end;
		    -ms-flex-pack: end;
		        justify-content: flex-end;
		margin-bottom: 15px;
	}
	.box-seo .item .seo-input input, .box-seo .item .seo-input textarea, .box-seo .item .seo-input .upload-img{
		margin-left: 15px;
		width: 80%;
		float: right;
	}
	.box-seo .item .seo-input .upload-img input{
		margin-left: 0;
		width: auto
	}
	.box-ckeditor{
		display: none;
		padding-top: 0;
	}
	.content-product{
		margin-bottom: 15px;
	}
	@media (max-width: 575.98px) {
		.show-info-order{
			display: block;
		}
	  	.group-category{
	  		display: none;
	  	}
	  	.group-category{
	  		position: fixed;
	  		height: auto!important;
		    width: 100%;
		    padding: 10px;
		    display: none;
		    top: 92px;
		    left: 0;
		    right: 0;
		    border-bottom: 2px solid #eee;
		    z-index: 99;
		    background: #fff;
		    margin-top: 0;
	  	}
	  	.dropdown-collapse{
	  		display: none;
	  	}
	  	.box-button-info-order{
	  		display: block;
	  	}
	  	.upload-multi-img{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		margin-top: 15px;
	  	}
		#info-dm, #seo-dm, #note-dm, #listimg-dm{
			display: none;
			width: 100%;
		}
		.group-custom-cate, .group-box-seo{
			width: 100%;
			margin: 0;
		}
		.box-cate-custom .item, 
		.box-seo .item .seo-input .upload-img,
		.box-seo .item .seo-input,
		.box-img-block{
			display: inline-block;
			width: 100%;
		}
		.box-img-meta{
			width: 100%;
		}
		.box-seo .item .seo-input .upload-img{
			margin: 0;
			margin-top: 10px;
		}
		.box-seo .item .seo-input .upload-img input{
			margin-top: 0;
		}
		.box-cate-custom .item:nth-child(odd), .box-img-meta{
			margin-left: 0;
		}
		.box-cate-custom .item:last-child{
			margin-bottom: 15px;
		}
		.box-seo .item .seo-input input, .box-seo .item .seo-input textarea{
			margin-left: 0;
			width: 100%;
			margin-top: 10px;
		}
		.box-cate-custom .item, .box-seo .item{
			margin: 0;
			margin-top: 10px;
		}
		.box-ckeditor{
			display: block;
			margin-top: 15px;
			padding: 0;
		}
		.box-img-block{
			white-space: nowrap;
    		overflow-x: scroll;
    		overflow-y: hidden;
		}
		.box-img-block .item{
			width: 45%;
			margin: 0;
			margin-right: 5%;
		}
		.box-img-block .item:last-child{
			margin-right: 0;
		}
		.box-img-block .img-upbox{
			white-space: initial;
		}
		.img-upbox .btn-del-img{
			top: 0;
			left: auto;
    		right: -10px;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-cate-custom .item{
			width: 100%;
		}
		.box-seo .item .seo-input input, .box-seo .item .seo-input textarea{
			width: 70%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.box-cate-custom .item{
			width: 100%;
		}
		.box-cate-custom .item:nth-child(odd){
			margin-left: 0;
		}
		.box-seo .item .seo-input input, .box-seo .item .seo-input textarea{
			width: 70%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.box-seo .item .seo-input input, .box-seo .item .seo-input textarea{
			width: 70%;
		}
		.box-seo .item .seo-input .upload-img input{
			width: 55%;
		}
		.box-cate-custom .item{
			width: 100%;
		}
		.box-cate-custom .item:nth-child(odd){
			margin-left: 0;
		}
	}
	@media (min-width: 1200px) {

	}
</style>
<main class="addstatic content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Thêm tin tĩnh</h1>
			<ul>
				<li>
					<a href="?action=static.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<li>
					<button type="button" class="button button-header link-custom black-custom">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
                    </button>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="tab-custom bg-black">
				<div class="item active">
					<a href="javascript:void(0)" data-id="info-dm" title="Chi tiết danh mục">
						<i class="fa fa-sticky-note" aria-hidden="true"></i>
						<label>Chi tiết</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="seo-dm" title="SEO">
						<i class="fa fa-bullhorn" aria-hidden="true"></i>
						<label>SEO</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="note-dm" title="Nội dung">
						<i class="fa fa-book" aria-hidden="true"></i>
						<label>Nội dung</label>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<div id="info-dm" class="group-custom-cate tab-content item show-inline">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">Chi tiết bài viết</a>
					<div class="box-cate-custom">
						<div class="item">
					    	<label for="name_vi">Tiêu đề tin:</label>
					    	<input autocomplete="off" type="text" name="name_vi" id="name_vi" placeholder="Nhập tên sản phẩm..." class="form-control" required="">
					    </div>
						<div class="item">
					    	<label for="sku">Hot:</label>
					    	<div class="custom-dropdown">
						    	<select class="form-control" id="is_show" name="showview">
						    		<option value="-1">Chọn trạng thái</option>
								  	<option value="0">Hot</option>
								  	<option value="1">Không hot</option>
								</select>
							</div>
					    </div>
					    <div class="item">
							<label>Ảnh đại diện (16x9):</label>
							<div class="upload-img">
	                            <div class="input-group up-img">
	                                <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
	                                <label class="button bg-green custom-upload">
	                                    <input type="file" id="ipt-img" class="form-control" name="imgs" onchange="readURL(this,1);" accept="image/*">Upload
	                                </label>
	                                <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
	                            </div>
	                        </div>
	                        <div class="box-img-meta">
	                        	<img src="" alt="hình" id="photo1">
	                        </div>
						</div>
					</div>
				</div>
				<div id="seo-dm" class="group-box-seo tab-content item">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">SEO</a>
					<div class="box-seo">
						<div class="item">
							<label for="slug_vi">Link URL:</label>
							<input autocomplete="off" type="text" name="slug_vi" id="slug_vi" placeholder="Nhập link url vd:danh-muc" value="" class="form-control">
						</div>
						<div class="item">
							<label for="slug_vi">Tags:</label>
							<input name="vtags" id="vTags" value="" type="hidden">
	                        <input id="Tags" value="collagen,nước uống collagen,collagen dạng bột,tủ lạnh,máy sấy,collagen1,nước uống collagen 1,collagen dạng bột 1,tủ lạnh 1,máy sấy 1" type="hidden">
	                        <ul id="ShowTag"></ul>
						</div>
						<div class="item">
							<label for="meta_web_title">Google:</label>
							<span class="seo-input">
								Title:
								<input autocomplete="off" type="text" name="meta_web_title" id="meta_web_title" placeholder="Nhập title" value="" class="form-control">
							</span>
							<span class="seo-input">
								Keyword:
								<input autocomplete="off" type="text" name="meta_web_keyword" id="meta_web_keyword" placeholder="Nhập keyword" value="" class="form-control">
							</span>
							<span class="seo-input">
								Description:
								<textarea name="meta_web_desc" id="meta_web_desc" class="form-control" rows="3" placeholder="Nhập description"></textarea>
							</span>
						</div>
						<div class="item">
							<label for="meta_web_title">Social:</label>
							<span class="seo-input">
								Title:
								<input autocomplete="off" type="text" name="og_title" id="og_title" placeholder="Meta O.g title..." value="" class="form-control">
							</span>
							<span class="seo-input">
								Description:
								<textarea name="og_desc" id="og_desc" class="form-control" rows="3" placeholder="Meta O.g descriptions..."></textarea>
							</span>
						</div>
					</div>
				</div>
				<div id="note-dm" class="tab-content item">
					<a class="content-product dropdown-collapse bg-black" href="javascript:void(0);">Nội dung <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div class="box-ckeditor">
						<div class="item">
							<label for="elm1">Mô tả ngắn:</label>
							<textarea name="notes_vi" id="notes_vi" rows="5" class="form-control"></textarea>
						</div>
						<div class="item">
							<label for="desc_vi">Chi tiết tin:</label>
							<textarea class="ckeditor" id="desc_vi" name="desc_vi"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/categoryproduct/add.php'); ?>
<script>
    function readURL(input,id) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	        	jQuery('#photo'+id).parent().css('display','block');
	          	jQuery('#photo'+id).attr('src', e.target.result);
	        };
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	
	jQuery(function(){
		CKEDITOR.instances["elm1"];
		var sampleTags = jQuery("#vTags").val();
        var sampleTags2 = jQuery("#Tags").val();
        sampleTags = sampleTags.split(",");
        sampleTags2 = sampleTags2.split(",");
        jQuery('#ShowTag').tagit({
          availableTags: sampleTags2,
          singleField: true,
          allowSpaces: true,
          allowDuplicates: false,
          singleFieldNode: jQuery('#vTags'),
          beforeTagAdded: function(event, ui) {
                if ($.inArray(ui.tagLabel, sampleTags2) == -1) {
                    alert(ui.tagLabel + ' không phải là tag khả dụng.');
                    jQuery('.tagit-new input').val('');
                    return false;
                }
            }
        });
        jQuery('#ShowTag2').tagit({
          availableTags: sampleTags2,
          singleField: true,
          allowSpaces: true,
          allowDuplicates: false,
          singleFieldNode: jQuery('#vTags2'),
          beforeTagAdded: function(event, ui) {
                if ($.inArray(ui.tagLabel, sampleTags2) == -1) {
                    alert(ui.tagLabel + ' không phải là tag khả dụng.');
                    jQuery('.tagit-new input').val('');
                    return false;
                }
            }
        });
		jQuery('.content-product').click(function(){
	    	if(jQuery('.box-ckeditor').css('display')=='none'){
	    		jQuery('.box-ckeditor').css('display','inline-block');
	    		jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
	    		if(jQuery('.nav-primary').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight() && jQuery('.nav-primary').outerHeight() > jQuery('body').outerHeight()){
					jQuery('.nav-primary').css('height','auto');
				}else if(jQuery('body').outerHeight() > jQuery('.nav-primary').outerHeight() && jQuery('body').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight()){
					jQuery('.nav-primary').css('height',jQuery('body').outerHeight());
				}
				else{
					jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
				}
	    	}
	    	else{
	    		jQuery('.box-ckeditor').css('display','none');
	    		jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
	    		if(jQuery('.nav-primary').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight() && jQuery('.nav-primary').outerHeight() > jQuery('body').outerHeight()){
					jQuery('.nav-primary').css('height','auto');
				}else if(jQuery('body').outerHeight() > jQuery('.nav-primary').outerHeight() && jQuery('body').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight()){
					jQuery('.nav-primary').css('height',jQuery('body').outerHeight());
				}
				else{
					jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
				}
	    	}
	    });

		jQuery(document).on('change', ':file', function() {
		    var input = jQuery(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		});
		jQuery(':file').on('fileselect', function(event, numFiles, label) {
          	var input = jQuery(this).parents('.up-img').find(':text'),
              	log = numFiles > 1 ? numFiles + ' files selected' : label;
          	if( input.length ) {
              	input.val(log);
              	if(input.val(log)){
                	jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
                	jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
              	}
          	} else {
              	if(log) ;
          	}
      	});
      	jQuery('.delete-img').on('click', function(e){
		    jQuery(this).parent().find('input[type=file]').val('');
		    jQuery(this).parent().find('input[type=text]').val('');
		    jQuery(this).parent().find('.custom-upload').css('display','flex');
		    jQuery('.box-img-meta').css('display','none');
		    jQuery(this).css('display','none');
		});
      	jQuery('.tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.tab-content').not('#' + data).removeClass('show-inline');

	    	jQuery(this).parent().addClass('active');
	    	jQuery('.tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    });
	});
</script>