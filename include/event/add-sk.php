<style>
	.box-event, .info-event, .show-promo{
		margin-top: 15px;
		display: inline-block;
		width: 100%;
	}
	.box-upload-icon .item, .show-promo .item{
		display: inline-block;
		width: 49%;
		float: left;
	}
	.box-event>.item:first-child{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-event>.item:last-child{
		width: 53%;
		float: left;
	}
	.box-event .item:last-child, .box-upload-icon .item:last-child, .show-promo .item:last-child{
		margin-left: 2%;
	}
	.box-upload-icon .item{
		width: 100%;
	}
	.box-upload-icon .item:last-child{
		margin-left: 0;
		margin-top: 15px;
	}
	.box-upload-icon .item input[type="file"]{
		display: none;
	}
	.box-upload-icon .item label{
		border: 1px solid #ccc;
		border-radius: 4px;
		height: 35px;
		width: 25%;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
		float: left;
		margin-right: 15px;
	}
	.box-upload-icon .item label i{
		margin-right: 10px;
	}
	.box-upload-icon .item label:hover{
		background: #222D32;
		color: #fff;
	}
	.box-upload-icon .item img{
		float: left;
		height: 35px;
		width: auto;
		margin-right: 15px;
	}
	.show-promo .item .checkbox-custom{
		padding: 7px 0 0 30px;
	}
	.show-promo .item:last-child .checkbox-custom{
		margin-bottom: 15px;
	}
	.show-promo .item:last-child p.detail-gift{
		margin-top: 15px;
	}
	.check-sale .item{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
		width: 100%;
		margin-top: 15px;
	}
	.check-sale .item:last-child{
		margin-left: 0;
	}
	.check-sale .item .checkbox-custom{
		width: 10%;
	}
	.check-sale .item input[type='text']{
		width: 60%;
	}
	.check-sale .item span{
		width: 20%;
		text-align: center;
	}
	.check-sale .item span.checkmark{
		width: 20px;
	}
	.time-graph{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
    	-webkit-box-pack: end;
    	    -ms-flex-pack: end;
    	        justify-content: flex-end;
    	margin: 15px 0;
	}
	.time-graph span{
		font-family:'Roboto',sans-serif; 
		font-weight: 400; 
		font-size: 14px; 
		text-align: right; 
		margin-bottom: 0; 
		width: auto;
	}
	.info-product {
	    float: left;
	    width: 45%;
	}
	.detail-product {
	    float: left;
	    width: 52%;
	    margin-left: 3%;
	}
	.thumb {
	    width: 100%;
	    border: 1px solid #cecece;
	    position: relative;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	}
	.thumb img {
	    display: inline-block;
	    height: 100%;
	    width: 100%;
	    -o-object-fit: cover;
	       object-fit: cover;
	}
	.block-sale3 {
	    width: 80px;
	    height: 80px;
	    top: 0;
	    left: 0;
	    right: auto;
	    text-align: left;
	    position: absolute;
	    padding-left: 10px;
	    padding-top: 10px;
	}
	.block-sale img, .block-sale2 img, .block-sale3 img {
	    height: auto;
	    width: 100%;
	}
	.detail-product .title, .detail-product .info-pro, .detail-product .info-pro span{
		font-family: 'Roboto', sans-serif;
	    font-weight: 500;
	}
	.detail-product .title {
	    font-size: 16px;
	    color: #2a2a2a;
	    margin-bottom: 15px;
	    line-height: 1.5;
	}
	.detail-product .info-pro{
		margin-bottom: 15px;
	}
	.detail-product .info-pro, .detail-product .info-pro span {
	    font-size: 13px;
	    color: #333333;
	}
	.detail-product .info-pro span, .detail-product span {
	    font-weight: 300;
	    color: #999999;
	    padding-right: 7px;
	}
	.all-price {
	    width: 100%;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	    -ms-flex-align: center;
	    align-items: center;
	}
	.all-price span {
	    font-weight: 500;
	    color: #333;
	    font-size: 14px;
	}
	.all-price .old-price {
	    font-size: 13px;
	    color: #999999;
	    text-decoration: line-through;
	    float: left;
	    padding: 0;
	    margin-left: 8px;
	    margin-bottom: 0;
	}
	.all-price .new-price, .promo-intro span {
	    font-size: 17px;
	    color: #e62e6b;
	    float: left;
	    margin: 0;
	    margin-left: 15px;
	}
	.promo-intro {
	    padding: 10px;
	    margin-top: 15px;
	    border: 1px solid #cbcbcb;
	    border-left: 3px solid #e62e6b;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	    -ms-flex-align: center;
	    align-items: center;
	}
	.promo-intro img {
	    margin-right: 10px;
	    width: 20px;
	}
	.promo-intro .title-promo {
	    font-size: 13px;
	    color: #333333;
	    margin-bottom: 0;
	    line-height: 1.5;
	    font-weight: 500;
	}
	#mota{
		margin: 10px 0 15px;
	}
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
		width: 100%;
		padding: 15px 0;
	}
	.search1 input{
		width: 91%;
		display: inline-block;
	}
	.search1 button{
		width: 8%;
		display: inline-block;
	}
	.box-table{
		width: 100%;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	    float: left;
	}
	.table-custom{
		margin-top: 0;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		height: auto;
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	img.gift-order{
		height: 20px!important; 
		bottom: 5px; 
		left: 0;  
		position: absolute;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.first-price{
		text-decoration: line-through;
	    font-size: 12px;
	    color: #333;
	    width: 100%;
	    display: inline-block;
	    margin-bottom: 10px;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.table-custom .checkbox-custom {
	    cursor: pointer;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    height: 20px;
	}
	#listsp-km{
		margin-top: 15px;
	}
	.removelist{
		padding: 0;
	}
	.removelist:hover{
		color: #ff0000!important;
	}
	@media (max-width: 575.98px) {
		.entry-header ul{
			display: none;
		}
		.entry-content{
			margin-top: 48px!important;
		}
		#info-km, #listsp-km{
			display: none;
		}
		.box-event>.item:first-child, .box-event>.item:last-child, .box-upload-icon .item, .show-promo .item, .info-product, .detail-product, #info-km, #listsp-km{
			width: 100%;
		}
		.box-event .item:last-child, .box-upload-icon .item:last-child, .show-promo .item:last-child, .detail-product{
			margin-left: 0;
		}
		.box-upload-icon .item label{
			-webkit-box-pack: initial;
			    -ms-flex-pack: initial;
			        justify-content: initial;
			padding: 0 15px;
			width: 70%;
		}
		.box-upload-icon .item{
			margin-bottom: 15px;
		}
		.box-upload-icon .item:last-child{
			margin-top: 0;
		}
		.box-upload-icon .item:last-child button{
			margin-top: 15px;
		}
		.info-event{
			margin-top: 0;
		}
		.show-promo .item{
			margin-bottom: 5px;
		}
		.search1 input {
		    width: 68%;
		}
		.search1 button {
		    width: 30%;
		}
		.info-detail-product, .detail-product{
			margin-top: 15px;
		}
	  	.box-table tr td:nth-child(3){
	  		width: 100%;
	  	}
		img.gift-order{
			left: auto;
		}
		.first-price{
			margin-bottom: 0;
			width: auto;
		}
		.table-custom > tbody > tr > td .box-time, .table-custom > tbody > tr > td .custom-dropdown{
			width: 100%;
			padding: 5px 0;
		}
		.removelist{
			margin: 0 0 45px;
		}
		.removelist i{
			width: auto;
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.box-event .item:last-child{
			margin-bottom: 15px;
		}
		.table-custom tr th:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-upload-icon .item, .show-promo .item{
			width: 100%;
		}
		.box-upload-icon .item:last-child, .show-promo .item:last-child, .detail-product{
			margin-left: 0;
		}
		.box-upload-icon .item label{
			-webkit-box-pack: initial;
			    -ms-flex-pack: initial;
			        justify-content: initial;
			padding: 0 15px;
			width: 70%;
		}
		.box-upload-icon .item{
			margin-bottom: 15px;
		}
		.info-event{
			margin-top: 0;
		}
		.show-promo .item:last-child, .detail-product{
			margin-top: 15px;
		}
		.info-product, .detail-product{
			width: 100%;
		}
		.search1 input{
			width: 80%;
		}
		.search1 button{
			width: 15%;
		}
		.table-custom {
		    white-space: nowrap;
		}
		.table-custom > tbody > tr > td:last-child {
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.box-upload-icon .item, .show-promo .item{
			width: 100%;
		}
		.box-upload-icon .item:last-child, .show-promo .item:last-child, .detail-product{
			margin-left: 0;
		}
		.box-upload-icon .item label{
			-webkit-box-pack: initial;
			    -ms-flex-pack: initial;
			        justify-content: initial;
			padding: 0 15px;
			width: 70%;
		}
		.box-upload-icon .item{
			margin-bottom: 15px;
		}
		.info-event{
			margin-top: 0;
		}
		.show-promo .item:last-child, .detail-product{
			margin-top: 15px;
		}
		.info-product, .detail-product{
			width: 100%;
		}
		.search1 input{
			width: 80%;
		}
		.search1 button{
			width: 15%;
		}
		.table-custom {
		    white-space: nowrap;
		}
		.table-custom > tbody > tr > td:last-child {
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
		.box-upload-icon .item, .show-promo .item{
			width: 100%;
		}
		.box-upload-icon .item:last-child, .show-promo .item:last-child, .detail-product{
			margin-left: 0;
		}
		.box-upload-icon .item label{
			-webkit-box-pack: initial;
			    -ms-flex-pack: initial;
			        justify-content: initial;
			padding: 0 15px;
			width: 70%;
		}
		.box-upload-icon .item{
			margin-bottom: 15px;
		}
		.info-event{
			margin-top: 0;
		}
		.show-promo .item:last-child, .detail-product{
			margin-top: 15px;
		}
		.info-product, .detail-product{
			width: 100%;
		}
		.search1 input{
			width: 80%;
		}
		.search1 button{
			width: 15%;
		}
		.table-custom {
		    white-space: nowrap;
		}
		.table-custom > tbody > tr > td:last-child {
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="event-sk content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Sản phẩm sự kiện</h1>
			<ul>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="tab-custom bg-black">
				<div class="item active">
					<a href="javascript:void(0)" data-id="info-km" title="Chi tiết khuyến mãi">
						<i class="fa fa-sticky-note" aria-hidden="true"></i>
						<label>Chi tiết</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="listsp-km" title="Danh sách sản phẩm">
						<i class="fa fa-cube" aria-hidden="true"></i>
						<label>Sản phẩm</label>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<div id="info-km" class="tab-content item show-inline">
					<div class="box-event">
						<div class="item">
							<div class="box-upload-icon">
								<div class="item">
									<input name="name" type="text" value="sản phẩm hot" class="form-control" placeholder="Nhập tên chương trình...">
								</div>
								<div class="item">
									<label for="icon-qt"><i class="fa fa-upload" aria-hidden="true"></i> Icon quà tặng</label>
									<input type="file" id="icon-qt" name="images_qt" onchange="changesImages()">
									<img src="assets/images/icon-gift.png" alt="icon">
									<button type="submit" class="button bg-green">Thêm</button>
								</div>
							</div>

							<div class="info-event">
								<input type="text" id="SKU" value="" class="form-control" placeholder="Nhập SKU...">
								<div class="time-graph">
			                        <div class="box-time">
			                        	<input type="hidden" name="start_date" id="date_start" value="">
								        <input type="hidden" name="end_date" id="date_end" value="">
			                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-event" id="date-chart" class="form-control ipt-date" placeholder="Chọn ngày hiển thị...">
			                            <i class="fa fa-calendar icon-time"></i>
			                        </div>
			                    </div>
					            <label for="mota" class="bold">Mô tả</label>
					            <input autocomplete="off" type="text" id="mota" name="mota" class="form-control ipt-date" placeholder="Nhập thông tin khuyến mãi">
					            <button type="submit" class="button bg-green">Thêm</button>
							</div>
						</div>
						<div class="item">
							<div id="load_product" class="info-detail-product">
								<div class="info-product">
									<div class="thumb">
										<img src="assets/images/no-img.png" alt="no-img">
										<div class="block-sale3">
			                                <img alt="hình" src="https://japana.vn/uploads/promotion/2018/12/20/1545299907-1530092015-vector-smart-object-min.png">
			                            </div>
									</div>
								</div>
								<div class="detail-product">
									<h3 class="title">Tên sản phẩm</h3>
									<p class="info-pro"><span>Mã sản phẩm: </span>Mã của sản phẩm</p>
									<p class="info-pro"><span>Thương hiệu: </span>Tên thương hiệu</p>
									<p class="info-pro"><span>Xuất sứ: </span>Tên xuất sứ</p>
									<p class="info-pro"><span>Quy cách: </span>Tên quy cách</p>
									<div class="all-price">
					                    <span>Giá: </span>
					                    <p class="old-price">Giá cũ</p>
					                    <p class="new-price">Giá mới</p>
					                </div>
					                <div class="promo-intro">
					                    <img src="assets/images/icon-promo.png" alt="icon">
					                    <p class="title-promo">
					                        Hiển thị thông tin khuyến mãi
					                    </p>
					                </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="listsp-km" class="tab-content item">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">
						Danh sách sản phẩm 
						<span onchange="reload();">
							<i class="fa fa-undo" aria-hidden="true"></i>
						</span>
					</a>
					<form name="quick_search" id="frm" action="" method="post" class="search1">
	                   	<input name="search" value="" type="text" class="form-control custom-ipt" placeholder="Tìm SKU..." onkeyup="initShowBtn();">
	                   <button type="submit" class="button bg-black">Tìm kiếm</button>
	                </form>
					<div class="box-table">
						<table class="table table-custom table-responsive">
						    <thead>
						        <tr>
						        	<th class="black-custom bold center-custom">
			                    		<label class="checkbox-custom">
					                     	<input value="0" id="checkAll" type="checkbox" class="selectall">
					                      	<span class="checkmark"></span>
					                    </label>
					                 </th>
						            <th class="black-custom bold center-custom">STT</th>
						            <th class="black-custom bold">SKU</th>
						            <th class="black-custom bold center-custom">Hình ảnh</th>
						            <th class="black-custom bold">Tên sản phẩm</th>
						            <th class="black-custom bold center-custom">Giảm giá</th>
						            <th class="black-custom bold center-custom">Quà tặng</th>
						            <th class="black-custom bold center-custom">Publish</th>
						            <th class="black-custom bold">Tác vụ</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						        	<td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(1);" name="checkboxMulti[1]" id="checkBox-1" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">1</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Giảm giá" class="center-custom">32%</td>
						            <td data-title="Quà tặng" class="center-custom">020EE47</td>
						            <td data-title="Publish" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						        	<td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(2);" name="checkboxMulti[2]" id="checkBox-2" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">2</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Giảm giá" class="center-custom">32%</td>
						            <td data-title="Quà tặng" class="center-custom">020EE47</td>
						            <td data-title="Publish" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						        	<td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(3);" name="checkboxMulti[3]" id="checkBox-3" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">3</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Giảm giá" class="center-custom">32%</td>
						            <td data-title="Quà tặng" class="center-custom">020EE47</td>
						            <td data-title="Publish" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						        	<td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(4);" name="checkboxMulti[4]" id="checkBox-4" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">4</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Giảm giá" class="center-custom">32%</td>
						            <td data-title="Quà tặng" class="center-custom">020EE47</td>
						            <td data-title="Publish" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						        	<td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(5);" name="checkboxMulti[5]" id="checkBox-5" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">5</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Giảm giá" class="center-custom">32%</td>
						            <td data-title="Quà tặng" class="center-custom">020EE47</td>
						            <td data-title="Publish" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						        	<td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(6);" name="checkboxMulti[6]" id="checkBox-6" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">6</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Giảm giá" class="center-custom">32%</td>
						            <td data-title="Quà tặng" class="center-custom">020EE47</td>
						            <td data-title="Publish" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						        	<td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(7);" name="checkboxMulti[7]" id="checkBox-7" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">7</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Giảm giá" class="center-custom">32%</td>
						            <td data-title="Quà tặng" class="center-custom">020EE47</td>
						            <td data-title="Publish" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						        	<td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(8);" name="checkboxMulti[8]" id="checkBox-8" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">8</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Giảm giá" class="center-custom">32%</td>
						            <td data-title="Quà tặng" class="center-custom">020EE47</td>
						            <td data-title="Publish" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						        	<td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(9);" name="checkboxMulti[9]" id="checkBox-9" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">9</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Giảm giá" class="center-custom">32%</td>
						            <td data-title="Quà tặng" class="center-custom">020EE47</td>
						            <td data-title="Publish" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						        	<td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(10);" name="checkboxMulti[10]" id="checkBox-10" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">10</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Giảm giá" class="center-custom">32%</td>
						            <td data-title="Quà tặng" class="center-custom">020EE47</td>
						            <td data-title="Publish" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
						            		<i class="fa fa-pencil-square-o"></i>
						            	</a>
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
					<button type="button" class="button button-header black-custom removelist"><i class="fa fa-minus-circle" aria-hidden="true"></i> Xoá khỏi danh sách</button>
					<?php include('include/pagination.php')?>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		var formatDate = {
		      	format: 'DD/MM/YYYY',
		      	daysOfWeek: [
		            "CN",
		            "T2",
		            "T3",
		            "T4",
		            "T5",
		            "T6",
		            "T7"
		        ],
		        monthNames: [
		            "Tháng 1",
		            "Tháng 2",
		            "Tháng 3",
		            "Tháng 4",
		            "Tháng 5",
		            "Tháng 6",
		            "Tháng 7",
		            "Tháng 8",
		            "Tháng 9",
		            "Tháng 10",
		            "Tháng 11",
		            "Tháng 12"
		        ],
		    }
		jQuery('input[name="date-event"]').daterangepicker({
			opens: 'left',
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#date_start').val(start.format('DD-MM-YYYY'));
			jQuery('#end_date').val(end.format('DD-MM-YYYY'));
		});
		jQuery('.tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.tab-content').not('#' + data).removeClass('show-inline');

	    	jQuery(this).parent().addClass('active');
	    	jQuery('.tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    });
	})
    
    jQuery("#checkAll").change(function () {
        var status = 0;
        if(jQuery(this).prop("checked") == true){
            status = 1;
        }
        jQuery("input:checkbox.checkboxMulti").prop('checked', status);
        jQuery("input:checkbox.checkboxMulti").prop('value', status);
    });
    
    function checkBox(id){
    	var status = 0;
        if(jQuery('#checkBox-'+id).val() == 0){
            status = 1;
        }
        $('#checkBox-'+id).val(status);
    }

	function updateTimeLine(){
		if(jQuery("#show_timeline").val() == 1){
			jQuery("#show_timeline").val(0);
        }else{
        	jQuery("#show_timeline").val(1);
        }
	}

	function click_khuyenmai(){
		var check = jQuery("#check_khuyen_mai").val();
		if(check == true){
			jQuery("#check_khuyen_mai").prop("checked", false);
			jQuery("#check_khuyen_mai").val(0);
			jQuery("#check_pt").prop("disabled", true);
			jQuery("#check_pt").val(0);
			jQuery("#check_pt").prop("checked", false);
			jQuery("#check_pt").next().addClass('bg-gray');
			jQuery("#text_pt").prop("disabled", true);
			jQuery("#text_pt").val("");
			jQuery("#check_vnd").prop("disabled", true);
			jQuery("#check_vnd").val(0);
			jQuery("#check_vnd").prop("checked", false);
			jQuery("#check_vnd").next().addClass('bg-gray');
			jQuery("#text_vnd").prop("disabled", true);
			jQuery("#text_vnd").val("");
		}else{
			jQuery("#check_khuyen_mai").prop( "checked", true);
			jQuery("#check_khuyen_mai").val(1);
			jQuery("#check_pt").prop("disabled", false);
			jQuery("#check_pt").next().removeClass('bg-gray');
			jQuery("#check_vnd").prop("disabled", false);
			jQuery("#check_vnd").next().removeClass('bg-gray');
		}
	}

	function click_gt_pt(id_text,id_check){
		var check = jQuery("#"+id_check).val();
		if(check == true){
			jQuery("#"+id_text).prop("disabled", true);
			jQuery("#"+id_check).val(0);
		}else{
			jQuery("#"+id_text).prop("disabled", false);
			jQuery("#"+id_check).val(1);
		}
	}
	
	function click_qt(){
		var check = jQuery("#check_qt").val();
		if(check == true){
			jQuery("#text_qt").prop("disabled", true);
			jQuery("#check_qt").val(0);
			jQuery("#text_qt").val("");
		}else{
			jQuery("#text_qt").prop("disabled", false);
			jQuery("#check_qt").val(1);
		}
	}
	
	function changesImages(){
    	document.getElementById("form_images").submit();
    }
</script>