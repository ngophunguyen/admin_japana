<style>
	.group-category{
		display: inline-block;
		float: left;
		width: 24%;
		border: 1px solid #eee;
		margin-top: 15px;
		overflow-y: scroll;
		overflow-x: hidden;
		position: relative;
	}
	.group-category .item{
		position: relative;
		height: 40px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-orient: horizontal;
		-webkit-box-direction: reverse;
		    -ms-flex-direction: row-reverse;
		        flex-direction: row-reverse;
		padding-right: 15px;
		border-bottom: 1px solid #eee;
	}
	.group-category .item a{
		position: absolute;
		top: 50%;
		left: 15px;
		-webkit-transform: translate(0,-50%);
		    -ms-transform: translate(0,-50%);
		        transform: translate(0,-50%);
	}
	.group-category .item a .checkmark{
		top: -3px;
	}
	.group-category .item a.active{
		color: #ff0000;
	}
	.group-category .item .show-category{
		display: inline-block;
	    width: 100%;
	    text-align: right;
	    cursor: pointer;
	}
	.group-custom-cate, .group-box-seo{
		display: inline-block;
		float: left;
		width: 36%;
		margin-top: 15px;
		margin-left: 2%;
		margin-bottom: 15px;
	}
	.box-cate-custom .item, .box-seo .item{
		display: inline-block;
		width: 48%;
		margin: 10px 0;
		float: left;
	}
	.box-cate-custom .item:nth-child(odd) {
	    margin-left: 4%;
	}
	.box-cate-custom .item:first-child{
		margin-left: 0;
	}
	.box-cate-custom .item:last-child, .box-seo .item .seo-input:last-child{
		margin-bottom: 0;
	}

	.box-cate-custom .item:first-child{
		width: 100%;
	}

	.input-group label.custom-upload{
		width: 100px!important;
	}
	.box-cate-custom .item label, .box-seo .item label, .box-ckeditor .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.box-ckeditor .item:last-child{
		margin-top: 15px;
	}
	.custom-dropdown:after{
		padding-right: 15px;
	}
	.box-img-meta{
		max-height: 180px;
		height: 180px;
		display: none;
		margin-top: 15px;
	}
	.box-img-meta img{
		height: auto;
		width: 100%;
	}
	.dropdown-collapse{
		float: left;
	}
	.box-seo{
		display: inline-block;
		width: 100%;
	}
	.box-seo .item{
		width: 100%;
	}
	.box-seo .item:last-child{
		margin-bottom: 0;
	}
	.box-seo .item .seo-input{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: baseline;
		    -ms-flex-align: baseline;
		        align-items: baseline;
		-webkit-box-pack: end;
		    -ms-flex-pack: end;
		        justify-content: flex-end;
		margin-bottom: 15px;
	}
	.box-seo .item .seo-input input, .box-seo .item .seo-input textarea, .box-seo .item .seo-input .upload-img, .box-img-meta{
		margin-left: 15px;
		width: 80%;
		float: right;
	}
	.box-seo .item .seo-input .upload-img input{
		margin-left: 0;
		width: auto
	}
	.box-img-meta{
		margin-top: 0;
	}
	.box-ckeditor{
		display: none;
		padding-top: 0;
	}
	.content-product{
		margin-bottom: 15px;
	}
	.box-button-info-order{
  		display: none;
  		text-align: center;
  	}
  	.box-button-info-order a:hover{
  		color: #fff;
  	}
  	.show-info-order{
		color: #fff;
		z-index: 999;
	}
	.box-img-block{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-img-block .item {
		width: 10%;
	    text-align: center;
	    display: inline-table;
	    position: relative;
	}
	.box-img-block .img-upbox {
	    margin-bottom: 0;
	    margin: 0 10px;
	    position: relative;
	}
	.box-img-block .upload-btn-block {
	    display: inline-block;
	    float: left;
	    border: 1px solid #ccc;
	    border-radius: 15px;
	}
	.box-img-block .upload-btn-block img {
	    width: 100%;
	    height: auto;
	    border-radius: 15px;
	}
	.img-upbox .btn-del-img {
	    position: absolute;
	    top: -10px;
	    left: -10px;
	    width: 25px;
	    height: 25px;
	    border-radius: 100%;
	    border: 1px solid #ccc;
	    font-size: 10px;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	    background: #fff;
	}
	.box-img-block span {
	    color: #999999;
	    cursor: pointer;
	    display: inline-block;
    	width: 100%;
    	margin-top: 5px;
	}
	.box-img-block .item:first-child .btn-chg-left, .box-img-block .item:last-child .btn-chg-right {
	    display: none;
	}
	.btn-chg-left, .btn-chg-right {
	    background: transparent;
	    border: 0;
	    cursor: pointer;
	    font-size: 30px;
	    color: #999;
	    padding: 0;
	}
	.btn-chg-left {
	    float: left;
	}
	.btn-chg-right {
	    float: right;
	}
	.upload-multi-img span{
		position: relative;
		cursor: pointer;
	}
	.upload-multi-img span input[type=file] {
	    position: absolute;
	    left: 0;
	    top: 0;
	    right: 0;
	    bottom: 0;
	    opacity: 0;
	    width: 100%;
	    cursor: pointer;
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:last-child{
		width: 55%;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	}
	.box-quick-search .item:first-child input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
 	.box-quick-search .item:last-child button{
 		width: auto;
 		padding: 0 15px;
 	}
	.box-quick-search .item:last-child .box-time{
		width: 40%;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
		float: right;
	}
	.search2, .search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.search1{
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
	}
	#show3 .search1 input:nth-child(2){
		margin: 0 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.box-table{
		width: 100%;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	    float: left;
	}
	.box-table input[type="number"]{
		width: 100px;
		margin: 0 auto;
	}
	.table-custom{
		margin-top: 0;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		height: auto;
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	img.gift-order{
		height: 20px!important; 
		bottom: 5px; 
		left: 0;  
		position: absolute;
	}
	.box-table tr td:nth-child(3){
		width: 250px;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.first-price{
		text-decoration: line-through;
	    font-size: 12px;
	    color: #333;
	    width: 100%;
	    display: inline-block;
	    margin-bottom: 10px;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.checkbox-custom{
	    cursor: pointer;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    height: 20px;
	}
	.checkbox-custom label{
	    color: #ccc;
	}
	.box-table tr td:nth-child(8), .box-table tr th:nth-child(8){
		width: 100px;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
		padding: 10px 0;
	}
	#listqt-dm .dropdown-collapse,#listmc-dm .dropdown-collapse,#listcb-dm .dropdown-collapse{
		margin-bottom: 15px;
	}
	@media (max-width: 575.98px) {
		.show-info-order{
			display: block;
		}
	  	.group-category{
	  		display: none;
	  	}
	  	.group-category{
	  		position: fixed;
	  		height: auto!important;
		    width: 100%;
		    display: none;
		    top: 92px;
		    left: 0;
		    right: 0;
		    border-bottom: 2px solid #eee;
		    z-index: 99;
		    background: #fff;
		    margin-top: 0;
	  	}
	  	.dropdown-collapse{
	  		display: none;
	  	}
	  	.box-button-info-order{
	  		display: block;
	  	}
	  	.upload-multi-img{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		margin-top: 15px;
	  	}
		#info-dm, #seo-dm, #note-dm, #listimg-dm, #listmc-dm, #listqt-dm, #listcb-dm{
			display: none;
			width: 100%;
		}
		.group-custom-cate, .group-box-seo{
			width: 100%;
			margin: 0;
		}
		.box-cate-custom .item, 
		.box-seo .item .seo-input .upload-img, 
		.box-img-meta, .box-seo .item .seo-input,
		.box-img-block{
			display: inline-block;
			width: 100%;
		}
		.box-seo .item .seo-input .upload-img{
			margin: 0;
			margin-top: 10px;
		}
		.box-seo .item .seo-input .upload-img input{
			margin-top: 0;
		}
		.box-cate-custom .item:nth-child(odd), .box-img-meta{
			margin-left: 0;
		}
		.box-cate-custom .item:last-child{
			margin-bottom: 15px;
		}
		.box-seo .item .seo-input input, .box-seo .item .seo-input textarea{
			margin-left: 0;
			width: 100%;
			margin-top: 10px;
		}
		.box-cate-custom .item, .box-seo .item{
			margin: 0;
			margin-top: 10px;
		}
		.box-ckeditor{
			display: block;
			margin: 15px 0;
			padding: 0;
		}
		.box-img-block{
			white-space: nowrap;
    		overflow-x: scroll;
    		overflow-y: hidden;
		}
		.box-img-block .item{
			width: 45%;
			margin: 0;
			margin-right: 5%;
		}
		.box-img-block .item:last-child{
			margin-right: 0;
		}
		.box-img-block .img-upbox{
			white-space: initial;
		}
		.img-upbox .btn-del-img{
			top: 0;
			left: auto;
    		right: -10px;
		}
		.box-quick-search .item, .box-quick-search .item:last-child, .box-quick-search .item:last-child .box-time, .box-table tr td:nth-child(3){
			width: 100%;
		}
		.box-quick-search .item{
			margin-top: 10px;
		}
		.search2{
			display: inline-block;
			width: 100%;
		}
		.box-quick-search .item:last-child button{
			margin-left: 0;
			margin-top: 10px;
		}
		.table-responsive > tbody > tr td:first-child {
		    display: none;
		}
		img.gift-order{
			left: auto;
		}
		.first-price{
			margin-bottom: 0;
			width: auto;
		}
		.table-custom > tbody > tr > td .box-time, .table-custom > tbody > tr > td .custom-dropdown{
			width: 100%;
			padding: 5px 0;
		}
		.box-table tr td:nth-child(8), .box-table tr th:nth-child(8){
			width: auto;
		}
		.box-table tr td:last-child{
			padding: 0;
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.box-table input[type="number"]{
			margin: initial;
		}
		#listcb-dm .search1{
			display: inline-block;
		}
		.box-quick-search .item:first-child input{
			width: 100%;
		}
		#show3 .search1 input:nth-child(1),#show3 .search1 input:nth-child(2){
			width: 100%;
			margin: 0 auto 10px;
		}
		.box-quick-search .item:last-child button{
			margin-bottom: 10px;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.group-category{
			width: 40%;
			margin-top: 15px;
		}
		.group-custom-cate{
			width: 58%;
			margin-top: 15px;
		}
		.group-box-seo{
			width: 100%;
			margin-left: 0;
		}
		.box-quick-search .item, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child{
	  	 	margin-top: 15px;
	  	}
	  	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input, .box-quick-search .item:last-child .box-time{
	  		width: 80%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.group-category{
			width: 40%;
			margin-top: 15px;
		}
		.group-custom-cate{
			width: 58%;
			margin-top: 15px;
		}
		.group-box-seo{
			width: 100%;
			margin-left: 0;
		}
		.box-quick-search .item, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child{
	  	 	margin-top: 15px;
	  	}
	  	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input, .box-quick-search .item:last-child .box-time{
	  		width: 80%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.box-seo .item .seo-input input, .box-seo .item .seo-input textarea{
			width: 70%;
		}
		.table-custom{
	  		white-space: nowrap;
	  	}
	  	.input-group label.custom-upload{
	  		border-radius: 0;
	  		margin-top: 10px;
	  	}
	}
	@media (min-width: 1200px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
</style>
<main class="editproduct content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chỉnh sửa sản phẩm</h1>
			<ul>
				<li>
					<button type="button" class="button button-header link-custom black-custom">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
                    </button>
				</li>
				<li>
					<a href="?action=products.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<li class="box-button-info-order">
					<a class="show-info-order link-custom black-custom" href="javascript:void(0);">
						<i class="fa fa-list" aria-hidden="true"></i> <label>Danh mục</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="tab-custom bg-black">
				<div class="item active">
					<a href="javascript:void(0)" data-id="info-dm" title="Chi tiết danh mục">
						<i class="fa fa-sticky-note" aria-hidden="true"></i>
						<label>Chi tiết</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="seo-dm" title="SEO">
						<i class="fa fa-bullhorn" aria-hidden="true"></i>
						<label>SEO</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="note-dm" title="Nội dung">
						<i class="fa fa-book" aria-hidden="true"></i>
						<label>Nội dung</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="listimg-dm" title="Hình ảnh">
						<i class="fa fa-picture-o" aria-hidden="true"></i>
						<label>Hình ảnh</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="listmc-dm" title="Sản phẩm mua cùng">
						<i class="fa fa-cubes" aria-hidden="true"></i>
						<label>Mua cùng</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="listqt-dm" title="Có thể bạn quan tâm">
						<i class="fa fa-star" aria-hidden="true"></i>
						<label>Quan tâm</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="listcb-dm" title="Combo">
						<i class="fa fa-plus-circle" aria-hidden="true"></i>
						<label>Combo</label>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<div class="group-category">
					<div class="item show-child-1 show-all-1">
						<input id="show-2" class="show-all-1" type="hidden" value="0">
						<a href="#" title="Collagen">
							<label class="checkbox-custom">Collagen
			                  	<input value="" id="all-2" type="checkbox" onclick="initCheckAll('2');" class="all-all-1" checked="checked">
			                  	<span class="checkmark"></span>
			                </label>
						</a>
						<span class="show-category" onclick="showChild(2)"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</div>
					<div style="display: none;" class="item show-child-2 show-all-1 show-all-2">
						<input id="show-3" class=" show-all-1 show-all-2" type="hidden" value="0">
						<a href="#" title="Nước uống Collagen" style="padding-left: 20px">
							<label class="checkbox-custom">Nước uống Collagen
			                  	<input value="" id="all-3" type="checkbox" onclick="initCheckAll('3');" class="all-all-2" checked="checked">
			                  	<span class="checkmark"></span>
			                </label>
						</a>
					</div>
					<div style="display: none;" class="item show-child-2 show-all-1 show-all-2">
						<input id="show-4" class=" show-all-1 show-all-2" type="hidden" value="0">
						<a href="#" title="Collagen dạng viên uống" style="padding-left: 20px">
							<label class="checkbox-custom">Collagen dạng viên uống
			                  	<input value="" id="all-4" type="checkbox" onclick="initCheckAll('4');" class="all-all-2">
			                  	<span class="checkmark"></span>
			                </label>
						</a>
					</div>
					<div style="display: none;" class="item show-child-2 show-all-1 show-all-2">
						<input id="show-5" class=" show-all-1 show-all-2" type="hidden" value="0">
						<a href="#" title="Collagen dạng bột - thạch ăn" style="padding-left: 20px">
							<label class="checkbox-custom">Collagen dạng bột - thạch ăn
			                  	<input value="" id="all-5" type="checkbox" onclick="initCheckAll('5');" class="all-all-2">
			                  	<span class="checkmark"></span>
			                </label>
						</a>
					</div>
					<div class="item show-child-1 show-all-1">
						<input id="show-6" class="show-all-1" type="hidden" value="0">
						<a href="#" title="Thực phẩm làm đẹp">
							<label class="checkbox-custom">Thực phẩm làm đẹp
			                  	<input value="" id="all-6" type="checkbox" onclick="initCheckAll('6');" class="all-all-1">
			                  	<span class="checkmark"></span>
			                </label>
						</a>
						<span class="show-category" onclick="showChild(6)"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</div>
					<div style="display: none;" class="item show-child-6 show-all-1 show-all-6">
						<input id="show-7" class=" show-all-1 show-all-6" type="hidden" value="0">
						<a href="#" title="Nước uống trắng da" style="padding-left: 20px">
							<label class="checkbox-custom">Nước uống trắng da
			                  	<input value="" id="all-7" type="checkbox" class="all-all-6">
			                  	<span class="checkmark"></span>
			                </label>
						</a>
					</div>
					<div style="display: none;" class="item show-child-6 show-all-1 show-all-6">
						<input id="show-8" class=" show-all-1 show-all-6" type="hidden" value="0">
						<a href="#" title="Viên Uống Trắng Da" style="padding-left: 20px">
							<label class="checkbox-custom">Viên Uống Trắng Da
			                  	<input value="" id="all-8" type="checkbox" onclick="initCheckAll('8');" class="all-all-6">
			                  	<span class="checkmark"></span>
			                </label>
						</a>
					</div>
					<div style="display: none;" class="item show-child-6 show-all-1 show-all-6">
						<input id="show-9" class=" show-all-1 show-all-6" type="hidden" value="0">
						<a href="#" title="Nhau Thai - Placenta" style="padding-left: 20px">
							<label class="checkbox-custom">Nhau Thai - Placenta
			                  	<input value="" id="all-9" type="checkbox" onclick="initCheckAll('9');" class="all-all-6">
			                  	<span class="checkmark"></span>
			                </label>
						</a>
					</div>
				</div>
				<div id="info-dm" class="group-custom-cate tab-content item show-inline">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">Chi tiết sản phẩm</a>
					<div class="box-cate-custom">
						<div class="item">
					    	<label for="name_vi">Tên sản phẩm:</label>
					    	<input autocomplete="off" type="text" name="name_vi" id="name_vi" value="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml" placeholder="Nhập tên sản phẩm..." class="form-control" required="">
					    </div>
						<div class="item">
					    	<label for="sku">Mã SKU:</label>
					    	<input autocomplete="off" type="text" name="sku" id="sku" value="087AA9" placeholder="Nhập mã..." class="form-control" required="">
					    </div>
					    <div class="item">
					    	<label for="price">Giá sản phẩm:</label>
					    	<input autocomplete="off" type="text" value="2.340.000" id="price" name="price" data-type='currency' class="form-control" required="">
					    </div>
					    <div class="item">
					    	<label for="expired_time">Hạn sử dụng (ngày):</label>
					    	<input autocomplete="off" type="text" value="1" id="expired_time" name="expired_time" data-type='currency' class="form-control">
					    </div>
					    <div class="item">
					    	<label for="id_country">Xuất xứ:</label>
					    	<select class="form-control" name="id_country" id="id_country">
					    		<option value="-1">Chọn...</option>
							  	<option value="0" selected="selected">Nhật Bản</option>
							</select>
					    </div>
					    <div class="item">
					    	<label for="id_brand">Thương hiệu:</label>
					    	<select class="form-control" name="id_brand" id="id_brand">
					    		<option value="-1">Chọn...</option>
							  	<option value="0" selected="selected">Daiichi Sankyo</option>
							  	<option value="1">Panasonic</option>
							  	<option value="2">Orihiro</option>
							  	<option value="3">Morinaga</option>
							</select>
					    </div>
					    <div class="item">
					    	<label for="id_madein">Sản xuất tại:</label>
					    	<select class="form-control" name="id_madein" id="id_madein">
					    		<option value="-1">Chọn...</option>
							  	<option value="0" selected="selected">Việt Nam</option>
							  	<option value="1">Mỹ</option>
							  	<option value="2">Thái Lan</option>
							  	<option value="3">Nhật Bản</option>
							</select>
					    </div>
					    <div class="item">
					    	<label for="kg">Khối lượng (gram)</label>
					    	<input autocomplete="off" type="text" value="1000" id="kg" name="kg" data-type='currency' class="form-control">
					    </div>
					    <div class="item">
					    	<label for="id_style">Quy cách</label>
					    	<select class="form-control" name="id_style" id="id_style">
					    		<option value="-1">Chọn...</option>
							  	<option value="0" selected="selected">Hộp</option>
							  	<option value="1">Tuýp</option>
							  	<option value="2">Chai</option>
							  	<option value="3">Gói</option>
							</select>
					    </div>
					    <div class="item">
					    	<label for="style">Tình trạng kho:</label>
					    	<div class="custom-dropdown">
					    		<select class="form-control" name="style" id="style">
						    		<option value="-1">Chọn...</option>
								  	<option value="0" selected="selected">Còn hàng</option>
								  	<option value="1">Hết hàng</option>
								</select>
					    	</div>
					    </div>
					</div>
				</div>
				<div id="seo-dm" class="group-box-seo tab-content item">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">SEO</a>
					<div class="box-seo">
						<div class="item">
							<label for="slug_vi">Link URL:</label>
							<input autocomplete="off" type="text" name="slug_vi" id="slug_vi" value="tinh-chat-nhau-thai-placenta-82x-450000mg-nhat-ban-500ml" placeholder="Nhập link url vd:danh-muc" value="" class="form-control">
						</div>
						<div class="item">
							<label for="slug_vi">Tags:</label>
							<input name="vtags" id="vTags" value="collagen,nước uống collagen,collagen dạng bột" type="hidden">
	                        <input id="Tags" value="collagen,nước uống collagen,collagen dạng bột,tủ lạnh,máy sấy,collagen1,nước uống collagen 1,collagen dạng bột 1,tủ lạnh 1,máy sấy 1" type="hidden">
	                        <ul id="ShowTag"></ul>
						</div>
						<div class="item">
							<label for="meta_web_title">Google:</label>
							<span class="seo-input">
								Title:
								<input autocomplete="off" type="text" name="meta_web_title" id="meta_web_title" placeholder="Nhập title" value="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml" class="form-control">
							</span>
							<span class="seo-input">
								Keyword:
								<input autocomplete="off" type="text" name="meta_web_keyword" id="meta_web_keyword" placeholder="Nhập keyword" value="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml" class="form-control">
							</span>
							<span class="seo-input">
								Description:
								<textarea name="meta_web_desc" id="meta_web_desc" class="form-control" rows="3" placeholder="Nhập description">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</textarea>
							</span>
						</div>
						<div class="item">
							<label for="meta_web_title">Social:</label>
							<span class="seo-input">
								Title:
								<input autocomplete="off" type="text" name="og_title" id="og_title" placeholder="Meta O.g title..." value="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml" class="form-control">
							</span>
							<span class="seo-input">
								Description:
								<textarea name="og_desc" id="og_desc" class="form-control" rows="3" placeholder="Meta O.g descriptions...">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</textarea>
							</span>
							<span class="seo-input">
								Image (16x9):
								<div class="upload-img">
		                            <div class="input-group up-img">
		                                <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
		                                <label class="button bg-green custom-upload">
		                                    <input type="file" class="form-control" name="icon" id="icon" onchange="readURL(this,1);" accept="image/*">Upload
		                                </label>
		                                <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
		                            </div>
		                        </div>
							</span>
							<span class="seo-input">
								<div class="box-img-meta" style="display: block;">
		                        	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="collagen" id="photo1">
		                        </div>
							</span>
						</div>
					</div>
				</div>
				<div id="note-dm" class="tab-content item">
					<a class="content-product dropdown-collapse bg-black" href="javascript:void(0);">Nội dung <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div class="box-ckeditor">
						<div class="item">
							<label for="elm1">Mô tả ngắn:</label>
							<textarea name="notes_vi" id="notes_vi" rows="5" class="form-control"></textarea>
						</div>
						<div class="item">
							<label for="desc_vi">Chi tiết sản phẩm:</label>
							<textarea class="ckeditor" id="desc_vi" name="desc_vi"></textarea>
						</div>
					</div>
				</div>
				<div id="listimg-dm" class="tab-content item">
					<a class="upload-multi-img dropdown-collapse bg-black" href="javascript:void(0);">Hình ảnh 
						<span>
							Thêm ảnh 
							<i class="fa fa-plus-circle" aria-hidden="true"></i>
							<input type="file" name="myfile[]" id="my-input" class="file-upl" onchange="readURLimg(this);" multiple="" accept="image/*">
						</span>
					</a>
					<div class="box-img-block">
			            <div class="item img-upbox itemimg-1">
			              	<input id="sortimg_1" type="hidden" name="nameimg[1]" value="41413719_1348257088641606_8929189922657533952_n.jpg">
			              	<div class="upload-btn-block">
			                	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="img" id="[object Object]" class="img-page">
			              	</div>
			              	<a href="javascript:void(0)" id="del-1" onclick="initClickRemoveSrcImgPro(1)" class="btn-del-img" title="xoá">
			                	<i class="ti ti-close"></i>
			              	</a>
			              	<span>
			                	<button type="button" onclick="addPrevItem(1)" id="add-prev-1" class="btn-chg-left">
			                  		<i class="fa fa-angle-left" aria-hidden="true"></i>
			               	 	</button>
			                	<button type="button" onclick="addNextItem(1)" id="add-next-1" class="btn-chg-right">
			                  		<i class="fa fa-angle-right" aria-hidden="true"></i>
			                	</button>
			              	</span>
			            </div>
			            <div class="item img-upbox itemimg-2">
			              	<input id="sortimg_2" type="hidden" name="nameimg[2]" value="41450589_1348257121974936_4921738964496285696_n.jpg">
			              	<div class="upload-btn-block">
			                	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="img" id="[object Object]" class="img-page">
			              	</div>
			              	<a href="javascript:void(0)" id="del-3" onclick="initClickRemoveSrcImgPro(2)" class="btn-del-img" title="xoá">
			                	<i class="ti ti-close"></i>
			              	</a>
			              	<span>
			                	<button type="button" onclick="addPrevItem(2)" id="add-prev-3" class="btn-chg-left">
			                  		<i class="fa fa-angle-left" aria-hidden="true"></i>
			               	 	</button>
			                	<button type="button" onclick="addNextItem(2)" id="add-next-3" class="btn-chg-right">
			                  		<i class="fa fa-angle-right" aria-hidden="true"></i>
			                	</button>
			              	</span>
			            </div>
			            <div class="item img-upbox itemimg-3">
			              	<input id="sortimg_3" type="hidden" name="nameimg[3]" value="41450589_1348257121974936_4921738964496285696_n.jpg">
			              	<div class="upload-btn-block">
			                	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="img" id="[object Object]" class="img-page">
			              	</div>
			              	<a href="javascript:void(0)" id="del-3" onclick="initClickRemoveSrcImgPro(3)" class="btn-del-img" title="xoá">
			                	<i class="ti ti-close"></i>
			              	</a>
			              	<span>
			                	<button type="button" onclick="addPrevItem(3)" id="add-prev-3" class="btn-chg-left">
			                  		<i class="fa fa-angle-left" aria-hidden="true"></i>
			               	 	</button>
			                	<button type="button" onclick="addNextItem(3)" id="add-next-3" class="btn-chg-right">
			                  		<i class="fa fa-angle-right" aria-hidden="true"></i>
			                	</button>
			              	</span>
			            </div>
			        </div>
				</div>
				<div id="listmc-dm" class="tab-content item">
					<a class="dropdown-collapse bg-black" onclick="showPro(1)" href="javascript:void(0);">Sản phẩm mua cùng <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div id="show1" class="tab-products">
						<div class="box-quick-search">
							<div class="item">
								<form name="quick_search" id="frm" action="" method="post" class="search1">
				                   <input name="insert-categpry-SKU" value="" type="text" class="form-control custom-ipt" placeholder="Tìm SKU, tên sản phẩm...">
				                   <button type="submit" class="button bg-green">Thêm</button>
				                </form>
							</div>
							<div class="item">
								<form name="status_search" id="frm" action="" method="post" class="search2">
									<div class="box-time">
										<input type="hidden" name="start-date" id="start-date" value="">
										<input type="hidden" name="end-date" id="end-date" value="">
			                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-order" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày...">
			                            <i class="fa fa-calendar icon-time"></i>
			                        </div>
			                        <button type="submit" class="button bg-green">Cập nhật</button>
								</form>
							</div>
						</div>
						<div class="box-table">
							<table class="table table-custom table-responsive">
							    <thead>
							        <tr>
							            <th class="black-custom bold center-custom">STT</th>
							            <th class="black-custom bold">SKU</th>
							            <th class="black-custom bold center-custom">Hình ảnh</th>
							            <th class="black-custom bold">Tên sản phẩm</th>
							            <th class="black-custom bold center-custom">Còn/hết</th>
							            <th class="black-custom bold center-custom">Publish</th>
							            <th class="black-custom bold center-custom">Vị trí</th>
							            <th class="black-custom bold center-custom">Tác vụ</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							            <td data-title="STT" class="center-custom">1</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="1" name="sort_1" id="sort_1">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(1);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">2</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta Nhật Bản 500ml</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="2" name="sort_2" id="sort_2">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(1);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">3</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất Placenta 82x 450000mg Nhật Bản 500ml</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="3" name="sort_3" id="sort_3">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(3);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">4</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta Nhật 500ml</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="4" name="sort_4" id="sort_4">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(4);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">5</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="5" name="sort_5" id="sort_5">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">6</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="6" name="sort_6" id="sort_6">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">7</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="7" name="sort_7" id="sort_7">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">8</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="8" name="sort_8" id="sort_8">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">9</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="9" name="sort_9" id="sort_9">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">10</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="10" name="sort_10" id="sort_10">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							    </tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="listqt-dm" class="tab-content item">
					<a class="dropdown-collapse bg-black" onclick="showPro(2)" href="javascript:void(0);">Có thể bạn quan tâm <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div id="show2" class="tab-products">
						<div class="box-quick-search">
							<div class="item">
								<form name="quick_search" id="frm" action="" method="post" class="search1">
				                   	<input name="insert-interest-SKU" value="" type="text" class="form-control custom-ipt" placeholder="Tìm SKU, tên sản phẩm...">
				                   	<button type="submit" class="button bg-green">Thêm</button>
				                </form>
							</div>
							<div class="item">
								<form name="status_search" id="frm" action="" method="post" class="search2">
									<div class="box-time">
										<input type="hidden" name="start_date1" id="start-date1" value="">
										<input type="hidden" name="end_date1" id="end_date1" value="">
			                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-order1" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày...">
			                            <i class="fa fa-calendar icon-time"></i>
			                        </div>
			                        <button type="submit" class="button bg-green">Cập nhật</button>
								</form>
							</div>
						</div>
						<div class="box-table">
							<table class="table table-custom table-responsive">
							    <thead>
							        <tr>
							            <th class="black-custom bold center-custom">STT</th>
							            <th class="black-custom bold">SKU</th>
							            <th class="black-custom bold center-custom">Hình ảnh</th>
							            <th class="black-custom bold">Tên sản phẩm</th>
							            <th class="black-custom bold center-custom">Còn/hết</th>
							            <th class="black-custom bold center-custom">Publish</th>
							            <th class="black-custom bold center-custom">Vị trí</th>
							            <th class="black-custom bold center-custom">Tác vụ</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							            <td data-title="STT" class="center-custom">1</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="1" name="sort_1" id="sort_1">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(1);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">2</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta Nhật Bản 500ml</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="2" name="sort_2" id="sort_2">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(1);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">3</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất Placenta 82x 450000mg Nhật Bản 500ml</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="3" name="sort_3" id="sort_3">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(3);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">4</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta Nhật 500ml</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="4" name="sort_4" id="sort_4">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(4);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">5</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="5" name="sort_5" id="sort_5">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">6</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="6" name="sort_6" id="sort_6">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">7</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="7" name="sort_7" id="sort_7">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">8</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="8" name="sort_8" id="sort_8">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">9</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="9" name="sort_9" id="sort_9">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">10</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg</td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Vị trí" class="center-custom">
								            <input type="number" class="form-control" value="10" name="sort_10" id="sort_10">
								        </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(5);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							    </tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="listcb-dm" class="tab-content item">
					<a class="dropdown-collapse bg-black" onclick="showPro(3)" href="javascript:void(0);">Sản phẩm combo <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div id="show3" class="tab-products">
						<div class="box-quick-search">
							<div class="item">
								<form name="quick_search" id="frm" action="" method="post" class="search1">
				                   	<input name="insert-interest-SKU" value="" type="text" class="form-control custom-ipt" placeholder="Tìm SKU, tên sản phẩm...">
				                   	<input value="" type="text" class="form-control custom-ipt" placeholder="Nhập số lượng">
				                   	<input value="" type="text" class="form-control custom-ipt" placeholder="Nhập giá">
				                   	<button type="submit" class="button bg-green">Thêm</button>
				                </form>
							</div>
						</div>
						<div class="box-table">
							<table class="table table-custom table-responsive">
							    <thead>
							        <tr>
							            <th class="black-custom bold center-custom">STT</th>
							            <th class="black-custom bold">SKU</th>
							            <th class="black-custom bold center-custom">Hình ảnh</th>
							            <th class="black-custom bold">Tên sản phẩm</th>
							            <th class="black-custom bold center-custom">Giá</th>
							            <th class="black-custom bold center-custom">Số lượng</th>
							            <th class="black-custom bold center-custom">Còn/hết</th>
							            <th class="black-custom bold center-custom">Publish</th>
							            <th class="black-custom bold center-custom">Tác vụ</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							            <td data-title="STT" class="center-custom">1</td>
							            <td data-title="SKU">087AA9</td>
							            <td data-title="Hình ảnh" class="center-custom">
							            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
							            </td>
							            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
							            <td data-title="Giá" class="center-custom">
								            <input type="number" class="form-control" value="2300000" name="price_combo_1" id="price_combo_1">
								        </td>
								        <td data-title="Số lượng" class="center-custom">
								            <input type="number" class="form-control" value="1" name="quantity_combo_1" id="quantity_combo_1">
								        </td>
							            <td data-title="Còn/hết" class="center-custom">Còn</td>
							            <td data-title="Publish" class="center-custom">Publish</td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:updateProductPositive(1);" class="link-custom black-custom" title="Lưu">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="#" class="link-custom red-custom" title="Xoá">
						                        <i class="ti-close" aria-hidden="true"></i>
						                    </a>
							            </td>
							        </tr>
							    </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/categoryproduct/add.php'); ?>
<script>
	function showPro(id){
    	if(jQuery('#show'+id).css('display')=='none'){
    		jQuery('#show'+id).css('display','inline-block');
    		jQuery('#show'+id).prev().find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
    		if(jQuery('.content-sidebar-wrap').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());	
			}
    	}
    	else{
    		jQuery('#show'+id).css('display','none');
    		jQuery('#show'+id).prev().find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
    		if(jQuery('.content-sidebar-wrap').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());	
			}
    	}
	}
	function initCheckAll(id){
	  	if(jQuery('#all-' + id).prop('checked')==false){
	    	jQuery('.all-all-' + id).prop('disabled',true);
	    	jQuery('.all-all-' + id).next().addClass('bg-gray');
	    	jQuery('.all-all-' + id).prop('checked',false);
	  	}else{
			jQuery('.all-all-' + id).prop('disabled',false);
			jQuery('.all-all-' + id).next().removeClass('bg-gray');
	  	}
	}
	var id = 0;
	function addNextItem(id){
	  jQuery('.itemimg-'+id).insertAfter(jQuery("#add-next-"+id).parent().parent().next('.item'));
	}

	function addPrevItem(id){
	  jQuery('.itemimg-'+id).insertBefore(jQuery("#add-prev-"+id).parent().parent().prev('.item'));
	}
	var idp = jQuery(".img-upbox").length;
	function readURLimg(input) {
	    if (input.files) {
	      var files = jQuery('#my-input')[0].files;
	      for (i = 0; i < files.length; i++) {  
	        var reader = new FileReader();
	        reader.name = files[i].name;
	        reader.onload = function(e) {
	          	block = jQuery('#img-'+id);
	          	id = idp + 1;
	          	idp = idp + 1;
	          	jQuery(".box-img-block").append(`
	            <div class='item img-upbox itemimg-`+idp+`'>
	              	<input id="sortimg_`+idp+`" type="hidden" name="nameimg[`+idp+`]" value="`+e.target.name+`"/>
	              	<div class='upload-btn-block'>
	                	<img src='`+e.target.result+`' alt='img' id='`+block+`' class='img-page'>
	              	</div>
	              	<a href='javascript:void(0)' id='del-`+idp+`' onclick='initClickRemoveSrcImgPro(`+idp+`)' class='btn-del-img' title='xoá'>
	                	<i class='ti ti-close'></i>
	              	</a>
	              	<span>
	                	<button type='button' onclick='addPrevItem(`+idp+`)' id='add-prev-`+idp+`' class='btn-chg-left'>
	                  		<i class='fa fa-angle-left' aria-hidden='true'></i>
	               	 	</button>
	                	<button type='button' onclick='addNextItem(`+idp+`)' id='add-next-`+idp+`' class='btn-chg-right'>
	                  		<i class='fa fa-angle-right' aria-hidden='true'></i>
	                	</button>
	              	</span>
	            </div>`);
	        }
	        reader.readAsDataURL(input.files[i]);
	      }
	    }
	}
	function initClickRemoveSrcImgPro(id){
	    jQuery('.itemimg-'+id).remove();
	}
	function showChild(id){
        var show = jQuery("#show-"+id).val();
        if(show == 0){
            jQuery(".show-child-"+id).show();
            jQuery("#show-"+id).next().addClass('active');
            jQuery("#show-"+id).val(1);
            jQuery('#show-'+id).next().next().html('<i class="fa fa-angle-down" aria-hidden="true"></i>');
        }else{
            jQuery(".show-all-"+id).hide();
            jQuery(".show-all-"+id).val(0);
            jQuery('#show-'+id).next().next().html('<i class="fa fa-angle-right" aria-hidden="true"></i>');
            jQuery("#show-"+id).val(0);
            jQuery("#show-"+id).next().removeClass('active');
        }
    }
    function readURL(input,id) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	        	jQuery('#photo'+id).parent().css('display','block');
	          	jQuery('#photo'+id).attr('src', e.target.result);
	          	if(jQuery('.group-box-seo').height()>jQuery('.group-custom-cate').height()){
					jQuery('.group-category').height(jQuery('.group-box-seo').height()-2);
				}
				else{
					jQuery('.group-category').height(jQuery('.group-custom-cate').height());
				}
	        };
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	
	jQuery(function(){
		CKEDITOR.instances["elm1"];
		new PerfectScrollbar('.group-category');

		var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    if(window.innerWidth > 576) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
	    jQuery('input[name="date-order"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date').val(end.format('DD-MM-YYYY'));
		});
		jQuery('input[name="date-order1"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date1').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date1').val(end.format('DD-MM-YYYY'));
		});
		
		jQuery('#cbo_username,#cbo_username2,#id_country,#id_brand,#id_madein,#id_style').select2();
		var sampleTags = jQuery("#vTags").val();
        var sampleTags2 = jQuery("#Tags").val();
        sampleTags = sampleTags.split(",");
        sampleTags2 = sampleTags2.split(",");
        jQuery('#ShowTag').tagit({
          availableTags: sampleTags2,
          singleField: true,
          allowSpaces: true,
          allowDuplicates: false,
          singleFieldNode: jQuery('#vTags'),
          beforeTagAdded: function(event, ui) {
                if ($.inArray(ui.tagLabel, sampleTags2) == -1) {
                    alert(ui.tagLabel + ' không phải là tag khả dụng.');
                    jQuery('.tagit-new input').val('');
                    return false;
                }
            }
        });
        jQuery('#ShowTag2').tagit({
          availableTags: sampleTags2,
          singleField: true,
          allowSpaces: true,
          allowDuplicates: false,
          singleFieldNode: jQuery('#vTags2'),
          beforeTagAdded: function(event, ui) {
                if ($.inArray(ui.tagLabel, sampleTags2) == -1) {
                    alert(ui.tagLabel + ' không phải là tag khả dụng.');
                    jQuery('.tagit-new input').val('');
                    return false;
                }
            }
        });
		jQuery('.content-product').click(function(){
	    	if(jQuery('.box-ckeditor').css('display')=='none'){
	    		jQuery('.box-ckeditor').css('display','inline-block');
	    		jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
	    		jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
	    	}
	    	else{
	    		jQuery('.box-ckeditor').css('display','none');
	    		jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
	    		jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
	    	}
	    });

		jQuery(document).on('change', ':file', function() {
		    var input = jQuery(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		});
		jQuery(':file').on('fileselect', function(event, numFiles, label) {
          	var input = jQuery(this).parents('.up-img').find(':text'),
              	log = numFiles > 1 ? numFiles + ' files selected' : label;
          	if( input.length ) {
              	input.val(log);
              	if(input.val(log)){
                	jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
                	jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
              	}
          	} else {
              	if(log) ;
          	}
      	});
      	jQuery('.delete-img').on('click', function(e){
		    jQuery(this).parent().find('input[type=file]').val('');
		    jQuery(this).parent().find('input[type=text]').val('');
		    jQuery(this).parent().find('.custom-upload').css('display','flex');
		    jQuery(this).parent().parent().next().css('display','none');
		    jQuery(this).css('display','none');
		    jQuery('.group-category').height(jQuery('.group-custom-cate').height());
		    if(jQuery('.group-box-seo').height()>jQuery('.group-custom-cate').height()){
				jQuery('.group-category').height(jQuery('.group-box-seo').height()-2);
			}
			else{
				jQuery('.group-category').height(jQuery('.group-custom-cate').height());
			}
		});
      	jQuery('.tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.tab-content').not('#' + data).removeClass('show-inline');

	    	jQuery(this).parent().addClass('active');
	    	jQuery('.tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    });

	    jQuery('.show-info-order').click(function(){
	    	if(jQuery('.group-category').css('display')=='none'){
	    		jQuery('.group-category').css('display','block');
	    		showBackgroundPopup();
	    	}
	    	else{
	    		jQuery('.group-category').css('display','none');
	    		deleteBackgroundPopup();
	    	}
	    });
		if(window.innerWidth > 768){
			if(jQuery('.group-box-seo').height()>jQuery('.group-custom-cate').height()){
				jQuery('.group-category').height(jQuery('.group-box-seo').height()-2);
			}
			else{
				jQuery('.group-category').height(jQuery('.group-custom-cate').height());
			}
		}
	});
</script>