<style>
  .modal-body .upload-img{
    margin-bottom: 15px;
  }
  @media (max-width: 575.98px) {
    
  }
  @media (min-width: 576px) and (max-width: 767.98px) {

  }
  @media (min-width: 768px) and (max-width: 991.98px) {

  }
  @media (min-width: 992px) and (max-width: 1199.98px) {
    
  }
  @media (min-width: 1200px) {
    
  }
</style>
<div class="modal medium-modal" id="addproduct-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Thêm sản phẩm bằng file Excel
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="item">
          <div class="upload-img">
            <div class="input-group up-img">
              <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có file nào được chọn" readonly="">
              <label class="button bg-green custom-upload">
                <input type="file" id="ipt-img" class="form-control" name="imgs" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">Tải file lên
              </label>
              <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
            </div>
          </div>
          <button type="button" class="button bg-black">Upload file</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  jQuery(function(){
    jQuery(document).on('change', ':file', function() {
        var input = jQuery(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });
    jQuery(':file').on('fileselect', function(event, numFiles, label) {
            var input = jQuery(this).parents('.up-img').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
            if( input.length ) {
                input.val(log);
                if(input.val(log)){
                  jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
                  jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
                }
            } else {
                if(log) ;
            }
        });
        jQuery('.delete-img').on('click', function(e){
        jQuery(this).parent().find('input[type=file]').val('');
        jQuery(this).parent().find('input[type=text]').val('');
        jQuery(this).parent().find('.custom-upload').css('display','flex');
        jQuery(this).css('display','none');
    });
  })
</script>