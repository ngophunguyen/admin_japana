<style>
	.avatar-header{
		margin-left: 10px;
	}
	.avatar-header a>i{
		font-size: 30px;
	}

	.avatar-header a img{
		width: 35px;
		height: 35px;
		-o-object-fit: cover;
		   object-fit: cover;
	}
	.avatar-header a{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}

	.avatar-header a img{
		-o-object-fit: cover;
		   object-fit: cover;
		border-radius: 100%;
	}

	.avatar-header a span{
		margin-left: 10px;
		font-size: 14px;
	}

	.avatar-header a:hover{
		color: #255980;
		cursor: pointer;
	}

	.avatar-header a span i{
		margin-left: 5px;
	}

	.box-account{
		position: absolute;
		top: 47px;
		right: 0;
		background: #fff;
		width: 280px;
		border: 1px solid #f1f1f1;
		border-radius: 0 0 4px 4px;
		display: none;
	}
	.box-account .user-header{
		text-align: center;
    	padding: 10px;
	}
	.box-account .user-header img{
		z-index: 5;
	    height: 90px;
	    width: 90px;
	    border: 3px solid;
	    border-color: transparent;
	    border-radius: 100%;
	}
	.box-account .user-header p{
		z-index: 5;
	    color: #fff;
	    color: rgba(255,255,255,0.8);
	    font-size: 17px;
	    margin-top: 10px;
	   	line-height: 1.5;
	}
	.box-account .user-header p small{
		display: block;
    	font-size: 12px;
	}
	.box-account ul{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: justify;
	        -ms-flex-pack: justify;
	            justify-content: space-between;
	}
	.box-account ul li a i{
		font-size: 16px;
	}
	.box-account ul li a{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: baseline;
		    -ms-flex-align: baseline;
		        align-items: baseline;
	}

	.box-account ul li{
		padding: 10px 15px;
    	margin-left: 0;
	}
	@media (max-width: 575.98px) {

	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.avatar-header{
			height: 38px;
			margin-right: 15px;
		}
		.avatar-header a{
			height: 100%;
		}
		.box-account{
			top: 103px;
		}
		.avatar-header>a>span{
			display: none;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.avatar-header{
			height: 38px;
			margin-right: 15px;
		}
		.avatar-header a{
			height: 100%;
		}
		.avatar-header>a>span{
			display: none;
		}
		.box-account{
			top: 103px;
		}

	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<li>
	<div class="avatar-header">
		<a href="javascript:void(0);" title="avatar">
			<img src="assets/images/user.jpg" alt="Nguyễn Thuỳ Vân">
			<!-- <i class="fa fa-user-circle-o" aria-hidden="true"></i> -->
			<span>Nguyễn Thuỳ Vân <i class="fa fa-caret-down" aria-hidden="true"></i></span>
		</a>
		<div class="box-account">
			<div class="user-header bg-black">
                <img src="assets/images/user.jpg" alt="Nguyễn Thuỳ Vân">
                <p>
                  Nguyễn Thuỳ Vân
                  <small>Admin</small>
                </p>
            </div>
			<ul>
				<li>
					<a href="?action=profile.php" title="Tài khoản">
						<i class="fa fa-user" aria-hidden="true"></i> <span>Tài khoản</span>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" title="Đăng xuất">
						<i class="fa fa-sign-out" aria-hidden="true"></i> <span>Đăng xuất</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</li>
<script>
	jQuery('.avatar-header>a').click(function(){
		jQuery(this).next('.box-account').toggle();
	});
</script>