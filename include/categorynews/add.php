<style>
  .group-custom-cate-popup, .group-box-seo-popup{
    display: inline-block;
    float: left;
    width: 49%;
    margin-top: 0;
    margin-left: 2%;
    margin-bottom: 15px;
  }
  .group-custom-cate-popup{
    margin-left: 0;
  }
  .content-product-popup{
    margin-bottom: 15px;
    float: left;
  }
  .box-ckeditor-popup{
    display: none;
    padding-top: 0;
  }
  @media (max-width: 575.98px) {
    .modal-body{
      padding: 0;
    }
    .group-custom-cate-popup, .group-box-seo-popup{
      width: 100%;
      margin-left: 0;
    }
    #themdanhmuc-modal .box-ckeditor{
      margin-top: 0;
      margin-bottom: 15px;
    }
    .group-custom-cate-popup, .group-box-seo-popup{
      margin-bottom: 0;
    }
    #themdanhmuc-modal .box-cate-custom .item:first-child{
      margin-top: 0;
    }
    #themdanhmuc-modal .box-cate-custom .item:last-child{
      margin-bottom: 0;
    }
    #info-adm, #seo-adm, #note-adm{
      display: none;
      padding: 15px
    }
    #seo-adm .box-seo .item:first-child{
      margin-top: 0;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {

  }
  @media (min-width: 768px) and (max-width: 991.98px) {
    .box-seo .item .seo-input input, .box-seo .item .seo-input textarea{
      width: 70%;
    }
    
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {
    #themdanhmuc-modal .box-seo .item .seo-input .upload-img{
      width: 70%;
    }
  }
  @media (min-width: 1200px) {
    
  }
</style>
<div class="modal large-modal" id="themdanhmuc-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Thêm danh mục
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="tab-custom bg-black">
          <div class="item active">
            <a href="javascript:void(0)" data-id="info-adm" title="Thông tin">
              <i class="fa fa-sticky-note" aria-hidden="true"></i>
              <label>Thông tin</label>
            </a>
          </div>
          <div class="item">
            <a href="javascript:void(0)" data-id="seo-adm" title="SEO">
              <i class="fa fa-bullhorn" aria-hidden="true"></i>
              <label>SEO</label>
            </a>
          </div>
          <div class="item">
            <a href="javascript:void(0)" data-id="note-adm" title="Nội dung">
              <i class="fa fa-book" aria-hidden="true"></i>
              <label>Nội dung</label>
            </a>
          </div>
        </div>
        <div id="info-adm" class="group-custom-cate-popup tab-content item show-inline">
          <a class="dropdown-collapse bg-black" href="javascript:void(0);">Chi tiết danh mục</a>
          <div class="box-cate-custom">
            <div class="item">
              <label for="id_parent1">Chọn nhóm cha:</label>
              <select class="form-control" name="id_parent1" id="id_parent1">
                <option value="-1">Chọn danh mục cha</option>
                <option value="0" selected="selected">Collagen</option>
                <option value="1">Thực phẩm làm đẹp</option>
                <option value="2">Giảm cân</option>
              </select>
            </div>
            <div class="item">
              <label for="fullname">Tên nhóm:</label>
              <input autocomplete="off" type="text" name="name_vi" id="name_vi" placeholder="Nhập tên nhóm" value="" class="form-control" required="">
            </div>
            <div class="item">
              <label for="email">STT:</label>
              <input autocomplete="off" type="text" name="sort" id="sort" value="" class="form-control" required="">
            </div>
            <div class="item">
              <label for="showview">Ẩn / hiện:</label>
              <div class="custom-dropdown">
                <select class="form-control" name="showview" id="showview">
                  <option value="-1">Chọn</option>
                  <option value="0">Ẩn</option>
                  <option value="1">Hiện</option>
                </select>
              </div>
            </div>
            <div class="item">
              <label>Ảnh đại diện (16x9):</label>
              <div class="upload-img">
                <div class="input-group up-img">
                  <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
                  <label class="button bg-green custom-upload">
                    <input type="file" id="ipt-img" class="form-control" name="imgs" onchange="readURLPopup(this,1);" accept="image/*">Upload
                  </label>
                  <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
                </div>
              </div>
              <div class="box-img-meta">
                <img src="" alt="hình" id="photo1popup">
              </div>
            </div>
          </div>
        </div>
        <div id="seo-adm" class="group-box-seo-popup tab-content item">
          <a class="dropdown-collapse bg-black" href="javascript:void(0);">SEO</a>
          <div class="box-seo">
            <div class="item">
              <label for="slug_vi">Link URL:</label>
              <input autocomplete="off" type="text" name="slug_vi" id="slug_vi" placeholder="Nhập link url vd:danh-muc" value="collagen" class="form-control">
            </div>
            <div class="item">
              <label for="slug_vi">Tags:</label>
              <input name="vtags" id="vTags2" value="" type="hidden">
              <input id="Tags" value="collagen,nước uống collagen,collagen dạng bột,tủ lạnh,máy sấy,collagen1,nước uống collagen 1,collagen dạng bột 1,tủ lạnh 1,máy sấy 1" type="hidden">
              <ul id="ShowTag2"></ul>
            </div>
            <div class="item">
              <label for="meta_web_title">Google:</label>
              <span class="seo-input">
                Title:
                <input autocomplete="off" type="text" name="meta_web_title" id="meta_web_title" placeholder="Nhập title" value="" class="form-control">
              </span>
              <span class="seo-input">
                Keyword:
                <input autocomplete="off" type="text" name="meta_web_keyword" id="meta_web_keyword" placeholder="Nhập keyword" value="" class="form-control">
              </span>
              <span class="seo-input">
                Description:
                <textarea name="meta_web_desc" id="meta_web_desc" class="form-control" rows="3" placeholder="Nhập description"></textarea>
              </span>
            </div>
            <div class="item">
              <label for="meta_web_title">Social:</label>
              <span class="seo-input">
                Title:
                <input autocomplete="off" type="text" name="meta_web_title" id="meta_web_title" placeholder="Nhập title" value="" class="form-control">
              </span>
              <span class="seo-input">
                Description:
                <textarea name="meta_web_desc" id="meta_web_desc" class="form-control" rows="3" placeholder="Nhập description"></textarea>
              </span>
              <span class="seo-input">
                  Image:
                  <div class="upload-img">
                    <div class="input-group up-img">
                        <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
                        <label class="button bg-green custom-upload">
                            <input type="file" class="form-control" name="icon" id="icon" onchange="readURLPopup(this,2);" accept="image/*">Upload
                        </label>
                        <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
                    </div>
                </div>
              </span>
              <div class="box-img-meta">
                <img src="" alt="hình" id="photo2popup">
              </div>
            </div>
          </div>
        </div>
        <div id="note-adm" class="tab-content item">
          <a class="content-product-popup dropdown-collapse bg-black" href="javascript:void(0);">Nội dung <i class="fa fa-caret-down" aria-hidden="true"></i></a>
          <div class="box-ckeditor box-ckeditor-popup show">
            <label>Mô tả ngắn:</label>
            <textarea rows="5" class="form-control" id="notes" name="notes"></textarea>
            <label>Nội dung:</label>
            <textarea class="ckeditor" id="elm1" name="desc_vi"></textarea>
          </div>
          <button type="button" class="button bg-green">Thêm</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function readURLPopup(input,id) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        jQuery('#photo'+id+'popup').parent().css('display','block');
        jQuery('#photo'+id+'popup').attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  jQuery(function(){
    jQuery('#themdanhmuc-modal .tab-custom .item a').click(function(){
      var data = jQuery(this).data('id');
      jQuery('#themdanhmuc-modal .tab-content').not('#' + data).removeClass('show-inline');
      jQuery(this).parent().addClass('active');
      jQuery('#themdanhmuc-modal .tab-custom .item a').not(this).parent().removeClass('active');
      jQuery('#themdanhmuc-modal #'+data).addClass('show-inline');
    });
    CKEDITOR.instances["elm1"];
    jQuery('.content-product-popup').click(function(){
      if(jQuery('.box-ckeditor-popup').css('display')=='none'){
        jQuery('.box-ckeditor-popup').css('display','inline-block');
        jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
      }
      else{
        jQuery('.box-ckeditor-popup').css('display','none');
        jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
      }
    });
  })
</script>