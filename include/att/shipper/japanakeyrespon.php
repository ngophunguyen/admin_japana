<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item button{
		float: left;
		margin-left: 15px;
	}
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}
	.table-custom > tbody > tr > td input{
		display: none;
		width: 250px;
	}
	.table-custom tbody tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item{
			width: 100%;
		}
		.refund .table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="japanakeyrespon content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Japana Key Respon</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#addkey-modal" class="link-custom black-custom open-receipt" title="Thêm key">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm key</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Nhập thông tin cần tìm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Key</th>
					            <th class="bg-black">Ghi chú</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Key">
					            	<input autocomplete="off" type="text" name="hide_name1" id="hide_name1" class="form-control" value="tong_tien">
									<span>tong_tien</span>
					            </td>
					            <td data-title="Ghi chú">
					            	<input autocomplete="off" type="text" name="hide_note1" id="hide_note1" class="form-control" value="Tổng tiền">
									<span>Tổng tiền</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(1);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Key">
					            	<input autocomplete="off" type="text" name="hide_name2" id="hide_name2" class="form-control" value="thoi_gian">
									<span>thoi_gian</span>
					            </td>
					            <td data-title="Ghi chú">
					            	<input autocomplete="off" type="text" name="hide_note2" id="hide_note2" class="form-control" value="Thời gian giao dự kiến">
									<span>Thời gian giao dự kiến lượng</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(2);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Key">
					            	<input autocomplete="off" type="text" name="hide_name3" id="hide_name3" class="form-control" value="trang_thai">
									<span>trang_thai</span>
					            </td>
					            <td data-title="Ghi chú">
					            	<input autocomplete="off" type="text" name="hide_note3" id="hide_note3" class="form-control" value="Trạng thái giao hàng">
									<span>Trạng thái giao hàng</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(3);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Key">
					            	<input autocomplete="off" type="text" name="hide_name4" id="hide_name4" class="form-control" value="the_tich">
									<span>the_tich</span>
					            </td>
					            <td data-title="Ghi chú">
					            	<input autocomplete="off" type="text" name="hide_note4" id="hide_note4" class="form-control" value="ml">
									<span>ml</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(4);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Key">
					            	<input autocomplete="off" type="text" name="hide_name5" id="hide_name5" class="form-control" value="notes">
									<span>notes</span>
					            </td>
					            <td data-title="Ghi chú">
					            	<input autocomplete="off" type="text" name="hide_note5" id="hide_note5" class="form-control" value="messge trả về từ api kết quả">
									<span>messge trả về từ api kết quả</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(5);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Key">
					            	<input autocomplete="off" type="text" name="hide_name6" id="hide_name6" class="form-control" value="giam_gia">
									<span>giam_gia</span>
					            </td>
					            <td data-title="Ghi chú">
					            	<input autocomplete="off" type="text" name="hide_note6" id="hide_note6" class="form-control" value="Số tiền giảm giá">
									<span>Số tiền giảm giá</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(6);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Key">
					            	<input autocomplete="off" type="text" name="hide_name7" id="hide_name7" class="form-control" value="phi_cod">
									<span>phi_cod</span>
					            </td>
					            <td data-title="Ghi chú">
					            	<input autocomplete="off" type="text" name="hide_note7" id="hide_note7" class="form-control" value="Phí COD">
									<span>Phí COD</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(7);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Key">
					            	<input autocomplete="off" type="text" name="hide_name8" id="hide_name8" class="form-control" value="phi_dich_vu">
									<span>phi_dich_vu</span>
					            </td>
					            <td data-title="Ghi chú">
					            	<input autocomplete="off" type="text" name="hide_note8" id="hide_note8" class="form-control" value="Phí dịch vụ thêm">
									<span>Phí dịch vụ thêm</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(8);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Key">
					            	<input autocomplete="off" type="text" name="hide_name9" id="hide_name9" class="form-control" value="ten_dich_vu">
									<span>ten_dich_vu</span>
					            </td>
					            <td data-title="Ghi chú">
					            	<input autocomplete="off" type="text" name="hide_note9" id="hide_note9" class="form-control" value="tên gói dịch vụ">
									<span>tên gói dịch vụ</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(9);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Key">
					            	<input autocomplete="off" type="text" name="hide_name10" id="hide_name10" class="form-control" value="dia_diem_ho_tro">
									<span>dia_diem_ho_tro</span>
					            </td>
					            <td data-title="Ghi chú">
					            	<input autocomplete="off" type="text" name="hide_note10" id="hide_note10" class="form-control" value="Địa điểm dvvc có hỗ trợ giao hay không">
									<span>Địa điểm dvvc có hỗ trợ giao hay không</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(10);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/att/shipper/japanakeyrespon/add.php'); ?>
<script>
	function editItem(id) {
	    jQuery('#hide_name' + id).css('display', 'block');
	    jQuery('#hide_name' + id).next().css('display', 'none');
	    jQuery('#hide_note' + id).css('display', 'block');
	    jQuery('#hide_note' + id).next().css('display', 'none');
	}
	jQuery(function(){
		
	})
</script>