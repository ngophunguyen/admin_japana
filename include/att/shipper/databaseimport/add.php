<style>
	.add-input-convert .item{
		display: inline-block;
		width: 32%;
		margin: 10px 2%;
		float: left;
	}
	.add-input-convert .item:nth-child(1), 
	.add-input-convert .item:nth-child(3), 
	.add-input-convert .item:nth-child(4),
	.add-input-convert .item:nth-child(6){
		margin: 10px 0;
	}
	.add-input-convert .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	@media (max-width: 575.98px) {
	  	.add-input-convert .item{
			display: inline-block;
			width: 100%;
			margin: 10px 0 0;
			float: left;
		}
		
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="add-databaseimport content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Thêm mới Bảng dữ liệu Import/Export</h1>
			<ul>
				<li>
					<a href="#" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
					</a>
				</li>
				<li>
					<a href="?action=include/att/shipper/databaseimport.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="add-input-convert">
					<form id="frm" name="frm" method="post" autocomplete="off">
					    <div class="item">
					    	<label for="name_table">Tên bảng:</label>
					    	<input autocomplete="off" type="text" name="name_table" id="name_table" placeholder="Nhập tên bảng" value="" class="form-control">
					    </div>
					    <div class="item">
					    	<label for="name_field">Tên field:</label>
					    	<input autocomplete="off" type="text" name="name_field" id="name_field" placeholder="Nhập tên field" value="" class="form-control" required="">
					    </div>
					    <div class="item">
					    	<label for="sort">Sắp xếp:</label>
					    	<input autocomplete="off" type="text" name="sort" id="sort" placeholder="Nhập vị trí" value="" class="form-control" required="">
					    </div>
					</form>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		
	})
</script>