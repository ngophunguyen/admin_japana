<style>
	#info-database .item{
		width: 30%;
		float: left;
		margin-top: 20px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	#info-database .item:nth-child(2){
		margin: 20px 5% 0;
	}
	#info-database .item:last-child{
		width: 100%;
	}
	#info-database .item label{
		font-weight: 500;
		font-size: 14px;
		color: #000;
		margin-right: 5px;
	}
	.upload-img{
		margin-left: 15px;
		width: 80%;
		float: right;
	}
	.upload-img input{
		margin-left: 0;
		width: auto
	}
	#info-database .item .button{
		color: #fff;
	}
	@media (max-width: 575.98px) {
		#info-database .item{
			width: 100%;
		}
		#info-database .item:nth-child(2){
			margin: 20px 0 0;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="name content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chi tiết Bảng dữ liệu Import/Export</h1>
			<ul>
				<li>
					<a href="?action=include/att/shipper/databaseimport.php" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Trở về</label>
					</a>
				</li>
				<li>
                    <a href="javascript:void(0)" title="Export file Excel" class="link-custom black-custom">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> <label>Export file excel</label>
                    </a>
                </li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div id="info-database">
					<div class="item">
						<label>Tên bảng:</label>
						<span>jp_city</span>
					</div>
					<div class="item">
						<label>Tên field:</label>
						<span>id,name,ghn</span>
					</div>
					<div class="item">
						<label>Sắp xếp:</label>
						<span>1</span>
					</div>
					<div class="item">
						<label>File Excel import:</label>
						<div class="upload-img">
                            <div class="input-group up-img">
                                <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có file nào được chọn" readonly="">
                                <label class="button bg-green custom-upload">
                                    <input type="file" class="form-control" name="file" id="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">Upload file
                                </label>
                                <button type="button" class="button bg-red custom-upload delete-img">Xóa file</button>
                            </div>
                        </div>
                        <button type="button" class="button bg-green">Import file</button>
					</div>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		jQuery(document).on('change', ':file', function() {
		    var input = jQuery(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		});
		jQuery(':file').on('fileselect', function(event, numFiles, label) {
          	var input = jQuery(this).parents('.up-img').find(':text'),
              	log = numFiles > 1 ? numFiles + ' files selected' : label;
          	if( input.length ) {
              	input.val(log);
              	if(input.val(log)){
                	jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
                	jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
              	}
          	} else {
              	if(log) ;
          	}
      	});
      	jQuery('.delete-img').on('click', function(e){
		    jQuery(this).parent().find('input[type=file]').val('');
		    jQuery(this).parent().find('input[type=text]').val('');
		    jQuery(this).parent().find('.custom-upload').css('display','flex');
		    jQuery(this).css('display','none');
		});
	})
</script>