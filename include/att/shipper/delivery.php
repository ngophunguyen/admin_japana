<style>
	.table-custom > tbody > tr > td input{
		display: none;
		width: 250px;
	}
	.table-custom tbody tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	@media (max-width: 575.98px) {
		.table-custom > tbody > tr > td input{
			width: 100%;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="delivery content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Đơn vị vận chuyển</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#adddvvc-modal" class="link-custom black-custom open-receipt" title="Thêm đơn vị vận chuyển">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Tên DVVC</th>
					            <th class="bg-black">Điện thoại</th>
					            <th class="bg-black">Email</th>
					            <th class="bg-black">Địa chỉ</th>
					            <th class="bg-black center-custom">Trạng thái</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Tên DVVC">
					            	<input autocomplete="off" type="text" name="hide_name1" id="hide_name1" class="form-control" value="Giao hàng tiết kiệm">
									<span>Giao hàng tiết kiệm</span>
					            </td>
					            <td data-title="Điện thoại">
					            	<input autocomplete="off" type="text" name="hide_sdt1" id="hide_sdt1" class="form-control" value="(028) 7300 6092">
									<span>(028) 7300 6092</span>
					            </td>
					            <td data-title="Email">
					            	<input autocomplete="off" type="text" name="hide_email1" id="hide_email1" class="form-control" value="cskh@ghtk.vn">
									<span>cskh@ghtk.vn</span>
					            </td>
					            <td data-title="Địa chỉ">
					            	<input autocomplete="off" type="text" name="hide_addr1" id="hide_addr1" class="form-control" value="46/2 Nguyễn Cửu Vân, P. 17, Q. Bình Thạnh, TP. Hồ Chí Minh">
									<span>46/2 Nguyễn Cửu Vân, P. 17, Q. Bình Thạnh, TP. Hồ Chí Minh</span>
					            </td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(1);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Tên DVVC">
					            	<input autocomplete="off" type="text" name="hide_name2" id="hide_name2" class="form-control" value="Ship an toàn">
									<span>Ship an toàn</span>
					            </td>
					            <td data-title="Điện thoại">
					            	<input autocomplete="off" type="text" name="hide_sdt2" id="hide_sdt2" class="form-control" value="1900 6051">
									<span>1900 6051</span>
					            </td>
					            <td data-title="Email">
					            	<input autocomplete="off" type="text" name="hide_email2" id="hide_email2" class="form-control" value="admin@shipantoan.vn">
									<span>admin@shipantoan.vn</span>
					            </td>
					            <td data-title="Địa chỉ">
					            	<input autocomplete="off" type="text" name="hide_addr2" id="hide_addr2" class="form-control" value="Lầu 3, số 96 – 98 Đào Duy Anh, Phường 9, Quận Phú Nhuận, TP HCM">
									<span>Lầu 3, số 96 – 98 Đào Duy Anh, Phường 9, Quận Phú Nhuận, TP HCM</span>
					            </td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(2);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Tên DVVC">
					            	<input autocomplete="off" type="text" name="hide_name3" id="hide_name3" class="form-control" value="Ahamove">
									<span>Ahamove</span>
					            </td>
					            <td data-title="Điện thoại">
					            	<input autocomplete="off" type="text" name="hide_sdt3" id="hide_sdt3" class="form-control" value="1900545411">
									<span>1900545411</span>
					            </td>
					            <td data-title="Email">
					            	<input autocomplete="off" type="text" name="hide_email3" id="hide_email3" class="form-control" value="">
									<span></span>
					            </td>
					            <td data-title="Địa chỉ">
					            	<input autocomplete="off" type="text" name="hide_addr3" id="hide_addr3" class="form-control" value="">
									<span></span>
					            </td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(3);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Tên DVVC">
					            	<input autocomplete="off" type="text" name="hide_name4" id="hide_name4" class="form-control" value="Văn phòng">
									<span>Văn phòng</span>
					            </td>
					            <td data-title="Điện thoại">
					            	<input autocomplete="off" type="text" name="hide_sdt4" id="hide_sdt4" class="form-control" value="0935 600 800">
									<span>0935 600 800</span>
					            </td>
					            <td data-title="Email">
					            	<input autocomplete="off" type="text" name="hide_email4" id="hide_email4" class="form-control" value="info@japana.vn">
									<span>info@japana.vn</span>
					            </td>
					            <td data-title="Địa chỉ">
					            	<input autocomplete="off" type="text" name="hide_addr4" id="hide_addr4" class="form-control" value="76 , Nguyễn Hảo Vĩnh , tân phú">
									<span>76 , Nguyễn Hảo Vĩnh , tân phú</span>
					            </td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(4);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Tên DVVC">
					            	<input autocomplete="off" type="text" name="hide_name5" id="hide_name5" class="form-control" value="Tín tốc">
									<span>Tín tốc</span>
					            </td>
					            <td data-title="Điện thoại">
					            	<input autocomplete="off" type="text" name="hide_sdt5" id="hide_sdt5" class="form-control" value="18006328">
									<span>18006328</span>
					            </td>
					            <td data-title="Email">
					            	<input autocomplete="off" type="text" name="hide_email5" id="hide_email5" class="form-control" value="cskh@ghtk.vn">
									<span>cskh@ghtk.vn</span>
					            </td>
					            <td data-title="Địa chỉ">
					            	<input autocomplete="off" type="text" name="hide_addr5" id="hide_addr5" class="form-control" value="46/2 Nguyễn Cửu Vân, P. 17, Q. Bình Thạnh, TP. Hồ Chí Minh">
									<span>46/2 Nguyễn Cửu Vân, P. 17, Q. Bình Thạnh, TP. Hồ Chí Minh</span>
					            </td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(5);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Tên DVVC">
					            	<input autocomplete="off" type="text" name="hide_name6" id="hide_name6" class="form-control" value="Ship 60 phút">
									<span>Ship 60 phút</span>
					            </td>
					            <td data-title="Điện thoại">
					            	<input autocomplete="off" type="text" name="hide_sdt6" id="hide_sdt6" class="form-control" value="0906919117">
									<span>0906919117</span>
					            </td>
					            <td data-title="Email">
					            	<input autocomplete="off" type="text" name="hide_email6" id="hide_email6" class="form-control" value="">
									<span></span>
					            </td>
					            <td data-title="Địa chỉ">
					            	<input autocomplete="off" type="text" name="hide_addr6" id="hide_addr6" class="form-control" value="20A Trần Văn Quang, Phường 10 , Quận Tân Bình, HCM">
									<span>20A Trần Văn Quang, Phường 10 , Quận Tân Bình, HCM</span>
					            </td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(6);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Tên DVVC">
					            	<input autocomplete="off" type="text" name="hide_name7" id="hide_name7" class="form-control" value="Viettel Post">
									<span>Viettel Post</span>
					            </td>
					            <td data-title="Điện thoại">
					            	<input autocomplete="off" type="text" name="hide_sdt7" id="hide_sdt7" class="form-control" value="19008095">
									<span>19008095</span>
					            </td>
					            <td data-title="Email">
					            	<input autocomplete="off" type="text" name="hide_email7" id="hide_email7" class="form-control" value="mailto:support@viettelpost.com.vn">
									<span>mailto:support@viettelpost.com.vn</span>
					            </td>
					            <td data-title="Địa chỉ">
					            	<input autocomplete="off" type="text" name="hide_addr7" id="hide_addr7" class="form-control" value="110C, Tân Quý, P. Tân Quý, Q. Tân Phú, Tp. Hồ Chí Minh">
									<span>110C, Tân Quý, P. Tân Quý, Q. Tân Phú, Tp. Hồ Chí Minh</span>
					            </td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(7);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Tên DVVC">
					            	<input autocomplete="off" type="text" name="hide_name8" id="hide_name8" class="form-control" value="Vietnampost">
									<span>Vietnampost</span>
					            </td>
					            <td data-title="Điện thoại">
					            	<input autocomplete="off" type="text" name="hide_sdt8" id="hide_sdt8" class="form-control" value="(028) 3825 6930">
									<span>(028) 3825 6930</span>
					            </td>
					            <td data-title="Email">
					            	<input autocomplete="off" type="text" name="hide_email8" id="hide_email8" class="form-control" value="">
									<span></span>
					            </td>
					            <td data-title="Địa chỉ">
					            	<input autocomplete="off" type="text" name="hide_addr8" id="hide_addr8" class="form-control" value="125 Hai Bà Trưng , P. Bến Nghé, Quận 1, HCM.">
									<span>125 Hai Bà Trưng , P. Bến Nghé, Quận 1, HCM.</span>
					            </td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(8);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/att/shipper/delivery/add.php'); ?>
<script>
	function editItem(id) {
	    jQuery('#hide_name' + id).css('display', 'block');
	    jQuery('#hide_name' + id).next().css('display', 'none');
	    jQuery('#hide_sdt' + id).css('display', 'block');
	    jQuery('#hide_sdt' + id).next().css('display', 'none');
	    jQuery('#hide_email' + id).css('display', 'block');
	    jQuery('#hide_email' + id).next().css('display', 'none');
	    jQuery('#hide_addr' + id).css('display', 'block');
	    jQuery('#hide_addr' + id).next().css('display', 'none');
	}
	jQuery(function(){
		
	})
</script>