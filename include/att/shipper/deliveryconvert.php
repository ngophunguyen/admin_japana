<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:last-child{
		width: 55%;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	}
	.box-quick-search .item:first-child input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
		float: right;
	}
	.box-quick-search .item .custom-dropdown{
		margin-left: 15px;
		width: 35%;
	}
	.search2, .search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.search1{
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.table-custom > tbody > tr > td input{
		display: none;
		width: 250px;
	}
	.table-custom tbody tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item, .search2, .search1,.box-quick-search .item:last-child{
			width: 100%;
		}
		.search2{
			margin-top: 15px;
		}
		.refund .table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.box-quick-search .item .custom-dropdown{
			margin-left: 0;
			margin-right: 15px;
			width: 45%;
			float: left;
		}
		.box-quick-search .item .custom-dropdown:nth-child(2){
			margin-right: 0;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="deliveryconvert content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chuyển đổi Key Japana sang Đơn vị vận chuyển</h1>
			<ul>
				<li>
					<a href="?action=include/att/shipper/deliveryconvert/add.php" class="link-custom black-custom" title="Thêm mới">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Nhập thông tin cần tìm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<form name="status_search" id="frm" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="id_delivery" name="id_delivery">
						    		<option value="-1">Chọn đơn vị vận chuyển</option>
								  	<option value="0">Giao hàng nhanh</option>
								  	<option value="1">Tín tốc</option>
								</select>
							</div>
							<div class="custom-dropdown">
	                            <select class="form-control" id="type" name="type">
						    		<option value="-1">Chọn phân loại</option>
								  	<option value="0">Tỉnh thành</option>
								  	<option value="1">Quận huyện</option>
								  	<option value="2">Phường xã</option>
								</select>
	                        </div>
	                        <button type="submit" class="button bg-black">Lọc</button>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Tên DVVC</th>
					            <th class="bg-black">Key Japana</th>
					            <th class="bg-black">Key DVVC</th>
					            <th class="bg-black">Giá trị DVVC</th>
					            <th class="bg-black">Loại</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" title="Giao hàng tiết kiệm">Giao hàng tiết kiệm</a>
					            </td>
					            <td data-title="Key Japana">the_tich</td>
					            <td data-title="Key DVVC">ml</td>
					            <td data-title="Giá trị DVVC">1000.000</td>
					            <td data-title="Loại"></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" title="Giao hàng tiết kiệm">Giao hàng tiết kiệm</a>
					            </td>
					            <td data-title="Key Japana">tong_gia_tien</td>
					            <td data-title="Key DVVC">value</td>
					            <td data-title="Giá trị DVVC">0.000</td>
					            <td data-title="Loại"></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">khoi_luong</td>
					            <td data-title="Key DVVC">Weight</td>
					            <td data-title="Giá trị DVVC">1.000</td>
					            <td data-title="Loại"></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">chieu_dai</td>
					            <td data-title="Key DVVC">Length</td>
					            <td data-title="Giá trị DVVC">1.000</td>
					            <td data-title="Loại"></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">chieu_rong</td>
					            <td data-title="Key DVVC">Width</td>
					            <td data-title="Giá trị DVVC">1000.000</td>
					            <td data-title="Loại"></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">chieu_cao</td>
					            <td data-title="Key DVVC">Height</td>
					            <td data-title="Giá trị DVVC">1.000</td>
					            <td data-title="Loại"></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">ma_dich_vu</td>
					            <td data-title="Key DVVC">ServiceID</td>
					            <td data-title="Giá trị DVVC">0.000</td>
					            <td data-title="Loại"></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">tong_gia_tien</td>
					            <td data-title="Key DVVC">InsuranceFee</td>
					            <td data-title="Giá trị DVVC">0.000</td>
					            <td data-title="Loại"></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">quan_huyen</td>
					            <td data-title="Key DVVC">ToDistrictID</td>
					            <td data-title="Giá trị DVVC">0.000</td>
					            <td data-title="Loại">Quận huyện</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">ma_van_don</td>
					            <td data-title="Key DVVC">OrderCode</td>
					            <td data-title="Giá trị DVVC">0.000</td>
					            <td data-title="Loại"></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvert/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		
	})
</script>