<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item button{
		float: left;
		margin-left: 15px;
	}
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}
	.table-custom > tbody > tr > td input{
		display: none;
		width: 250px;
	}
	.table-custom tbody tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item{
			width: 100%;
		}
		.refund .table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="databaseimport content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Bảng dữ liệu Import/Export</h1>
			<ul>
				<li>
					<a href="?action=include/att/shipper/databaseimport/add.php" class="link-custom black-custom open-receipt" title="Thêm mới">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Nhập thông tin cần tìm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Tên bảng</th>
					            <th class="bg-black">Tên field</th>
					            <th class="bg-black center-custom">Sắp xếp</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Tên bảng">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" title="jp_city">
					            		jp_city
					            	</a>
					            </td>
					            <td data-title="Tên field">id,name,ghn</td>
					            <td data-title="Sắp xếp" class="center-custom">1</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="?action=include/att/shipper/databaseimport/detail.php" class="link-custom black-custom" title="Xem chi tiết import/export">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Tên bảng">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" title="jp_city">
					            		jp_city
					            	</a>
					            </td>
					            <td data-title="Tên field">id,name,ghtk</td>
					            <td data-title="Sắp xếp" class="center-custom">2</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="?action=include/att/shipper/databaseimport/detail.php" class="link-custom black-custom" title="Xem chi tiết import/export">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Tên bảng">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" title="jp_city">
					            		jp_city
					            	</a>
					            </td>
					            <td data-title="Tên field">id,name,tintoc</td>
					            <td data-title="Sắp xếp" class="center-custom">3</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="?action=include/att/shipper/databaseimport/detail.php" class="link-custom black-custom" title="Xem chi tiết import/export">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Tên bảng">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" title="jp_city">
					            		jp_city
					            	</a>
					            </td>
					            <td data-title="Tên field">id,name,vietnampost</td>
					            <td data-title="Sắp xếp" class="center-custom">3</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="?action=include/att/shipper/databaseimport/detail.php" class="link-custom black-custom" title="Xem chi tiết import/export">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Tên bảng">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" title="jp_city_ward">
					            		jp_city_ward
					            	</a>
					            </td>
					            <td data-title="Tên field">id,name,ghn</td>
					            <td data-title="Sắp xếp" class="center-custom">1</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="?action=include/att/shipper/databaseimport/detail.php" class="link-custom black-custom" title="Xem chi tiết import/export">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Tên bảng">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" title="jp_city_ward">
					            		jp_city_ward
					            	</a>
					            </td>
					            <td data-title="Tên field">id,name,ghtk</td>
					            <td data-title="Sắp xếp" class="center-custom">2</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="?action=include/att/shipper/databaseimport/detail.php" class="link-custom black-custom" title="Xem chi tiết import/export">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Tên bảng">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" title="jp_city_ward">
					            		jp_city_ward
					            	</a>
					            </td>
					            <td data-title="Tên field">id,name,tintoc</td>
					            <td data-title="Sắp xếp" class="center-custom">3</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="?action=include/att/shipper/databaseimport/detail.php" class="link-custom black-custom" title="Xem chi tiết import/export">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Tên bảng">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" title="jp_city_ward">
					            		jp_city_ward
					            	</a>
					            </td>
					            <td data-title="Tên field">id,name,vietnampost</td>
					            <td data-title="Sắp xếp" class="center-custom">4</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="?action=include/att/shipper/databaseimport/detail.php" class="link-custom black-custom" title="Xem chi tiết import/export">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Tên bảng">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" title="jp_city_zone">
					            		jp_city_zone
					            	</a>
					            </td>
					            <td data-title="Tên field">id,name,ghn</td>
					            <td data-title="Sắp xếp" class="center-custom">1</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="?action=include/att/shipper/databaseimport/detail.php" class="link-custom black-custom" title="Xem chi tiết import/export">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Tên bảng">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" title="jp_city_zone">
					            		jp_city_zone
					            	</a>
					            </td>
					            <td data-title="Tên field">id,name,ghtk</td>
					            <td data-title="Sắp xếp" class="center-custom">2</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/databaseimport/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="?action=include/att/shipper/databaseimport/detail.php" class="link-custom black-custom" title="Xem chi tiết import/export">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/att/shipper/japanakey/add.php'); ?>
<script>
	jQuery(function(){
		
	})
</script>