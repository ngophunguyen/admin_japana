<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		width: 55%;
		float: right;
	}
	.box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
		float: right;
	}
	.box-quick-search .item .custom-dropdown{
		margin-left: 15px;
		width: 35%;
	}
	.search2{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.table-custom > tbody > tr > td input{
		display: none;
		width: 250px;
	}
	.table-custom tbody tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item, .search2, .search1,.box-quick-search .item:last-child{
			width: 100%;
		}
		.refund .table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.box-quick-search .item .custom-dropdown{
			margin-left: 0;
			margin-right: 15px;
			width: 45%;
			float: left;
		}
		.box-quick-search .item .custom-dropdown:nth-child(2){
			margin-right: 0;
		}
		.table-custom tbody tr td{
			word-break: break-word;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="deliveryjson content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">JSON Đơn vị vận chuyển</h1>
			<ul>
				<li>
					<a href="?action=include/att/shipper/deliveryjson/add.php" class="link-custom black-custom" title="Thêm mới">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="status_search" id="frm" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="id_delivery" name="id_delivery">
						    		<option value="-1">Chọn đơn vị vận chuyển</option>
								  	<option value="0">Giao hàng nhanh</option>
								  	<option value="1">Tín tốc</option>
								</select>
							</div>
							<div class="custom-dropdown">
	                            <select class="form-control" id="type" name="type">
						    		<option value="-1">Chọn loại API</option>
								  	<option value="0">Tính phí vận chuyển</option>
								  	<option value="1">Đăng đơn</option>
								  	<option value="2">Hủy đơn</option>
								  	<option value="3">Trạng thái đơn</option>
								</select>
	                        </div>
	                        <button type="submit" class="button bg-black">Lọc</button>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Đơn vị vận chuyển</th>
					            <th class="bg-black">Loại</th>
					            <th class="bg-black">Link</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Link">https://apiv3-test.ghn.vn/api/v1/apiv3/CalculateFee</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" title="Tín tốc">Tín tốc</a>
					            </td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Link">https://dc.tintoc.net/app/api-customer/quotations/calculate-delivery-fee</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" title="Giao hàng tiết kiệm">Giao hàng tiết kiệm</a>
					            </td>
					            <td data-title="Loại"></td>
					            <td data-title="Link">https://services.giaohangtietkiem.vn/services/shipment/fee</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" title="Giao hàng tiết kiệm">Giao hàng tiết kiệm</a>
					            </td>
					            <td data-title="Loại">Đăng đơn</td>
					            <td data-title="Link">https://services.giaohangtietkiem.vn/services/shipment/order</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" title="Giao hàng tiết kiệm">Giao hàng tiết kiệm</a>
					            </td>
					            <td data-title="Loại">Hủy đơn</td>
					            <td data-title="Link">https://services.giaohangtietkiem.vn/services/shipment/cancel/</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" title="Giao hàng tiết kiệm">Giao hàng tiết kiệm</a>
					            </td>
					            <td data-title="Loại">Trạng thái đơn</td>
					            <td data-title="Link">https://services.giaohangtietkiem.vn/services/shipment/v2/</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Loại">Đăng đơn</td>
					            <td data-title="Link">https://apiv3-test.ghn.vn/api/v1/apiv3/CreateOrder</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Loại">Hủy đơn</td>
					            <td data-title="Link">https://apiv3-test.ghn.vn/api/v1/apiv3/CancelOrder</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Loại">Trạng thái đơn</td>
					            <td data-title="Link">https://apiv3-test.ghn.vn/api/v1/apiv3/OrderInfo</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Tên DVVC">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" title="Tín tốc">Tín tốc</a>
					            </td>
					            <td data-title="Loại">Đăng đơn</td>
					            <td data-title="Link">https://dc.tintoc.net/app/api-customer/v2/order/create</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryjson/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		
	})
</script>