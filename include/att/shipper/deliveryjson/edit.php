<style>
	.add-input-convert .item{
		display: inline-block;
		width: 32%;
		margin: 10px 2%;
		float: left;
	}
	.add-input-convert .item:nth-child(1), 
	.add-input-convert .item:nth-child(3), 
	.add-input-convert .item:nth-child(4),
	.add-input-convert .item:nth-child(6){
		margin: 10px 0;
	}
	.add-input-convert .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	@media (max-width: 575.98px) {
	  	.add-input-convert .item{
			display: inline-block;
			width: 100%;
			margin: 10px 0 0;
			float: left;
		}
		
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="edit-json content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Thêm JSON Đơn vị vận chuyển</h1>
			<ul>
				<li>
					<a href="#" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
					</a>
				</li>
				<li>
					<a href="?action=include/att/shipper/deliveryjson.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="add-input-convert">
					<form id="frm" name="frm" method="post" autocomplete="off">
					    <div class="item">
					    	<label for="id_delivery">Đơn vị vận chuyển:</label>
					    	<div class="custom-dropdown">
						    	<select class="form-control" name="id_delivery" id="id_delivery">
						    		<option value="-1">Chọn đơn vị</option>
								  	<option value="0" selected="selected">Giao hàng nhanh</option>
								  	<option value="1">Tín tốc</option>
								</select>
							</div>
					    </div>
					    <div class="item">
					    	<label for="link">Link API:</label>
					    	<input type="text" value="https://apiv3-test.ghn.vn/api/v1/apiv3/CalculateFee" placeholder="Nhập link API..." id="link" name="link" class="form-control">
					    </div>
					    <div class="item">
					    	<label for="type">Loại giá trị:</label>
					    	<div class="custom-dropdown">
						    	<select class="form-control" name="type" id="type">
						    		<option value="-1">Chọn loại</option>
								  	<option value="0" selected="selected">Tính phí vận chuyển</option>
								  	<option value="1">Đăng đơn</option>
								  	<option value="2">Hủy đơn</option>
								  	<option value="3">Trạng thái đơn</option>
								</select>
							</div>
					    </div>
					    <div class="item">
					    	<label for="json_post">JSON post:</label>
					    	<textarea rows="20" name="json_post" id="json_post" class="form-control">
					    		{
								    "token": "75e88bd35ee6419fbfc5dcadfc7b2f2c",
								    "PaymentTypeID": 1,
								    "FromDistrictID": 1552,
								    "FromWardCode": "580805",
								    "ToDistrictID": 1552,
								    "ToWardCode": "21609",
								    "CoDAmount": 1500000,
								   "AffiliateID": 485411,
								   "ClientID": 485411,
								    "ServiceID": 53321,
								    "Content": "ten sp",
								    "CouponCode": "",
								    "Weight": 500,
								    "Length": 10,
								    "Width": 10,
								    "Height": 10,
								    "CheckMainBankAccount": false,
								    "OrderCosts": [
								        {
								            "ServiceID": 100022
								        },
								        {
								            "ServiceID": 53337
								        },
								 {
								            "ServiceID": 16
								        }
								    ],
								 "InsuranceFee": 1000003
								}
					    	</textarea>
					    </div>
					    <div class="item">
					    	<label for="json_get">JSON get:</label>
					    	<textarea rows="20" name="json_get" id="json_get" class="form-control"></textarea>
					    </div>
					    <div class="item">
					    	<label for="xml_post">XML post:</label>
					    	<textarea rows="20" name="xml_post" id="xml_post" class="form-control"></textarea>
					    </div>
					</form>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		
	})
</script>