<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		width: 55%;
		float: right;
	}
	.box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
		float: right;
	}
	.box-quick-search .item .custom-dropdown{
		margin-left: 15px;
		width: 35%;
	}
	.search2{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.table-custom > tbody > tr > td input{
		display: none;
		width: 250px;
	}
	.table-custom tbody tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	@media (max-width: 575.98px) {
		.entry-header ul{
			display: none;
		}
		.box-quick-search .item, .search2, .search1,.box-quick-search .item:last-child{
			width: 100%;
		}
		.refund .table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.box-quick-search .item .custom-dropdown{
			margin-left: 0;
			margin-right: 15px;
			width: 45%;
			float: left;
		}
		.box-quick-search .item .custom-dropdown:nth-child(2){
			margin-right: 0;
		}
		.table-custom tbody tr td{
			word-break: break-word;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="toolcity content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Kiểm tra dữ liệu</h1>
			<ul>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="status_search" id="frm" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="datatype" name="datatype">
						    		<option value="-1">Chọn tỉnh thành</option>
								  	<option value="0">Tỉnh thành</option>
								  	<option value="1">Quận huyện</option>
								  	<option value="2">Phường xã</option>
								</select>
							</div>
							<div class="custom-dropdown">
	                            <select class="form-control" id="codedvvc" name="codedvvc">
						    		<option value="-1">Chọn đơn vị vận chuyển</option>
								  	<option value="0">Giao hàng nhanh</option>
								  	<option value="1">Tín tốc</option>
								</select>
	                        </div>
	                        <button type="submit" class="button bg-black">Kiểm tra</button>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">					            
					        	<th class="bg-black">Japana</th>
					            <th class="bg-black">Đơn vị vận chuyển</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="Japana">Thành phố Hà Nội</td>
					            <td data-title="ĐVVC"></td>
					        </tr>
					        <tr>
					            <td data-title="Japana">Tỉnh Hà Giang</td>
					            <td data-title="ĐVVC"></td>
					        </tr>
					        <tr>
					            <td data-title="Japana">Tỉnh Cao Bằng</td>
					            <td data-title="ĐVVC"></td>
					        </tr>
					        <tr>
					            <td data-title="Japana">Tỉnh Bắc Kạn</td>
					            <td data-title="ĐVVC"></td>
					        </tr>
					        <tr>
					            <td data-title="Japana">Tỉnh Tuyên Quang</td>
					            <td data-title="ĐVVC"></td>
					        </tr>
					        <tr>
					            <td data-title="Japana">Tỉnh Lào Cai</td>
					            <td data-title="ĐVVC"></td>
					        </tr>
					        <tr>
					            <td data-title="Japana">Tỉnh Điện Biên</td>
					            <td data-title="ĐVVC"></td>
					        </tr>
					        <tr>
					            <td data-title="Japana">Tỉnh Lai Châu</td>
					            <td data-title="ĐVVC"></td>
					        </tr>
					        <tr>
					            <td data-title="Japana">Tỉnh Sơn La</td>
					            <td data-title="ĐVVC"></td>
					        </tr>
					        <tr>
					            <td data-title="Japana">Tỉnh Yên Bái</td>
					            <td data-title="ĐVVC"></td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		
	})
</script>