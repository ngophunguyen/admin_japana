<style>
	.add-input-convert .item{
		display: inline-block;
		width: 32%;
		margin: 10px 2%;
		float: left;
	}
	.add-input-convert .item:nth-child(1), 
	.add-input-convert .item:nth-child(3), 
	.add-input-convert .item:nth-child(4),
	.add-input-convert .item:nth-child(6){
		margin: 10px 0;
	}
	.add-input-convert .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	@media (max-width: 575.98px) {
	  	.add-input-convert .item{
			display: inline-block;
			width: 100%;
			margin: 10px 0 0;
			float: left;
		}
		
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="add-convert content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chuyển đổi Key Japana sang Đơn vị vận chuyển trả về</h1>
			<ul>
				<li>
					<a href="#" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
					</a>
				</li>
				<li>
					<a href="?action=include/att/shipper/deliveryconvertrespon.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="add-input-convert">
					<form id="frm" name="frm" method="post" autocomplete="off">
					    <div class="item">
					    	<label for="id_dvvc">Đơn vị vận chuyển:</label>
					    	<div class="custom-dropdown">
						    	<select class="form-control" name="id_dvvc" id="id_dvvc">
						    		<option value="-1">Chọn đơn vị</option>
								  	<option value="0">Giao hàng nhanh</option>
								  	<option value="1">Tín tốc</option>
								</select>
							</div>
					    </div>
					    <div class="item">
					    	<label for="id_key_japana">Key Japana:</label>
					    	<div class="custom-dropdown">
						    	<select class="form-control" name="id_key_japana" id="id_key_japana">
						    		<option value="-1">Chọn key japana</option>
								  	<option value="0">total (Tổng tiền)</option>
								  	<option value="1">khoi_luong (Khối lượng)</option>
								  	<option value="2">the_tich (ml)</option>
								</select>
							</div>
					    </div>
					    <div class="item">
					    	<label for="password">Key DVVC:</label>
					    	<input autocomplete="off" type="text" name="key_dvvc" id="key_dvvc" placeholder="Nhập key" value="" class="form-control">
					    </div>
					    <div class="item">
					    	<label for="fullname">Giá trị:</label>
					    	<input autocomplete="off" type="text" name="value_convert" id="value_convert" placeholder="Nhập giá trị" value="" class="form-control" required="">
					    </div>
					    <div class="item">
					    	<label for="mobile">Loại giá trị:</label>
					    	<div class="custom-dropdown">
						    	<select class="form-control" name="type" id="type">
						    		<option value="-1">Chọn loại</option>
								  	<option value="0">Tính phí vận chuyển</option>
								  	<option value="1">Đăng đơn</option>
								  	<option value="2">Hủy đơn</option>
								  	<option value="3">Trạng thái đơn</option>
								</select>
							</div>
					    </div>
					</form>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		
	})
</script>