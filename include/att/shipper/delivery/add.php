<style>
	.box-adddvvc{
    display: inline-block;
    width: 100%;
  }
  .box-adddvvc .item{
    width: 49%;
    float: left;
    margin-bottom: 15px;
  }
  .box-adddvvc .item:nth-child(even){
    margin-left: 2%;
  }
  .box-adddvvc .item label{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 5px;
    font-weight: 500;
    width: 100%;
  }
  @media (max-width: 575.98px) {
    .box-adddvvc .item{
      width: 100%
    }
    .box-adddvvc .item:nth-child(even){
      margin-left: 0;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {  
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal medium-modal" id="adddvvc-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Thêm đơn vị vận chuyển
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
      	<div class="box-adddvvc">
      		<div class="item">
      			<label for="name">Tên đơn vị vận chuyển</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="name" name="name" placeholder="Nhập tên đơn vị vận chuyển">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="mobile">Số điện thoại</label>
      			<div class="box-input-addpro">
	      			<input type="number" class="form-control" id="mobile" name="mobile" placeholder="Nhập số điện thoại">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="email">Email</label>
      			<div class="box-input-addpro">
	      			<input type="email" class="form-control" id="email" name="email" placeholder="Nhập mail">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="address">Địa chỉ</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="address" name="address" placeholder="Nhập địa chỉ">
	      		</div>
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="button bg-red">Hủy</button>
        <button type="button" class="button bg-green">Lưu</button>
      </div>
    </div>
  </div>
</div>
<script>
	jQuery(function(){
		
	})
</script>