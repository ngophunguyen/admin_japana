<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item button{
		float: left;
		margin-left: 15px;
	}
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}
	.table-custom > tbody > tr > td input{
		display: none;
		width: 250px;
	}
	.table-custom tbody tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item{
			width: 100%;
		}
		.refund .table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="deliveryconvertrespon content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chuyển đổi Key Japana sang Đơn vị vận chuyển trả về</h1>
			<ul>
				<li>
					<a href="?action=include/att/shipper/deliveryconvertrespon/add.php" class="link-custom black-custom open-receipt" title="Thêm mới">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Nhập thông tin cần tìm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Đơn vị vận chuyển</th>
					            <th class="bg-black">Key Japana</th>
					            <th class="bg-black">Key DVVC</th>
					            <th class="bg-black">Giá trị DVVC</th>
					            <th class="bg-black">Loại</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" title="Giao hàng tiết kiệm">Giao hàng tiết kiệm</a>
					            </td>
					            <td data-title="Key Japana">tong_tien</td>
					            <td data-title="Key DVVC">fee</td>
					            <td data-title="Giá trị DVVC">Tổng phí</td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">trang_thai</td>
					            <td data-title="Key DVVC">weight</td>
					            <td data-title="Giá trị DVVC">1000</td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" title="Giao hàng tiết kiệm">Giao hàng tiết kiệm</a>
					            </td>
					            <td data-title="Key Japana">thoi_gian</td>
					            <td data-title="Key DVVC">tong_tien</td>
					            <td data-title="Giá trị DVVC">0</td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">thoi_gian</td>
					            <td data-title="Key DVVC">dich_vu</td>
					            <td data-title="Giá trị DVVC">0</td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">trang_thai</td>
					            <td data-title="Key DVVC">CODE_ORDER</td>
					            <td data-title="Giá trị DVVC">1</td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">tong_tien</td>
					            <td data-title="Key DVVC">CalculatedFee</td>
					            <td data-title="Giá trị DVVC">Tổng phí</td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" title="Giao hàng tiết kiệm">Giao hàng tiết kiệm</a>
					            </td>
					            <td data-title="Key Japana">dia_diem_ho_tro</td>
					            <td data-title="Key DVVC">fee.delivery</td>
					            <td data-title="Giá trị DVVC">phi</td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" title="Giao hàng nhanh">Giao hàng nhanh</a>
					            </td>
					            <td data-title="Key Japana">ma_van_don</td>
					            <td data-title="Key DVVC">OrderCode</td>
					            <td data-title="Giá trị DVVC"></td>
					            <td data-title="Loại">Đăng đơn</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" title="Tín tốc">Tín tốc</a>
					            </td>
					            <td data-title="Key Japana">tong_tien</td>
					            <td data-title="Key DVVC">totalFee</td>
					            <td data-title="Giá trị DVVC">Tổng tiền</td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="DVVC">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" title="Vietnampost">Vietnampost</a>
					            </td>
					            <td data-title="Key Japana">tong_tien</td>
					            <td data-title="Key DVVC">TongCuocSauVAT</td>
					            <td data-title="Giá trị DVVC">Tổng cước phát</td>
					            <td data-title="Loại">Tính phí vận chuyển</td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/att/shipper/deliveryconvertrespon/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/att/shipper/japanakey/add.php'); ?>
<script>
	jQuery(function(){
		
	})
</script>