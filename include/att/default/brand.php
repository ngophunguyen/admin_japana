<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 50%;
		float: left;
	}
	.box-quick-search .item:first-child input{
		width: 50%;
		float: left;
	}
	.box-quick-search .item:first-child button{
		float: left;
		margin-left: 15px;
	}	
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	}
	.table-custom tbody tr td{
		height: auto;
    	position: relative;
	}
	.table-custom tbody tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		padding: 15px 0;
	}
	.table-custom tr td img {
	    width: auto;
	    height: 60px;
	}
	.table-custom > tbody > tr > td a{
		float: left;
	}
	.table-custom > tbody > tr > td .checkmark{
		left: 50%;
		transform: translateX(-50%);
	}
	@media (max-width: 575.98px) {
		.box-quick-search{
			margin-top: 0;
		}
		.box-quick-search .item{
			width: 100%;
		}
		.box-quick-search .item:first-child input{
			width: 70%;
		}
		.box-quick-search .item:last-child{
			margin-top: 15px;
		}
		.table-custom tbody tr td:last-child{
			float: inherit;
			padding: 0;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-quick-search .item:first-child button{
	  		width: 30%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 70%;
	  	}
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.box-quick-search .item:first-child button{
	  		width: 30%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 70%;
	  	}
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="brand content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách thương hiệu</h1>
			<ul>
				<li>
					<a href="?action=include/att/default/brand/add.php" class="link-custom black-custom" title="Thêm thương hiệu">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm thương hiệu</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post" class="search1">
	                       <input autocomplete="off" name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm thương hiệu...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black center-custom">Hình ảnh</th>
					            <th class="bg-black">Thương hiệu</th>
					            <th class="bg-black center-custom">Hot</th>
					            <th class="bg-black">Link URL</th>
					            <th class="bg-black center-custom">Tình trạng</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/brand/1556163965-1-(17).jpeg" alt="Orihiro">
					            </td>
					            <td data-title="Thương hiệu">
					            	<a href="?action=include/att/default/brand/edit.php" title="Orihiro">
					                  	Orihiro
					                </a>
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      	<input value="" type="checkbox" checked="">
				                      	<span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Link URL">
					            	orihiro-nhat-ban-brand
					            </td>
					            <td data-title="Tình trạng" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/brand/1556163994-1-(8).jpeg" alt="Panasonic">
					            </td>
					            <td data-title="Thương hiệu">
					            	<a href="?action=include/att/default/brand/edit.php" title="Panasonic">
					                  	Panasonic
					                </a>
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      	<input value="" type="checkbox" checked="">
				                      	<span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Link URL">
					            	panasonic-nhat-ban-brand
					            </td>
					            <td data-title="Tình trạng" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/brand/1556164046-1-(9).jpeg" alt="Thermos">
					            </td>
					            <td data-title="Thương hiệu">
					            	<a href="?action=include/att/default/brand/edit.php" title="Thermos">
					                  	Thermos
					                </a>
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      	<input value="" type="checkbox" checked="">
				                      	<span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Link URL">
					            	thermos-nhat-ban-brand
					            </td>
					            <td data-title="Tình trạng" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/brand/1556164060-1-(10).jpeg" alt="Transino">
					            </td>
					            <td data-title="Thương hiệu">
					            	<a href="?action=include/att/default/brand/edit.php" title="Transino">
					                  	Transino
					                </a>
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      	<input value="" type="checkbox" checked="">
				                      	<span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Link URL">
					            	transino-nhat-ban-brand
					            </td>
					            <td data-title="Tình trạng" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/brand/1556164034-1-(11).jpeg" alt="SK-II">
					            </td>
					            <td data-title="Thương hiệu">
					            	<a href="?action=include/att/default/brand/edit.php" title="SK-II">
					                  	SK-II
					                </a>
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      	<input value="" type="checkbox" checked="">
				                      	<span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Link URL">
					            	sk-ii-nhat-ban-brand
					            </td>
					            <td data-title="Tình trạng" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/brand/1556163965-1-(17).jpeg" alt="Orihiro">
					            </td>
					            <td data-title="Thương hiệu">
					            	<a href="?action=include/att/default/brand/edit.php" title="Orihiro">
					                  	Orihiro
					                </a>
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      	<input value="" type="checkbox" checked="">
				                      	<span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Link URL">
					            	orihiro-nhat-ban-brand
					            </td>
					            <td data-title="Tình trạng" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/brand/1556163994-1-(8).jpeg" alt="Panasonic">
					            </td>
					            <td data-title="Thương hiệu">
					            	<a href="?action=include/att/default/brand/edit.php" title="Panasonic">
					                  	Panasonic
					                </a>
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      	<input value="" type="checkbox" checked="">
				                      	<span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Link URL">
					            	panasonic-nhat-ban-brand
					            </td>
					            <td data-title="Tình trạng" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/brand/1556164046-1-(9).jpeg" alt="Thermos">
					            </td>
					            <td data-title="Thương hiệu">
					            	<a href="?action=include/att/default/brand/edit.php" title="Thermos">
					                  	Thermos
					                </a>
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      	<input value="" type="checkbox" checked="">
				                      	<span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Link URL">
					            	thermos-nhat-ban-brand
					            </td>
					            <td data-title="Tình trạng" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/brand/1556164060-1-(10).jpeg" alt="Transino">
					            </td>
					            <td data-title="Thương hiệu">
					            	<a href="?action=include/att/default/brand/edit.php" title="Transino">
					                  	Transino
					                </a>
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      	<input value="" type="checkbox" checked="">
				                      	<span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Link URL">
					            	transino-nhat-ban-brand
					            </td>
					            <td data-title="Tình trạng" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/brand/1556164034-1-(11).jpeg" alt="SK-II">
					            </td>
					            <td data-title="Thương hiệu">
					            	<a href="?action=include/att/default/brand/edit.php" title="SK-II">
					                  	SK-II
					                </a>
					            </td>
					            <td data-title="Hot" class="center-custom">
					            	<label class="checkbox-custom">
				                      	<input value="" type="checkbox" checked="">
				                      	<span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Link URL">
					            	sk-ii-nhat-ban-brand
					            </td>
					            <td data-title="Tình trạng" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
			
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		if(window.innerWidth < 576){
			jQuery('.entry-content').css('margin-bottom','15px')
		}
	})
</script>