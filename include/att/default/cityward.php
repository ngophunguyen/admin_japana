<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 100%;
		float: left;
	}
	.box-quick-search .item button{
		float: left;
		margin-left: 15px
	}
	.select2{
		margin-right: 15px;
	}
	.box-quick-search .item input{
		width: 100%;
		float: left;
	}
	.box-quick-search .item button{
		width: 20%;
	}	
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}
	#s2id_id_cityzone{
		margin-left: 15px;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	}
	.table-custom tbody tr td{
		height: auto;
    	position: relative;
	}
	.table-custom tbody tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	.table-custom > tbody > tr > td a{
		float: left;
	}
	.table-custom > tbody > tr > td input{
		display: none;
		width: 250px;
	}
	@media (max-width: 575.98px) {
		.entry-header ul{
			display: none;
		}
		.box-quick-search{
			margin-top: 0;
		}
		.box-quick-search .item{
			width: 100%;
		}
		.box-quick-search .item input{
			width: 100%;
		}
		.box-quick-search .item{
			margin-top: 15px;
		}
		.search1{
			display: block;
		}
		.select2{
			margin-right: 0;
			margin-bottom: 15px;
		}
		.box-quick-search .item button{
			width: 48%;
			margin-left: 0;
			margin-top: 15px;
		}
		.box-quick-search .item button:last-child{
			margin-left: 4%;
		}
		.table-custom > tbody > tr > td input{
			width: 100%;
		}
		.table-custom tbody tr td:last-child{
			float: inherit;
			padding: 0;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-quick-search .item button{
	  		width: 30%;
	  	}
	  	.box-quick-search .item input{
	  		width: 70%;
	  	}
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.box-quick-search .item:first-child button{
	  		width: 30%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 70%;
	  	}
		.table-custom{
	  		white-space: nowrap;
	  	}
	  	.box-quick-search .item button{
			width: 50%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	  	.box-quick-search .item button{
			width: 50%;
		}
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="cityward content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách phường/xã</h1>
			<ul>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post" class="search1">
							<select class="form-control" name="id_city" id="id_city">
					    		<option value="-1">Chọn tỉnh/thành...</option>
							  	<option value="0">Thành phố Hà Nội</option>
							  	<option value="1">Thành phố Hồ Chí Minh</option>
							  	<option value="2">Thành phố Đà Nẵng</option>
							  	<option value="3">Thành phố Hải Phòng</option>
							  	<option value="4">Thành phố Cần Thơ</option>
							</select>
							<select class="form-control" name="s2id_id_cityzone" id="s2id_id_cityzone">
					    		<option value="-1">Chọn quận/huyện...</option>
					    		<option value="0">Quận Tân Phú</option>
							  	<option value="1">Quận Tân Bình</option>
							</select>
	                       <input autocomplete="off" name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tên phường/xã...">
	                       <button type="submit" class="button bg-green">Thêm mới</button>
	                       <button type="button" class="button bg-black">Lọc</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Tỉnh/thành</th>
					            <th class="bg-black">Quận/huyện</th>
					            <th class="bg-black">Phường/xã</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Tỉnh/thành">
									Thành phố Hà Nội
					            </td>
					            <td data-title="Quận/huyện">
									Quận Ba Đình
					            </td>
					            <td data-title="Phường/xã">
					                <input autocomplete="off" type="text" name="hide_name1" id="hide_name1" class="form-control" value="Phường Phúc Xá">
									<span>Phường Phúc Xá</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(1);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Tỉnh/thành">
									Thành phố Hà Nội
					            </td>
					            <td data-title="Quận/huyện">
									Quận Ba Đình
					            </td>
					            <td data-title="Phường/xã">
					                <input autocomplete="off" type="text" name="hide_name2" id="hide_name2" class="form-control" value="Phường Trúc Bạch">
									<span>Phường Trúc Bạch</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(2);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Tỉnh/thành">
									Thành phố Hà Nội
					            </td>
					            <td data-title="Quận/huyện">
									Quận Ba Đình
					            </td>
					            <td data-title="Phường/xã">
					                <input autocomplete="off" type="text" name="hide_name3" id="hide_name3" class="form-control" value="Phường Vĩnh Phúc">
									<span>Phường Vĩnh Phúc</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(3);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Tỉnh/thành">
									Thành phố Hà Nội
					            </td>
					            <td data-title="Quận/huyện">
									Quận Ba Đình
					            </td>
					            <td data-title="Phường/xã">
					                <input autocomplete="off" type="text" name="hide_name4" id="hide_name4" class="form-control" value="Phường Cống Vị">
									<span>Phường Cống Vị</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(4);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Tỉnh/thành">
									Thành phố Hà Nội
					            </td>
					            <td data-title="Quận/huyện">
									Quận Ba Đình
					            </td>
					            <td data-title="Phường/xã">
					                <input autocomplete="off" type="text" name="hide_name5" id="hide_name5" class="form-control" value="Phường Liễu Giai">
									<span>Phường Liễu Giai</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(5);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Tỉnh/thành">
									Thành phố Hà Nội
					            </td>
					            <td data-title="Quận/huyện">
									Quận Ba Đình
					            </td>
					            <td data-title="Phường/xã">
					                <input autocomplete="off" type="text" name="hide_name6" id="hide_name6" class="form-control" value="Phường Nguyễn Trung Trực">
									<span>Phường Nguyễn Trung Trực</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(6);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Tỉnh/thành">
									Thành phố Hà Nội
					            </td>
					            <td data-title="Quận/huyện">
									Quận Ba Đình
					            </td>
					            <td data-title="Phường/xã">
					                <input autocomplete="off" type="text" name="hide_name7" id="hide_name7" class="form-control" value="Phường Quán Thánh">
									<span>Phường Quán Thánh</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(7);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Tỉnh/thành">
									Thành phố Hà Nội
					            </td>
					            <td data-title="Quận/huyện">
									Quận Ba Đình
					            </td>
					            <td data-title="Phường/xã">
					                <input autocomplete="off" type="text" name="hide_name8" id="hide_name8" class="form-control" value="Phường Ngọc Hà">
									<span>Phường Ngọc Hà</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(8);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Tỉnh/thành">
									Thành phố Hà Nội
					            </td>
					            <td data-title="Quận/huyện">
									Quận Ba Đình
					            </td>
					            <td data-title="Phường/xã">
					                <input autocomplete="off" type="text" name="hide_name9" id="hide_name9" class="form-control" value="Phường Điện Biên">
									<span>Phường Điện Biên</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(9);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Tỉnh/thành">
									Thành phố Hà Nội
					            </td>
					            <td data-title="Quận/huyện">
									Quận Ba Đình
					            </td>
					            <td data-title="Phường/xã">
					                <input autocomplete="off" type="text" name="hide_name10" id="hide_name10" class="form-control" value="Phường Đội Cấn">
									<span>Phường Đội Cấn</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(10);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
			
		</div>
	</article>
</main>
<script>
	function editItem(id) {
	    jQuery('#hide_name' + id).css('display', 'block');
	    jQuery('#hide_name' + id).next().css('display', 'none');
	}
	jQuery(function(){
		jQuery('#id_city,#s2id_id_cityzone').select2();
		if(window.innerWidth < 576){
			jQuery('.entry-content').css('margin-bottom','15px')
		}
	})
</script>