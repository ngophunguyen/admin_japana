<style>
	.editset .title{
		margin: 15px 0 0;
	}
	.box-editset .item{
		width: 49%;
		float: left;
		margin-top: 15px;
	}
	.box-editset .item:last-child{
		margin-left: 2%;
	}
	.box-editset .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.box-img-meta{
		display: none;
		margin-top: 15px;
	}
	.box-img-meta img{
		width: 100%;
	}
	.table-custom{
		margin-top: 0;
	}
	#name{
		margin-bottom: 15px;
	}
	.input-group{
		flex-wrap: initial;
	}
	.box-editset .item .custom-upload{
		width: 15%;
	}
	@media (max-width: 575.98px) {
		.box-editset .item{
			width: 100%
		}
		.box-editset .item:last-child{
			margin-left: 0;
		}
		.box-editset .item .custom-upload{
			width: 30%;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="editset content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chi tiết thuộc tính</h1>
			<ul>
				<li>
					<button type="button" class="button button-header link-custom black-custom">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
                    </button>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-editset">
					<div class="item">
				    	<label for="name">Giá trị:</label>
				    	<input autocomplete="off" type="text" name="name" id="name" placeholder="Nhập giá trị..." class="form-control" required="">
				    	<label for="imgs">Hình ảnh:</label>
				    	<div class="upload-img">
                            <div class="input-group up-img">
                                <input autocomplete="off" type="text" id="imgs" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
                                <label class="button bg-green custom-upload">
                                    <input type="file" class="form-control" name="icon" id="icon" onchange="readURL(this,1);" accept="image/*">Upload
                                </label>
                                <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
                            </div>
                        </div>
                        <div class="box-img-meta">
                        	<img src="" alt="hình ảnh" id="photo1">
                        </div>
				    </div>
				    <div class="item">
				    	<label>Thuộc tính [ Tiêu chí ]</label>
				    	<div class="box-table">
							<table class="table table-custom table-striped table-responsive">
							    <thead class="bg-black">
							        <tr class="bg-black">
							            <th class="bg-black center-custom">STT</th>
							            <th class="bg-black">Giá trị</th>
							            <th class="bg-black">Hình ảnh</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							            <td data-title="STT" class="center-custom">1</td>
							            <td data-title="Giá trị">
							            	<a href="javascript:void(0);" title="Tên sản phẩm">Tên sản phẩm</a>
							            </td>
							            <td data-title="Hình ảnh"></td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">2</td>
							            <td data-title="Giá trị">
							            	<a href="javascript:void(0);" title="Hình ảnh">Hình ảnh</a>
							            </td>
							            <td data-title="Hình ảnh"></td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">3</td>
							            <td data-title="Giá trị">
							            	<a href="javascript:void(0);" title="Xuất xứ">Xuất xứ</a>
							            </td>
							            <td data-title="Hình ảnh"></td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">4</td>
							            <td data-title="Giá trị">
							            	<a href="javascript:void(0);" title="Dung tích">Dung tích</a>
							            </td>
							            <td data-title="Hình ảnh"></td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">5</td>
							            <td data-title="Giá trị">
							            	<a href="javascript:void(0);" title="Giấy CBSP checkcosmetic.net">Giấy CBSP checkcosmetic.net</a>
							            </td>
							            <td data-title="Hình ảnh"></td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">6</td>
							            <td data-title="Giá trị">
							            	<a href="javascript:void(0);" title="SP chính hãng có tem nhãn phụ">SP chính hãng có tem nhãn phụ</a>
							            </td>
							            <td data-title="Hình ảnh"></td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">7</td>
							            <td data-title="Giá trị">
							            	<a href="javascript:void(0);" title="Trên 50% hoặc ít nhất trước 8 tháng khi hết HSD">Trên 50% hoặc ít nhất trước 8 tháng khi hết HSD</a>
							            </td>
							            <td data-title="Hình ảnh"></td>
							        </tr>
							    </tbody>
							</table>
						</div>
				    </div>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	function readURL(input,id) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	        	jQuery('#photo'+id).parent().css('display','block');
	          	jQuery('#photo'+id).attr('src', e.target.result);
	        };
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	jQuery(function(){
		jQuery(document).on('change', ':file', function() {
		    var input = jQuery(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		});
		jQuery(':file').on('fileselect', function(event, numFiles, label) {
          	var input = jQuery(this).parents('.up-img').find(':text'),
              	log = numFiles > 1 ? numFiles + ' files selected' : label;
          	if( input.length ) {
              	input.val(log);
              	if(input.val(log)){
                	jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
                	jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
              	}
          	} else {
              	if(log) ;
          	}
      	});
      	jQuery('.delete-img').on('click', function(e){
		    jQuery(this).parent().find('input[type=file]').val('');
		    jQuery(this).parent().find('input[type=text]').val('');
		    jQuery(this).parent().find('.custom-upload').css('display','flex');
		    jQuery('.box-img-meta').css('display','none');
		    jQuery(this).css('display','none');
		});
	})
</script>