<style>
	.table-custom tbody tr td input{
		width: 150px;
		margin: 0 auto;
	}
	@media (max-width: 575.98px) {
		.table-custom tbody tr td input{
			width: 100%;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="status content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách trạng thái</h1>
			<ul>
				<li>
					<a href="?action=include/att/order/status/add.php" class="link-custom black-custom" title="Thêm trạng thái">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<li>
                    <a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
                    	<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
                    </a>
                 </li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">ID</th>
					            <th class="bg-black">Tên trạng thái</th>
					            <th class="bg-black center-custom">Vị trí</th>
					            <th class="bg-black">Trạng thái liên kết</th>
					            <th class="bg-black center-custom">Độ ưu tiên</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Mới đặt">Mới đặt</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(1);" class="form-control" value="1" name="stt" id="stt-1">
					            </td>
					            <td data-title="Liên kết">Đã xác nhận, Hủy đơn</td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="1" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Đã xác nhận">Đã xác nhận</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(2);" class="form-control" value="2" name="stt" id="stt-2">
					            </td>
					            <td data-title="Liên kết">Chờ hàng không ngày, Có hàng, Hết hàng, Chờ duyệt, Hủy đơn</td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="1" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Có hàng">Có hàng</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(3);" class="form-control" value="3" name="stt" id="stt-3">
					            </td>
					            <td data-title="Liên kết">Chờ hàng có ngày, Có hàng, Hết hàng, Chờ duyệt, Hủy đơn</td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="2" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Chờ hàng không ngày">Chờ hàng không ngày</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(4);" class="form-control" value="4" name="stt" id="stt-4">
					            </td>
					            <td data-title="Liên kết">Chờ hàng không ngày, Chờ hàng có ngày, Có hàng, Hết hàng, Hủy đơn</td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="4" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Chờ hàng có ngày">Chờ hàng có ngày</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(5);" class="form-control" value="5" name="stt" id="stt-5">
					            </td>
					            <td data-title="Liên kết">Chờ hàng có ngày, Có hàng, Hết hàng, Hủy đơn</td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="5" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Hết hàng">Hết hàng</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(6);" class="form-control" value="6" name="stt" id="stt-6">
					            </td>
					            <td data-title="Liên kết">Chờ hàng có ngày, Có hàng, Hết hàng, Hủy đơn</td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="1" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Chờ duyệt">Chờ duyệt</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(7);" class="form-control" value="7" name="stt" id="stt-7">
					            </td>
					            <td data-title="Liên kết">Lập lệnh xuất, Trả phiếu</td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="1" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Lập lệnh xuất">Lập lệnh xuất</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(8);" class="form-control" value="8" name="stt" id="stt-8">
					            </td>
					            <td data-title="Liên kết">Đang giao hàng, Trả hàng</td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="1" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Đang giao hàng">Đang giao hàng</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(9);" class="form-control" value="9" name="stt" id="stt-9">
					            </td>
					            <td data-title="Liên kết">Hoàn thành, Trả hàng</td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="1" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">11</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Hoành thành">Hoành thành</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(10);" class="form-control" value="10" name="stt" id="stt-10">
					            </td>
					            <td data-title="Liên kết">Trả hàng</td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="1" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">12</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Trả hàng">Trả hàng</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(11);" class="form-control" value="11" name="stt" id="stt-11">
					            </td>
					            <td data-title="Liên kết"></td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="1" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">13</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Hủy đơn">Hủy đơn</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(12);" class="form-control" value="12" name="stt" id="stt-12">
					            </td>
					            <td data-title="Liên kết"></td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="1" name="priority" id="priority-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">14</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/status/edit.php" title="Trả phiếu">Trả phiếu</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(13);" class="form-control" value="13" name="stt" id="stt-13">
					            </td>
					            <td data-title="Liên kết"></td>
					            <td data-title="Độ ưu tiên" class="center-custom">
					            	<input type="number" class="form-control" value="1" name="priority" id="priority-1">
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){

	})
</script>