<style>
	.box-addstatus{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-addstatus .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.box-addstatus .item:first-child{
		width: 10%;
		float: left;
		margin-right: 2%;
	}
	.box-addstatus .item:last-child{
		width: 88%;
		float: left;
	}
	@media (max-width: 575.98px) {
		.box-addstatus .item{
			width: 100%!important;
			margin: 0 0 15px!important;
			float: left;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="editstatusarises content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chỉnh sửa trạng thái</h1>
			<ul>
				<li>
					<button type="button" class="button button-header link-custom black-custom">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
                    </button>
				</li>
				<li>
					<a href="?action=include/att/order/status.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-addstatus">
					<div class="item">
						<label for="stt">Vị trí</label>
						<input autocomplete="off" type="text" placeholder="Nhập vị trí..." value="1" id="stt" name="stt" class="form-control">
					</div>
					<div class="item">
						<label for="name">Tên trạng thái</label>
						<input autocomplete="off" type="text" value="Hủy kế toán" id="name" placeholder="Nhập tên trạng thái..." name="name" class="form-control">
					</div>
				</div>
				
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		jQuery('#s2id_status_link').select2();
	})
</script>