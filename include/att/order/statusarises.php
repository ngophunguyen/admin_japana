<style>
	.table-custom tbody tr td input{
		width: 150px;
		margin: 0 auto;
	}
	@media (max-width: 575.98px) {
		.table-custom tbody tr td input{
			width: 100%;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="statusarises content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Trạng thái phát sinh</h1>
			<ul>
				<li>
					<a href="?action=include/att/order/statusarises/add.php" class="link-custom black-custom" title="Thêm trạng thái">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<li>
                    <a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
                    	<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
                    </a>
                 </li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">ID</th>
					            <th class="bg-black">Tên trạng thái</th>
					            <th class="bg-black center-custom">Vị trí</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/statusarises/edit.php" title="Hủy kế toán">Hủy kế toán</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(1);" class="form-control" value="1" name="stt" id="stt-1">
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Tên trạng thái">
					            	<a href="?action=include/att/order/statusarises/edit.php" title="Trả hàng 1 phần">Trả hàng 1 phần</a>
					            </td>
					            <td data-title="Vị trí" class="center-custom">
					            	<input type="number" onchange="updateStt(2);" class="form-control" value="2" name="stt" id="stt-2">
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){

	})
</script>