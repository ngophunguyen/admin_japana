<style>
	.box-addstatus{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-addstatus .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.box-addstatus .item:first-child{
		width: 10%;
		float: left;
		margin-right: 2%;
	}
	.box-addstatus .item:nth-child(2){
		width: 29%;
		margin-right: 2%;
		float: left;
	}
	.box-addstatus .item:nth-child(3){
		width: 40%;
		margin-right: 2%;
		float: left;
	}
	.box-addstatus .item:last-child{
		width: 15%;
		float: left;
	}
	.select2-container .select2-selection--multiple{
		min-height: 38px;
	}
	.select2-container--default.select2-container--focus .select2-selection--multiple,.select2-container--default .select2-selection--multiple{
		border: 1px solid #ced4da;
	}
	.select2-container--default .select2-selection--multiple .select2-selection__choice{
		padding: 7px;
	}
	@media (max-width: 575.98px) {
		.box-addstatus .item{
			width: 100%!important;
			margin: 0 0 15px!important;
			float: left;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="editstatus content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chỉnh sửa trạng thái</h1>
			<ul>
				<li>
					<button type="button" class="button button-header link-custom black-custom">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
                    </button>
				</li>
				<li>
					<a href="?action=include/att/order/status.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-addstatus">
					<div class="item">
						<label for="stt">Vị trí</label>
						<input autocomplete="off" type="text" placeholder="Nhập vị trí..." value="1" id="stt" name="stt" class="form-control">
					</div>
					<div class="item">
						<label for="name">Tên trạng thái</label>
						<input autocomplete="off" type="text" value="Mới đặt" id="name" placeholder="Nhập tên trạng thái..." name="name" class="form-control">
					</div>
					<div class="item">
						<label for="s2id_status_link">Trạng thái liên kết</label>
						<select class="form-control" name="s2id_status_link[]" id="s2id_status_link" multiple="multiple">
						  	<option value="0">Mới đặt</option>
						  	<option value="1" selected="selected">Đã xác nhận</option>
						  	<option value="2">Có hàng</option>
						  	<option value="3">Chờ hàng không ngày</option>
						  	<option value="4">Chờ hàng có ngày</option>
						  	<option value="5">Hết hàng</option>
						  	<option value="6">Chờ duyệt</option>
						  	<option value="7">Lập lệnh xuất</option>
						  	<option value="8">Đang giao hàng</option>
						  	<option value="9">Hoàn thành</option>
						  	<option value="10">Trả hàng</option>
						  	<option value="11" selected="selected">Hủy đơn</option>
						  	<option value="12">Trả phiếu</option>
						</select>
					</div>
					<div class="item">
						<label for="s2id_status_link">Hiển thị</label>
						<div class="custom-dropdown">
					    	<select class="form-control" name="show" id="show">
					    		<option value="-1">Chọn trạng thái hiển thị</option>
							  	<option value="0">Đơn hàng</option>
							  	<option value="1">Sản phẩm</option>
							  	<option value="2">Không hiển thị</option>
							  	<option value="3">Không hiển thị bộ lọc</option>
							</select>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		jQuery('#s2id_status_link').select2();
	})
</script>