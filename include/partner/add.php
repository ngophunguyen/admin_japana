<style>
	.box-addncc{
    display: inline-block;
    width: 100%;
  }
  .box-addncc .item{
    width: 49%;
    float: left;
    margin-bottom: 15px;
  }
  .box-addncc .item:nth-child(even){
    margin-left: 2%;
  }
  .box-addncc .item label{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 5px;
    font-weight: 500;
    width: 100%;
  }
  @media (max-width: 575.98px) {
    .box-addncc .item{
      width: 100%
    }
    .box-addncc .item:nth-child(even){
      margin-left: 0;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {  
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal medium-modal" id="addncc-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Thêm nhà cung cấp
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
      	<div class="box-addncc">
      		<div class="item">
      			<label for="id_ncc">Mã nhà cung cấp</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="id_ncc" name="code" placeholder="Nhập mã">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="name_ncc">Tên nhà cung cấp</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="name_ncc" name="name" placeholder="Nhập tên nhà cung cấp">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="name_user_ncc">Người đại diện</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="name_user_ncc" name="fullname" placeholder="Nhập tên">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="number_ncc">Số điện thoại liên hệ</label>
      			<div class="box-input-addpro">
	      			<input type="number" class="form-control" id="number_ncc" name="phone" placeholder="Nhập số điện thoại">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="mail_ncc">Email</label>
      			<div class="box-input-addpro">
	      			<input type="email" class="form-control" id="mail_ncc" name="email" placeholder="Nhập mail">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="addr_ncc">Địa chỉ</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="addr_ncc" name="address" placeholder="Nhập địa chỉ">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="bank_ncc">Số tài khoản</label>
      			<div class="box-input-addpro">
	      			<input type="number" class="form-control" id="bank_ncc" name="bank" placeholder="Nhập số tài khoản ngân hàng">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="name_bank_ncc">Tại ngân hàng</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="name_bank_ncc" name="bank_name" placeholder="Nhập tên ngân hàng và chi nhánh">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="mst">Mã số thuế</label>
      			<div class="box-input-addpro">
	      			<input type="number" class="form-control" id="mst" name="mst" placeholder="Nhập mã số thuế">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="note">Ghi chú</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="note" name="note" placeholder="Nhập ghi chú">
	      		</div>
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="button bg-black">Hủy</button>
        <button type="button" class="button bg-black">Lưu</button>
      </div>
    </div>
  </div>
</div>
<script>
	jQuery(function(){
		
	})
</script>