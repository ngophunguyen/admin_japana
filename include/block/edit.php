<style>
	.style-block label, .name-block .item label, .images-block .item label, .sku-block label, .images-more-size .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.style-block, .title, .name-block, .images-block, .images-more-size, .images-block .box-input, .box-input{
		margin-top: 10px;
	}
	.style-block .custom-dropdown, .style-block .custom-dropdown select{
		display: inline-block;
		width: auto;
	}
	.style-block .custom-dropdown:after{
		right: 5px;
	}
	.name-block, .images-block, .images-more-size{
		display: inline-block;
		width: 100%;
	}
	.title{
		font-size: 14px;
		border-bottom: 1px solid #222D32;
		display: inline-block;
	}
	.images-more-size{
		margin-top: 0;
	}
	.name-block .item, .images-block .item, .images-more-size .item{
		width: 49%;
		float: left;
		display: inline-block;
	}
	.name-block .item:last-child, .images-block .item:last-child, .images-more-size .item:last-child{
		margin-left: 2%;
	}
	.images-block .item label:first-child, .images-more-size .item label:first-child{
		margin-bottom: 0;
		height: 30px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
    	-webkit-box-align: center;
    	    -ms-flex-align: center;
    	        align-items: center;
	}
	.images-block .item label:first-child i, .images-more-size .item label:first-child i{
		margin-right: 5px;
	}
	.images-block .item:first-child label:first-child i, .images-more-size .item:first-child label:first-child i{
		font-size: 25px;
	}
	.images-block .item:last-child label:first-child i, .images-more-size .item:last-child label:first-child i{
		font-size: 30px;
	}
	.images-block .item img{
		width: 100%;
		height: auto;
		margin: 5px 0;
	}
	.upload-photo {
	    display: none;
	    position: absolute;
	    z-index: -1;
	}
	.sku-block{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		width: 50%;
	}
	.sku-block label{
		display: inline-block;
		float: left;
		width: auto;
		margin: 0;
		margin-right: 10px;
	}
	.sku-block input{
		float: left;
		width: 60%;
	}
	.images-block .box-input{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: baseline;
		    -ms-flex-align: baseline;
		        align-items: baseline;
		-webkit-box-pack: end;
		    -ms-flex-pack: end;
		        justify-content: flex-end;
		margin-bottom: 10px;
	}
	.images-block .box-input input{
		margin-left: 10px;
	}
	.images-more-size .item{
		margin-bottom: 15px;
	}
	.box-input{
		position: relative;
	}
	.size-input{
		width: 85%;
	}
	.images-more-size .item a{
		position: absolute;
		right: 0;
		bottom: 50%;
		-webkit-transform: translate(0,50%);
		    -ms-transform: translate(0,50%);
		        transform: translate(0,50%);
	}
	@media (max-width: 575.98px) {
		.style-block .custom-dropdown, 
		.style-block .custom-dropdown select, 
		.name-block .item, 
		.sku-block,
		.images-block .item, 
		.images-more-size .item{
			width: 100%;
		}
		.name-block .item:last-child, .images-block .item:last-child{
			margin-left: 0;
			margin-top: 10px;
		}
		.btn-create-input{
			padding-bottom: 15px;
		}
		.name-block .item:last-child, .images-block .item:last-child, .images-more-size .item:last-child{
			margin-left: 0;
		}
		.sku-block input{
			width: 40%;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.sku-block{
			width: 100%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.sku-block{
			width: 100%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {

	}
</style>
<main class="block content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chỉnh sửa block</h1>
			<ul>
				<li>
					<button type="button" class="button button-header link-custom black-custom">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
                    </button>
				</li>
				<li>
					<a href="?action=block.php" class="link-custom red-custom" title="Thoát">
						<i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="style-block">
					<label for="type">Kiểu block:</label>
			    	<div class="custom-dropdown">
			    		<select class="form-control" name="type" id="type">
						  	<option value="1" selected="">Header</option>
							<option value="2">Footer</option>
							<option value="3">Block không edit không xóa</option>
							<option value="4">Block không edit được xóa</option>
							<option value="5">Block được edit được xóa</option>
							<option value="6">Ẩn Block</option>
						</select>
			    	</div>
				</div>
				<div class="name-block">
					<div class="item">
						<label for="type">Tên block:</label>
						<input autocomplete="off" type="text" name="name" id="name" value="Countdown banner 4 hình" placeholder="Nhập tên block" class="form-control">
					</div>
					<div class="item">
						<label for="type">Tên code:</label>
						<input autocomplete="off" type="text" name="name_code" id="name_code" value="Countdown1" placeholder="Nhập tên code" class="form-control">
					</div>
				</div>
				<div class="images-block">
					<div class="item">
						<label> 
							<i class="fa fa-desktop" aria-hidden="true"></i> Desktop
						</label>
						<img src="assets/images/no-img2.png" id="photo-1" alt="images">
						<label for="uplphoto-1"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tải ảnh</label>
						<input type="file" name="images_desktop" class="upload-photo" id="uplphoto-1" onchange="readURLimg(this,1);">
					</div>
					<div class="item">
						<label> 
							<i class="fa fa-mobile" aria-hidden="true"></i> Mobile
						</label>
						<img src="assets/images/no-img2.png" id="photo-2" alt="images">
						<label for="uplphoto-2"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tải ảnh</label>
						<input type="file" name="images_mobile" class="upload-photo" id="uplphoto-2" onchange="readURLimg(this,2);">
					</div>
				</div>
				<div class="sku-block">
					<label class="checkbox-custom"> Nhập nhiều SKU:
	                  	<input value="" id="sku" name="multi_sku" type="checkbox">
	                  	<span class="checkmark"></span>
	                </label>
					<input autocomplete="off" type="text" name="sku_quantity" value="0" class="form-control" placeholder="Nhập số lượng SKU">
				</div>
				<div class="images-block">
					<div class="item">
						<label> 
							<i class="fa fa-desktop" aria-hidden="true"></i> Desktop
						</label>
						<span class="box-input">
							Images:
							<input autocomplete="off" type="text" name="multi_images_mobile" id="multi_images_mobile" placeholder="Nhập kích thước hình ảnh" value="580x220" class="form-control">
						</span>
						<label class="checkbox-custom"> Hiển thị thời gian
							<input value="" id="t-pc" name="t-pc" type="checkbox">
		                  	<span class="checkmark"></span>
		                </label>
					</div>
					<div class="item">
						<label> 
							<i class="fa fa-mobile" aria-hidden="true"></i> Mobile
						</label>
						<span class="box-input">
							Images:
							<input autocomplete="off" type="text" name="multi_images_desktop" id="multi_images_desktop" placeholder="Nhập kích thước hình ảnh" value="780x260" class="form-control">
						</span>
						<label class="checkbox-custom"> Hiển thị thời gian
		                  	<input value="" id="t-mobile" name="t-mobile" type="checkbox">
		                  	<span class="checkmark"></span>
		                </label>
					</div>
				</div>
				<h3 class="title">Thêm hình ảnh có nhiều kích thước:</h3>
				<div class="images-more-size">
					<div class="item">
						<label> 
							<i class="fa fa-desktop" aria-hidden="true"></i> Desktop
						</label>
					</div>
					<div class="item">
						<label> 
							<i class="fa fa-mobile" aria-hidden="true"></i> Mobile
						</label>
					</div>
				</div>
				<a href="javascript:void(0);" class="btn-create-input black-custom bold left-custom" title="Thêm">
					<i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm
				</a>
			</div>
		</div>
	</article>
</main>
<script>
	function readURLimg(input,id) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	          	jQuery('#photo-'+id).attr('src', e.target.result);
	        };
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	function initDeleteInput(id){
	    jQuery('#pc-'+id).parent().remove();
	    jQuery('#mobile-'+id).parent().remove();
	    if(window.innerWidth > 576) {
	    	if(jQuery('.nav-primary').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight() && jQuery('.nav-primary').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height','auto');
			}else if(jQuery('body').outerHeight() > jQuery('.nav-primary').outerHeight() && jQuery('body').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
	    }
	}
	jQuery(function(){
		var id = jQuery('.images-more-size .size-input').length;
	  	jQuery('.btn-create-input').click(function(){ 
		    id = id + 1;
		    jQuery(".images-more-size .item:first-child").append(
		    	`<div class="box-input">
		    		<input type="text" id="pc-`+id+`" name="images_size_desktop[]" value="" placeholder="Nhập kích thước PC-`+id+`" class="form-control size-input">
		    	</div>`
		    );
		    jQuery(".images-more-size .item:last-child").append(
		    	`<div class="box-input">
		    		<input type="text" id="mobile-`+id+`" name="images_size_mobile[]" value="" placeholder="Nhập kích thước Mobile-`+id+`" class="form-control size-input">
		    		<a href="javascript:void(0);" onclick='initDeleteInput(`+id+`)' class="red-custom bold" title="Xóa">
						<i class="fa fa-times" aria-hidden="true"></i> Xóa
					</a>
		    	</div>`
		    );
		    if(window.innerWidth > 576) {
		    	if(jQuery('.nav-primary').outerHeight() > jQuery('body').outerHeight() && jQuery('.nav-primary').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight()){
					jQuery('.nav-primary').css('height','auto');
				}else if(jQuery('body').outerHeight() > jQuery('.nav-primary').outerHeight() && jQuery('body').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight()){
					jQuery('.nav-primary').css('height',jQuery('body').outerHeight());
				}
				else{
					jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
				}
		    }
		});
	})
</script>