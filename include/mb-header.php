<style>
.site-header{
	position:fixed!important;
	top:0;
	left:0;
	width:100%;
	background:#fff;
	z-index:999
}
.icon-nav,.user-top{
	display:block;
	position:absolute;
	top:14px
}
.user-top{
	display:-webkit-box;
	display:-ms-flexbox;
	display:flex;
	-webkit-box-align: center;
	    -ms-flex-align: center;
	        align-items: center;
	-webkit-box-pack: center;
	    -ms-flex-pack: center;
	        justify-content: center;
	right: 0;
    top: 0;
    width: 48px;
    height: 48px;
    padding: 7px;
}
.user-top i{
	font-size: 25px;
}
.user-top img{
	width: 100%;
	height: 100%;
	border-radius: 100%;
	-o-object-fit: cover;
	   object-fit: cover;
}
.top-header{
	position:relative;
	display:-webkit-box;
	display:-ms-flexbox;
	display:flex;
	-webkit-box-align:center;
	-ms-flex-align:center;
	align-items:center;
	-webkit-box-pack:justify;
	-ms-flex-pack:justify;
	justify-content:space-between
}
.icon-nav{
	left:0;
	top:0;
	display:-webkit-box;
	display:-ms-flexbox;
	display:flex;
	-webkit-box-align:center;
	-ms-flex-align:center;align-items:center;
	height: 48px;
    width: 48px;
}
.icon-line{
	display:block;
	position:relative;
	height:1px;
	-webkit-transition:.3s;
	-o-transition:.3s;
	transition:.3s;
	width:30px;
	margin:0 auto
}
.icon-line span{
	background:#21201e;
	border-radius:5px;
	display:block;
	position:relative;
	height:1px;
	-webkit-transition:.3s;
	-o-transition:.3s;
	transition:.3s
}
.icon-line span:before,.icon-line span:after{
	content:'';
	position:absolute;
	left:0;
	top:0;
	width:100%;
	height:100%;
	background:#21201e;
	display:block
}
.icon-line span:before{
	-ms-transform:translate(0,-10px);
	-webkit-transform:translate(0,-10px);
	transform:translate(0,-10px)
}
.icon-line span:after{
	-ms-transform:translate(0,10px);
	-webkit-transform:translate(0,10px);
	transform:translate(0,10px)
}
.side-header-info .box-item .box-user img{
	width: 100%;
	height: 100%;
	border-radius: 100%;
}

.side-header-info .box-item .box-user{
	color: #999999;
	background: url(assets/images/bg-header.png) no-repeat;
	background-size: auto 100%;
	text-align: center;
	display: inline-block;
	width: 100%;
	padding: 15px 0;
}

.side-header-info .box-item .box-user .box-img{
	font-size: 30vw;
	width: 37vw;
	height: 37vw;
	border-radius: 100%;
	margin: 0 auto;
}

.side-header-info .box-item .box-user p{
  	font-size: 4.5vw;
  	color: rgba(255,255,255,0.8);
  	font-weight: 300;
  	margin: 10px 0 0;
  	background: #222D32;
	display: inline-block;
	padding: 5px 10px;
	border-radius: 15px;
	border: 1px solid rgba(0,0,0,0.1);
}

.side-header-info{
	display: none;
	position: fixed;
	top: 0;
	width: 100%;
	height: 100%;
	z-index: 999;
}

.side-header-info .box-item{
	width: 85%;
	text-align: center;
	height: 100%!important;
	background: #222D32;
	z-index: 999;
	float: right;
	overflow-y: scroll;
	overflow-x: hidden;
	position: relative;
}

.side-header-info .box-item ul{
  	text-align: left;
}

.side-header-info .box-item ul li{
	padding: 2.3vh 2vh;
	border-bottom: .7px solid rgba(255,255,255,0.2);
}

.side-header-info .box-item ul li:last-child{
	border-bottom: 0;
}

.side-header-info .box-item ul li a span{
  	margin-left: 10px;
}

.side-header-info .box-item ul li a{
	font-weight: 300;
	font-size: 4vw;
	color: rgba(255,255,255,0.8);
}
.closed-mb2{
	text-align: left;
    background: transparent;
    border: 0;
	padding: 10px 0;
	padding-right: 0;
    padding-left: 3.5vw;
	color: rgba(255,255,255,0.8);
}
.closed-mb2 span{
	font-size: 8.5vw;
}
.entry-footer2{
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    -webkit-box-pack: center;
        -ms-flex-pack: center;
            justify-content: center;
    border-top: .7px solid rgba(255,255,255,0.2);
    height: 48px;
    width: 85%;
    position: fixed;
    bottom: 0;
    right: 0;
    z-index: 999;
    color: rgba(255,255,255,0.8);
    font-size: 4vw;
}

@media (max-width: 575.98px) {

}
@media (min-width: 576px) and (max-width: 767.98px) {
  	.user-top{
		display: none;
	}
}
@media (min-width: 768px) and (max-width: 991.98px) {
	.site-header{
		display: none;
	}
  	.user-top{
		display: none;
	}
}
@media (min-width: 992px) and (max-width: 1199.98px) {
	.site-header{
		display: none;
	}
  	.user-top{
		display: none;
	}
}
@media (min-width: 1200px) {
	.site-header{
		display: none;
	}
  	.user-top{
		display: none;
	}
}
</style>
<header class="site-header" data-hide-during-focus="false">
	<div class="top-header">
		<div class="container">
			<div class="icon-nav">
				<span class="icon-line">
					<span></span>
				</span>
			</div>
			<div class="user-top">
				<a href="javascript:void(0);" title="User">
					<img src="assets/images/user.jpg" alt="Thùy Vân Nguyễn">
					<!-- <i class="fa fa-user-circle" aria-hidden="true"></i> -->
				</a>
			</div>
		</div>
	</div>
</header>
<div class="side-header-info">
	<div class="bg-nav"><button type="button" class="closed-mb2"><span class="ti-close"></span></button></div>
	<div class="box-item">
		<div class="box-user">
			<div class="box-img">
				<img src="assets/images/user.jpg" alt="Thùy Vân Nguyễn">
			</div>
			<p>Thùy Vân Nguyễn</p>
		</div>
		
		<ul>
			<li>
				<a href="?action=profile.php" title="Quản lý tài khoản">
					<i class="fa fa-user" aria-hidden="true"></i>
					<span>Quản lý tài khoản</span>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);" title="Đăng xuất">
					<i class="fa fa-sign-out" aria-hidden="true"></i>
					<span>Đăng xuất</span>
				</a>
			</li>
		</ul>
		<div class="entry-footer2">
			Copyright © 2019 Japana Việt Nam
		</div>
	</div>
</div>
<script>
	jQuery('.user-top').click(function(){
		jQuery('.side-header-info').toggle('slide', { direction: 'right' }, 300);
		jQuery('body').css('overflow','hidden');
	});
	jQuery('.closed-mb2').click(function(){
		jQuery('.user-top').trigger('click', true);
		jQuery('body').css('overflow','auto');
	});
</script>