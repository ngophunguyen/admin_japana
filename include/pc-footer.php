<style>
	.entry-footer{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	    padding: 8px 20px;
	    border-top: 2px solid #f1f1f1;
	    height: 48px;
	    position: fixed;
	    bottom: 0;
	    left: 6%;
	    right: 0;
	    z-index: 999;
	    background: #fff;
	    display: none;
	}

	@media (max-width: 575.98px) {
	  	.entry-footer{
	  		display: none;
	  	}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	  	.entry-footer{
	  		left: 10%;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
	  	.entry-footer{
	  		left: 10%;
	  	}
	  	.box-revenue .item .price{
	  		font-size: 1.8vw;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.entry-footer{
	  		left: 10%;
	  	}
	  	.box-revenue .item .price{
	  		font-size: 1.8vw;
	  	}
	}
	@media (min-width: 1200px) {
  	
	}
</style>
<footer class="entry-footer">
	© 2019 - Bản quyền của Công Ty Cổ Phần Japana Việt Nam - Japana.vn
</footer>
<script>
	jQuery(function(){
		if(window.innerWidth > 576) {
			jQuery('.entry-footer').css('display','flex');
	    }
	});
 
</script>