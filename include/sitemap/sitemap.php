<style>
.accept-sitemap{
	text-align: center;
}
.accept-sitemap p{
	margin-bottom: 15px;
}
@media (max-width: 575.98px) {
	.table-custom > tbody > tr > td:last-child{
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
	}
	.table-custom > tbody > tr > td:first-child{
		display: none;
	}
}
@media (min-width: 576px) and (max-width: 767.98px) {
}
@media (min-width: 768px) and (max-width: 991.98px) {	
}
@media (min-width: 992px) and (max-width: 1199.98px) {	
}
@media (min-width: 1200px) {
}
</style>
<main class="sitemap content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Sitemap</h1>
			<ul>
				<li>
					<button type="button" class="button button-header link-custom black-custom" data-toggle="modal" data-target="#UpdateSitemap">
                        <i class="fa fa-refresh" aria-hidden="true"></i> <label>Cập nhật sitemap</label>
                    </button>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black">STT</th>
					            <th class="bg-black">Tên sitemap</th>
					            <th class="bg-black">Ngày cập nhật</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT">1</td>
					            <td data-title="Tên sitemap">ror.xml</td>
					            <td data-title="Ngày cập nhật">21-02-2019 | 11:46</td>
					        </tr>
					        <tr>
					            <td data-title="STT">2</td>
					            <td data-title="Tên sitemap">sitemap.xml</td>
					            <td data-title="Ngày cập nhật">21-02-2019 | 11:46</td>
					        </tr>
					        <tr>
					            <td data-title="STT">3</td>
					            <td data-title="Tên sitemap">sitemap.html</td>
					            <td data-title="Ngày cập nhật">21-02-2019 | 11:46</td>
					        </tr>
					        <tr>
					            <td data-title="STT">4</td>
					            <td data-title="Tên sitemap">urllist.txt</td>
					            <td data-title="Ngày cập nhật">21-02-2019 | 11:46</td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<form name="frm" id="frm" action="" method="post" enctype="multipart/form-data">
	<div class="modal small-modal" id="UpdateSitemap" tabindex="-1" data-backdrop="static" data-keyboard="false">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <div class="dropdown-collapse customer-dropdown bg-black">
	          Cập nhật sitemap
	          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
	            <span class="ti-close" data-dismiss="modal"></span>
	          </a>
	        </div>
	      </div>
	      <div class="modal-body">
	      	<div class="accept-sitemap">
	      		<p>Bạn muốn cập nhật lại Sitemap?</p>
		        <button type="submit" class="button bg-black">Có</button>
		        <button type="button" class="button bg-black" data-dismiss="modal" aria-hidden="true">Không</button>
	      	</div>
	      	
	      </div>
	    </div>
	  </div>
	</div>
</form>
<script>
	jQuery(function(){

	})
</script>