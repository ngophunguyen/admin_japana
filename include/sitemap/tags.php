<style>
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		padding: 10px 0;
	}
	@media (max-width: 575.98px) {
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.box-table tr td:last-child{
			padding: 0;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="tags content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách tags</h1>
			<ul>
				<li>
					<a href="javascript:void(0)" data-toggle="modal" data-target="#addtag-modal" class="link-custom black-custom" title="Thêm tag">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm tag</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black">Tags</th>
					            <th class="bg-black">Slug</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="Tags">ăn gì để tăng cân</td>
					            <td data-title="Slug">an-gi-de-tang-can</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#edittag-1" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:delete_product(1);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="Tags">áo chống nắng thun lạnh</td>
					            <td data-title="Slug">ao-chong-nang-thun-lanh</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#edittag-1" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:delete_product(1);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="Tags">áo chống nắng Uniqlo</td>
					            <td data-title="Slug">ao-chong-nang-uniqlo</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#suatag-1" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:delete_product(1);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="Tags">áo khoác CaRô</td>
					            <td data-title="Slug">ao-khoac-caro</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#edittag-1" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:delete_product(1);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/tags/add.php'); ?>
<?php include('include/tags/edit.php'); ?>
<script>
	jQuery(function(){

	})
</script>