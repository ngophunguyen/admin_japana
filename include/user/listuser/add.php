<style>
	.custom-dropdown:after{
		padding-right: 15px;
	}
	.add-user .item{
		display: inline-block;
		width: 32%;
		margin: 10px 2%;
		float: left;
	}
	.add-user .item:nth-child(1), 
	.add-user .item:nth-child(3), 
	.add-user .item:nth-child(4),
	.add-user .item:nth-child(6){
		margin: 10px 0;
	}
	.add-user .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	@media (max-width: 575.98px) {
	  	.add-user .item{
			display: inline-block;
			width: 100%;
			margin: 10px 0 0;
			float: left;
		}
		
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="add-list-user content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Thêm mới user</h1>
			<ul>
				<li>
					<a href="#" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
					</a>
				</li>
				<li>
					<a href="?action=include/user/list-user.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="add-user">
					<form id="frm" name="frm" method="post" autocomplete="off">
					    <div class="item">
					    	<label for="username">Username:</label>
					    	<input autocomplete="off" type="text" name="username" id="username" placeholder="Nhập username" value="" required="" class="form-control">
					    </div>
					    <div class="item">
					    	<label for="password">Password:</label>
					    	<input autocomplete="off" type="password" name="password" id="password" placeholder="Nhập password" value="" class="form-control">
					    </div>
					    <div class="item">
					    	<label for="id_usergroup">Chọn nhóm:</label>
					    	<div class="custom-dropdown">
						    	<select class="form-control" name="id_usergroup" id="id_usergroup">
						    		<option value="-1">Chọn nhóm user</option>
								  	<option value="0">Admin</option>
								  	<option value="1">Content</option>
								  	<option value="2">Kế toán</option>
								</select>
							</div>
					    </div>
					    <div class="item">
					    	<label for="fullname">Họ và tên:</label>
					    	<input autocomplete="off" type="text" name="fullname" id="fullname" placeholder="Nhập họ và tên" value="" class="form-control" required="">
					    </div>
					    <div class="item">
					    	<label for="mobile">Nhập số điện thoại:</label>
					    	<input autocomplete="off" type="number" name="mobile" id="mobile" placeholder="Nhập số điện thoại" value="" class="form-control" required="">
					    </div>
					    <div class="item">
					    	<label for="email">Email:</label>
					    	<input autocomplete="off" type="email" name="email" id="email" placeholder="Email" value="" class="form-control" required="">
					    </div>
					</form>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		
	})
</script>