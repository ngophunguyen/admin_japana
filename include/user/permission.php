<style>
	.custom-dropdown{
		width: 50%;
	}
	.custom-dropdown:after{
		padding-right: 15px;
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 50%;
		float: left;
	}
	.table-custom > tbody > tr > td:last-child, .table-custom > thead > tr > th:last-child{
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    padding: 15px 0;
	}
	.table-custom > thead > tr > th:last-child{
		padding-left: 15px;
	}

	@media (max-width: 575.98px) {
		.custom-dropdown, .box-quick-search .item{
			width: 100%;
		}
		.box-quick-search .item{
			width: 100%;
		}
		.box-quick-search .item:first-child input{
			width: 70%;
		}
		.table-custom > tbody > tr > td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
		.table-custom > tbody > tr > td:last-child{
			padding: 0;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {

	}
	@media (min-width: 768px) and (max-width: 991.98px) {

	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="info-order content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Phân quyền nhóm user</h1>
			<ul>
				<li>
					<a href="?action=include/user/permission/add.php" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="parents" name="parents">
						    		<option value="-1">Xem tất cả quyền</option>
								  	<option value="0">-- Quản lý sản phẩm</option>
								  	<option value="1">---- Danh mục</option>
								  	<option value="2">---- Sản phẩm</option>
								</select>
							</div>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
						<thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black">STT</th>
					            <th class="bg-black">Tên nhóm</th>
					            <th class="bg-black center-custom">Level</th>
					            <th class="bg-black">Tùy chỉnh</th>
					        </tr>
					    </thead>
					    <tbody>
					    	<tr>
					    		<td data-title="STT">1</td>
					            <td data-title="Tên nhóm">
					            	<a href="?action=include/user/permission/edit.php" title="Admin">
					            		-- 1-Root 
					            	</a>
					            </td>
					            <td data-title="Level" class="center-custom">0</td>
					            <td data-title="Tuỳ chỉnh">
					            	<a href="?action=include/user/permission/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="#" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					    	</tr>
					    	<tr>
					    		<td data-title="STT">2</td>
					            <td data-title="Tên nhóm" style="padding-left: 40px;">
					            	<a href="?action=include/user/permission/edit.php" title="Admin">
					            		-- 2-Quản lý sản phẩm 
					            	</a>
					            </td>
					            <td data-title="Level" class="center-custom">1</td>
					            <td data-title="Tuỳ chỉnh">
					            	<a href="?action=include/user/permission/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="#" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					    	</tr>
					    	<tr>
					    		<td data-title="STT">3</td>
					            <td data-title="Tên nhóm" style="padding-left: 60px;">
					            	<a href="?action=include/user/permission/edit.php" title="Admin">
					            		-- 14-Danh mục
					            	</a>
					            </td>
					            <td data-title="Level" class="center-custom">2</td>
					            <td data-title="Tuỳ chỉnh">
					            	<a href="?action=include/user/permission/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="#" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					    	</tr>
					    	<tr>
					    		<td data-title="STT">4</td>
					            <td data-title="Tên nhóm" style="padding-left: 80px;">
					            	<a href="?action=include/user/permission/edit.php" title="Admin">
					            		-- 16-Thêm mới
					            	</a>
					            </td>
					            <td data-title="Level" class="center-custom">3</td>
					            <td data-title="Tuỳ chỉnh">
					            	<a href="?action=include/user/permission/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="#" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					    	</tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		
	})
</script>