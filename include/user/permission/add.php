<style>
	.custom-dropdown:after{
		padding-right: 15px;
	}
	.add-user .item{
		display: inline-block;
		width: 32%;
		margin: 10px 2%;
		float: left;
	}
	.add-user .item:nth-child(1), 
	.add-user .item:nth-child(3){
		margin: 10px 0;
	}
	.add-user .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.add-user .item:last-child{
		margin: 10px 0;
		width: 100%;
	}
	@media (max-width: 575.98px) {
	  	.add-user .item{
			display: inline-block;
			width: 100%;
			margin: 10px 0 0;
			float: left;
		}
		
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="add-permission content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Thêm quyền cho nhóm</h1>
			<ul>
				<li>
					<a href="#" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
					</a>
				</li>
				<li>
					<a href="?action=include/user/permission.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="add-user">
					<form id="frm" name="frm" method="post" autocomplete="off">
						<div class="item">
					    	<label for="id_usergroup">Parents:</label>
					    	<div class="custom-dropdown">
					    		<select class="form-control" name="id_usergroup" id="id_usergroup">
						    		<option value="-1">Chọn mục cha</option>
								  	<option value="0">-- 1 id-2 Quản lý sản phẩm</option>
								  	<option value="1">---- 2 id-14 Danh mục</option>
								  	<option value="2">------ 3 id-16 Thêm mới</option>
								</select>
					    	</div>
					    </div>
					    <div class="item">
					    	<label for="username">Tên mục phân quyền:</label>
					    	<input autocomplete="off" type="text" name="name" id="name_vi" placeholder="Nhập tên" value="" required="" class="form-control">
					    </div>
					    <div class="item">
					    	<label for="fullname">Slug:</label>
					    	<input autocomplete="off" type="text" name="slug" id="slug" placeholder="Nhập slug" value="" class="form-control" required="">
					    </div>
					    <div class="item">
					    	<label for="email">Mô tả:</label>
					    	<textarea name="notes" id="notes" class="form-control" rows="5"></textarea>
					    </div>
					</form>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		
	})
</script>