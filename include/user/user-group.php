<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:first-child input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item:first-child button{
		float: left;
		margin-left: 15px;
	}	
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}

	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	}
	.table-custom thead tr th:last-child{
		padding-right: 30px;
	}
	.table-custom tbody tr td:last-child{
		float: right;
	}
	.table-custom > tbody > tr > td a{
		float: left;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item{
			width: 100%;
		}
		.table-custom tbody tr td:last-child{
			float: inherit;
		}
		.table-custom > tbody > tr > td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {

	}
	@media (min-width: 768px) and (max-width: 991.98px) {

	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="user-group content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Nhóm user</h1>
			<ul>
				<li>
					<a href="?action=include/user/usergroup/add.php" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm nhóm</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm nhóm user">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Tên nhóm</th>
					            <th class="bg-black center-custom">Số thành viên</th>
					            <th class="bg-black right-custom">Tuỳ chỉnh</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Admin">Admin</a></td>
					            <td data-title="Số thành viên" class="center-custom">1</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Content">Content</a></td>
					            <td data-title="Số thành viên" class="center-custom">8</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Kế toán">Kế toán</a></td>
					            <td data-title="Số thành viên" class="center-custom">4</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Marketing">Marketing</a></td>
					            <td data-title="Số thành viên" class="center-custom">1</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Phát triển sản phẩm">Phát triển sản phẩm</a></td>
					            <td data-title="Số thành viên" class="center-custom">8</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Quản lý">Quản lý</a></td>
					            <td data-title="Số thành viên" class="center-custom">4</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Quản lý kho">Quản lý kho</a></td>
					            <td data-title="Số thành viên" class="center-custom">1</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Quản lý Marketing">Quản lý Marketing</a></td>
					            <td data-title="Số thành viên" class="center-custom">8</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Quản lý PTSP - Thu mua - Kho">Quản lý PTSP - Thu mua - Kho</a></td>
					            <td data-title="Số thành viên" class="center-custom">4</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Quản lý TVBH">Quản lý TVBH</a></td>
					            <td data-title="Số thành viên" class="center-custom">1</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">11</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Thu mua hàng hoá">Thu mua hàng hoá</a></td>
					            <td data-title="Số thành viên" class="center-custom">8</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">12</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Trưởng kế toán">Trưởng kế toán</a></td>
					            <td data-title="Số thành viên" class="center-custom">4</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">13</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Tư vấn bán hàng">Tư vấn bán hàng</a></td>
					            <td data-title="Số thành viên" class="center-custom">1</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">14</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Xử lý đơn hàng">Xử lý đơn hàng</a></td>
					            <td data-title="Số thành viên" class="center-custom">8</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">15</td>
					            <td data-title="Tên nhóm"><a href="?action=include/user/usergroup/edit.php" title="Đóng gói">Đóng gói</a></td>
					            <td data-title="Số thành viên" class="center-custom">4</td>
					            <td data-title="Tuỳ chỉnh" class="right-custom">
					            	<a href="?action=include/user/usergroup/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		var heightBottomPagination = jQuery('.pagination-custom').outerHeight();
		if(window.innerWidth < 576){
			jQuery('.box-table').css('margin-bottom',heightBottomPagination);
		}
	})
</script>