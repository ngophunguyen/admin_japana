<style>
	.custom-dropdown:after{
		padding-right: 15px;
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
		margin-bottom: 15px;
	}
	.box-quick-search .item{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		width: 50%;
	}
	.box-quick-search .item input{
		width: 75%;
		float: right;
		margin-left: 15px;
	}
	.box-permiss {
	    display: inline-block;
	    width: 100%;
	}
	.box-pa-title {
	    height: 38px;
	}
	.box-pa-title ul, .box-permiss .item {
	    display: inline-block;
	    width: 100%;
	    padding-left: 20px;
	    margin-bottom: 0;
	    border-bottom: 1px solid #f1f1f1;
	}
	.box-permiss .box-pa-title ul li {
	    float: left;
	    font-weight: 500;
	    font-size: 14px;
	    color: #333333;
	    height: 38px;
	    padding: 10px 0;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
    	-webkit-box-align: center;
    	    -ms-flex-align: center;
    	        align-items: center;
	}
	.box-permiss .box-pa-title ul li:first-child, .box-permiss .box-pa-title ul li:nth-child(2) {
	    width: 35%;
	}
	.box-permiss .box-pa-title ul li:nth-child(3), .box-permiss .box-pa-title ul li:last-child {
	    width: 15%;
	}
	.box-all-permiss .item .permiss-item{
		display:-webkit-box;
		display:-ms-flexbox;
		display:flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center; 
		height:39px;
		float:left;
		width:35%;
		padding: 6px 0;
	}
	.box-all-permiss .item .permiss-item>a{
		margin-right: 5px;
	}
	.box-all-permiss .item div:nth-child(3), .box-all-permiss .item div:nth-child(4){
		display:-webkit-box;
		display:-ms-flexbox;
		display:flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center; 
		height: 39px;
		float: left; 
		width:15%;
		padding: 6px 0;
	}
	.checkbox-permiss{
		display:-webkit-box;
		display:-ms-flexbox;
		display:flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		height:39px;
		float: left; 
		width:35%;
		padding: 8px 0;
	}
	.checkbox-custom {
	    cursor: pointer;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    height: 20px;
	    width: 20%;
	}
	ul.mobile-permiss{
		display: none;
	}
	.list-user-ingroup{
		display: none;
		width: 100%;
		margin-bottom: 15px;
	}
	.show-user{
		margin-bottom: 15px;
	}
	.table-custom{
		margin-top: 0;
	}
	@media (max-width: 575.98px) {
		.box-permiss .box-pa-title ul li:nth-child(3), 
		.box-permiss .box-pa-title ul li:nth-child(4),
		.box-all-permiss .item div:nth-child(3), 
		.box-all-permiss .item div:nth-child(4),
		ul.pc-permiss{
		    display: none;
		}
		.box-pa-title ul, .box-permiss .item{
			padding-left: 0;
		}
		.box-permiss .box-pa-title ul li:first-child, .box-all-permiss .item .permiss-item{
			width: 65%;
		}
		.box-permiss .box-pa-title ul li:nth-child(2), .checkbox-permiss{
			width: 35%;
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		    -webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		    -webkit-box-pack: justify;
		        -ms-flex-pack: justify;
		            justify-content: space-between;
		}
		.box-quick-search .item{
			width: 100%;
		}
		.box-quick-search .item input{
			margin-left: 0;
		}
		ul.mobile-permiss{
			display: block;
		}
		.box-all-permiss .item .permiss-item{
			padding-left: 0!important;
		}
		.box-permiss .box-pa-title ul li:nth-child(2) span{
			width: 20%;
		}
		.checkbox-custom{
			width: auto;
			padding-left: 20px;
		}
		.table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-permiss .box-pa-title ul li:nth-child(3), 
		.box-permiss .box-pa-title ul li:nth-child(4),
		.box-all-permiss .item div:nth-child(3), 
		.box-all-permiss .item div:nth-child(4),
		ul.pc-permiss{
		    display: none;
		}
		ul.mobile-permiss{
			display: block;
		}
		.box-permiss .box-pa-title ul li:first-child, .box-all-permiss .item .permiss-item{
			width: 65%;
		}
		.checkbox-custom{
			width: auto;
			padding-left: 25px;
			margin-right: 10px;
		}
		.box-permiss .box-pa-title ul li:nth-child(2), .checkbox-permiss{
			width: 35%;
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		    -webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		    -webkit-box-pack: justify;
		        -ms-flex-pack: justify;
		            justify-content: space-between;
		}
		.box-permiss .box-pa-title ul li:nth-child(2) span{
			width: 20%;
		}
		.box-quick-search .item{
			width: 70%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.box-permiss .box-pa-title ul li:nth-child(3), 
		.box-permiss .box-pa-title ul li:nth-child(4),
		.box-all-permiss .item div:nth-child(3), 
		.box-all-permiss .item div:nth-child(4){
		    display: none;
		}
		.checkbox-custom{
			width: auto;
			padding-left: 25px;
			margin-right: 10px;
		}
		.box-permiss .box-pa-title ul li:first-child, .box-all-permiss .item .permiss-item{
			width: 70%;
		}
		.box-permiss .box-pa-title ul li:nth-child(2), .checkbox-permiss{
			width: 30%;
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		    -webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		    -webkit-box-pack: justify;
		        -ms-flex-pack: justify;
		            justify-content: space-between;
		}
		.box-quick-search .item input{
			margin-left: 0;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="edit-user-group content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Phân quyền cho nhóm</h1>
			<ul>
				<li>
					<a href="?action=include/user/usergroup/add.php" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
					</a>
				</li>
				<li>
					<a href="?action=include/user/user-group.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<form name="frm" id="frm" action="" method="post" class="search1">
						<div class="item">
							Tên nhóm:
							<input type="text" name="name" class="form-control custom-ipt" value="" placeholder="Đặt tên nhóm"required="">
						</div>
					</form>
				</div>
				<a class="show-user dropdown-collapse bg-black" href="javascript:void(0);">Thành viên trong nhóm <i class="fa fa-caret-down" aria-hidden="true"></i></a>
				<div class="list-user-ingroup">
					<div class="box-table">
						<table class="table table-custom table-striped table-responsive">
						    <thead class="bg-black">
						        <tr class="bg-black">
						            <th class="bg-black">STT</th>
						            <th class="bg-black">User</th>
						            <th class="bg-black">Họ và tên</th>
						            <th class="bg-black center-custom">Publish</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td data-title="STT">1</td>
						            <td data-title="User"><a href="?action=include/user/listuser/edit.php" title="Admin">dev_admin</a></td>
						            <td data-title="Họ và tên">Admin</td>
						            <td data-title="Publish">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT">2</td>
						            <td data-title="User"><a href="?action=include/user/listuser/edit.php" title="Admin">nghi_content</a></td>
						            <td data-title="Họ và tên">Nghi Content</td>
						            <td data-title="Publish">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT">3</td>
						            <td data-title="User"><a href="?action=include/user/listuser/edit.php" title="Admin">thu_qc</a></td>
						            <td data-title="Họ và tên">Kinh Thị Kim Thư</td>
						            <td data-title="Publish">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT">4</td>
						            <td data-title="User"><a href="?action=include/user/listuser/edit.php" title="Admin">test_admin</a></td>
						            <td data-title="Họ và tên">Admin Test</td>
						            <td data-title="Publish">
						            	<input type="checkbox" class="checkbox-ios" />
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
				</div>
				
				<a class="dropdown-collapse bg-black" href="javascript:void(0);">Bảng phân quyền</a>
				<div class="box-permiss">
                    <div class="box-pa-title">
                        <ul class="pc-permiss bg-gray">
                            <li>Danh mục</li>
                            <li>Quyền truy cập</li>
                            <li>Ngày cập nhật</li>
                            <li>User cập nhật</li>
                        </ul>
                        <ul class="mobile-permiss bg-gray">
                        	<li>Danh mục</li>
                            <li>
                            	<span>All</span> <span>Xem</span> <span>Sửa</span></li>
                        </ul>
                    </div>
                    <div class="box-all-permiss">
                    	<div class="item show-child-1 show-all-1">
                            <div class="permiss-item">
                                <input id="show-2" class=" show-all-1" type="hidden" value="0">
                                <a class="showdiv" href="javascript:showChild(2)">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </a> Quản lý sản phẩm
                            </div>
                            <div class="checkbox-permiss">
                                <input type="hidden" id="parents-2" value="1">
                                <input type="hidden" id="class-2" value=".parents-2">
                                <label for="all-2" class="checkbox-custom">All
				                  	<input id="all-2" type="checkbox" class="all-child-1 all-all-1 parents-2" onclick="initCheckAllParent('2');" name="permission[quan-ly-san-pham][all]" checked>
				                  	<span class="checkmark"></span>
				                </label>
				                <label for="view-2" class="checkbox-custom">Xem
				                  	<input id="view-2" type="checkbox" class="view-child-1 view-all-1" onclick="initCheckViewParent('2');" name="permission[quan-ly-san-pham][view]" checked>
				                  	<span class="checkmark"></span>
				                </label>
				                <label for="edit-2" class="checkbox-custom">Sửa
									<input id="edit-2" type="checkbox" class="edit-child-1 edit-all-1" onclick="initCheckEditParent('2');" name="permission[quan-ly-san-pham][edit]" checked>
									<span class="checkmark bg-gray"></span>
				                </label>
                            </div>
                            <div>
                            	26-03-2019 | 09:37
                            </div>
                            <div>
                            	Admin
                            </div>
                        </div>
                        <div style="display: none;" class="item show-child-2 show-all-1 show-all-2">
                            <div class="permiss-item" style="padding-left: 40px;">
                            	<input id="show-14" class=" show-all-1 show-all-2" type="hidden" value="0">
                                <a class="showdiv" href="javascript:showChild(14)">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </a> Danh mục
                            </div>
                            <div class="checkbox-permiss">
                                <input type="hidden" id="parents-14" value="2">
                                <input type="hidden" id="class-14" value=".parents-2, .parents-14">

                                <label for="all-14" class="checkbox-custom">All
				                  	<input id="all-14" type="checkbox" class="all-child-2 all-child-1 all-all-2 parents-14" onclick="initCheckAllParent('14');" name="permission[quan-ly-san-pham][danh-muc][all]" checked>
				                  	<span class="checkmark bg-gray"></span>
				                </label>
				                <label for="view-14" class="checkbox-custom">Xem
				                  	<input id="view-14" data-id="14" type="checkbox" class="view-child-2 view-all-1 view-all-2" onclick="initCheckViewParent('14');" name="permission[quan-ly-san-pham][danh-muc][view]" checked>
				                  	<span class="checkmark bg-gray"></span>
				                </label>
				                <label for="edit-14" class="checkbox-custom">Sửa
									<input id="edit-14" type="checkbox" class="edit-child-2 edit-all-1 edit-all-2" onclick="initCheckEditParent('14');" name="permission[quan-ly-san-pham][danh-muc][edit]" checked>
									<span class="checkmark bg-gray"></span>
				                </label>
                            </div>
                            <div>
                            	26-03-2019 | 09:37
                            </div>
                            <div>
                            	Admin
                            </div>
                        </div>
                        <div style="display: none;" class="item show-child-14 show-all-1 show-all-2 show-all-14">
                            <div class="permiss-item" style="padding-left: 80px;">
                                <input id="show-16" class=" show-all-1 show-all-2 show-all-14" type="hidden" value="0" style="display: none;">
                                <a class="showdiv" href="javascript:showChild(16)"><i class="fa fa-plus" aria-hidden="true"></i></a> Thêm mới
                            </div>
                            <div class="checkbox-permiss">
                                <input type="hidden" id="parents-16" value="14">
                                <input type="hidden" id="class-16" value=".parents-2, .parents-14, .parents-16">
                                <label for="all-16" class="checkbox-custom">All
                                    <input class="all-child-14 all-all-1 all-all-2 all-all-14 parents-16" type="checkbox" id="all-16" onclick="initCheckAllParent('16');" name="permission[quan-ly-san-pham][danh-muc][them-moi][all]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="view-16" class="checkbox-custom">Xem
                                    <input class="view-child-14 view-all-1 view-all-2 view-all-14" type="checkbox" id="view-16" data-id="16" onclick="initCheckViewParent('16');" name="permission[quan-ly-san-pham][danh-muc][them-moi][view]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="edit-16" class="checkbox-custom">Sửa
                                    <input type="checkbox" id="edit-16" class="edit-child-14 edit-all-1 edit-all-2 edit-all-14" onclick="initCheckEditParent('16');" name="permission[quan-ly-san-pham][danh-muc][them-moi][edit]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                            </div>
                            <div>
                            	26-03-2019 | 09:37
                            </div>
                            <div>
                            	Admin
                            </div>
                        </div>
                        <div style="display: none;" class="item show-child-16 show-all-1 show-all-2 show-all-14 show-all-16">
                        	<div class="permiss-item" style="padding-left: 120px;">
	                            Chọn nhóm cha                                                
	                        </div>
                            <div class="checkbox-permiss">
                                <input type="hidden" id="parents-18" value="16">
                                <input type="hidden" id="class-18" value=".parents-2, .parents-14, .parents-16, .parents-18">
                                <label for="all-18" class="checkbox-custom">All
                                    <input class="all-child-16  all-all-1 all-all-2 all-all-14 all-all-16 parents-18" type="checkbox" id="all-18" onclick="initCheckAllParent('18');" name="permission[quan-ly-san-pham][danh-muc][them-moi][chon-nhom-cha][all]" checked>
                                   <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="view-18" class="checkbox-custom">Xem
                                	<input class="view-child-16 view-all-1 view-all-2 view-all-14 view-all-16" type="checkbox" id="view-18" data-id="18" onclick="initCheckViewParent('18');" name="permission[quan-ly-san-pham][danh-muc][them-moi][chon-nhom-cha][view]" checked>
									<span class="checkmark bg-gray"></span>
                                </label>
                                <label for="edit-18" class="checkbox-custom">Sửa
                                    <input type="checkbox" id="edit-18" class="edit-child-16 edit-all-1 edit-all-2 edit-all-14 edit-all-16" onclick="initCheckEditParent('18');" name="permission[quan-ly-san-pham][danh-muc][them-moi][chon-nhom-cha][edit]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                            </div>
                            <div>
                            	26-03-2019 | 09:37
                            </div>
                            <div>
                            	Admin
                            </div>
                        </div>
                        <div style="display: none;" class="item show-child-2  show-all-1 show-all-2">
                            <div class="permiss-item" style="padding-left: 40px;">
                                <input id="show-15" class=" show-all-1 show-all-2" type="hidden" value="0">
                                <a class="showdiv" href="javascript:showChild(15)"><i class="fa fa-plus" aria-hidden="true"></i></a> Sản phẩm
                            </div>
                            <div class="checkbox-permiss">
                                <input type="hidden" id="parents-15" value="2">
                                <input type="hidden" id="class-15" value=".parents-2, .parents-15">
                                <label for="all-15" class="checkbox-custom">All
                                    <input class="all-child-2  all-all-1 all-all-2 parents-15" type="checkbox" id="all-15" onclick="initCheckAllParent('15');" name="permission[quan-ly-san-pham][san-pham][all]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="view-15" class="checkbox-custom">Xem
                                    <input class="view-child-2 view-all-1 view-all-2" type="checkbox" id="view-15" data-id="15" onclick="initCheckViewParent('15');"name="permission[quan-ly-san-pham][san-pham][view]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="edit-15" class="checkbox-custom">Sửa
                                    <input type="checkbox" id="edit-15" class="edit-child-2 edit-all-1 edit-all-2" onclick="initCheckEditParent('15');" name="permission[quan-ly-san-pham][san-pham][edit]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                            </div>
                            <div>
                            	26-03-2019 | 09:37
                            </div>
                            <div>
                            	Admin
                            </div>
                        </div>
                        <div style="display: none;" class="item show-child-15  show-all-1 show-all-2 show-all-15">
                            <div class="permiss-item" style="padding-left: 80px;">
                            	<input id="show-35" class=" show-all-1 show-all-2 show-all-15" type="hidden" value="1">
                                <a class="showdiv" href="javascript:showChild(35)"><i class="fa fa fa-plus" aria-hidden="true"></i></a> Danh sách
                            </div>
                            <div class="checkbox-permiss">
                                <input type="hidden" id="parents-35" value="15">
                                <input type="hidden" id="class-35" value=".parents-2, .parents-15, .parents-35">
                                <label for="all-35" class="checkbox-custom">All
                                    <input class="all-child-15 all-all-1 all-all-2 all-all-15 parents-35" type="checkbox" id="all-35" onclick="initCheckAllParent('35');" name="permission[quan-ly-san-pham][san-pham][danh-sach][all]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="view-35" class="checkbox-custom">Xem
                                    <input class="view-child-15 view-all-1 view-all-2 view-all-15" type="checkbox" id="view-35" data-id="35" onclick="initCheckViewParent('35');" name="permission[quan-ly-san-pham][san-pham][danh-sach][view]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="edit-35" class="checkbox-custom">Sửa
                                    <input type="checkbox" id="edit-35" class="edit-child-15 edit-all-1 edit-all-2 edit-all-15" onclick="initCheckEditParent('35');" name="permission[quan-ly-san-pham][san-pham][danh-sach][edit]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                            </div>
                            <div>
                            	26-03-2019 | 09:37
                            </div>
                            <div>
                            	Admin
                            </div>
                        </div>
                        <div style="display: none;" class="item show-child-35  show-all-1 show-all-2 show-all-15 show-all-35">
                            <div class="permiss-item" style="padding-left: 120px;">
                            	Public
                            </div>
                            <div class="checkbox-permiss">
                                <input type="hidden" id="parents-38" value="35">
                                <input type="hidden" id="class-38" value=".parents-2, .parents-15, .parents-35, .parents-38">
                                <label for="all-38" class="checkbox-custom">All
                                    <input class="all-child-35  all-all-1 all-all-2 all-all-15 all-all-35 parents-38" type="checkbox" id="all-38" onclick="initCheckAllParent('38');" name="permission[quan-ly-san-pham][san-pham][danh-sach][public][all]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="view-38" class="checkbox-custom">Xem
                                    <input class="view-child-35 view-all-1 view-all-2 view-all-15 view-all-35" type="checkbox" id="view-38" data-id="38" onclick="initCheckViewParent('38');" name="permission[quan-ly-san-pham][san-pham][danh-sach][public][view]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="edit-38" class="checkbox-custom">Sửa
                                    <input type="checkbox" id="edit-38" class="edit-child-35 edit-all-1 edit-all-2 edit-all-15 edit-all-35" onclick="initCheckEditParent('38');" name="permission[quan-ly-san-pham][san-pham][danh-sach][public][edit]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                            </div>
                            <div>
                            	26-03-2019 | 09:37
                            </div>
                            <div>
                            	Admin
                            </div>
                        </div>
                        <div class="item show-child-1 show-all-1">
                            <div class="permiss-item">
                            	<input id="show-3" class=" show-all-1" type="hidden" value="0">
                                <a class="showdiv" href="javascript:showChild(3)"><i class="fa fa-plus" aria-hidden="true"></i></a> Sale
                           	</div>
                            <div class="checkbox-permiss">
                                <input type="hidden" id="parents-3" value="1">
                                <input type="hidden" id="class-3" value=".parents-3">
                                <label for="all-3" class="checkbox-custom">All
                                    <input class="all-child-1 all-all-1 parents-3" type="checkbox" id="all-3" onclick="initCheckAllParent('3');" name="permission[sale][all]" checked>
                                    <span class="checkmark"></span>
                                </label>
                                <label for="view-3" class="checkbox-custom">Xem
                                    <input class="view-child-1 view-all-1" type="checkbox" id="view-3" data-id="3" onclick="initCheckViewParent('3');" name="permission[sale][view]" checked>
                                    <span class="checkmark"></span>
                                </label>
                                <label for="edit-3" class="checkbox-custom">Sửa
                                    <input type="checkbox" id="edit-3" class="edit-child-1 edit-all-1" onclick="initCheckEditParent('3');" name="permission[sale][edit]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                            </div>
                            <div>
                            	26-03-2019 | 09:37
                            </div>
                            <div>
                            	Admin
                            </div>
                        </div>
                        <div style="display: none;" class="item show-child-3  show-all-1 show-all-3">
                            <div class="permiss-item" style="padding-left: 40px;">
	                            Tư vấn điện thoại
	                        </div>
                            <div class="checkbox-permiss">
                                <input type="hidden" id="parents-73" value="3">
                                <input type="hidden" id="class-73" value=".parents-3, .parents-73">
                                <label for="all-73" class="checkbox-custom">All
                                    <input class="all-child-3 all-all-1 all-all-3 parents-73" type="checkbox" id="all-73" onclick="initCheckAllParent('73');" name="permission[sale][tu-van-dien-thoai][all]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="view-73" class="checkbox-custom">Xem
                                    <input class="view-child-3 view-all-1 view-all-3" type="checkbox" id="view-73" data-id="73" onclick="initCheckViewParent('73');" name="permission[sale][tu-van-dien-thoai][view]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                                <label for="edit-73" class="checkbox-custom">Sửa
                                    <input type="checkbox" id="edit-73" class="edit-child-3 edit-all-1 edit-all-3" onclick="initCheckEditParent('73');" name="permission[sale][tu-van-dien-thoai][edit]" checked>
                                    <span class="checkmark bg-gray"></span>
                                </label>
                            </div>
                            <div>
                            	26-03-2019 | 09:37
                            </div>
                            <div>
                            	Admin
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(".checkbox-ios").iosCheckbox();
	function initCheckAllParent(id){
	  	if(jQuery('#all-' + id).prop('checked')==false){
	    	jQuery('.all-all-' + id).prop('checked',false);
	    	jQuery('.all-all-' + id).prop('disabled',true);
	    	jQuery('.all-all-' + id).next().addClass('bg-gray');

	    	jQuery('.edit-all-' + id).prop('checked',false);
	    	jQuery('.edit-all-' + id).prop('disabled',true);
	    	jQuery('.edit-all-' + id).next().addClass('bg-gray');

	    	jQuery('.view-all-' + id).prop('checked',false);
	    	jQuery('.view-all-' + id).prop('disabled',true);
	    	jQuery('.view-all-' + id).next().addClass('bg-gray');

	    	jQuery('#edit-' + id).prop('checked',false);
	    	jQuery('#edit-' + id).prop('disabled',true);
	    	jQuery('#edit-' + id).next().addClass('bg-gray');

	    	jQuery('#view-' + id).prop('checked',false);
	    	jQuery('#view-' + id).prop('disabled',false);
	    	jQuery('#view-' + id).next().removeClass('bg-gray');

	    	jQuery(jQuery("#class-"+id).val()).prop('checked',false);
	  	}else{
			jQuery('#edit-' + id).prop('checked',true);
			jQuery('#edit-' + id).prop('disabled',false);
			jQuery('#edit-' + id).next().removeClass('bg-gray');

			jQuery('#view-' + id).prop('checked',true);
			jQuery('#view-' + id).prop('disabled',false);
			jQuery('#view-' + id).next().removeClass('bg-gray');

			jQuery('.all-all-' + id).prop('checked',true);
			jQuery('.all-all-' + id).prop('disabled',false);
			jQuery('.all-all-' + id).next().removeClass('bg-gray');

			jQuery('.edit-all-' + id).prop('checked',true);
	    	jQuery('.edit-all-' + id).prop('disabled',false);
	    	jQuery('.edit-all-' + id).next().removeClass('bg-gray');

	    	jQuery('.view-all-' + id).prop('checked',true);
	    	jQuery('.view-all-' + id).prop('disabled',false);
	    	jQuery('.view-all-' + id).next().removeClass('bg-gray');
	  	}
	}
	function initCheckViewParent(id){
	  	if(jQuery('#view-'+id).prop('checked') == false){
	    	jQuery('.view-all-'+id).prop('checked',false);
	    	jQuery('.view-all-'+id).prop('disabled',true);
	    	jQuery('.view-all-' + id).next().addClass('bg-gray');

	    	jQuery('.edit-all-'+id).prop('checked',false);
	    	jQuery('.edit-all-'+id).prop('disabled',true);
	    	jQuery('.edit-all-' + id).next().addClass('bg-gray');

	    	jQuery('.all-all-'+id).prop('checked',false);
	    	jQuery('.all-all-'+id).prop('disabled',true);
	    	jQuery('.all-all-' + id).next().addClass('bg-gray');

	    	jQuery('#all-'+id).prop('checked',false);

	    	jQuery('#edit-'+id).prop('checked',false);
	    	jQuery('#edit-'+id).prop('disabled',true);
	    	jQuery('#edit-' + id).next().addClass('bg-gray');

	    	jQuery(jQuery("#class-"+id).val()).prop('checked',false);
	  	}else{
			if(jQuery('#parents-'+id).val() == 1){
				jQuery('.view-child-' + id).prop('disabled',false);
				jQuery('.view-child-' + id).next().removeClass('bg-gray');

				jQuery('#edit-'+id).prop('disabled',false);
				jQuery('#edit-' + id).next().removeClass('bg-gray');
			}else{
				if(jQuery('#edit-'+jQuery('#parents-'+id).val()).prop('checked') == false){
					jQuery('.view-child-' + id).prop('disabled',false);
					jQuery('.view-child-' + id).next().removeClass('bg-gray');
				}else{
					jQuery('.view-child-' + id).prop('disabled',false);
					jQuery('.view-child-' + id).next().removeClass('bg-gray');
					jQuery('#edit-'+id).prop('disabled',false);
					jQuery('#edit-' + id).next().removeClass('bg-gray');

				}
			}
	  	}
	}
	function initCheckEditParent(id){
	  	if(jQuery('#edit-'+ id).prop('checked')==true){
		  	jQuery('.all-child-'+id).prop('disabled',false);
		  	jQuery('.all-child-' + id).next().removeClass('bg-gray');

		  	jQuery( ".view-child-"+id ).each(function( index ) {
			  	if(jQuery(this).prop('checked') == true){
				  	jQuery("#edit-"+jQuery(this).data("id")).prop('disabled',false);
				  	jQuery('#edit-' + jQuery(this).data("id")).next().removeClass('bg-gray');
			  	}
		  	});
	  	}else{
		  	jQuery('.all-all-'+id).prop('disabled',true);
		  	jQuery('.all-all-' + id).next().addClass('bg-gray');

		  	jQuery('.all-all-'+id).prop('checked',false);
		  	jQuery('#all-'+id).prop('checked',false);
		  	jQuery(jQuery("#class-"+id).val()).prop('checked',false);

		  	jQuery('.edit-all-'+id).prop('disabled',true);
		  	jQuery('.edit-all-' + id).next().addClass('bg-gray');

		  	jQuery('.edit-all-'+id).prop('checked',false);
	  	}
	}
	function showChild(id){
        var show = jQuery("#show-"+id).val();
        if(show == 0){
            jQuery(".show-child-"+id).show();
            jQuery("#show-"+id).val(1);
            jQuery('#show-'+id).next().html('<i class="fa fa-minus" aria-hidden="true"></i>');
            if(jQuery('.content-sidebar-wrap').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());	
			}
        }else{
            jQuery(".show-all-"+id).hide();
            jQuery(".show-all-"+id).val(0);
            jQuery('#show-'+id).next().html('<i class="fa fa-plus" aria-hidden="true"></i>');
            jQuery('.show-all-'+id).find('.showdiv').html('<i class="fa fa-plus" aria-hidden="true"></i>');
            jQuery("#show-"+id).val(0);
            if(jQuery('.content-sidebar-wrap').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());	
			}
        }
    }
	jQuery(function(){
		if(window.innerWidth < 767) {
			jQuery(".checkbox-custom").contents().filter(function(){ return this.nodeType == 3; }).remove();
		}
		jQuery('.show-user').click(function(){
	    	if(jQuery('.list-user-ingroup').css('display')=='none'){
	    		jQuery('.list-user-ingroup').css('display','block');
	    		jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
	    	}
	    	else{
	    		jQuery('.list-user-ingroup').css('display','none');
	    		jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
	    	}
	    });
	})
</script>