<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 50%;
		float: left;
	}
	.box-quick-search .item:first-child input{
		width: 50%;
		float: left;
	}
	.box-quick-search .item:first-child button{
		float: left;
		margin-left: 15px;
	}	
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}

	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	}
	.table-custom thead tr th:last-child{
		padding-right: 30px;
	}
	.custom-dropdown{
		display: inline-block;
		width: 50%;
		float: right;
	}
	.custom-dropdown:after{
		padding-right: 15px;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item{
			width: 100%;
		}
		.box-quick-search .item:first-child input{
			width: 70%;
		}
		.box-quick-search .item:last-child{
			margin-top: 15px;
		}
		.custom-dropdown{
			width: 100%;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
		.table-custom > tbody > tr > td:last-child{
			justify-content: inherit;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {

	}
	@media (min-width: 768px) and (max-width: 991.98px) {

	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="listuser content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách user</h1>
			<ul>
				<li>
					<a href="?action=include/user/listuser/add.php" class="link-custom black-custom" title="Thêm user">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm user</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post" class="search1">
	                       <input autocomplete="off" name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm theo user hoặc họ tên">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<form name="status_search" id="frm" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="id_usergroup" name="change_usergroup">
						    		<option value="-1">Lọc theo nhóm user</option>
								  	<option value="0">Admin</option>
								  	<option value="1">Content</option>
								</select>
							</div>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">User</th>
					            <th class="bg-black">Họ và tên</th>
					            <th class="bg-black">Nhóm user</th>
					            <th class="bg-black center-custom">Publish</th>
					            <th class="bg-black center-custom">Tùy chỉnh</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="User"><a href="?action=include/user/listuser/edit.php" title="Admin">dev_admin</a></td>
					            <td data-title="Họ và tên">Admin</td>
					            <td data-title="Nhóm user">Admin</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tuỳ chỉnh" class="center-custom">
					            	<a href="?action=include/user/listuser/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="User"><a href="?action=include/user/listuser/edit.php" title="Admin">nghi_content</a></td>
					            <td data-title="Họ và tên">Nghi Content</td>
					            <td data-title="Nhóm user">Content</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tuỳ chỉnh" class="center-custom">
					            	<a href="?action=include/user/listuser/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="User"><a href="?action=include/user/listuser/edit.php" title="Admin">thu_qc</a></td>
					            <td data-title="Họ và tên">Kinh Thị Kim Thư</td>
					            <td data-title="Nhóm user">Marketing</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tuỳ chỉnh" class="center-custom">
					            	<a href="?action=include/user/listuser/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="User"><a href="?action=include/user/listuser/edit.php" title="Admin">test_admin</a></td>
					            <td data-title="Họ và tên">Admin Test</td>
					            <td data-title="Nhóm user">Admin</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" />
					            </td>
					            <td data-title="Tuỳ chỉnh" class="center-custom">
					            	<a href="?action=include/user/listuser/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
			
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		if(window.innerWidth < 576){
			jQuery('.entry-content').css('margin-bottom','15px')
		}
	})
</script>