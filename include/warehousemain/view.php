<style>
	.info-product-kho{
    display: inline-block;
    width: 100%;
  }
  .info-product-kho .item{
    width: 50%;
    float: left;
  }
  .info-product-kho .item label, .total-price label{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 5px;
    font-weight: 500;
  }
  .total-price span{
    font-size: 16px;
  }
  .box-payback-table{
      display: inline-block;
      float: left;
      width: 100%;
  }
  #viewproduct-modal .table thead th{
    border-bottom: 2px solid #dee2e6;
  }
  .box-payback-table .box-table tr td:last-child{
    -webkit-box-pack: center;
        -ms-flex-pack: center;
            justify-content: center;
  }
  #viewproduct-modal .item{
    display: inline-block;
    width: 100%;
  }
  @media (max-width: 575.98px) {
    .modal-header .dropdown-collapse{
      -webkit-box-pack: justify;
          -ms-flex-pack: justify;
              justify-content: space-between;
    }
    .info-product-kho .item{
      width: 100%;
    }
    .table-custom tr td:first-child{
      display: none;
    }
    #viewproduct-modal .table-custom tr td:last-child{
      justify-content: initial;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal large-modal" id="viewproduct-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Hàng về ngày 28-05-2019
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="item">
          <div class="info-product-kho">
            <div class="item">
              <label>Mã SKU:</label>
              <span>KMNCC-037GDA04</span>
            </div>
            <div class="item">
              <label>Tên sản phẩm:</label>
              <span>Khuyến mãi đồ chơi lắp ráp thông minh</span>
            </div>
          </div>
          <div class="box-payback-table">
            <div class="box-table">
              <table class="table table-custom table-addpro table-responsive">
                <thead>
                  <tr>
                    <th class="black-custom bold center-custom">STT</th>
                    <th class="black-custom bold center-custom">Mã vạch</th>
                    <th class="black-custom bold center-custom">Số lượng</th>
                    <th class="black-custom bold">Ngày nhập</th>
                    <th class="black-custom bold">Ngày hết hạn</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td data-title="STT" class="center-custom">1</td>
                    <td data-title="Mã vạch" class="center-custom">6388-4</td>
                    <td data-title="Số lượng" class="center-custom">2</td>
                    <td data-title="Ngày nhập"></td>
                    <td data-title="Ngày hết hạn">10-08-2020</td>
                  </tr>
                  <tr>
                    <td data-title="STT" class="center-custom">2</td>
                    <td data-title="Mã vạch" class="center-custom">6388-2</td>
                    <td data-title="Số lượng" class="center-custom">2</td>
                    <td data-title="Ngày nhập">16-09-2019</td>
                    <td data-title="Ngày hết hạn">31-12-2021</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
	jQuery(function(){

	})
</script>