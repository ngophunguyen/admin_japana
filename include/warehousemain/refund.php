<style>
  .info-create-receipt{
    display: flex;
    justify-content: space-between;
    width: 100%;
  }
  .info-create-receipt .item{
    width: 24%;
    float: left;
    margin-bottom: 10px;
  }
  .info-create-receipt .item label{
    font-size: 14px;
    line-height: 1.5;
    font-weight: 500;
    margin-right: 5px;
    margin-bottom: 5px;
  }
  .info-create-receipt .item input{
    width: 100%;
  }
  .box-payback-table{
    display: inline-block;
    float: left;
    width: 100%;
  }
  #yeucautrahang-modal .table-custom > thead > tr{
    border-bottom: 2px solid #eee;
  }
  .box-check>.item{
    float: right;
    width: 35%
  }
  .title-check{
    display: inline-block;
    width: 100%;
    border-bottom: 1px solid #ccc;
    padding-bottom: 10px;
  }
  .box-cmt{
    padding: 10px 5px;
  }
  .box-cmt textarea{
    margin-bottom: 15px;
  }
  .box-cmt button{
    float: right;
    margin-left: 10px;
  }
  #info-crc, #kiemduyet-crc{
    display: inline-block;
    position: relative;
    width: 100%;
  }
  .all-price-receipt{
    margin-top: 15px;
  }
  .title-refund{
    font-size: 16px;
    border-bottom:2px solid #eee;
    padding-bottom: 7px;
  }
  .addproduct .item{
    display: flex;
    align-items: center;
    width: 40%;
  }
  .addproduct .item button{
    margin-left: 15px;
  }
  .soluong-open{
    width: 90px;
    margin: 0 auto
  }
  @media (max-width: 575.98px) {
    #createreceipt-modal .modal-body{
      padding: 0;
    }
    .info-create-receipt .item, .info-create-receipt .item input, .box-check>.item{
      width: 100%;
    }
    .info-create-receipt .item:nth-child(even){
      margin-left: 0;
    }
    .info-create-receipt .item label{
      margin-bottom: 5px;
    }
    .title-check li:first-child, .detail-check>li:first-child,.table-custom tr td:first-child{
      display: none;
    }
    .title-list li{
      white-space: nowrap;
      overflow: scroll;
    }
    .box-check>.item .title-check li{
      display: block;
    }
    .all-price-receipt{
      margin-top: 0;
    }
    .info-create-receipt{
      display: inline-block;
      width: 100%;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
    .title-list li{
      white-space: nowrap;
      overflow: scroll;
    }
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
    .title-list li{
      white-space: nowrap;
      overflow: scroll;
    }
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {  
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal super-large-modal" id="yeucautrahang-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Tạo phiếu yêu cầu trả hàng
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <h3 class="title-refund">Nhập thông tin phiếu cần tạo</h3>
        <div class="info-create-receipt">
          <div class="item">
            <label>Nhà cung cấp:</label>
            <select class="form-control" name="id_supplier" id="id_supplier">
              <option value="-1">Chọn nhà cung cấp</option>
              <option value="0">Công ty trách nhiệm hữu hạn Asahi Nutifood</option>
              <option value="1">Giao nhận 365</option>
              <option value="2">SBS</option>
            </select>
          </div>
          <div class="item">
            <label>Đơn vị vận chuyển:</label>
            <select class="form-control" name="id_transport" id="id_transport">
              <option value="-1">Chọn đơn vị vận chuyển</option>
              <option value="0">Giao hàng tiết kiệm</option>
              <option value="1">Tín tốc</option>
              <option value="2">Giao hàng nhanh</option>
            </select>
          </div>
          <div class="item">
            <label>Phí vận chuyển:</label>
            <input type="text" value="" id="cost" name="transport_fee" placeholder="Nhập phí vận chuyển" class="form-control" data-type="currency">
          </div>
          <div class="item">
            <label>Mã vận đơn:</label>
            <input type="text" value="" id="code" name="transport_code" placeholder="Nhập mã vận đơn" class="form-control" data-type="currency">
          </div>
        </div>
        <h3 class="title-refund">Tìm kiếm sản phẩm cần trả hàng</h3>
        <div class="info-create-receipt addproduct">
          <div class="item">
            <input type="text" value="" id="keyword" name="keyword" placeholder="Tìm theo sku, mã kho hoặc tên sản phẩm" class="form-control" data-type="currency">
            <button type="button" class="button bg-green">Thêm</button>
          </div>
        </div>
        <div class="box-payback-table">
          <div class="box-table">
            <table class="table table-custom table-addpro table-responsive">
              <thead>
                <tr>
                  <th class="black-custom bold center-custom">STT</th>
                  <th class="black-custom bold">SKU</th>
                  <th class="black-custom bold">Tên sản phẩm</th>
                  <th class="black-custom bold center-custom">Mã kho</th>
                  <th class="black-custom bold center-custom">Số lượng</th>
                  <th class="black-custom bold">Ngày hết hạn</th>
                  <th class="black-custom bold center-custom">SL thực trả</th>
                  <th class="black-custom bold">Chọn</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-title="STT" class="center-custom">1</td>
                  <td data-title="SKU">001EE631</td>
                  <td data-title="Tên sản phẩm">Kem dưỡng ẩm Naturie Skin Conditioning Gel 180g</td>
                  <td data-title="Mã kho" class="center-custom">1006-1</td>
                  <td data-title="Số lượng" class="center-custom">1</td>
                  <td data-title="Ngày hết hạn">01-05-2021</td>
                  <td data-title="SL thực trả" class="center-custom">
                    <input type="number" class="form-control soluong-open" value="0" name="probill[1]" id="qty1" disabled="">
                  </td>
                  <td data-title="Chọn">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" id="chk_receipt1" onclick="checkedandopen(1)" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="box-check">
          <div class="item">
            <ul class="title-check">
              <li class="bold">Ghi chú</li>
            </ul>
            <div class="box-cmt">
              <textarea id="comment" rows="3" class="form-control input-cmt"></textarea>
              <div class="action-button">
                <button type="button" class="button bg-green">Tạo phiếu</button>
                <button type="button" data-dismiss="modal" class="button bg-red">Hủy</button>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function checkedandopen(id){
    if(jQuery('#chk_receipt'+id).prop('checked')==false){
      jQuery('#qty'+id).prop('disabled',true);
      jQuery('#qty'+id).addClass('bg-gray');
    }else{
      jQuery('#qty'+id).prop('disabled',false);
      jQuery('#qty'+id).removeClass('bg-gray');
    }
  }
	jQuery(function(){
      jQuery('#id_transport,#id_supplier').select2();
      jQuery('#createreceipt-modal .tab-custom .item a').click(function(){
        var data = jQuery(this).data('id');
        jQuery('#createreceipt-modal .tab-content').not('#' + data).removeClass('show-inline');
        jQuery(this).parent().addClass('active');
        jQuery('#createreceipt-modal .tab-custom .item a').not(this).parent().removeClass('active');
        jQuery('#createreceipt-modal #'+data).addClass('show-inline');
      });
    });
</script>