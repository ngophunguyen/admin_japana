<style>
	.box-addbanner{
    display: inline-block;
    width: 100%;
  }
  .box-addbanner .item{
    width: 49%;
    float: left;
    margin-bottom: 15px;
  }
  .box-addbanner .item:nth-child(even){
    margin-left: 2%;
  }
  .box-addbanner .item label{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 5px;
    font-weight: 500;
    width: 100%;
  }
  .box-addbanner .item:last-child{
    width: 100%;
    margin-bottom: 0;
  }
  .box-img-meta{
    max-height: 180px;
    height: 180px;
    display: none;
    margin-top: 15px;
    display: none;
  }
  .box-img-meta img{
    height: 100%;
    width: auto;
  }
  .input-group label.custom-upload{
    width: 100px!important;
  }
  @media (max-width: 575.98px) {
    .box-addbanner .item{
      width: 100%
    }
    .box-addbanner .item:nth-child(even){
      margin-left: 0;
    }
    .box-img-meta img{
      width: 100%;
      height: auto;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {  
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal medium-modal" id="addbanner-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Thêm banner
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
      	<div class="box-addbanner">
      		<div class="item">
      			<label for="name">Tên banner:</label>
	      		<input type="text" class="form-control" id="name" name="name" placeholder="Tên banner">
      		</div>
      		<div class="item">
      			<label for="link">Link Url:</label>
	      		<input type="text" class="form-control" id="link" name="link" placeholder="Link Url">
      		</div>
      		<div class="item">
      			<label for="imgs">Hình ảnh</label>
      			<div class="upload-img">
              <div class="input-group up-img">
                <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
                <label class="button bg-green custom-upload">
                  <input type="file" id="imgs" class="form-control" name="imgs" onchange="readURL(this);" accept="image/*">Upload
                </label>
                <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
              </div>
            </div>
            <div class="box-img-meta">
              <img src="" alt="banner" id="photo">
            </div>
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="button bg-red">Hủy</button>
        <button type="button" class="button bg-green">Lưu</button>
      </div>
    </div>
  </div>
</div>
<script>
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          jQuery('#photo').parent().css('display','block');
          jQuery('#photo').attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
	jQuery(function(){
		jQuery(document).on('change', ':file', function() {
      var input = jQuery(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });
    jQuery(':file').on('fileselect', function(event, numFiles, label) {
      var input = jQuery(this).parents('.up-img').find(':text'),
        log = numFiles > 1 ? numFiles + ' files selected' : label;
      if( input.length ) {
        input.val(log);
        if(input.val(log)){
          jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
          jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
        }
      } else {
        if(log) ;
      }
    });
    jQuery('.delete-img').on('click', function(e){
      jQuery(this).parent().find('input[type=file]').val('');
      jQuery(this).parent().find('input[type=text]').val('');
      jQuery(this).parent().find('.custom-upload').css('display','flex');
      jQuery(this).parent().parent().next().css('display','none');
      jQuery(this).css('display','none');
    });
	})
</script>