<style>
	.box-item{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
		width: 100%;
		margin-top: 15px;
	}
	.box-item .item{
		width: 49%;
		float: left;
	}
	.box-item .item label {
	    font-size: 14px;
	    line-height: 1.5;
	    margin-bottom: 5px;
	    font-weight: 500;
	    width: 100%;
	}
	.radio-custom{
		display: initial;
	}
	.box-radio{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		width: 50%;
		float: left;
	}
	.box-radio span{
		width: inherit;
		margin-left: 5px;
	}
	.box-item .item:nth-child(2) .box-radio label{
		width: auto!important;
	}
	#note{
		display: none;
		margin-top: 15px;
	}
	.detail-gift{
		font-size: 13px;
		margin-top: 10px;
		display: inline-block;
	}
	.time-graph{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
    	-webkit-box-pack: end;
    	    -ms-flex-pack: end;
    	        justify-content: flex-end;
    	width: 49%;
    	float: left;
	}
	#limit_used{
		width: 49%;
		float: left;
		margin-left: 2%;
	}
	.box-table{
		width: 100%;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	    float: left;
	}
	.table-custom{
		margin-top: 0;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		height: auto;
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	img.gift-order{
		height: 20px!important; 
		bottom: 5px; 
		left: 0;  
		position: absolute;
	}
	.box-table tr td:nth-child(5){
		width: 300px;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.first-price{
		text-decoration: line-through;
	    font-size: 12px;
	    color: #333;
	    width: 100%;
	    display: inline-block;
	    margin-bottom: 10px;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.table-custom .checkbox-custom {
	    cursor: pointer;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    height: 20px;
	}
	.dropdown-collapse, .box-search-products{
		margin-top: 15px;
	}
	.box-search-products{
	    display: inline-block;
	    width: 100%;
	}
	.box-search-products .item{
	    width: 49%;
	    float: left;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: justify;
	        -ms-flex-pack: justify;
	            justify-content: space-between;
	}
	.box-search-products .item:last-child{
	    margin-left: 2%;
	}
	.box-search-products .item label {
	    font-weight: 500;
	    font-size: 14px;
	    margin: 0 0 15px;
	}
	.box-search-products .item input, .box-search-products .item .custom-dropdown{
	    width: 75%;
	    float: left;
	}
	.box-search-products .item button{
	    width: 20%;
	    float: right;
	}
	.dropdown-table{
	    background: #fff;
	    padding: 10px;
	    position: absolute;
	    width: 200px;
	    display: none;
	    border: 1px solid #f1f1f1;
	    z-index: 100;
	}
	.dropdown-table li a:hover{
		color: red;
	}
	.col-dropdown:hover{
		cursor: pointer;
	}
	.dropdown-table li{
		padding: 5px;
	}
	.disable-input {
	    background: #fbfbfb;
	    color: #666666;
	}
	@media (max-width: 575.98px) {
		#info-dm, #listsp-dm{
			display: none;
			width: 100%;
		}
		.box-item, .box-item .item, .time-graph, #limit_used, .box-search-products .item{
			display: inline-block;
			width: 100%;
			margin: 0;
		}
		.box-item .item:last-child, .box-item .item .box-radio:nth-child(3), #limit_used{
			margin-top: 10px;
		}
		.box-item .item .box-radio{
			width: 100%;
		}
		.box-table tr td:nth-child(3){
			width: auto;
		}
		img.gift-order{
			left: auto;
		}
		.first-price{
			margin-bottom: 0;
			width: auto;
		}
		.box-search-products .item:last-child{
			margin: 15px 0;
		}
		.table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: initial;
			    -ms-flex-pack: initial;
			        justify-content: initial;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-radio, .time-graph, #limit_used{
			width: 100%;
		}
		#limit_used{
			margin: 15px 0 0;
		}
		.box-item .item .box-radio:nth-child(3){
			margin-top: 10px;
		}
		.table-custom {
		    white-space: nowrap;
		}
		.first-price{
			margin-right: 10px;
			width: auto;
		}
		.table-custom > tbody > tr > td:last-child{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.box-radio, .time-graph, #limit_used{
			width: 100%;
		}
		#limit_used{
			margin: 15px 0 0;
		}
		.box-item .item .box-radio:nth-child(3){
			margin-top: 10px;
		}
		.table-custom {
		    white-space: nowrap;
		}
		.first-price{
			margin-right: 10px;
			width: auto;
		}
		.table-custom > tbody > tr > td:last-child{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
		.box-radio{
			width: 100%;
		}
		.box-item .item .box-radio:nth-child(3){
			margin-top: 10px;
		}
		.table-custom {
		    white-space: nowrap;
		}
		.first-price{
			margin-right: 10px;
			width: auto;
		}
		.table-custom > tbody > tr > td:last-child{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="editprogram content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Tên chương trình</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom btn-custom-info" title="Lưu lại">
						<i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="tab-custom bg-black">
				<div class="item active">
					<a href="javascript:void(0)" data-id="info-dm" title="Chi tiết danh mục">
						<i class="fa fa-sticky-note" aria-hidden="true"></i>
						<label>Chi tiết</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="listsp-dm" title="Danh sách sản phẩm">
						<i class="fa fa-cube" aria-hidden="true"></i>
						<label>Sản phẩm</label>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<div id="info-dm" class="tab-content item show-inline">
					<div class="box-item">
						<div class="item">
							<label>Tên chương trình:</label>
							<div class="box-input">
								<input value="Tên chương trình" name="name" type="text" class="form-control" placeholder="Nhập tên nhóm...">
							</div>
						</div>
						<div class="item">
							<label>Thông điệp hiển thị:</label>
							<div class="box-input">
								<input value="Thông điệp hiển thị" name="description" type="text" class="form-control" placeholder="Thông điệp hiển thị....">
							</div>
						</div>
					</div>
					<div class="box-item">
						<div class="item">
							<label>Điều kiện:</label>
							<div class="box-radio">
								<label class="radio-custom" for="giatriDH">
					              	<input value="1" id="giatriDH" type="radio" name="type" checked="">
					              	<span class="checkmark"></span>
					              	Giá trị ĐH &#8805;
					            </label>
					            <input name="min_price" type="text" class="form-control inputgiatriDH" placeholder="Nhập số" data-type='currency'>
					            <span>( VND )</span>
							</div>
							<div class="box-radio">
								<label class="radio-custom" for="apdungMa">
					              	<input value="2" id="apdungMa" type="radio" name="type">
					              	<span class="checkmark"></span>
					              	Mã giảm giá
					            </label>
					            <input name="code" type="text" class="form-control inputapdungMa" placeholder="Nhập mã">
							</div>
							<textarea id="note" class="form-control" rows="3" name="note" placeholder="Nhập điều kiện được khuyến mãi đơn hàng"></textarea>
						</div>
						<div class="item">
							<label>Giảm giá:</label>
							<div class="box-radio">
								<label class="radio-custom" for="radiophantram">
					              	<input id="radiophantram" checked="" type="radio" name="discount_type" value="1">
					              	<span class="checkmark"></span>
					            </label>
					            <input value="" name="discount_percent" type="text" class="form-control" placeholder="Nhập số" data-type='currency'>
					            <span>( % )</span>
							</div>
							<div class="box-radio">
								<label class="radio-custom" for="radiotien">
					              	<input id="radiotien" type="radio" name="discount_type" value="2">
					              	<span class="checkmark"></span>
					            </label>
					            <input value="" name="discount" type="text" class="form-control" placeholder="Nhập số" data-type='currency'>
					            <span>( VND )</span>
							</div>
						</div>
					</div>
					<div class="box-item">
						<div class="item">
							<label>Quà tặng:</label>
							<div class="box-input">
								<input name="gift_sku" type="text" class="form-control" value="" placeholder="SKU quà tặng">
							</div>
							<span class="detail-gift red-custom">Chưa có mã SKU</span>
						</div>
						<div class="item">
							<label>Thời gian áp dụng:</label>
							<div class="time-graph">
			                	<div class="box-time">
				                	<input type="hidden" name="start_date" id="date_start" value="">
				                	<input type="hidden" name="end_date" id="end_date" value="">
		                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-event" id="date-event" class="form-control ipt-date" placeholder="Chọn ngày...">
		                            <i class="fa fa-calendar icon-time"></i>
		                        </div>
			                </div>
			                <input id="limit_used" type="number" name="limit_used" class="form-control" value="" placeholder="Nhập số lượng mã giảm giá">
						</div>
					</div>
				</div>
				<div id="listsp-dm" class="tab-content item">
					<a class="dropdown-collapse bg-black apdung-title" href="javascript:void(0);">Danh sách sản phẩm áp dụng</a>
					<a class="dropdown-collapse bg-black loaitru-title" href="javascript:void(0);">Danh sách sản phẩm loại trừ</a>
					<div class="box-search-products">
			          	<div class="item">
	                        <input type="hidden" name="id" value="">
	                        <input type="hidden" name="user_id" value="">
			            	<input onkeyup="initShowBtn();" type="text" name="sku" placeholder="Tìm theo SKU..." class="form-control">
			              	<button onclick="submitForm('#frmAddProductByGroup')" type="button" class="button bg-green">Thêm</button>
			          	</div>
			          	<div class="item">
	                        <input type="hidden" name="id" value="">
	                        <input type="hidden" name="user_id" value="1">
	                        <input id="typeAdd" type="hidden" name="type" value="1">
			            	<div class="custom-dropdown">
			              		<select class="form-control list-event-add-product" name="style" id="style">
					                <option value="-1">Chọn...</option>
					                <option value="0" selected="selected">Tất cả</option>
					                <option value="1">Sản phẩm khuyến mãi</option>
					                <option value="2">Sản phẩm hot</option>
					                <option value="3">Sản phẩm mới</option>
			              		</select>
			            	</div>
			            	<button onclick="submitForm('#frmAddProductByGroup')" type="button" class="button bg-green">Thêm</button>
			          	</div>
			        </div>
			        
					<div class="box-table">
						<table class="table table-custom table-responsive">
						    <thead>
						        <tr>
						        	<th class="black-custom bold center-custom">
			                    		<label class="checkbox-custom">
					                     	<input value="0" id="checkAll" type="checkbox" class="selectall">
					                      	<span class="checkmark"></span>
					                    </label>
					                 </th>
						            <th class="black-custom bold center-custom">STT</th>
						            <th class="black-custom bold">SKU</th>
						            <th class="black-custom bold center-custom">Hình ảnh</th>
						            <th class="black-custom bold">Tên sản phẩm</th>
						            <th class="black-custom bold">
						            	<span class="col-dropdown">
						            		Sự kiện 
						            		<i class="fa fa-caret-down" aria-hidden="true"></i>
						            	</span>
						            	<ul class="dropdown-table dropdown-event-filter">
	                                        <li>
	                                        	<a href="javascript:;" title="Sản phẩm khuyến" data-type="1" data-id="1">Sản phẩm khuyến mãi</a>
	                                        </li>
	                                        <li>
	                                        	<a href="javascript:;" title="Giao hàng 60'" data-type="1" data-id="63">Giao hàng 60'</a>
	                                        </li>
	                                        <li>
	                                        	<a href="javascript:;" title="Sản phẩm mới" data-type="1" data-id="66">Sản phẩm mới</a>
	                                        </li>
	                                        <li>
	                                        	<a href="javascript:;" title="sản phẩm hot" data-type="1" data-id="67">sản phẩm hot</a>
	                                        </li>
	                                    </ul>
						            </th>
						            <th class="black-custom bold">Giá cũ</th>
						            <th class="black-custom bold">Giá mới</th>
						            <th class="black-custom bold">Tác vụ</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(1);" name="checkboxMulti[1]" id="checkBox-1" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">1</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Sự kiện">Sản phẩm mới</td>
						            <td data-title="Giá cũ">1,500,000 VND</td>
						            <td data-title="Giá mới">1,500,000 VND</td>
						            <td data-title="Tác vụ">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(2);" name="checkboxMulti[2]" id="checkBox-2" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">2</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Sự kiện">Sản phẩm chiến lược</td>
						            <td data-title="Giá cũ">1,500,000 VND</td>
						            <td data-title="Giá mới">1,500,000 VND</td>
						            <td data-title="Tác vụ">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(3);" name="checkboxMulti[3]" id="checkBox-3" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">3</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Sự kiện">Sản phẩm chiến lược</td>
						            <td data-title="Giá cũ">1,500,000 VND</td>
						            <td data-title="Giá mới">1,500,000 VND</td>
						            <td data-title="Tác vụ">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(4);" name="checkboxMulti[4]" id="checkBox-4" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">4</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Sự kiện">Sản phẩm mới,Sản phẩm chiến lược</td>
						            <td data-title="Giá cũ">1,500,000 VND</td>
						            <td data-title="Giá mới">1,500,000 VND</td>
						            <td data-title="Tác vụ">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="Xoá">
						            	<label class="checkbox-custom">
						                  <input class="checkboxMulti" onclick="checkBox(5);" name="checkboxMulti[5]" id="checkBox-5" value="0" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="STT" class="center-custom">5</td>
						            <td data-title="SKU">087AA9</td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
						            </td>
						            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						            <td data-title="Sự kiện">Sản phẩm mới</td>
						            <td data-title="Giá cũ">1,500,000 VND</td>
						            <td data-title="Giá mới">1,500,000 VND</td>
						            <td data-title="Tác vụ">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
						            		<i class="fa fa-trash-o"></i>
						            	</a>
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
					<button onclick="checkBoxMulti();" type="button" class="button button-header red-custom removelist"><i class="fa fa-minus-circle" aria-hidden="true"></i> Xoá khỏi danh sách</button>
					<?php include('include/pagination.php')?>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		jQuery("#checkAll").change(function () {
	        var status = 0;
	        if(jQuery(this).prop("checked") == true){
	            status = 1;
	        }
	        jQuery("input:checkbox.checkboxMulti").prop('checked', status);
	        jQuery("input:checkbox.checkboxMulti").prop('value', status);
	    });
		var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    if(window.innerWidth > 500) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
		jQuery('input[name="date-event"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			locale: formatDate
		},function(start,end) {
			jQuery('#date_start').val(start.format('DD-MM-YYYY'));
			jQuery('#end_date').val(end.format('DD-MM-YYYY'));
		});

		jQuery('.col-dropdown').click(function(e){
		    e.stopPropagation();
		    jQuery(".dropdown-table").slideToggle();
		});

		jQuery(document).click(function(){
		    jQuery('.dropdown-table').slideUp();
		});

		jQuery('.tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.tab-content').not('#' + data).removeClass('show-inline');

	    	jQuery(this).parent().addClass('active');
	    	jQuery('.tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    });

		if(jQuery('#giatriDH').prop('checked')==true){
			jQuery('.inputgiatriDH').removeClass('disable-input').prop('disabled',false);
		    jQuery('.inputapdungMa').addClass('disable-input').prop('disabled',true);
		    jQuery('.apdung-title').css('display','none');
		    jQuery('.loaitru-title').css('display','flex');
		    jQuery('#note').css('display','inline-block');
		}
		else{
			jQuery('.inputapdungMa').removeClass('disable-input').prop('disabled',false);
		    jQuery('.inputgiatriDH').addClass('disable-input').prop('disabled',true);
		    jQuery('.apdung-title').css('display','flex');
		    jQuery('.loaitru-title').css('display','none');
		    jQuery('#note').css('display','none');
		}
	    jQuery('#giatriDH').click(function() {
	    	jQuery('.inputgiatriDH').removeClass('disable-input').prop('disabled',false);
		    jQuery('.inputapdungMa').addClass('disable-input').prop('disabled',true);
		    jQuery('.apdung-title').css('display','none');
		    jQuery('.loaitru-title').css('display','flex');
		    jQuery('#note').css('display','inline-block');
		});
	    jQuery('#apdungMa').click(function() {
	    	jQuery('.inputapdungMa').removeClass('disable-input').prop('disabled',false);
		    jQuery('.inputgiatriDH').addClass('disable-input').prop('disabled',true);
		    jQuery('.apdung-title').css('display','flex');
		    jQuery('.loaitru-title').css('display','none');
		    jQuery('#note').css('display','none');
		});
	})
</script>