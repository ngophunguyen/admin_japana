<style>
	.box-quick-search {
	    display: inline-block;
	    width: 100%;
	    margin-top: 15px;
	}
	.box-quick-search .item {
	    display: inline-block;
	    width: 50%;
	    float: left;
	}
	.box-quick-search .item:first-child input{
		width: 50%;
    	float: left;
    	margin-right: 15px;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		padding: 15px;
	}
	@media (max-width: 575.98px) {
		.entry-header ul{
			display: none;
		}
		.entry-content{
			margin-top: 48px!important;
		}
		.box-quick-search .item, .box-quick-search .item:first-child input{
			width: 100%;
			margin-right: 0;
			margin-bottom: 10px;
		}
		.box-quick-search .item{
			margin-bottom: 0;
		}
		.box-table{
			margin-bottom: 30px;
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
		.box-table tr td:last-child{
			padding: 0;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom {
		    white-space: nowrap;
		}
		.box-quick-search .item{
			width: 100%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom {
		    white-space: nowrap;
		}
		.box-quick-search .item{
			width: 100%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
		.table-custom {
		    white-space: nowrap;
		}
		.box-quick-search .item{
			width: 100%;
		}
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="group-products content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Tạo nhóm sản phẩm</h1>
			<ul>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post" class="search1">
							<input type="hidden" name="user_id" value="1">
	                       	<input value="" name="name" type="text" placeholder="Nhập tên nhóm..." class="form-control custom-ipt">
	                       	<button type="submit" class="button bg-black">Tạo nhóm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Tên nhóm</th>
					            <th class="bg-black">Ngày tạo</th>
					            <th class="bg-black">Nhân viên tạo</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Tên nhóm"><a href="?action=include/promotion/addpro-group.php" title="Xem">Trung Thu</a></td>
					            <td data-title="Ngày tạo">23/04/2019 - 15:46</td>
					            <td data-title="Nhân viên tạo">Deverloper</td>  
					            <td data-title="Tác vụ">
					            	<a href="?action=include/promotion/addpro-group.php" class="link-custom black-custom" title="Xem">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Tên nhóm"><a href="?action=include/promotion/addpro-group.php" title="Xem">Block - Quà Tặng Đặc Biệt</a></td>
					            <td data-title="Ngày tạo">23/04/2019 - 15:46</td>
					            <td data-title="Nhân viên tạo">Deverloper</td>  
					            <td data-title="Tác vụ">
					            	<a href="?action=include/promotion/addpro-group.php" class="link-custom black-custom" title="Xem">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Tên nhóm"><a href="?action=include/promotion/addpro-group.php" title="Xem">Block - Khuyến Mãi - Bình Giữ Nhiệt</a></td>
					            <td data-title="Ngày tạo">23/04/2019 - 15:46</td>
					            <td data-title="Nhân viên tạo">Deverloper</td>  
					            <td data-title="Tác vụ">
					            	<a href="?action=include/promotion/addpro-group.php" class="link-custom black-custom" title="Xem">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Tên nhóm"><a href="?action=include/promotion/addpro-group.php" title="Xem">Event - Khuyến Mãi</a></td>
					            <td data-title="Ngày tạo">23/04/2019 - 15:46</td>
					            <td data-title="Nhân viên tạo">Deverloper</td>  
					            <td data-title="Tác vụ">
					            	<a href="?action=include/promotion/addpro-group.php" class="link-custom black-custom" title="Xem">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Tên nhóm"><a href="?action=include/promotion/addpro-group.php" title="Xem">Event - Khuyến mãi - Thực Phẩm Chức Năng</a></td>
					            <td data-title="Ngày tạo">23/04/2019 - 15:46</td>
					            <td data-title="Nhân viên tạo">Deverloper</td>  
					            <td data-title="Tác vụ">
					            	<a href="?action=include/promotion/addpro-group.php" class="link-custom black-custom" title="Xem">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Tên nhóm"><a href="?action=include/promotion/addpro-group.php" title="Xem">	Event - Khuyến Mãi - Chăm Sóc Cơ Thể</a></td>
					            <td data-title="Ngày tạo">23/04/2019 - 15:46</td>
					            <td data-title="Nhân viên tạo">Deverloper</td>  
					            <td data-title="Tác vụ">
					            	<a href="?action=include/promotion/addpro-group.php" class="link-custom black-custom" title="Xem">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Tên nhóm"><a href="?action=include/promotion/addpro-group.php" title="Xem">Event - Khuyến Mãi Collagen</a></td>
					            <td data-title="Ngày tạo">23/04/2019 - 15:46</td>
					            <td data-title="Nhân viên tạo">Deverloper</td>  
					            <td data-title="Tác vụ">
					            	<a href="?action=include/promotion/addpro-group.php" class="link-custom black-custom" title="Xem">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Tên nhóm"><a href="?action=include/promotion/addpro-group.php" title="Xem">Event - Khuyến Mãi SKII</a></td>
					            <td data-title="Ngày tạo">23/04/2019 - 15:46</td>
					            <td data-title="Nhân viên tạo">Deverloper</td>  
					            <td data-title="Tác vụ">
					            	<a href="?action=include/promotion/addpro-group.php" class="link-custom black-custom" title="Xem">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Tên nhóm"><a href="?action=include/promotion/addpro-group.php" title="Xem">MGG - Giảm 10% - T9 - T910</a></td>
					            <td data-title="Ngày tạo">23/04/2019 - 15:46</td>
					            <td data-title="Nhân viên tạo">Deverloper</td>  
					            <td data-title="Tác vụ">
					            	<a href="?action=include/promotion/addpro-group.php" class="link-custom black-custom" title="Xem">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Tên nhóm"><a href="?action=include/promotion/addpro-group.php" title="Xem">	Block - Sản Phẩm Hot</a></td>
					            <td data-title="Ngày tạo">23/04/2019 - 15:46</td>
					            <td data-title="Nhân viên tạo">Deverloper</td>  
					            <td data-title="Tác vụ">
					            	<a href="?action=include/promotion/addpro-group.php" class="link-custom black-custom" title="Xem">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){

	})
</script>