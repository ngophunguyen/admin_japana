<style>
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		padding: 15px;
	}
	@media (max-width: 575.98px) {
		.entry-header ul{
			display: none;
		}
		.entry-content{
			margin-top: 48px!important;
		}
		.box-table{
			margin-bottom: 30px;
		}
		.table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: initial;
			    -ms-flex-pack: initial;
			        justify-content: initial;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom {
		    white-space: nowrap;
		}
		.box-quick-search .item{
			width: 100%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom {
		    white-space: nowrap;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
		.table-custom {
		    white-space: nowrap;
		}
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="list-promo content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách mã khuyến mãi</h1>
			<ul>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Mã khuyến mãi</th>
					            <th class="bg-black">Tên chương trình</th>
					            <th class="bg-black">Thời gian</th>
					            <th class="bg-black">Đã sử dụng</th>
					            <th class="bg-black">Trạng thái</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Mã KM">T910</td>
					            <td data-title="Chương trình">MGG - Giảm 10% - T9 - T910</td>
					            <td data-title="Thời gian">23/04/2019 - 28/04/2019</td>
					            <td data-title="Đã sử dụng">20/80</td>  
					            <td data-title="Trạng thái">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Mã KM">T920</td>
					            <td data-title="Chương trình">MGG - Giảm 20% - T9 - T920</td>
					            <td data-title="Thời gian">2018/09/12 - 2018/09/30</td>
					            <td data-title="Đã sử dụng">8/50</td>  
					            <td data-title="Trạng thái">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Mã KM">km100k</td>
					            <td data-title="Chương trình">km 100%</td>
					            <td data-title="Thời gian">2018/11/07 - 2018/12/08</td>
					            <td data-title="Đã sử dụng">14/100</td>  
					            <td data-title="Trạng thái">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Mã KM">km100v2</td>
					            <td data-title="Chương trình">km100%ver2</td>
					            <td data-title="Thời gian">23/04/2019 - 28/04/2019</td>
					            <td data-title="Đã sử dụng">20/80</td>  
					            <td data-title="Trạng thái">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Mã KM">kmt1</td>
					            <td data-title="Chương trình">kmtest1</td>
					            <td data-title="Thời gian">23/04/2019 - 28/04/2019</td>
					            <td data-title="Đã sử dụng">20/80</td>  
					            <td data-title="Trạng thái">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Mã KM">T910</td>
					            <td data-title="Chương trình">MGG - Giảm 10% - T9 - T910</td>
					            <td data-title="Thời gian">23/04/2019 - 28/04/2019</td>
					            <td data-title="Đã sử dụng">20/80</td>  
					            <td data-title="Trạng thái">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Mã KM">T1210</td>
					            <td data-title="Chương trình">T1210</td>
					            <td data-title="Thời gian">23/04/2019 - 28/04/2019</td>
					            <td data-title="Đã sử dụng">20/80</td>  
					            <td data-title="Trạng thái">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Mã KM">JAPANA5487</td>
					            <td data-title="Chương trình">JAPANA5487eee</td>
					            <td data-title="Thời gian">23/04/2019 - 28/04/2019</td>
					            <td data-title="Đã sử dụng">20/80</td>  
					            <td data-title="Trạng thái">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Mã KM">japana01</td>
					            <td data-title="Chương trình">mgg01</td>
					            <td data-title="Thời gian">23/04/2019 - 28/04/2019</td>
					            <td data-title="Đã sử dụng">20/80</td>  
					            <td data-title="Trạng thái">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Mã KM">GIAM10T8</td>
					            <td data-title="Chương trình">Giảm 10% T8</td>
					            <td data-title="Thời gian">23/04/2019 - 28/04/2019</td>
					            <td data-title="Đã sử dụng">20/80</td>  
					            <td data-title="Trạng thái">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){

	})
</script>