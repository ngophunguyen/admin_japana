<style>
  .checkbox-custom{
    cursor: pointer;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    height: 20px;
  }
  .checkbox-custom label{
    color: #ccc;
  }
  .box-search-products{
    display: inline-block;
    width: 100%;
  }
  .box-search-products .item{
    width: 49%;
    float: left;
    display: inline-block;
  }
  .box-search-products .item:last-child{
    margin-left: 2%;
  }
  .box-search-products .item .custom-dropdown{
    width: 100%;
    display: inline-block;
  }
  .box-search-products .item label {
    font-weight: 500;
    font-size: 14px;
    margin: 0 0 15px;
  }
  .box-search-products .item input{
    width: 75%;
    float: left;
  }
  .box-search-products .item button{
    width: 20%;
    float: right;
  }
  #addpro-modal .box-table .table-custom tr th:nth-child(3), #addpro-modal .box-table .table-custom tr td:nth-child(3){
    width: 130px;
  }
  #addpro-modal .box-table .table-custom thead {
    border-bottom: 1px solid #eee;
  }
  @media (max-width: 575.98px) {
    .box-search-products .item{
      width: 100%;
      margin-bottom: 10px;
    }
    .box-search-products .item:last-child{
      margin-left: 0;
    }
    .box-search-products .item label{
      margin-bottom: 5px;
    }
    .box-search-products .item input{
      width: 65%;
    }
    .box-search-products .item button{
      width: 30%;
    }
    #addpro-modal .box-table .table-custom > tbody > tr td:nth-child(2), #addpro-modal .box-table .table-custom > tbody > tr td:first-child{
      display: none;
    }
    #addpro-modal .box-table .table-custom tr th:nth-child(3), #addpro-modal .box-table .table-custom tr td:nth-child(3){
      width: 100%;
    }
    #addpro-modal .box-table{
      margin-bottom: 0;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {

  }
  @media (min-width: 768px) and (max-width: 991.98px) {

  }
  @media (min-width: 992px) and (max-width: 1199.98px) {
    
  }
  @media (min-width: 1200px) {
    
  }
</style>
<div class="modal large-modal" id="addpro-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Thêm sản phẩm
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="box-search-products">
          <div class="item">
            <label for="sku" class="black-custom">
              Nhập 1 hoặc nhiều SKU
            </label>
            <div class="box-input-coupon">
              <input type="text" name="sku" id="sku" class="form-control" placeholder="Nhập một hoặc nhiều SKU...">
              <button type="submit" class="button bg-black">Tìm kiếm</button>
            </div>
          </div>
          <div class="item">
            <label for="name_vi" class="black-custom">
              Nhập theo nhóm
            </label>
            <div class="custom-dropdown">
              <select class="form-control list-event-add-product" name="style" id="style">
                <option value="-1">Chọn...</option>
                <option value="0" selected="selected">Tất cả</option>
                <option value="1">Sản phẩm khuyến mãi</option>
                <option value="2">Sản phẩm hot</option>
                <option value="3">Sản phẩm mới</option>
              </select>
            </div>
          </div>
        </div>
        <div class="box-payback-table">
          <div class="box-table">
            <table class="table table-custom table-addpro table-responsive">
              <thead>
                <tr>
                  <th class="black-custom bold center-custom">
                    <label class="checkbox-custom">
                      <input value="" id="checkAll" type="checkbox" onclick="checkall()">
                      <span class="checkmark"></span>
                    </label>
                  </th>
                  <th class="black-custom bold center-custom">STT</th>
                  <th class="black-custom bold">SKU</th>
                  <th class="black-custom bold">Tên sản phẩm</th>
                  <th class="black-custom bold center-custom">Giảm giá</th>
                  <th class="black-custom bold">Quà tặng</th>
                  <th class="black-custom bold">Sự kiện</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">1</td>
                  <td data-title="SKU">012DF4</td>
                  <td data-title="Tên sản phẩm">Viên uống đẹp da CoQ10 Aoza Nhật Bản 150 viên</td>
                  <td data-title="Giảm giá" class="center-custom">40%</td>
                  <td data-title="Quà tặng">KM001</td>
                  <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
                </tr>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">2</td>
                  <td data-title="SKU">012DF4</td>
                  <td data-title="Tên sản phẩm">Viên uống đẹp da CoQ10 Aoza Nhật Bản 150 viên</td>
                  <td data-title="Giảm giá" class="center-custom">40%</td>
                  <td data-title="Quà tặng">KM001</td>
                  <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
                </tr>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">3</td>
                  <td data-title="SKU">012DF4</td>
                  <td data-title="Tên sản phẩm">Viên uống đẹp da CoQ10 Aoza Nhật Bản 150 viên</td>
                  <td data-title="Giảm giá" class="center-custom">40%</td>
                  <td data-title="Quà tặng">KM001</td>
                  <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
                </tr>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">4</td>
                  <td data-title="SKU">012DF4</td>
                  <td data-title="Tên sản phẩm">Viên uống đẹp da CoQ10 Aoza Nhật Bản 150 viên</td>
                  <td data-title="Giảm giá" class="center-custom">40%</td>
                  <td data-title="Quà tặng">KM001</td>
                  <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
                </tr>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">5</td>
                  <td data-title="SKU">012DF4</td>
                  <td data-title="Tên sản phẩm">Viên uống đẹp da CoQ10 Aoza Nhật Bản 150 viên</td>
                  <td data-title="Giảm giá" class="center-custom">40%</td>
                  <td data-title="Quà tặng">KM001</td>
                  <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
                </tr>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">6</td>
                  <td data-title="SKU">012DF4</td>
                  <td data-title="Tên sản phẩm">Viên uống đẹp da CoQ10 Aoza Nhật Bản 150 viên</td>
                  <td data-title="Giảm giá" class="center-custom">40%</td>
                  <td data-title="Quà tặng">KM001</td>
                  <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
                </tr>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">7</td>
                  <td data-title="SKU">012DF4</td>
                  <td data-title="Tên sản phẩm">Viên uống đẹp da CoQ10 Aoza Nhật Bản 150 viên</td>
                  <td data-title="Giảm giá" class="center-custom">40%</td>
                  <td data-title="Quà tặng">KM001</td>
                  <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
                </tr>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">8</td>
                  <td data-title="SKU">012DF4</td>
                  <td data-title="Tên sản phẩm">Viên uống đẹp da CoQ10 Aoza Nhật Bản 150 viên</td>
                  <td data-title="Giảm giá" class="center-custom">40%</td>
                  <td data-title="Quà tặng">KM001</td>
                  <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
                </tr>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">9</td>
                  <td data-title="SKU">012DF4</td>
                  <td data-title="Tên sản phẩm">Viên uống đẹp da CoQ10 Aoza Nhật Bản 150 viên</td>
                  <td data-title="Giảm giá" class="center-custom">40%</td>
                  <td data-title="Quà tặng">KM001</td>
                  <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
                </tr>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">10</td>
                  <td data-title="SKU">012DF4</td>
                  <td data-title="Tên sản phẩm">Viên uống đẹp da CoQ10 Aoza Nhật Bản 150 viên</td>
                  <td data-title="Giảm giá" class="center-custom">40%</td>
                  <td data-title="Quà tặng">KM001</td>
                  <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="button bg-green">Thêm</button>
      </div>
    </div>
  </div>
</div>
<script>
  function checkall(){
    var i;
    var checkBox = document.getElementById("checkAll");
    var childcheck = document.getElementsByClassName("chk2");
    var childchecklen = childcheck.length;
    if (checkBox.checked == true){
        for (i = 0; i < childchecklen; i += 1) {
            childcheck[i].checked = true;
        }
    }
    else{
        for (i = 0; i < childchecklen; i += 1) {
            childcheck[i].checked = false;
        }
    }
  }
  jQuery(function(){
  })
</script>