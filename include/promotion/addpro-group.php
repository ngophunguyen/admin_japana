<style>
	.seo-input{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: baseline;
		    -ms-flex-align: baseline;
		        align-items: baseline;
		margin: 15px 0 0;
	}
	.seo-input input{
		margin-left: 15px;
		width: 50%;
		float: right;
	}
	.box-quick-search {
	    display: inline-block;
	    width: 100%;
	    margin-top: 15px;
	}
	.box-quick-search .item {
	    display: inline-block;
	    width: 50%;
	    float: left;
	}
	.box-quick-search .item:first-child input{
		width: 50%;
    	float: left;
	}
	.box-quick-search .item:first-child button {
	    float: left;
	    margin-left: 15px;
	}
	.custom-dropdown{
		width: auto;
		float: right;
	}
	.custom-dropdown:after{
		padding: 12px 15px;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin: 15px 0;
	    position: relative;
	}
	.table-custom{
		margin-top: 0;
		white-space: nowrap;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		height: auto;
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	img.gift-order{
		height: 20px!important; 
		bottom: 5px; 
		left: 0;  
		position: absolute;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		padding: 15px 0;
	}
	.table-custom .checkbox-custom {
	    cursor: pointer;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    height: 20px;
	}
	.removelist{
		padding: 0;
	}
	.removelist:hover{
		color: #ff0000!important;
	}
	@media (max-width: 575.98px) {
		.seo-input, .seo-input input, .seo-input label, .custom-dropdown, .box-quick-search .item{
			width: 100%;
			display: inline-block;
			margin-left: 0;
		}
		.box-quick-search{
			margin-top: 10px;
		}
		.seo-input label, .box-quick-search .item:first-child{
			margin-bottom: 10px;
		}
		.box-table tr td:nth-child(3){
	  		width: 100%;
	  	}
	  	.table-custom > tbody > tr > td{
			white-space: normal;
		}
		img.gift-order{
			left: auto;
		}
		.first-price{
			margin-bottom: 0;
			width: auto;
		}
		.table-custom > tbody > tr > td .box-time, .table-custom > tbody > tr > td .custom-dropdown{
			width: 100%;
			padding: 5px 0;
		}
		.table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: initial;
			    -ms-flex-pack: initial;
			        justify-content: initial;
			padding: 0;
		}
		.removelist{
			margin: 0 0 45px;
		}
		.removelist i{
			width: auto;
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="addpro-group content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Nhóm sản phẩm</h1>
			<ul>
				<li>
					<button type="button" data-toggle="modal" data-target="#addpro-modal" class="button button-header black-custom">
	                    <i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
	                </button>
				</li>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" onclick="submitFrmUpdateGroup()" title="Lưu lại">
						<i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<span class="seo-input">
					<label>Tên nhóm:</label>
					<input autocomplete="off" type="text" name="name" placeholder="Nhập tên nhóm..." value="" class="form-control">
				</span>
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post" class="search1">
	                       <input autocomplete="off" name="search" value="" type="text" class="form-control custom-ipt" placeholder="Tìm SKU...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<div class="custom-dropdown">
				    		<select class="form-control" name="style" id="style">
					    		<option value="-1">Chọn...</option>
							  	<option value="0" selected="selected">Tất cả</option>
							  	<option value="1">Sản phẩm khuyến mãi</option>
							  	<option value="2">Sản phẩm hot</option>
							  	<option value="3">Sản phẩm mới</option>
							</select>
				    	</div>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-responsive">
					    <thead>
					        <tr>
					        	<th class="black-custom bold center-custom">
		                    		<label class="checkbox-custom">
				                     	<input value="0" id="checkAll" type="checkbox" class="selectall">
				                      	<span class="checkmark"></span>
				                    </label>
				                 </th>
					            <th class="black-custom bold center-custom">STT</th>
					            <th class="black-custom bold">SKU</th>
					            <th class="black-custom bold center-custom">Hình ảnh</th>
					            <th class="black-custom bold">Tên sản phẩm</th>
					            <th class="black-custom bold">Giảm giá</th>
					            <th class="black-custom bold">Quà tặng</th>
					            <th class="black-custom bold">Sự kiện</th>
					            <th class="black-custom bold">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					        	<td data-title="Xoá">
					            	<label class="checkbox-custom">
					                  <input class="checkboxMulti" name="product_list_id" value="0" type="checkbox">
					                  <span class="checkmark"></span>
					                </label>
					            </td>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="SKU">087AA9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giảm giá">32%</td>
					            <td data-title="Quà tặng">020EE47</td>
					            <td data-title="Sự kiện">Sản phẩm khuyến mãi,sản phẩm hot</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					        	<td data-title="Xoá">
					            	<label class="checkbox-custom">
					                  <input class="checkboxMulti" name="product_list_id" value="0" type="checkbox">
					                  <span class="checkmark"></span>
					                </label>
					            </td>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="SKU">087AA9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giảm giá">32%</td>
					            <td data-title="Quà tặng">020EE47</td>
					            <td data-title="Sự kiện">Sản phẩm khuyến mãi</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					        	<td data-title="Xoá">
					            	<label class="checkbox-custom">
					                  <input class="checkboxMulti" name="product_list_id" value="0" type="checkbox">
					                  <span class="checkmark"></span>
					                </label>
					            </td>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="SKU">087AA9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giảm giá">32%</td>
					            <td data-title="Quà tặng">020EE47</td>
					            <td data-title="Sự kiện">sản phẩm hot</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					        	<td data-title="Xoá">
					            	<label class="checkbox-custom">
					                  <input class="checkboxMulti" name="product_list_id" value="0" type="checkbox">
					                  <span class="checkmark"></span>
					                </label>
					            </td>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="SKU">087AA9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giảm giá">32%</td>
					            <td data-title="Quà tặng">020EE47</td>
					            <td data-title="Sự kiện">sản phẩm hot</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					        	<td data-title="Xoá">
					            	<label class="checkbox-custom">
					                  <input class="checkboxMulti" name="product_list_id" value="0" type="checkbox">
					                  <span class="checkmark"></span>
					                </label>
					            </td>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="SKU">087AA9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giảm giá">32%</td>
					            <td data-title="Quà tặng">020EE47</td>
					            <td data-title="Sự kiện">Sản phẩm khuyến mãi</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					        	<td data-title="Xoá">
					            	<label class="checkbox-custom">
					                  <input class="checkboxMulti" name="product_list_id" value="0" type="checkbox">
					                  <span class="checkmark"></span>
					                </label>
					            </td>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="SKU">087AA9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giảm giá">32%</td>
					            <td data-title="Quà tặng">020EE47</td>
					            <td data-title="Sự kiện">sản phẩm hot</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					        	<td data-title="Xoá">
					            	<label class="checkbox-custom">
					                  <input class="checkboxMulti" name="product_list_id" value="0" type="checkbox">
					                  <span class="checkmark"></span>
					                </label>
					            </td>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="SKU">087AA9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giảm giá">32%</td>
					            <td data-title="Quà tặng">020EE47</td>
					            <td data-title="Sự kiện">Sản phẩm khuyến mãi</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					        	<td data-title="Xoá">
					            	<label class="checkbox-custom">
					                  <input class="checkboxMulti" name="product_list_id" value="0" type="checkbox">
					                  <span class="checkmark"></span>
					                </label>
					            </td>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="SKU">087AA9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giảm giá">32%</td>
					            <td data-title="Quà tặng">020EE47</td>
					            <td data-title="Sự kiện">Sản phẩm khuyến mãi</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					        	<td data-title="Xoá">
					            	<label class="checkbox-custom">
					                  <input class="checkboxMulti" name="product_list_id" value="0" type="checkbox">
					                  <span class="checkmark"></span>
					                </label>
					            </td>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="SKU">087AA9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giảm giá">32%</td>
					            <td data-title="Quà tặng">020EE47</td>
					            <td data-title="Sự kiện">sản phẩm hot</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					        	<td data-title="Xoá">
					            	<label class="checkbox-custom">
					                  <input class="checkboxMulti" name="product_list_id" value="0" type="checkbox">
					                  <span class="checkmark"></span>
					                </label>
					            </td>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="SKU">087AA9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giảm giá">32%</td>
					            <td data-title="Quà tặng">020EE47</td>
					            <td data-title="Sự kiện">Sản phẩm khuyến mãi</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<button onclick="checkBoxMulti();" type="button" class="button button-header black-custom removelist"><i class="fa fa-minus-circle" aria-hidden="true"></i> Xoá khỏi danh sách</button>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<?php include('include/promotion/addpro-modal.php')?>
<script>
	jQuery(function(){
		jQuery("#checkAll").change(function () {
	        var status = 0;
	        if(jQuery(this).prop("checked") == true){
	            status = 1;
	        }
	        jQuery("input:checkbox.checkboxMulti").prop('checked', status);
	        jQuery("input:checkbox.checkboxMulti").prop('value', status);
	    });
	})
</script>