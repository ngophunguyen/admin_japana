<style>
.pagination-custom {
  	display: -webkit-box;
  	display: -ms-flexbox;
  	display: flex;
	-webkit-box-align: center;
	    -ms-flex-align: center;
	        align-items: center;
	-webkit-box-pack: center;
	    -ms-flex-pack: center;
	        justify-content: center;
  	float: right;
}

.pagination-custom li{
	color: black;
	width: 38px;
	height: 38px;
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-align: center;
	    -ms-flex-align: center;
	        align-items: center;
	-webkit-box-pack: center;
	    -ms-flex-pack: center;
	        justify-content: center;
	border: 1px solid #ccc;
	cursor: pointer;
}

.pagination-custom li{
	text-decoration: none;
}
.pagination-custom li.active,.pagination-custom li:hover{
	background-color: #222D32;
	border: 1px solid #222D32;
}

.pagination-custom li.active a, .pagination-custom li:hover a{
	color: #fff;
}

.pagination-custom li:first-child {
	border-top-left-radius: 5px;
	border-bottom-left-radius: 5px;
	margin-left: 0;
}

.pagination-custom li:last-child {
	border-top-right-radius: 5px;
	border-bottom-right-radius: 5px;
	margin-right: 0;
}
@media (max-width: 575.98px) {
  	.pagination-custom{
  		background: #fff;
  		border-top: 1px solid #ccc;
  		z-index: 0;
  		position: fixed;
		bottom: 0;
		left: 0;
		right: 0;
  	}
  	.pagination-custom li{
  		border: 0;
  	}
  	.pagination-custom{
		width: 100%;
		background-color: #fff;
	}
}
@media (min-width: 576px) and (max-width: 767.98px) {
  	
}
@media (min-width: 768px) and (max-width: 991.98px) {
  	
}
@media (min-width: 992px) and (max-width: 1199.98px) {

}
@media (min-width: 1200px) {
  	
}
</style>
<ul class="pagination-custom">
	<li>
		<a href="#"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
	</li>
	<li class="active">
		<a href="#">1</a>
	</li>
	<li>
		<a href="#">2</a>
	</li>
	<li>
		<a href="#">3</a>
	</li>
	<li>
		<a href="#">4</a>
	</li>
	<li>
		<a href="#">5</a>
	</li>
	<li>
		<a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
	</li>
</ul>