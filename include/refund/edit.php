<style>
	.box-table{
		position: relative;
		margin-bottom: 15px;
	}
	.table-custom{
		white-space: nowrap;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	img.gift-order{
		height: 20px!important; 
		bottom: 5px; 
		left: 0;  
		position: absolute;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.box-table tr td .checkbox-custom {
	    cursor: pointer;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	    -ms-flex-align: center;
	    align-items: center;
	    height: 20px;
	}
	.box-table tr td:nth-child(11) input{
		width: 100px;
		display: initial;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		padding: 15px 0;
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:first-child button{
		float: left;
		margin-left: 15px;
	}
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	     -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.link-show{
		display: none;
	}
	table tr td:nth-child(3){
	    width: 250px;
	    max-width: 250px;
	    min-width: 250px;
	    white-space: normal;
	  }
	@media (max-width: 575.98px) {
		.box-quick-search .item, 
	  	.box-quick-search .item:first-child input{
	  		width: 100%;
	  	}
		.search1{
	  		display: inline-block;
	  		width: 100%;
	  	}
	  	.box-quick-search .item:first-child button{
	  		margin-left: 0;
	  		margin-top: 15px;
	  	}
	  	.box-table tr td .checkbox-custom{
	  		padding-left: 0;
	  	}
	  	.table-responsive > tbody > tr td:first-child{
	  		display: none;
	  	}
	  	.table-custom > tbody > tr > td{
			white-space: normal;
		}
		.box-table tr td:last-child{
			padding: 0;
		}
		
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="refund-detail content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách trả - GHN</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#createreceipt-modal" class="link-custom black-custom open-receipt" title="Tạo phiếu trả">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Tạo phiếu trả</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Nhập thông tin cần tìm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">SKU</th>
					            <th class="bg-black">Tên sản phẩm</th>
					            <th class="bg-black right-custom">Giá bán</th>
					            <th class="bg-black right-custom">Giá mua</th>
					            <th class="bg-black center-custom">Chiết khấu</th>
					            <th class="bg-black center-custom">Số lượng</th>
					            <th class="bg-black center-custom">Mã JP</th>
					            <th class="bg-black">Khách hàng</th>
					            <th class="bg-black">Ngày trả hàng</th>
					            <th class="bg-black center-custom">Thực nhập</th>
					            <th class="bg-black center-custom">Check</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="SKU">027HI257</td>
					            <td data-title="Tên sản phẩm">Máy hút bụi Panasonic MC-CL453RN46 (Tím)</td>
					            <td data-title="Giá bán" class="right-custom">3,070,000 đ</td>
					            <td data-title="Giá mua" class="right-custom">
					            	<input type="text" data-type="currency" id="pro_price_buy1" name="pro_price_buy1" value="2342550" class="form-control link-show">
					            	<span>2,342,550 đ</span>
					            </td>
					            <td data-title="Chiết khấu" class="center-custom">23.7 %</td>
					            <td data-title="Số lượng" class="center-custom">1</td>
					            <td data-title="Mã JP" class="center-custom">JP-32142</td>
					            <td data-title="Khách hàng">ĐẶNG TUẤN ÁNH</td>
					            <td data-title="Ngày trả hàng">27-05-2019</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[1]" id="qty1" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" id="chk_receipt1" onclick="checkedandopen(1)" class="chk2">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editPrice(1)" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu lại">
					            		<i class="fa fa-floppy-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(1,'http://test.japana.vn/admincp/goodsreceipt/del/id=1')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="SKU">001DFB207</td>
					            <td data-title="Tên sản phẩm">Nước nhỏ mắt Santen FX vàng 12ml</td>
					            <td data-title="Giá bán" class="right-custom">270,000 đ</td>
					            <td data-title="Giá mua" class="right-custom">
					            	<input type="text" data-type="currency" id="pro_price_buy2" name="pro_price_buy2" value="150000" class="form-control link-show">
					            	<span>150,000 đ</span>
					            </td>
					            <td data-title="Chiết khấu" class="center-custom">44.4 %</td>
					            <td data-title="Số lượng" class="center-custom">2</td>
					            <td data-title="Mã JP" class="center-custom">JP-32574</td>
					            <td data-title="Khách hàng">TRƯƠNG THANH THỊNH</td>
					            <td data-title="Ngày trả hàng">27-05-2019</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[2]" id="qty2" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" id="chk_receipt2" onclick="checkedandopen(2)" class="chk2">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editPrice(2)" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu lại">
					            		<i class="fa fa-floppy-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(2,'http://test.japana.vn/admincp/goodsreceipt/del/id=1')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="SKU">036CB01</td>
					            <td data-title="Tên sản phẩm">Viên uống hỗ trợ giảm cân NRL Pharma Lactoferrin GX 90 viên</td>
					            <td data-title="Giá bán" class="right-custom">1,540,000 đ</td>
					            <td data-title="Giá mua" class="right-custom">
					            	<input type="text" data-type="currency" id="pro_price_buy3" name="pro_price_buy3" value="770000" class="form-control link-show">
					            	<span>770,000 đ</span>
					            </td>
					            <td data-title="Chiết khấu" class="center-custom">50.0 %</td>
					            <td data-title="Số lượng" class="center-custom">1</td>
					            <td data-title="Mã JP" class="center-custom">JP-32446</td>
					            <td data-title="Khách hàng">Chị Dương Phi</td>
					            <td data-title="Ngày trả hàng">27-05-2019</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[3]" id="qty1" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" id="chk_receipt1" onclick="checkedandopen(3)" class="chk2">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editPrice(3)" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu lại">
					            		<i class="fa fa-floppy-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(3,'http://test.japana.vn/admincp/goodsreceipt/del/id=1')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<?php include('include/refund/create-refund.php')?>
<script>
	function editPrice(id){
		jQuery('#pro_price_buy'+id).css('display','block');
		jQuery('#pro_price_buy'+id).next().css('display','none');
	}
	function checkedandopen(id){
	  if(jQuery('#chk_receipt'+id).prop('checked')==false){
	    jQuery('#qty'+id).prop('disabled',true);
	    jQuery('#qty'+id).addClass('bg-gray');
	  }else{
	    jQuery('#qty'+id).prop('disabled',false);
	    jQuery('#qty'+id).removeClass('bg-gray');
	  }
	}
	jQuery(function(){
		var heightPagination = jQuery('.pagination-custom').outerHeight();
		jQuery('.open-receipt').click(function(e){
			jQuery(window).trigger('resize');
		});
		
	    if(window.innerWidth < 576) {
	    	jQuery('.refund-detail .table-responsive > tbody > tr:last-child').css('margin-bottom',heightPagination);
	    }
	})
</script>