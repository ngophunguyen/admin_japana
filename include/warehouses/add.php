<style>
	.box-addkho{
    display: inline-block;
    width: 100%;
  }
  .box-addkho .item{
    width: 49%;
    float: left;
    margin-bottom: 15px;
  }
  .box-addkho .item:nth-child(even){
    margin-left: 2%;
  }
  .box-addkho .item label{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 5px;
    font-weight: 500;
    width: 100%;
  }
  @media (max-width: 575.98px) {
    .box-addkho .item{
      width: 100%
    }
    .box-addkho .item:nth-child(even){
      margin-left: 0;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {  
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal medium-modal" id="themkho-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Thêm kho hàng
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
      	<div class="box-addkho">
          <div class="item">
            <label for="mobile">Mã kho:</label>
            <div class="box-input-addpro">
              <input type="text" class="form-control" id="id_kho" name="id_kho" placeholder="Nhập mã kho">
            </div>
          </div>
      		<div class="item">
      			<label for="name">Tên kho:</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="name_kho" name="name" placeholder="Nhập tên kho">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="address">Địa chỉ kho:</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="address_kho" name="address" placeholder="Nhập địa chỉ kho">
	      		</div>
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="button bg-red">Hủy</button>
        <button type="button" class="button bg-green">Lưu</button>
      </div>
    </div>
  </div>
</div>
<script>
	jQuery(function(){
		
	})
</script>