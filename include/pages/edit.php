<style>
	.create-page .item{
		float: left;
		width: 49%;
	}
	.create-page .item:nth-child(2){
		margin-left: 2%;
	}
	.create-page label, .banner label{
		font-size: 14px;
		line-height: 1.5;
		margin: 5px 0;
		font-weight: 500;
		width: 100%
	}
	.box-seo .seo-input .upload-img, 
	.box-seo .seo-input{
		display: inline-block;
		width: 100%;
	}
	.box-seo .seo-input{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: baseline;
	        -ms-flex-align: baseline;
	            align-items: baseline;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	    margin-bottom: 15px;
	}
	.box-seo .seo-input .upload-img{
		margin: 0;
	}
	.box-seo .seo-input .upload-img input{
		margin-top: 0;
	}
	.box-seo .seo-input input, .box-seo .seo-input textarea, .box-seo .seo-input .upload-img, .box-img-meta{
		margin-left: 15px;
	    width: 80%;
	    float: right;
	}
	.box-seo .seo-input .upload-img input{
		margin-left: 0;
		width: auto
	}
	.box-img-meta{
		margin-top: 0;
	}
	.box-img-meta img{
		width: 100%;
		height: auto;
	}
	.input-group label.custom-upload{
		width: 100px!important;
		margin: 0;
	}
	.title-block{
		text-align: center;
		display: inline-block;
		width: 100%;
	}
	.table-custom > tbody > tr > td:last-child, .table-custom > thead > tr > th:last-child{
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    padding: 15px 0;
	}
	.table-custom > thead > tr > th:last-child{
		padding-left: 15px;
	}
	.table-custom > tbody > tr td .input-pos{
	    width: 90px;
	    margin: 0 auto;
	}
	.table-custom > tbody > tr td .checkbox-custom {
	    cursor: pointer;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    height: 20px;
	}
	.table-custom > tbody > tr td .checkbox-custom .checkmark{
		left: 39%;
	}
	.btn-create-input{
		margin: 15px 0;
		display: inline-block;
	}
	.btn-create-input:hover{
		color: #222D32!important;
	}
	.box-chose-block{
		display: inline-block;
		width: 100%;
	}
	.box-chose-block .item{
		width: 49%;
		float: left;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		margin-bottom: 15px;
	}
	.box-chose-block .checkbox-custom{
		margin: 9px 0;
		width: auto;
	}
	.box-chose-block .item:nth-child(2),.box-chose-block .item:nth-child(4){
		-webkit-box-pack: end;
		    -ms-flex-pack: end;
		        justify-content: flex-end;
		margin-left: 2%;
	}
	.custom-dropdown{
		margin-left: 10px;
	}
	#ipt-name-block{
		width: 50%;
	}
	.custom-dropdown:after{
		padding: 12px;
	}
	.time-graph{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
    	-webkit-box-pack: end;
    	    -ms-flex-pack: end;
    	        justify-content: flex-end;
    	width: 50%;
	}
	.box-table {
	    width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin: 15px 0;
	    position: relative;
	}
	.table-custom{
		white-space: nowrap;
	}
	.box-chose-block .item .checkbox-custom{
		margin-right: 15px;
	}
	.box-chose-block .item .checkbox-custom:last-child{
		margin-right: 0;
	}
	.banner .item label .fa-desktop{
		font-size: 25px;
	}
	.banner .item label .fa-mobile{
		font-size: 35px;
	}
	.box-img-block {
	    display: inline-block;
	    width: 100%;
	}
	.box-img-block .item {
	    text-align: center;
	    display: inline-table;
	    margin: 10px 0 15px;
	    position: relative;
	    width: 70px;
	}
	.box-img-block label {
	    height: 40px;
	    border: 1px dashed #d9d9d9;
	    border-radius: 4px;
	    color: #d9d9d9;
	    font-size: 15px;
	    cursor: pointer;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	    margin: 0!important
	}
	.box-img-block .upload-btn-block input[type=file] {
	    position: absolute;
	    left: 0;
	    top: 0;
	    right: 0;
	    bottom: 0;
	    opacity: 0;
	    width: 100%;
	    cursor: pointer;
	}
	.box-img-block .upload-btn-block {
	    position: relative;
	    overflow: hidden;
	    display: inline-block;
	    float: left;
	    width: 100%;
	    height: 40px;
	}
	.upload-btn-block .img-page{
		width: 100%;
		height: 40px;
		-o-object-fit: cover;
		   object-fit: cover;
	}
	.box-img-block span {
	    color: #999999;
	    font-size: 13px;
	    width: 100%;
	    display: inline-block;
	    margin-top: 5px;
	    text-align: center;
	}
	.box-img-block .upload-btn-block:hover .btn-upl {
	    border: 1px solid #222D32;
	    color: #222D32;
	}
	.btn-del-img {
	    position: absolute;
	    border-radius: 100%;
	    width: 17px;
	    height: 17px;
	    background: #fff;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	    -ms-flex-align: center;
	    align-items: center;
	    -webkit-box-pack: center;
	    -ms-flex-pack: center;
	    justify-content: center;
	    border: 1px solid #ccc;
	    font-size: 7px;
	    top: -5px;
	    left: -5px;
	}
	.box-input, .banner .item:nth-child(2){
		position: relative;
	}
	.banner .item:nth-child(2) .box-input{
		margin-top: 30px;
	}
	.input-item{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: end;
		    -ms-flex-pack: end;
		        justify-content: flex-end;
		width: 85%;
		margin-top: 15px;
		position: relative;
	}
	.box-input .input-item:first-child{
		margin-top: 0;
	}
	.input-item .input-block{
		margin-left: 15px;
		width: 65%;
		float: right;
	}
	.input-item .input-position{
		margin-left: 3%;
		width: 12%;
	}
	.input-item label{
		font-weight: 400;
		width: auto;
	}
	.input-other{
		margin-left: 15px;
		width: 80%;
	}
	.pos-title{
		position: absolute;
	    top: 0;
	    right: 0;
	    left: 0;
	    text-align: right;
	    padding-right: 18%;
	}
	.box-button{
		position: absolute;
	    top: 0;
	    right: 0;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	}
	.create-page-div{
		margin-top: 15px;
	}
	.box-img-block .item{
		margin-right: 2%;
		margin-left: 0!important;
	}
	.box-search-products{
		display: inline-block;
		width: 100%;
	}
	.box-search-products .item{
	    width: 48%;
	    float: left;
	}
	.box-search-products .item:first-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	.box-search-products .item:last-child{
	    margin-left: 4%;
	}
	.box-search-products .item label {
	    font-weight: 500;
	    font-size: 14px;
	    margin: 0 0 15px;
	}
	.box-search-products .item input{
	    width: 75%;
	    float: left;
	}
	.box-search-products .item button{
	    width: 15%;
	    float: right;
	    margin-left: 15px;
	}
	.box-input-coupon{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: end;
		    -ms-flex-pack: end;
		        justify-content: flex-end;
	}
	.add-pro{
		margin-top: 0;
	}
	.select2-container{
		width: auto!important;
	}
	.box-chose-block{
		display: none;
	}
@media (max-width: 575.98px) {
	.create-page .item, 
	.box-seo .seo-input, 
	.box-seo .seo-input input, 
	.box-seo .seo-input textarea, 
	.box-seo .seo-input .upload-img, 
	.box-img-meta{
		display: inline-block;
		width: 100%
	}
	.custom-upload input[type='file']{
		display: none;
	}
	.create-page .item:last-child{
		margin-left: 0;
	}
	#seo-page, #list-page, #add-page{
		display: none;
		width: 100%;
	}
	.box-seo .seo-input .upload-img{
		margin: 0;
		margin-top: 10px;
	}
	.seo-input .upload-img input{
		margin-top: 0;
	}
	.box-seo .seo-input input, .box-seo .seo-input textarea{
		margin-left: 0;
		width: 100%;
		margin-top: 10px;
	}
	.title-block{
		margin-top: 15px;
	}
	.box-table, .table-custom{
		margin-top: 0;
	}
	.table-custom tr td:first-child{
		display: none;
	}
	.table-responsive > tbody > tr td:nth-child(9), .table-responsive > tbody > tr td:nth-child(10){
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.table-responsive > tbody > tr td:nth-child(4){
		white-space: initial;
		margin: 7px 0;
	}
	.box-img-block .item{
		width: 70px;
	}
	.box-input, .banner .item:nth-child(2){
		margin-left: 0;
	}
	.pos-title{
		display: none;
	}
	.banner .item:nth-child(2) .box-input{
		margin-top: 0;
	}
	.input-item{
		display: inline-block;
		width: 100%;
	}
	.input-item label{
		width: 100%;
	}
	.input-item .input-block{
		width: 70%;
		float: left;
		margin-left: 0;
	}
	.input-item .input-position{
		display: inline-block;
		float: left;
		width: 27%;
	}
	.input-other{
		width: 100%;
		margin-left: 0;
	}
	.box-button{
		position: static;
		float: right;
	}
	.box-button a{
		width: 38px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
    	-webkit-box-align: center;
    	    -ms-flex-align: center;
    	        align-items: center;
	}
	.custom-dropdown{
		margin-left: 0;
		margin-top: 10px;
	}
	.box-chose-block .item:first-child{
		margin-top: 15px;
	}
	.box-chose-block .item:nth-child(2), .box-chose-block .item:nth-child(4){
		margin-left: 0;
	}
	#ipt-name-block, .time-graph{
		width: 100%;
	}
	.box-search-products .item button{
		width: 100px;
	}
	.select2-container{
		width: 65%!important;
	}
	.box-search-products .item:last-child{
		margin: 10px 0;
	}
	.table-custom > tbody > tr td .checkbox-custom .checkmark{
		left: 0;
	}
	.table-responsive > tbody > tr td:nth-child(9), .table-responsive > tbody > tr td:nth-child(10){
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
	}
	.table-custom > tbody > tr > td:last-child .link-custom i{
		margin-bottom: 0;
	}
	.add-pro .table-custom > tbody > tr > td:last-child{
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
	}
}
@media (min-width: 576px) and (max-width: 767.98px) {
	.box-seo .seo-input input, .box-seo .seo-input textarea, .box-seo .seo-input .upload-img, .box-img-meta{
		width: 65%;
		margin-left: 10px;
	}
	.box-seo .seo-input .upload-img input{
		width: 60%;
	}
	.input-group label.custom-upload{
		width: 60px!important;
	}
	.input-item{
		width: 80%;
	}
	.input-other{
		width: 70%;
		margin-left: 5px;
	}
	.banner .item:first-child{
		width: 35%;
	}
	.banner .item:nth-child(2){
		width: 63%;
	}
	.input-item .input-block{
		width: 52%;
		margin-left: 5px;
	}
	.input-item .input-position{
		width: 15%;
	}
	#pc-sl .item, #mobile-sl .item{
		width: 30%;
	}
	.box-button a{
		padding: 11px;
	}
	.pos-title{
		padding-right: 23%;
	}
	.box-chose-block .time-graph{
		width: 45%;
	}
	.box-chose-block .item{
		width: 35%;
	}
	.box-chose-block .item .custom-dropdown{
		width: 65%;
	}
	.box-chose-block .item:nth-child(2), .box-chose-block .item:nth-child(4){
		width: 65%;
		margin-left: 0;
	}
	#ipt-name-block{
		width: 90%;
	}
}
@media (min-width: 768px) and (max-width: 991.98px) {
	.box-seo .seo-input input, .box-seo .seo-input textarea, .box-seo .seo-input .upload-img, .box-img-meta{
		width: 65%;
		margin-left: 10px;
	}
	.box-seo .seo-input .upload-img input{
		width: 60%;
	}
	.input-group label.custom-upload{
		width: 60px!important;
	}
	.input-item{
		width: 80%;
	}
	.input-other{
		width: 70%;
		margin-left: 5px;
	}
	.banner .item:first-child{
		width: 35%;
	}
	.banner .item:nth-child(2){
		width: 63%;
	}
	.input-item .input-block{
		width: 52%;
		margin-left: 5px;
	}
	.input-item .input-position{
		width: 15%;
	}
	#pc-sl .item, #mobile-sl .item{
		width: 30%;
	}
	.box-button a{
		padding: 11px;
	}
	.pos-title{
		padding-right: 23%;
	}
	.box-chose-block .time-graph{
		width: 45%;
	}
	.box-chose-block .item{
		width: 35%;
	}
	.box-chose-block .item .custom-dropdown{
		width: 65%;
	}
	.box-chose-block .item:nth-child(2), .box-chose-block .item:nth-child(4){
		width: 65%;
		margin-left: 0;
	}
	#ipt-name-block{
		width: 90%;
	}
}
@media (min-width: 992px) and (max-width: 1199.98px) {
	#pc-sl .item, #mobile-sl .item{
		width: 30%;
	}
	.banner .item:first-child{
		width: 35%;
	}
	.banner .item:nth-child(2){
		width: 63%;
	}
	.input-item{
		width: 80%;
	}
	.box-seo .seo-input input, .box-seo .seo-input textarea, .box-seo .seo-input .upload-img, .box-img-meta{
		margin-left: 5px;
		width: 78%;
	}
	.box-table, .table-custom{
		margin-top: 0;
	}
	.box-search-products{
		margin-bottom: 10px;
	}

}
@media (min-width: 1200px) {

}
</style>
<main class="block content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Homepage</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Xem trước">
						<i class="fa fa-eye" aria-hidden="true"></i> <label>Xem trước</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu nháp">
						<i class="fa fa-file-text" aria-hidden="true"></i> <label>Lưu nháp</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" onclick="document.getElementById('save_type').value = 'publish'; document.getElementById('frm').submit();" class="link-custom black-custom" title="Publish">
						<i class="green-custom fa fa-check" aria-hidden="true"></i> <label>Publish</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="tab-custom bg-black">
				<div class="item active">
					<a href="javascript:void(0)" data-id="seo-page" title="SEO">
						<i class="fa fa-bullhorn" aria-hidden="true"></i>
						<label>SEO</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="list-page" title="Danh sách">
						<i class="fa fa-list" aria-hidden="true"></i>
						<label>Danh sách</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="add-page" title="Thêm block">
						<i class="fa fa-puzzle-piece" aria-hidden="true"></i>
						<label>Thêm block</label>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<div class="create-page">
					<div id="seo-page" class="tab-content show-inline">
						<div class="item">
							<label for="name">Tên page:</label>
							<input autocomplete="off" type="text" name="name" id="name" placeholder="Tên page" value="Homepage" class="form-control">
							<label for="url">Link URL:</label>
							<input autocomplete="off" type="text" name="url" id="url" placeholder="Link URL" value="index" class="form-control" disabled="disabled">
							<div class="box-seo">
								<label for="meta_web_title">Google:</label>
								<span class="seo-input">
									Title:
									<input autocomplete="off" type="text" name="meta_web_title" id="meta_web_title" placeholder="Nhập title" value="home" class="form-control">
								</span>
								<span class="seo-input">
									Keyword:
									<input autocomplete="off" type="text" name="meta_web_keyword" id="meta_web_keyword" placeholder="Nhập keyword" value="home" class="form-control">
								</span>
								<span class="seo-input">
									Description:
									<textarea name="meta_web_desc" id="meta_web_desc" class="form-control" rows="3" placeholder="Nhập description">home</textarea>
								</span>
							</div>
						</div>
						<div class="item">
							<div class="box-seo">
								<label for="meta_web_title">Social:</label>
								<span class="seo-input">
									Title:
									<input autocomplete="off" type="text" name="og_title" id="og_title" placeholder="Meta O.g title..." value="home" class="form-control">
								</span>
								<span class="seo-input">
									Description:
									<textarea name="og_desc" id="og_desc" class="form-control" rows="3" placeholder="Meta O.g descriptions...">home</textarea>
								</span>
								<span class="seo-input">
									Image (16x9):
									<div class="upload-img">
			                            <div class="input-group up-img">
			                                <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
			                                <label class="button bg-green custom-upload">
			                                    <input type="file" class="form-control" name="icon" id="icon" onchange="readURL(this,1);" accept="image/*">Upload
			                                </label>
			                                <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
			                            </div>
			                        </div>
								</span>
								<span class="seo-input">
									<div class="box-img-meta">
			                        	<img src="assets/images/collagen-sp.jpg" alt="collagen" id="photo1">
			                        </div>
								</span>
							</div>
						</div>
					</div>
					<div id="list-page" class="tab-content">
						<h3 class="title-block red-custom">----- Các block trong page -----</h3>
						<div class="box-table">
							<table class="table table-custom table-striped table-responsive">
							    <thead class="bg-black">
							        <tr class="bg-black">
							            <th class="bg-black center-custom">STT</th>
							            <th class="bg-black">Tên block</th>
							            <th class="bg-black">Tên code</th>
							            <th class="bg-black">Thời gian hiển thị</th>
							            <th class="bg-black center-custom">PC</th>
							            <th class="bg-black center-custom">Mobile</th>
							            <th class="bg-black center-custom">Mở Container</th>
							            <th class="bg-black center-custom">Đóng Container</th>
							            <th class="bg-black">Publish</th>
							            <th class="bg-black center-custom">Hiển thị</th>
							            <th class="bg-black center-custom">Vị trí</th>
							            <th class="bg-black center-custom">Tác vụ</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							            <td data-title="STT" class="center-custom">1</td>
							            <td data-title="Tên block">Header (mặc định)</td>
							            <td data-title="Tên code">header</td>
							            <td data-title="Thời gian hiện">Mặc định</td>
							            <td data-title="PC" class="center-custom"></td>
							            <td data-title="Mobile" class="center-custom"></td>
							            <td data-title="Mở Contain" class="center-custom"></td>
							            <td data-title="Đóng Contain" class="center-custom"></td>
							            <td data-title="Publish"></td>
							            <td data-title="Hiển thị" class="center-custom"></td>
							            <td data-title="Vị trí" class="center-custom"></td>
							            <td data-title="Tác vụ" class="center-custom"></td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">2</td>
							            <td data-title="Tên block">Slider tràn màn hình</td>
							            <td data-title="Tên code">slider_home</td>
							            <td data-title="Thời gian hiện">10/08/2018 00:00 - 31/12/2222 00:00</td>
							            <td data-title="PC" class="center-custom">
							            	<label class="checkbox-custom">
							                  	<input type="checkbox" checked="checked" disabled>
							                  	<span class="checkmark"></span>
							                </label>
							            </td>
							            <td data-title="Mobile" class="center-custom">
							            	<label class="checkbox-custom">
							                  	<input type="checkbox" checked="checked" disabled>
							                  	<span class="checkmark"></span>
							                </label>
							            </td>
							            <td data-title="Mở Contain" class="center-custom">
							            	<label class="checkbox-custom">
							                  	<input type="checkbox" disabled>
							                  	<span class="checkmark"></span>
							                </label>
							            </td>
							            <td data-title="Đóng Contain" class="center-custom">
							            	<label class="checkbox-custom">
							                  	<input type="checkbox" disabled>
							                  	<span class="checkmark"></span>
							                </label>
							            </td>
							            <td data-title="Publish">
							            	<input type="checkbox" class="checkbox-ios" checked="checked" />
							            </td>
							            <td data-title="Hiển thị" class="center-custom">
							            	<i class="green-custom fa fa-check" aria-hidden="true"></i>
							            </td>
							            <td data-title="Vị trí" class="center-custom">
							            	<input type="number" class="form-control input-pos" value="1" name="sort_616" id="sort_616">
							            </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu vị trí">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa block">
							            		<i class="fa fa-pencil-square-o"></i>
							            	</a>
							            	<a href="javascript:void(0);" class="link-custom red-custom" title="Xóa">
							            		<i class="fa fa-times"></i>
							            	</a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">3</td>
							            <td data-title="Tên block">block 01</td>
							            <td data-title="Tên code">countdown1</td>
							            <td data-title="Thời gian hiện">10/08/2018 09:12 - 31/12/2222 00:00</td>
							            <td data-title="PC" class="center-custom">
							            	<label class="checkbox-custom">
							                  	<input type="checkbox" checked="checked" disabled>
							                  	<span class="checkmark"></span>
							                </label>
							            </td>
							            <td data-title="Mobile" class="center-custom">
							            	<label class="checkbox-custom">
							                  	<input type="checkbox" checked="checked" disabled>
							                  	<span class="checkmark"></span>
							                </label>
							            </td>
							            <td data-title="Mở Contain" class="center-custom">
							            	<label class="checkbox-custom">
							                  	<input type="checkbox" disabled>
							                  	<span class="checkmark"></span>
							                </label>
							            </td>
							            <td data-title="Đóng Contain" class="center-custom">
							            	<label class="checkbox-custom">
							                  	<input type="checkbox" disabled>
							                  	<span class="checkmark"></span>
							                </label>
							            </td>
							            <td data-title="Publish">
							            	<input type="checkbox" class="checkbox-ios"/>
							            </td>
							            <td data-title="Hiển thị" class="center-custom">
							            	<i class="gray-custom fa fa-ellipsis-h" aria-hidden="true"></i>
							            </td>
							            <td data-title="Vị trí" class="center-custom">
							            	<input type="number" class="form-control input-pos" value="2" name="sort_616" id="sort_616">
							            </td>
							            <td data-title="Tác vụ" class="center-custom">
							            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu vị trí">
							            		<i class="fa fa-save"></i>
							            	</a>
							            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa block">
							            		<i class="fa fa-pencil-square-o"></i>
							            	</a>
							            	<a href="javascript:void(0);" class="link-custom red-custom" title="Xóa">
							            		<i class="fa fa-times"></i>
							            	</a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">4</td>
							            <td data-title="Tên block">Footer (mặc định)</td>
							            <td data-title="Tên code">footer</td>
							            <td data-title="Thời gian hiện">Mặc định</td>
							            <td data-title="PC" class="center-custom"></td>
							            <td data-title="Mobile" class="center-custom"></td>
							            <td data-title="Mở Contain" class="center-custom"></td>
							            <td data-title="Đóng Contain" class="center-custom"></td>
							            <td data-title="Publish"></td>
							            <td data-title="Hiển thị" class="center-custom"></td>
							            <td data-title="Vị trí" class="center-custom"></td>
							            <td data-title="Tác vụ" class="center-custom"></td>
							        </tr>
							    </tbody>
							</table>
						</div>
					</div>
					<div id="add-page" class="tab-content">
						<a id="add-block" href="javascript:void(0);" class="btn-create-input green-custom bold left-custom" title="Thêm">
							<i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm block
						</a>
						<div class="box-chose-block">
							<div class="item">
								Kiểu block: 
								<div class="custom-dropdown">
						    		<select id="show-block" name="block_type" class="form-control">
	                                    <option value="0">Chọn kiểu banner</option>
	                                    <option value="52,5">Slider</option>
	                                    <option value="56,5">banner landingpage</option>
	                                    <option value="58,5">5.1.1 Banner</option>
	                                    <option value="60,5">Countdown banner 4 hình</option>
	                                    <option value="62,5">Countdown theo SKUs</option>
	                                    <option value="64,5">4.4.4 banner</option>
	                                    <option value="66,5">product_3_column</option>
	                                    <option value="70,5">1 banner multiple products</option>
	                                    <option value="74,4">slider brand</option>
	                                    <option value="76,4">news_slide</option>
	                                    <option value="78,5">product_filter1</option>
	                                    <option value="80,3">product detail</option>
	                                    <option value="82,4">Danh Sách Sản Phẩm Xem Nhiều</option>
	                                    <option value="84,4">product1_column1</option>
	                                    <option value="86,5">Sản Phẩm Đã Xem</option>
	                                    <option value="113,4">Trang Tag</option>
	                                    <option value="114,5">1 banner 6 products</option>
	                            	</select>
						    	</div>
							</div>
							<div class="item">
								<label class="checkbox-custom">Thời gian áp dụng
				                  	<input type="checkbox">
				                  	<span class="checkmark"></span>
				                </label>
				                <div class="time-graph">
				                	<div class="box-time">
					                	<input type="hidden" name="start_date" id="date_start" value="">
					                	<input type="hidden" name="end_date" id="end_date" value="">
			                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-block" id="date-block" class="form-control ipt-date" placeholder="Chọn ngày...">
			                            <i class="fa fa-calendar icon-time"></i>
			                        </div>
				                </div>
							</div>
							<div class="item">
								<input type="text" id="ipt-name-block" name="name_block_pages" class="form-control" value="" placeholder="Bạn chưa đặt tên block" required="required">
							</div>
							<div class="item">
								<label class="checkbox-custom">PC
				                  	<input type="checkbox" id="pc" name="desktop">
				                  	<span class="checkmark"></span>
				                </label>
				                <label class="checkbox-custom">Mobile
				                  	<input type="checkbox" id="mobile" name="mobile">
				                  	<span class="checkmark"></span>
				                </label>
				                <label class="checkbox-custom">Mở Container
				                  	<input type="checkbox" id="open-container" name="begin">
				                  	<span class="checkmark"></span>
				                </label>
				                <label class="checkbox-custom">Đóng Container
				                  	<input type="checkbox" id="close-container" name="end">
				                  	<span class="checkmark"></span>
				                </label>
							</div>
						</div>
						<label>Banner (Slider)</label>
						<div id="banner-slider" class="banner">
							<div class="item">
								<label> 
									<i class="fa fa-desktop" aria-hidden="true"></i> Desktop
								</label>
								<div id="pc-sl" class="box-img-block">
									<div class="item">
										<div class="upload-btn-block">
											<img src="" alt="img" id="img-multi-desktop-1" class="img-page hidden">
											<label class="btn-upl">
												<i class="ti ti-plus"></i>
	                                        </label>
	                                        <input type="file" name="file_multi_images_desktop[]" id="input-img-multi-desktop-1" class="file-upl" onchange="readURLbanner(this, 1, 'multi-desktop');" accept="image/*">
										</div>
										<a href="javascript:void(0)" id="del-multi-desktop-1" onclick="initClickRemoveSrc(1, 'multi-desktop')" class="btn-del-img black-custom hidden" title="Xoá">
											<i class="ti ti-close"></i>
										</a>
	                                    <span class="init-size-desktop">1600x400</span>
									</div>
								</div>
								<label> 
									<i class="fa fa-mobile" aria-hidden="true"></i> Mobile
								</label>
								<div id="mobile-sl" class="box-img-block">
									<div class="item">
										<div class="upload-btn-block">
											<img src="" alt="img" id="img-multi-mobile-1" class="img-page hidden">
											<label class="btn-upl">
												<i class="ti ti-plus"></i>
	                                        </label>
	                                        <input type="file" name="file_multi_images_mobile[]" id="input-img-multi-mobile-1" class="file-upl" onchange="readURLbanner(this, 1, 'multi-mobile');" accept="image/*">
										</div>
										<a href="javascript:void(0)" id="del-multi-mobile-1" onclick="initClickRemoveSrc(1, 'multi-mobile')" class="btn-del-img black-custom hidden" title="Xoá">
											<i class="ti ti-close"></i>
										</a>
	                                    <span class="init-size-mobile">1600x400</span>
									</div>
								</div>
							</div>
							<div class="item">
								<label class="pos-title">Vị trí</label>
								<div class="box-input">
									<div class="input-item">
										<label>Tên 1:</label>
										<input autocomplete="off" type="text" name="banner_slider_name[]" placeholder="Nhập tên ảnh" class="form-control input-block">
										<input value="1" type="number" name="position[]" class="form-control input-position">
									</div>
									<div class="input-item">
										<label>Link 1:</label>
										<input autocomplete="off" type="text" name="banner_slider_link[]" placeholder="Nhập link" class="form-control input-other">
									</div>
									<div class="input-item">
										<label>Thời gian 1:</label>
										<div class="time-graph input-other">
						                	<div class="box-time">
							                	<input type="hidden" name="banner_slider_start_date[]" value="">
							                	<input type="hidden" name="banner_slider_end_date[]" value="">
					                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-banner" class="form-control date-banner" placeholder="Chọn ngày...">
					                            <i class="fa fa-calendar icon-time"></i>
					                        </div>
						                </div>
									</div>
									<div class="box-button">
										<a onclick="document.getElementById('save_type_block').value = 'add'; document.getElementById('frm-block').submit();" href="javascript:void(0);" class="link-custom black-custom" title="Lưu" style="cursor: pointer;">
						            		<i class="fa fa-save"></i>
						            	</a>
						            	<a onclick="initDeleteDiv(1)" href="javascript:void(0)" class="link-custom red-custom" title="Xoá" style="cursor: pointer;">
											<i class="ti ti-close"></i>
										</a>
									</div>
								</div>
							</div>
							<a href="javascript:void(0);" id="add-div" class="create-page-div green-custom bold custom-fright" title="Thêm">
								<i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm ảnh
							</a>
						</div>
						<label>Banner</label>
						<div class="banner">
							<div class="item">
								<label> 
									<i class="fa fa-desktop" aria-hidden="true"></i> Desktop
								</label>
								<div id="pc-sl" class="box-img-block">
									<div class="item">
										<div class="upload-btn-block">
											<img src="" alt="img" id="img-desktop-0" class="img-page hidden">
											<label class="btn-upl">
												<i class="ti ti-plus"></i>
	                                        </label>
	                                        <input type="file" name="file_images_desktop[]" id="input-img-desktop-0" class="file-upl" onchange="readURLbanner(this, 0, 'desktop');" accept="image/*">
										</div>
										<a href="javascript:void(0)" id="del-img-desktop-0" onclick="initClickRemoveSrc(0, 'desktop')" class="btn-del-img black-custom hidden" title="Xoá">
											<i class="ti ti-close"></i>
										</a>
	                                    <span>1600x400</span>
	                                    <input type="hidden" name="images_size_desktop[]" value="1600x400">
									</div>
								</div>
								<label> 
									<i class="fa fa-mobile" aria-hidden="true"></i> Mobile
								</label>
								<div id="mobile-sl" class="box-img-block">
									<div class="item">
										<div class="upload-btn-block">
											<img src="" alt="img" id="img-mobile-0" class="img-page hidden">
											<label class="btn-upl">
												<i class="ti ti-plus"></i>
	                                        </label>
	                                        <input type="file" name="file_images_mobile[]" id="input-img-mobile-0" class="file-upl" onchange="readURLbanner(this, 0, 'mobile');" accept="image/*">
										</div>
										<a href="javascript:void(0)" id="del-img-mobile-0" onclick="initClickRemoveSrc(0, 'mobile')" class="btn-del-img black-custom hidden" title="Xoá">
											<i class="ti ti-close"></i>
										</a>
	                                    <span>1600x400</span>
	                                    <input type="hidden" name="images_size_mobile[]" value="1600x400">
									</div>
								</div>
							</div>
							<div class="item">
								<div class="box-input">
									<div class="input-item">
										<label>Tên 1:</label>
										<input autocomplete="off" type="text" name="banner_name[]" placeholder="Nhập tên ảnh" class="form-control input-other">
									</div>
									<div class="input-item">
										<label>Link 1:</label>
										<input autocomplete="off" type="text" name="banner_link[]" placeholder="Nhập link" class="form-control input-other">
									</div>
								</div>
							</div>
						</div>
						<label>Sản phẩm</label>
						<div class="box-search-products">
				          	<div class="item">
			              		<input onkeyup="initShowBtn();" type="text" name="search" placeholder="Tìm theo SKU..." class="form-control">
			              		<button type="submit" class="button bg-black">Thêm</button>
				          	</div>
				          	<div class="item">
				            	<div class="box-input-coupon">
						    		<select id="group-promotion-sku" name="block_type" class="form-control">
	                                    <option data-type="promotion" value="0">Chọn danh sách hoặc nhóm sản phẩm</option>
	                                    <option data-type="promotion" value="52,5">Block giao hàng 60'</option>
	                                    <option data-type="collection" value="56,5">Block new</option>
	                                    <option data-type="collection" value="58,5">Event Shiseido</option>
	                                    <option data-type="collection" value="60,5">Event Collagen</option>
	                            	</select>
				              		<button type="submit" class="button bg-black">Thêm</button>
				            	</div>
				          	</div>
				        </div>
						<div class="add-pro box-table">
				            <table class="table table-custom table-addpro table-responsive">
				              	<thead class="bg-black">
				                	<tr class="bg-black">
										<th class="center-custom bg-black">STT</th>
										<th class="bg-black">SKU</th>
										<th class="bg-black">Tên sản phẩm</th>
										<th class="center-custom bg-black">Vị trí</th>
										<th class="bg-black">Tác vụ</th>
				                	</tr>
				              	</thead>
				              	<tbody>
					                <tr>
					                  	<td data-title="STT" class="center-custom">1</td>
					                  	<td data-title="SKU">087AA9</td>
					                  	<td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					                  	<td data-title="Vị trí" class="center-custom">
					                  		<input onchange="this.setAttribute('value', this.value)" type="number" value="1" class="form-control input-pos" name="sku_sort[]" id="sort-3925">
					                  	</td>
					                  	<td data-title="Tác vụ">
					                    	<a href="javascript:void(0);" class="link-custom red-custom" title="Xóa" style="cursor: pointer;">
							            		<i class="fa fa-times"></i>
							            	</a>
					                  	</td>
					                </tr>
				              	</tbody>
				            </table>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	function initClickRemoveSrc(id, typeName){
	    jQuery('#img-'+typeName+'-'+id).attr('src','');
	    jQuery('#input-img-'+typeName+'-'+id).val('');
	    jQuery('#img-'+typeName+'-'+id).addClass('hidden');
	    jQuery('#img-'+typeName+'-'+id).next().removeClass('hidden');
	    jQuery('#img-'+typeName+'-'+id).parent().next().addClass('hidden');
	}

	function readURLbanner(input,id,typeName) {
	  	if (window.File && window.FileReader && window.FileList && window.Blob)
	  	{
			var fsize = input.files[0].size;
			var ftype = input.files[0].type;
			var fname = input.files[0].name;
	      	if(fsize>1048576)
	      	{
	        	alert("File "+fname+" dung lượng quá lớn không thể upload, dung lượng file tối đa 1MB");
	      	}else{
	        	if (input.files && input.files[0]) {
	            	var reader = new FileReader();
	            	reader.onload = function (e) {
		                if(jQuery('#img-'+typeName+'-'+id).attr('src', e.target.result)){
		                  	jQuery('#img-'+typeName+'-'+id).removeClass('hidden');
		                  	jQuery('#img-'+typeName+'-'+id).next().addClass('hidden');
		                  	jQuery('#img-'+typeName+'-'+id).parent().next().removeClass('hidden');
		                }
		            };
	            	reader.readAsDataURL(input.files[0]);
	        	}
	      	}
	  	}
	}
	var ids = jQuery('#banner-slider .box-input').length;
	jQuery('.create-page-div').click(function(){
	    id = ids + 1;
	    ids = ids + 1;
	    var initSizeDesktop = jQuery("#banner-slider .init-size-desktop").first().text();
	    var initSizeMobile = jQuery("#banner-slider .init-size-mobile").first().text();
	    jQuery("#banner-slider>.item:nth-child(2)").append(
	    	`<div id="item_`+ids+`" class="box-input">
				<div class="input-item">
					<label>Tên `+ids+`:</label>
					<input autocomplete="off" type="text" name="banner_slider_name[]" placeholder="Nhập tên ảnh" class="form-control input-block">
					<input value="`+ids+`" type="number" name="position[]" class="form-control input-position">
				</div>
				<div class="input-item">
					<label>Link `+ids+`:</label>
					<input autocomplete="off" type="text" name="banner_slider_link[]" placeholder="Nhập link" class="form-control input-other">
				</div>
				<div class="input-item">
					<label>Thời gian `+ids+`:</label>
					<div class="time-graph input-other">
	                	<div class="box-time">
		                	<input type="hidden" name="banner_slider_start_date[]" value="">
		                	<input type="hidden" name="banner_slider_end_date[]" value="">
                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-banner" class="form-control date-banner" placeholder="Chọn ngày...">
                            <i class="fa fa-calendar icon-time"></i>
                        </div>
	                </div>
				</div>
				<div class="box-button">
					<a onclick="document.getElementById('save_type_block').value = 'add'; document.getElementById('frm-block').submit();" href="javascript:void(0);" class="link-custom black-custom" title="Lưu" style="cursor: pointer;">
	            		<i class="fa fa-save"></i>
	            	</a>
	            	<a onclick="initDeleteDiv(`+ids+`)" href="javascript:void(0)" class="link-custom red-custom" title="Xoá" style="cursor: pointer;">
						<i class="ti ti-close"></i>
					</a>
				</div>
			</div>`
	  	);

	    jQuery('#pc-sl').append(
	    	`<div id="pc_sl_`+id+`" class="item">
				<div class="upload-btn-block">
					<img src="" alt="img" id="img-multi-desktop-` + id + `" class="img-page hidden">
					<label class="btn-upl">
						<i class="ti ti-plus"></i>
                    </label>
                    <input type="file" name="file_multi_images_desktop[]" id="input-img-multi-desktop-` + id + `" class="file-upl" onchange="readURLbanner(this, ` + id + `, 'multi-desktop');" accept="image/*">
				</div>
				<a href="javascript:void(0)" id="del-multi-desktop-` + id + `" onclick="initClickRemoveSrc(` + id + `, 'multi-desktop')" class="btn-del-img black-custom hidden" title="Xoá">
					<i class="ti ti-close"></i>
				</a>
                <span class="init-size-desktop">` + initSizeDesktop + `</span>
			</div>`
	    );
	    jQuery("#mobile-sl").append(
	    	`<div id="mobile_sl_`+id+`" class="item">
				<div class="upload-btn-block">
					<img src="" alt="img" id="img-multi-mobile-` + id + `" class="img-page hidden">
					<label class="btn-upl">
						<i class="ti ti-plus"></i>
                    </label>
                    <input type="file" name="file_multi_images_mobile[]" id="input-img-multi-mobile-` + id + `" class="file-upl" onchange="readURLbanner(this, ` + id + `, 'multi-mobile');" accept="image/*">
				</div>
				<a href="javascript:void(0)" id="del-multi-mobile-` + id + `" onclick="initClickRemoveSrc(` + id + `, 'multi-mobile')" class="btn-del-img black-custom hidden" title="Xoá">
					<i class="ti ti-close"></i>
				</a>
                <span class="init-size-mobile">` + initSizeMobile + `</span>
			</div>`
	    );
	    if(window.innerWidth > 576) {
			if(jQuery('.nav-primary').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight() && jQuery('.nav-primary').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height','auto');
			}else if(jQuery('body').outerHeight() > jQuery('.nav-primary').outerHeight() && jQuery('body').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
		}
	    
	});
	
	function initDeleteDiv(id){
		jQuery('#item_'+id).remove();
		if(window.innerWidth > 576) {
			if(jQuery('body').outerHeight() > jQuery('.nav-primary').outerHeight() && jQuery('body').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
		}
		jQuery('#pc_sl_'+id).remove();
		jQuery('#mobile_sl_'+id).remove();
	}

	function readURL(input,id) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	        	jQuery('#photo'+id).parent().removeClass('hidden');
	          	jQuery('#photo'+id).attr('src', e.target.result);
	          	if(jQuery('.group-box-seo').height()>jQuery('.group-custom-cate').height()){
					jQuery('.group-category').height(jQuery('.group-box-seo').height()-2);
				}
				else{
					jQuery('.group-category').height(jQuery('.group-custom-cate').height());
				}
	        };
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	jQuery(function(){
		jQuery("#add-block").click(function(){
		    jQuery(".box-chose-block").css('display','block');
		    jQuery(this).hide();
		    if(window.innerWidth > 576) {
				if(jQuery('body').outerHeight() > jQuery('.nav-primary').outerHeight() && jQuery('body').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight()){
					jQuery('.nav-primary').css('height',jQuery('body').outerHeight());
				}
				else{
					jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
				}
			}
		  });
		var formatDate = {
	      	format: 'DD/MM/YYYY H:mm',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
		jQuery('input[name="date-block"]').daterangepicker({
			opens: 'left',
			timePicker: true,
			timePicker24Hour: true,
			autoApply: true,
			startDate: moment().startOf('hour'),
			endDate: moment().startOf('hour').add(32, 'hour'),
			locale: formatDate
		},function(start,end) {
			jQuery('#date_start').val(start.format('DD-MM-YYYY H:mm'));
			jQuery('#end_date').val(end.format('DD-MM-YYYY H:mm'));
		});
		jQuery(document).on('click', '.date-banner', function(){
		    jQuery(this).daterangepicker({
				opens: 'left',
				timePicker: true,
				timePicker24Hour: true,
				autoApply: true,
				startDate: moment().startOf('hour'),
				endDate: moment().startOf('hour').add(32, 'hour'),
				locale: formatDate
			},function(start,end) {
				// jQuery('input[name="banner_slider_start_date"]').val(start.format('DD-MM-YYYY H:mm'));
				// jQuery('input[name="banner_slider_end_date"]').val(end.format('DD-MM-YYYY H:mm'));
			});
			jQuery(this).removeClass('date-banner');
		    var ds = jQuery(this).val();
		    jQuery(this).val(ds);
		});
		
		jQuery(document).on('change', ':file', function() {
		    var input = jQuery(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		});
		jQuery(':file').on('fileselect', function(event, numFiles, label) {
          	var input = jQuery(this).parents('.up-img').find(':text'),
              	log = numFiles > 1 ? numFiles + ' files selected' : label;
          	if( input.length ) {
              	input.val(log);
              	if(input.val(log)){
                	jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
                	jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
              	}
          	} else {
              	if(log) ;
          	}
      	});
      	jQuery('.tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.tab-content').not('#' + data).removeClass('show-inline');
	    	jQuery(this).parent().addClass('active');
	    	jQuery('.tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    });
      	jQuery('.delete-img').on('click', function(e){
		    jQuery(this).parent().find('input[type=file]').val('');
		    jQuery(this).parent().find('input[type=text]').val('');
		    jQuery(this).parent().find('.custom-upload').css('display','flex');
		    jQuery('.box-img-meta').addClass('hidden');
		    jQuery(this).css('display','none');
		});

		jQuery('#group-promotion-sku').select2();
	})
</script>