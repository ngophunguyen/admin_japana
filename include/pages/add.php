<style>
	.create-page .item{
		float: left;
		width: 49%;
	}
	.create-page .item:last-child{
		margin-left: 2%;
	}
	.create-page label{
		font-size: 14px;
		line-height: 1.5;
		margin: 5px 0;
		font-weight: 500;
		width: 100%
	}
	.box-seo .seo-input .upload-img, 
	.box-seo .seo-input{
		display: inline-block;
		width: 100%;
	}
	.box-seo .seo-input{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: baseline;
	        -ms-flex-align: baseline;
	            align-items: baseline;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	    margin-bottom: 15px;
	}
	.box-seo .seo-input .upload-img{
		margin: 0;
	}
	.box-seo .seo-input .upload-img input{
		margin-top: 0;
	}
	.box-seo .seo-input input, .box-seo .seo-input textarea, .box-seo .seo-input .upload-img, .box-img-meta{
		margin-left: 15px;
	    width: 80%;
	    float: right;
	}
	.box-seo .seo-input .upload-img input{
		margin-left: 0;
		width: auto
	}
	.box-img-meta{
		margin-top: 0;
	}
	.box-img-meta img{
		width: 100%;
		height: auto;
	}
	.input-group label.custom-upload{
		width: 100px!important;
		margin: 0;
	}
	@media (max-width: 575.98px) {
		.create-page .item, .box-seo .seo-input, .box-seo .seo-input input, .box-seo .seo-input textarea, .box-seo .seo-input .upload-img, .box-img-meta{
			display: inline-block;
			width: 100%
		}
		.create-page .item:last-child{
			margin-left: 0;
		}
		.box-seo .seo-input input, .box-seo .seo-input textarea{
			margin-top: 10px;
		}
		.box-seo .seo-input .upload-img{
			margin: 0;
			margin-top: 10px;
		}
		.seo-input .upload-img input{
			margin-top: 0;
		}
		.custom-upload input[type='file']{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-seo .seo-input input, .box-seo .seo-input textarea, .box-seo .seo-input .upload-img {
		    width: 65%;
		    margin-left: 10px;
		}
		.input-group label.custom-upload, .delete-img{
		    width: 60px!important;
		}
		.box-seo .seo-input .upload-img input {
		    width: 60%;
		}
		.box-img-meta{
			width: 100%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.box-seo .seo-input input, .box-seo .seo-input textarea, .box-seo .seo-input .upload-img {
		    width: 65%;
		    margin-left: 10px;
		}
		.input-group label.custom-upload, .delete-img{
		    width: 60px!important;
		}
		.box-seo .seo-input .upload-img input {
		    width: 60%;
		}
		.box-img-meta{
			width: 100%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.box-seo .seo-input input, .box-seo .seo-input textarea, .box-seo .seo-input .upload-img{
			margin-left: 5px;
			width: 78%;
		}
		.box-img-meta{
			width: 100%;
		}
	}
	@media (min-width: 1200px) {

	}
</style>
<main class="block content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Tạo page</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Xem trước">
						<i class="fa fa-eye" aria-hidden="true"></i> <label>Xem trước</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu nháp">
						<i class="fa fa-file-text" aria-hidden="true"></i> <label>Lưu nháp</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="create-page">
					<div class="item">
						<label for="name">Tên page:</label>
						<input autocomplete="off" type="text" name="name" id="name" placeholder="Tên page" value="" class="form-control">
						<label for="url">Link URL:</label>
						<input autocomplete="off" type="text" name="url" id="url" placeholder="Link URL" value="" class="form-control">
						<div class="box-seo">
							<label for="meta_web_title">Google:</label>
							<span class="seo-input">
								Title:
								<input autocomplete="off" type="text" name="meta_web_title" id="meta_web_title" placeholder="Nhập title" value="" class="form-control">
							</span>
							<span class="seo-input">
								Keyword:
								<input autocomplete="off" type="text" name="meta_web_keyword" id="meta_web_keyword" placeholder="Nhập keyword" value="" class="form-control">
							</span>
							<span class="seo-input">
								Description:
								<textarea name="meta_web_desc" id="meta_web_desc" class="form-control" rows="3" placeholder="Nhập description"></textarea>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="box-seo">
							<label for="meta_web_title">Social:</label>
							<span class="seo-input">
								Title:
								<input autocomplete="off" type="text" name="og_title" id="og_title" placeholder="Meta O.g title..." value="" class="form-control">
							</span>
							<span class="seo-input">
								Description:
								<textarea name="og_desc" id="og_desc" class="form-control" rows="3" placeholder="Meta O.g descriptions..."></textarea>
							</span>
							<span class="seo-input">
								Image (16x9):
								<div class="upload-img">
		                            <div class="input-group up-img">
		                                <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
		                                <label class="button bg-green custom-upload">
		                                    <input type="file" class="form-control" name="icon" id="icon" onchange="readURL(this,1);" accept="image/*">Upload
		                                </label>
		                                <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
		                            </div>
		                        </div>
							</span>
							<span class="seo-input">
								<div class="box-img-meta hidden">
		                        	<img src="assets/images/collagen-sp.jpg" alt="collagen" id="photo1">
		                        </div>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	function readURL(input,id) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	        	jQuery('#photo'+id).parent().removeClass('hidden');
	          	jQuery('#photo'+id).attr('src', e.target.result);
	          	if(jQuery('.group-box-seo').height()>jQuery('.group-custom-cate').height()){
					jQuery('.group-category').height(jQuery('.group-box-seo').height()-2);
				}
				else{
					jQuery('.group-category').height(jQuery('.group-custom-cate').height());
				}
	        };
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	jQuery(function(){
		jQuery(document).on('change', ':file', function() {
		    var input = jQuery(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		});
		jQuery(':file').on('fileselect', function(event, numFiles, label) {
          	var input = jQuery(this).parents('.up-img').find(':text'),
              	log = numFiles > 1 ? numFiles + ' files selected' : label;
          	if( input.length ) {
              	input.val(log);
              	if(input.val(log)){
                	jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
                	jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
              	}
          	} else {
              	if(log) ;
          	}
      	});
      	jQuery('.delete-img').on('click', function(e){
		    jQuery(this).parent().find('input[type=file]').val('');
		    jQuery(this).parent().find('input[type=text]').val('');
		    jQuery(this).parent().find('.custom-upload').css('display','flex');
		    jQuery('.box-img-meta').addClass('hidden');
		    jQuery(this).css('display','none');
		});
	})
</script>