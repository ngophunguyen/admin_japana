<style>
	#info-pn .item{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		float: left;
		width: 25%;
		margin-top: 20px;
	}
	#info-pn .item:last-child{
		width: 50%;
	}
	#info-pn .item:last-child span, #info-pn .item:last-child .custom-dropdown{
		margin-right: 5px;
	}
	#info-pn .item label{
		font-weight: 500;
		font-size: 14px;
		color: #000;
		margin-right: 5px;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    position: relative;
	}
	.table-custom{
		margin-bottom: 15px;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.link-show{
		display: none;
	}
	.title-check, .title-list li{
	    display: inline-block;
	    width: 100%;
	}
	.title-check{
	    border-bottom: 1px solid #ccc;
	}
	.title-check li{
	    float: left;
	    width: 45%;
	    padding: 5px;
	}
	.box-cmt{
	    padding: 10px 5px;
	}
	.box-cmt textarea, .upload-img{
	    margin-bottom: 15px;
	}
	.box-cmt button{
	    float: right;
	    margin-left: 10px;
	}
	.box-cmt .item{
		display: inline-block;
		width: 100%;
		float: left;
		line-height: 1.5;
		margin-bottom: 15px;
	}
	.box-cmt .item p:first-child span:last-child{
		font-size: 13px;
	}
	.box-cmt .item p:last-child{
		font-size: 14px;
	}
	#comment_kh{
	    border: 1px solid #cbcbcb;
	    padding: 10px;
	    border-radius: 5px;
	    margin-bottom: 12px;
	    max-height: 164px;
	    overflow-y: scroll;
	    overflow-x: hidden;
	    background: #fff;
	    position: relative;
	  }
	.box-input-cmt{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
	}
	.box-input-cmt .input-cmt{
		width: 84%;
	    float: left;
	    resize: none;
	}
	.box-input-cmt button{
		float: right;
		margin-left: 2%;
		height: 35px;
	}
	.cmt-staff{
		display: inline-block;
		width: 100%;
	}
	.cmt-staff .item:nth-child(even){
		text-align: right;
	}
	#info-pn, #products-pn , #note-pn{
		display: inline-block;
		width: 100%;
	}
	#note-pn{
	    float: right;
	    width: 35%
	}
	.show-coupon-box{
		display: none;
	}
	.barcode-check{
		display: flex;
	}
	.barcode-check input{
		margin: 10px 15px 0 0;
	}
	.barcode-check input:last-child{
		width: 80px;
	}
	@media (max-width: 575.98px) {
		#info-pn, #products-pn, #note-pn{
			display: none;
			width: 100%;
		}
		#products-pn button{
			margin-bottom: 30px;
		}
		#info-pn .item:last-child{
			display: initial;
		}
		#info-pn .item, #info-pn .item:last-child{
			width: 100%;
		}
		#info-pn .item:last-child .custom-dropdown{
			margin: 10px 0;
		}
		.box-coupon-total{
			display: none;
			width: 100%;
			background: #fff;
			position: fixed;
		    left: 0;
		    right: 0;
		    padding: 0 15px;
		    overflow: scroll;
		    bottom: 35px;
		    border-top: 1px solid #eee;
		    z-index: 2;
		}
		.show-coupon-box{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		    -webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		    -webkit-box-pack: center;
		        -ms-flex-pack: center;
		            justify-content: center;
		    height: 35px;
			width: 100%;
			position: fixed;
			left: 0;
			right: 0;
			bottom: 0;
			z-index: 2;
		}
		.show-coupon-box a{
			color: #fff;
    		line-height: 1.5;
		}
		.slide-top{
			-webkit-animation: slide-top 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) infinite both;
			        animation: slide-top 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) infinite both;
		}
		@-webkit-keyframes slide-top {
		  0% {
		    -webkit-transform: translateY(10px);
		            transform: translateY(10px);
		  }
		  100% {
		    -webkit-transform: translateY(-10px);
		            transform: translateY(-10px);
		  }
		}
		@keyframes slide-top {
		  0% {
		    -webkit-transform: translateY(10px);
		            transform: translateY(10px);
		  }
		  100% {
		    -webkit-transform: translateY(-10px);
		            transform: translateY(-10px);
		  }
		}
		.title-check{
			margin-top: 15px;
		}
	    .title-list li{
	      	white-space: nowrap;
	      	overflow: scroll;
	    }
	    #note-pn .item .title-check li{
	    	display: block;
	    }
	    #products-pn{
	    	padding-top: 15px;
	    	height: 100%;
	    }
	    #products-pn .table-custom{
	    	margin-top: 0;
	    }
		.table-custom tr td:first-child{
			display: none;
		}
		.table-custom > tbody > tr > td{
			white-space: normal;
		}
		.barcode-check{
			margin-top: 15px;
		}
		#products-pn .table-custom > tbody > tr > td:last-child{
			justify-content: baseline;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		#info-pn .item{
			width: 32%;
		}
		#info-pn .item:last-child{
			width: 100%;
		}
		#note-pn{
			width: 100%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		#info-pn .item{
			width: 32%;
		}
		#info-pn .item:last-child{
			width: 100%;
		}
		#note-pn{
			width: 100%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="listreceiptrefund-detail content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chi tiết phiếu trả</h1>
			<ul>
				<li>
					<a href="?action=listreceiptrefund.php" class="link-custom black-custom" title="Trở về">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Trở về</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="tab-custom bg-black">
				<div class="item active">
					<a href="javascript:void(0)" data-id="info-pn" title="Thông tin">
						<i class="fa fa-user" aria-hidden="true"></i>
						<label>Thông tin</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="products-pn" title="Sản phẩm">
						<i class="fa fa-list" aria-hidden="true"></i>
						<label>Sản phẩm</label>
					</a>
				</div>
				<div class="item bg-black">
					<a href="javascript:void(0)" data-id="note-pn" title="Ghi chú">
						<i class="fa fa-sticky-note" aria-hidden="true"></i>
						<label>Ghi chú</label>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<div id="info-pn" class="tab-content item show-inline">
					<div class="item">
						<label>Mã phiếu trả:</label>
						<span>PTH-89</span>
					</div>
					<div class="item">
						<label>Vận chuyển:</label>
						<span>Giao hàng tiết kiệm</span>
					</div>
					<div class="item">
						<label>Người tạo:</label>
						<span>Trần Thị Ngọc Trân</span>
					</div>
					<div class="item">
						<label>Nhà cung cấp:</label>
						<span>NCC_069</span>
					</div>
					<div class="item">
						<label>Mã NCC:</label>
						<span>NCC_025</span>
					</div>
					<div class="item">
						<label>Phí vận chuyển:</label>
						<span>0 đ</span>
					</div>
					<div class="item">
						<label>Ngày tạo:</label>
						<span>22-05-2019 11:49:35</span>
					</div>
					<div class="item">
						<label>Mã vận đơn:</label>
						<span>0</span>
					</div>
					<div class="item">
						<label>Trạng thái:</label>
						<span>Phiếu mới</span>
						<div class="custom-dropdown">
				    		<select class="form-control" name="id_status">
					    		<option value="-1">Chọn trạng thái</option>
							</select>
				    	</div>
				    	<button type="submit" class="button bg-green">Lưu lại</button>
					</div>
				</div>
				<div id="products-pn" class="tab-content item">
					<div class="box-table">
						<table class="table table-custom table-striped table-responsive">
						    <thead class="bg-black">
						        <tr class="bg-black">
						            <th class="bg-black center-custom">STT</th>
						            <th class="bg-black">SKU</th>
						            <th class="bg-black">Tên sản phẩm</th>
						            <th class="bg-black center-custom">Hạn sử dụng</th>
						            <th class="bg-black center-custom">Code mã vạch</th>
						            <th class="bg-black center-custom">SL quét</th>
						            <th class="bg-black center-custom">Số lượng</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td data-title="STT" class="center-custom">1</td>
						            <td data-title="SKU">069IB05</td>
						            <td data-title="Tên sản phẩm">Bột trà xanh Uji Matcha Yano Nhật Bản 100g</td>
						            <td data-title="Hạn sử dụng" class="center-custom">19-12-2019</td>
						            <td data-title="Code mã vạch" class="center-custom"><span class="red-custom">6306-1</span></td>
						            <td data-title="Giá bán" class="center-custom"><span class="red-custom">0</span></td>
						            <td data-title="Chiết khấu" class="center-custom">2</td>
						        </tr>
						    </tbody>
						</table>
					</div>
					<div class="box-coupon-total">
						<div class="barcode-check custom-fleft">
							<input type="text" name="" onkeypress="checkMaVach(event);" id="code_mavach" placeholder="Nhập mã sản phẩm" class="form-control">
							<input type="text" name="" id="code_sl" class="form-control" value="1" placeholder="Nhập số">
						</div>
					</div>
					<div class="show-coupon-box bg-black">
						<a href="javascript:void(0);" title="Show">
							Mở thông tin <i class="fa fa-angle-double-up slide-top"></i>
						</a>
					</div>
				</div>
	          	<div class="box-check">
		            <div id="note-pn" class="tab-content item">
		            	<div class="item">
		              		<ul class="title-check">
		                		<li class="bold">Ghi chú</li>
		              		</ul>
		              		<div class="box-cmt">
		              		<div id="comment_kh">
			                  	<div class="item cmt-customer">
				                    <p class="red-custom bold">Quản lý TVBH <span class="black-custom">- Nguyễn Thu Hà</span> <span class="gray-custom normal">(27/05/2019 10:40:33)</span></p>
				                    <p class="detail-cmt">Khách mua loại khác dành cho công nghiệp</p>
			                  	</div>
			                  	<div class="cmt-staff">
									<div class="item">
										<p class="blue-custom bold">Thu mua <span class="black-custom">- Hương</span> <span class="gray-custom normal">(27-12-2018 | 10:45)</span></p>
										<p class="detail-cmt">Giao hàng nhanh, trước 14h hôm nay</p>
									</div>
									<div class="item">
										<p class="blue-custom bold">Tư vấn bán hàng <span class="black-custom">- Hằng</span> <span class="gray-custom normal">(27-12-2018 | 10:45)</span></p>
										<p class="detail-cmt">Khách yêu cầu không giao nhanh</p>
									</div>
									<div class="item">
										<p class="blue-custom bold">Xử lý đơn hàng <span class="black-custom">- Lam</span> <span class="gray-custom normal">(27-12-2018 | 10:45)</span></p>
										<p class="detail-cmt">Giao nhanh cho chị</p>
									</div>
								</div>
			                </div>
			                <textarea id="comment" rows="2" class="form-control input-cmt"></textarea>
			                <button type="button" data-dismiss="modal" class="button bg-red">Hủy</button>
			                <button type="button" class="button bg-green">Lưu</button>
			              </div>
		            	</div>
		            </div>
	          	</div> 
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(document).one('focus.input-cmt', 'textarea.input-cmt', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    }).on('input.input-cmt', 'textarea.input-cmt', function(){
        var minRows = this.getAttribute('data-min-rows')|0, rows;
        this.rows = minRows;
        rows = Math.floor((this.scrollHeight - this.baseScrollHeight)/16);
        this.rows = minRows + rows;
    });
	jQuery(function(){
		jQuery('.show-coupon-box a').click(function(){
			if(jQuery('.box-coupon-total').css('display')=='none'){
				jQuery('.box-coupon-total').css('display','inline-block');
				jQuery(this).text("");
				jQuery(this).append("Đóng thông tin <i class='fa fa-angle-double-down slide-top'></i>");
				showBackgroundPopup();
			}
			else{
				jQuery('.box-coupon-total').css('display','none');
				deleteBackgroundPopup();
				jQuery(this).text("");
				jQuery(this).append("Mở thông tin <i class='fa fa-angle-double-up slide-top'></i>");
			}
		});
		new PerfectScrollbar('#comment_kh');
		jQuery('.tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.tab-content').not('#' + data).removeClass('show-inline');
	    	jQuery(this).parent().addClass('active');
	    	jQuery('.tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    });
	})
</script>