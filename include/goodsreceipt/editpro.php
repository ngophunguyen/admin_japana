<style>
	.box-editpro{
    display: inline-block;
    width: 100%;
  }
  .box-editpro .item{
    width: 49%;
    float: left;
    margin-bottom: 15px;
  }
  .box-editpro .item:nth-child(even){
    margin-left: 2%;
  }
  .box-editpro .item label{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 5px;
    font-weight: 500;
    width: 100%;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
  }
  @media (max-width: 575.98px) {
    .box-editpro .item{
      width: 100%;
    }
    .box-editpro .item:nth-child(even){
      margin-left: 0;
    }
    #editpro-modal .action-button{
      padding: 0;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {  
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal medium-modal" id="editpro-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Chỉnh sửa sản phẩm
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
      	<div class="box-editpro">
      		<div class="item">
      			<label for="id_pro">
              Mã sản phẩm 
              <a href="javascript:void(0);" id="ShowHistoryPageEdit" data-toggle="modal" data-target="#historybuypro-modal" data-dismiss="modal" title="Xem lịch sử mua hàng">
                <i class="fa fa-angle-double-right" aria-hidden="true"></i> Xem lịch sử mua hàng
              </a>
            </label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control bg-gray" id="id_pro" placeholder="Nhập mã sản phẩm" disabled="">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="name_pro">Tên sản phẩm</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="name_pro" placeholder="Nhập tên sản phẩm">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="price_buy">Giá mua</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="price_buy" name="price_buy" placeholder="Nhập giá mua">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="price_sell">Giá bán</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control bg-gray" id="price_sell" name="price_sell" placeholder="Nhập giá bán" disabled="">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="qty_pro">Số lượng</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="qty_pro" name="quantity" placeholder="Nhập số lượng">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="profit_edit">Chiết khấu</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control bg-gray" id="profit_edit" name="profit" placeholder="Nhập chiết khấu" disabled="">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="type_pro_edit">Loại sản phẩm</label>
      			<div class="custom-dropdown">
              <select class="form-control" id="type_pro_edit" name="product_type">
                  <option value="-1">Chọn loại</option>
                  <option value="0">Chiến lược</option>
                  <option value="1">Tập trung</option>
                  <option value="2">Còn lại</option>
              </select>
            </div>
      		</div>
      		<div class="item">
      			<label for="total_pro">Tổng tiền</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control bg-gray" id="total_pro" name="total" placeholder="Nhập tổng tiền" disabled="">
	      		</div>
      		</div>
      		<div class="item">
      			<label for="datepicker">Ngày hàng về</label>
      			<div class="box-input-addpro">
	      			<div class="box-time">
                <input autocomplete="off" onkeypress="return false;" type="text" id="datepicker" name="date_arrival" class="form-control datereceipt" value="">
                <i class="fa fa-calendar icon-time"></i>
              </div>
	      		</div>
      		</div>
      		<div class="item">
      			<label for="id_warehouse">Kho hàng</label>
      			<div class="custom-dropdown">
              <select class="form-control" id="id_warehouse" name="id_warehouse">
                  <option value="-1">Chọn kho</option>
                  <option value="0">Kho hàng bán</option>
                  <option value="1">Kho ký gửi</option>
                  <option value="2">Kho gameshow</option>
                  <option value="3">Kho trưng bày</option>
              </select>
            </div>
      		</div>
          <div class="item">
            <label for="id_duyet">Xét duyệt quản lý:<span class="red-custom">Chấp nhận đặt hàng</span></label>
            <div class="box-input-addpro">
              <input type="text" class="form-control" id="id_duyet" placeholder="Xét duyệt quản lý">
            </div>
          </div>
          <div class="item">
            <label for="id_thue_ncc">Ghi chú</label>
            <div class="box-input-addpro">
              <input type="text" class="form-control bg-gray" id="ghichu" name="note" placeholder="Nhập ghi chú" disabled="">
            </div>
          </div>
          <div class="item">
            <div class="box-input-addpro">
              <label class="checkbox-custom"> Có phí vận chuyển
                <input value="1" id="fee_edit" name="fee" type="checkbox">
                <span class="checkmark"></span>
              </label>
            </div>
          </div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" data-toggle="modal" data-target="#addpro-modal" class="button bg-red">Hủy</button>
          <button type="button" class="button bg-green">Lưu</button>
      </div>
    </div>
  </div>
</div>
<script>
	jQuery(function(){
		
	})
</script>