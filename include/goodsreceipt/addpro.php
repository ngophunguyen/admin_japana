<style>
	.action-button{
		display: inline-block;
		width: 100%;
		text-align: right;
	}
	.box-addpro .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%;
	}
	.box-addpro .item{
		width: 32%;
		float: left;
		margin-bottom: 15px;
		position: relative;
	}
	.box-addpro>.item:nth-child(2){
		margin: 0 2%;
	}
	.box-addpro>.item:nth-child(4){
		width: 100%;
	}
	.box-table-sku{
		display: none;
		position: absolute;
	    left: 0;
	    background: #fff;
	    z-index: 99;
	    border: 1px solid #ccc;
	}
	.box-table-sku .table-custom{
		margin-top: 0;
	}
	.box-table-sku .table-custom > thead > tr > th{
		color: initial;
	}
	.list-price-pro{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
		width: 100%;
	}
	.list-price-pro .item{
		width: 19%;
	}
	.addncc-modal{
		min-width: 38px;
		height: 38px;
		border-radius: 0 4px 4px 0;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
		float: left;
	}
	.box-input-addpro{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	.box-input-addpro .custom-dropdown{
		width: 100%;
		float: left;
	}
	.box-input-addpro .custom-dropdown #id_supplier{
		border-radius: 4px 0 0 4px;
	}
	.box-payback-table{
	    display: inline-block;
	    float: left;
	    width: 100%;
	}
	.box-payback-table .box-table tr td:last-child{
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.box-payback-table .box-table tr td input[name="price_buy"],
	.box-payback-table .box-table tr td input[name="qty_pro"],
	.select-type,
	.date-time{
		display: none;
		width: auto;
	}
	.select-type select, .date-time input{
		width: auto;
	}
	.select-type:after{
		padding: 12px;
	}
	#view_historybuy{
		float: right;
	}
	.box-input-addpro .select2-container--default .select2-selection--single{
		border-radius:  4px 0 0 4px ;
	}
	#info-rc, #sanpham-rc{
		display: inline-block;
		position: relative;
		width: 100%;
	}
	@media (max-width: 575.98px) {
		.box-addpro .item{
			width: 100%;
		}
		#s2id_id_supplier{
			border-radius: 4px 0 0 4px;
		}
		.box-addpro>.item:nth-child(2){
			margin: 0;
			margin-bottom: 15px;
		}
		.list-price-pro{
	      display: inline-block;
	    }
		#info-rc, #sanpham-rc{
			display: none;
			width: 100%;
			padding: 15px;
		}
		.table-addpro{
			margin-top: 0;
		}
		#addpro-modal .modal-body{
			padding: 0;
		}
		#sanpham-rc .box-payback-table .box-table tbody tr:last-child{
			margin-bottom: 0;
		}
		.box-payback-table .box-table tr td:last-child{
			-ms-flex-pack: distribute;
			    justify-content: space-around;
		}
		#sanpham-rc .box-table tr td input, #sanpham-rc .box-table tr td select{
			width: inherit;
		}
		#sanpham-rc .select-type, #sanpham-rc .date-time{
			width: 100%;
		}
		.table-custom > tbody > tr > td{
			white-space: normal;
		}
		.box-table-sku .table-responsive > tbody > tr td:first-child{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}
		.box-table-sku .table-responsive > tbody > tr{
			margin-bottom: 0;
		}
		.list-price-pro .item:last-child{
			margin-bottom: 0;
		}
		.box-addpro button{
			margin-bottom: 15px;
		}
		.list-price-pro{
			margin-bottom: 10px;
		}
		#sanpham-rc .action-button{
			padding: 0;
		}
		.table-custom tr th:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<div class="modal super-large-modal" id="addpro-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Thêm sản phẩm
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
      	<div class="tab-custom bg-black">
			<div class="item active">
				<a href="javascript:void(0)" data-id="info-rc" title="Thông tin">
					<i class="fa fa-sticky-note" aria-hidden="true"></i>
					<label>Thông tin</label>
				</a>
			</div>
			<div class="item">
				<a href="javascript:void(0)" data-id="sanpham-rc" title="Sản phẩm">
					<i class="fa fa-cube" aria-hidden="true"></i>
					<label>Sản phẩm</label>
				</a>
			</div>
		</div>
	    <div id="info-rc" class="box-addpro tab-content item show-inline">
      		<div class="item">
      			<label for="id_supplier">Chọn nhà cung cấp</label>
      			<div class="box-input-addpro">
					<select class="form-control" id="s2id_id_supplier">
			    		<option value="-1">Chọn nhà cung cấp</option>
					  	<option value="0">SBS (Anh Phi)</option>
					  	<option value="1">JP Shop Nhật (Chị Thu)</option>
					  	<option value="2">Chị Châu SK-II</option>
					</select>
					<a href="javascript:void(0)" data-dismiss="modal" data-toggle="modal" data-target="#addncc-modal" title="Thêm nhà cung cấp" class="addncc-modal white-custom bg-green">
                        <i class="ti-plus"></i>
                    </a>
      			</div>
      		</div>
      		<div class="item">
      			<label for="id_pro">
      				Mã sản phẩm
      				<a id="view_historybuy" href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#historybuy-modal" title="Xem lịch sử mua hàng"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
	                    Xem lịch sử mua hàng
	                </a>
      			</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="id_pro" name="sku" placeholder="Nhập mã sản phẩm">
	      		</div>
	      		<div class="box-table box-table-sku">
                    <table id="ListProAfterSearch" class="table table-custom table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>SKU</th>
                                <th>Tên sản phẩm</th>
                            </tr>
                        </thead>
                        <tbody>    
                        	<tr>
								<td data-title="SKU">041ECI25</td>
								<td data-title="Tên sản phẩm">Giấy thấm dầu DHC Oil Blotting Paper (100 miếng) </td>
							</tr>
							<tr>
								<td data-title="SKU">041ECI23</td>
								<td data-title="Tên sản phẩm">Bông tẩy trang DHC Silky Cotton (80 miếng)</td>
							</tr>
						</tbody>
                    </table>
                </div>
      		</div>
      		<div class="item">
      			<label for="id_warehouse">Kho hàng</label>
      			<div class="box-input-addpro">
      				<div class="custom-dropdown">
						<select class="form-control" id="id_warehouse" name="id_warehouse">
				    		<option value="-1">Chọn kho</option>
						  	<option value="0">Kho hàng bán</option>
						  	<option value="1">Kho ký gửi</option>
						  	<option value="2">Kho gameshow</option>
						  	<option value="3">Kho trưng bày</option>
						</select>
					</div>
      			</div>
      		</div>
      		<div class="item">
      			<label for="name_pro">Tên sản phẩm</label>
      			<div class="box-input-addpro">
	      			<input type="text" class="form-control" id="name_pro" name="name" placeholder="Nhập tên sản phẩm" disabled="">
	      		</div>
      		</div>
      		<div class="list-price-pro">
      			<div class="item">
	      			<label for="price_buy">Giá mua (đ)</label>
	      			<div class="box-input-addpro">
		      			<input type="text" onkeyup="countProfit();" class="form-control" id="price_buy" name="price_buy" placeholder="Nhập giá mua" disabled="">
		      		</div>
	      		</div>
	      		<div class="item">
	      			<label for="price_sell_show">Giá bán trước KM (đ)</label>
	      			<div class="box-input-addpro">
		      			<input type="text" class="form-control" id="price_sell_show" name="price_sell_show" placeholder="Nhập giá bán trước KM" disabled="">
		      			<input type="hidden" value="" name="price_sell" id="price_sell">
		      		</div>
	      		</div>
	      		<div class="item">
	      			<label for="price_sell_show">Giá bán sau KM (đ)</label>
	      			<div class="box-input-addpro">
		      			<input type="text" class="form-control" id="price_sell_show" name="price_sell_show" placeholder="Nhập giá bán sau KM" disabled="">
		      			<input type="hidden" value="" name="price_sell" id="price_sell">
		      		</div>
	      		</div>
	      		<div class="item">
	      			<label for="qty_pro">Số lượng</label>
	      			<div class="box-input-addpro">
		      			<input type="text" onblur="countTotal()" class="form-control" id="qty_pro" name="quantity" placeholder="Nhập số lượng" disabled="">
		      		</div>
	      		</div>
	      		<div class="item">
	      			<label for="profit_show">Tổng tiền (đ)</label>
	      			<div class="box-input-addpro">
		      			<input type="text" class="form-control" id="profit_show" name="name" placeholder="Nhập tổng tiền" disabled="">
		      			<input type="hidden" value="" name="total" id="total_pro">
		      		</div>
	      		</div>
      		</div>
      		<div class="list-price-pro">
      			<div class="item">
	      			<label for="name_pro">Chiết khấu</label>
	      			<div class="box-input-addpro">
		      			<input type="text" class="form-control" id="name_pro" name="name" placeholder="Nhập chiết khấu" disabled="">
		      			<input type="hidden" value="" name="profit" id="profit_add">
		      		</div>
	      		</div>
      			<div class="item">
	      			<label for="type_pro">Loại sản phẩm</label>
	      			<div class="box-input-addpro">
		      			<div class="custom-dropdown">
							<select class="form-control" id="type_pro" name="product_type">
							  	<option value="0">Chiến lược</option>
							  	<option value="1">Tập trung</option>
							  	<option value="2">Còn lại</option>
							</select>
						</div>
		      		</div>
	      		</div>
	      		<div class="item">
	      			<label for="date-pro">Ngày hàng về</label>
	      			<div class="box-input-addpro">
		      			<div class="box-time">
		            		<input type="hidden" id="date-pro" name="date_arrival" class="form-control" value="">
				    		<input autocomplete="off" onkeypress="return false;" type="text" id="date-pro" name="date_arrival" class="form-control" value="">
	                        <i class="fa fa-calendar icon-time"></i>
	                    </div>
		      		</div>
	      		</div>
	      		<div class="item">
	      			<label for="ghichu">Ghi chú</label>
	      			<div class="box-input-addpro">
		      			<input type="text" class="form-control" id="ghichu" name="note" placeholder="Ghi chú">
		      		</div>
	      		</div>
	      		<div class="item">
	      			<label for="fee"></label>
	      			<div class="box-input-addpro">
		      			<label class="checkbox-custom"> Có phí vận chuyển
	                      	<input value="1" id="fee" name="fee" type="checkbox">
	                      	<span class="checkmark"></span>
	                    </label>
		      		</div>
	      		</div>
      		</div>
      		<button type="button" class="button bg-green custom-fright">Thêm</button>
	    </div>
	    <div id="sanpham-rc" class="box-payback-table tab-content item">
			<div class="box-table">
				<table class="table table-custom table-addpro table-responsive">
				  <thead>
				    <tr>
						<th class="black-custom bold center-custom">STT</th>
						<th class="black-custom bold">SKU</th>
						<th class="black-custom bold">Tên sản phẩm</th>
						<th class="black-custom bold right-custom">Giá bán</th>
						<th class="black-custom bold right-custom">Giá mua</th>
						<th class="black-custom bold center-custom">Lợi nhuận</th>
						<th class="black-custom bold center-custom">Số lượng</th>
						<th class="black-custom bold right-custom">Tổng tiền</th>
						<th class="black-custom bold">Loại</th>
						<th class="black-custom bold">Ngày hàng về</th>
						<th class="black-custom bold right-custom">Tác vụ</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
						<td data-title="STT" class="center-custom">1</td>
						<td data-title="SKU">087AA9</td>
						<td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						<td data-title="Giá bán" class="right-custom">495.000 đ</td>
						<td data-title="Giá mua" class="right-custom">
							<input autocomplete="off" type="text" name="price_buy" class="form-control price_buy1" value="2.000">
							<span>2.000 đ</span>
						</td>
						<td data-title="Lợi nhuận" class="center-custom">99.6 %</td>
						<td data-title="Số lượng" class="center-custom">
							<input autocomplete="off" type="text" name="qty_pro" class="form-control qty_pro1" value="1">
							<span>2</span>
						</td>
						<td data-title="Tổng tiền" class="right-custom">4.000 đ</td>
						<td data-title="Loại">
							<div class="custom-dropdown select-type type_pro1">
								<select class="form-control" id="type_pro" name="product_type">
								  	<option value="0" selected="">Chiến lược</option>
								  	<option value="1">Tập trung</option>
								  	<option value="2">Còn lại</option>
								</select>
							</div>
							<span>Chiến lược</span>
						</td>
						<td data-title="Ngày hàng về">
							<div class="box-time date-time date-order1">
								<input autocomplete="off" onkeypress="return false;" type="text" id="date-pro" name="date_arrival" class="form-control" value="">
								<i class="fa fa-calendar icon-time"></i>
							</div>
							<span>31-05-2019</span>
						</td>
						<td data-title="Tác vụ" class="right-custom">
							<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItemProList(1)" title="Chỉnh sửa">
				        		<i class="fa fa-pencil-square-o"></i>
				        	</a>
				        	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
				        		<i class="fa fa-floppy-o" aria-hidden="true"></i>
				        	</a>
				        	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
				        		<i class="fa fa-trash-o"></i>
				        	</a>
						</td>
				    </tr>
				    <tr>
						<td data-title="STT" class="center-custom">2</td>
						<td data-title="SKU">087AA9</td>
						<td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
						<td data-title="Giá bán" class="right-custom">495.000 đ</td>
						<td data-title="Giá mua" class="right-custom">
							<input autocomplete="off" type="text" name="price_buy" class="form-control price_buy2" value="2.000">
							<span>2.000 đ</span>
						</td>
						<td data-title="Lợi nhuận" class="center-custom">99.6 %</td>
						<td data-title="Số lượng" class="center-custom">
							<input autocomplete="off" type="text" name="qty_pro" class="form-control qty_pro2" value="2">
							<span>2</span>
						</td>
						<td data-title="Tổng tiền" class="right-custom">4.000 đ</td>
						<td data-title="Loại">
							<div class="custom-dropdown select-type type_pro2">
								<select class="form-control" id="type_pro" name="product_type">
								  	<option value="0" selected="">Chiến lược</option>
								  	<option value="1">Tập trung</option>
								  	<option value="2">Còn lại</option>
								</select>
							</div>
							<span>Chiến lược</span>
						</td>
						<td data-title="Ngày hàng về">
							<div class="box-time date-time date-order2">
								<input autocomplete="off" onkeypress="return false;" type="text" id="date-pro" name="date_arrival" class="form-control" value="">
								<i class="fa fa-calendar icon-time"></i>
							</div>
							<span>31-05-2019</span>
						</td>
						<td data-title="Tác vụ" class="right-custom">
							<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItemProList(2)" title="Chỉnh sửa">
				        		<i class="fa fa-pencil-square-o"></i>
				        	</a>
				        	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
				        		<i class="fa fa-floppy-o" aria-hidden="true"></i>
				        	</a>
				        	<a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
				        		<i class="fa fa-trash-o"></i>
				        	</a>
						</td>
				    </tr>
				  </tbody>
				</table>
			</div>
			<div class="action-button">
	        	<button type="button" data-dismiss="modal" class="button bg-red">Hủy</button>
	        	<button type="button" class="button bg-green">Lưu</button>
	        </div>
		</div>
      </div>
    </div>
  </div>
</div>
<?php include('include/goodsreceipt/addncc.php'); ?>
<script>
	function editItemProList(id) {
	    jQuery('.price_buy' + id).css('display', 'block');
	    jQuery('.price_buy' + id).next().css('display', 'none');
	    jQuery('.qty_pro' + id).css('display', 'block');
	    jQuery('.qty_pro' + id).next().css('display', 'none');
	    jQuery('.type_pro' + id).css('display', 'block');
	    jQuery('.type_pro' + id).next().css('display', 'none');
	    jQuery('.date-order' + id).css('display', 'block');
	    jQuery('.date-order' + id).next().css('display', 'none');
	}
	jQuery(function(){
		var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    jQuery('#s2id_id_supplier').select2();
	    if(window.innerWidth > 500) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
	    jQuery('input[name="date_arrival"]').daterangepicker({
			singleDatePicker: true,
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			locale: formatDate
		},function(start) {
			jQuery('#date-pro').val(start.format('DD-MM-YYYY'));
		});

		jQuery('#addpro-modal .tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('#addpro-modal .tab-content').not('#' + data).removeClass('show-inline');
	    	jQuery(this).parent().addClass('active');
	    	jQuery('#addpro-modal .tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#addpro-modal #'+data).addClass('show-inline');
	    });

	    jQuery("#id_pro").on("keyup", function() {
		    jQuery('.box-table-sku').css('display','inline-block');
		    if(jQuery('.box-table-sku tbody tr').length>6){
		      jQuery('.box-table-sku').css('overflow-y','scroll');
		    }
		    var value = jQuery(this).val();
		      	jQuery(".box-table-sku tbody tr").each(function(index) {
		          	if (index !== 0) {
		              	$row = jQuery(this);
		              	var id = $row.find("td:first").text();
		            if (id.toLowerCase().indexOf(value.toLowerCase()) === -1) {
		                $row.hide();
		            }
		            else {
		                $row.show();
		            }
		        }
		    });
		});
		jQuery(window).click(function() {
		    jQuery('.box-table-sku').hide()
		});
	})
</script>