<style>
  .info-create-receipt{
    display: inline-block;
    width: 100%;
  }
  .info-create-receipt .item{
    width: 49%;
    float: left;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    margin-bottom: 10px;
  }
  .info-create-receipt .item:nth-child(even){
    margin-left: 2%;
  }
  .info-create-receipt .item label{
    font-size: 14px;
    line-height: 1.5;
    font-weight: 500;
    margin-right: 5px;
  }
  .info-create-receipt .item input{
    width: auto;
  }
  .box-payback-table{
    display: inline-block;
    float: left;
    width: 100%;
  }
  .date-receipt{
    width: auto;
  }
  .checkbox-receipt span{
    left: 50%;
    -webkit-transform: translateX(-50%;);
        -ms-transform: translateX(-50%;);
            transform: translateX(-50%;);
  }
  .box-check>.item{
    float: left;
    width: 35%
  }
  .box-check>.item:first-child{
    width: 65%;
  }
  .title-check, .detail-check, .number-list li, .title-list li, .listpro-slider li{
    display: inline-block;
    width: 100%;
  }
  .title-check{
    border-bottom: 1px solid #ccc;
  }
  .check-kcs{
    display: inline;
  }
  .check-kcs .checkmark{
    left: 50%;
    -webkit-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
            transform: translateX(-50%);
  }
  .title-check li, .detail-check li{
    float: left;
  }
  .title-check li{
    padding: 5px;
  }
  .title-check li:first-child, .detail-check>li:first-child{
    width: 5%;
  }
  .title-check li:nth-child(2), .detail-check>li:nth-child(2){
    width: 50%;
  }
  .title-check li:last-child, .detail-check>li:last-child{
    width: 45%;
  }
  .number-list li:first-child, .title-list li:first-child{
    visibility: hidden;
  }
  .number-list li, .title-list li, .listpro-slider .item li{
    padding: 5px;
  }
  .listpro-slider .item li .custom-dropdown{
    display: inline-block;
  }
  .listpro-slider .item li .custom-dropdown:after{
    padding: 12px;
  }
  .listpro-slider .item li:last-child{
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    -webkit-box-pack: center;
        -ms-flex-pack: center;
            justify-content: center;
  }
  .slick-arrow {
      position: absolute;
      top: 30%;
      font-size: 20px;
  }
  .listpro-slider .slick-prev, .listpro-slider .slick-next {
    color: #ccc;
    cursor: pointer;
    top: -5px;
    width: 30px;
    height: 30px;
    display: -webkit-box !important;
    display: -ms-flexbox !important;
    display: flex !important;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    z-index: 999;
  }
  .listpro-slider .slick-next {
    right: 0;
  }
  .listpro-slider .slick-prev {
    left: 0;
  }
  .listpro-slider .slick-next:hover, .listpro-slider .slick-prev:hover{
    color: #333;
  }
  .box-cmt{
    padding: 10px 5px;
  }
  .box-cmt textarea{
    margin-bottom: 15px;
  }
  .box-cmt button{
    float: right;
    margin-left: 10px;
  }
  #info-crc, #kiemduyet-crc{
    display: inline-block;
    position: relative;
    width: 100%;
  }
  .all-price-receipt{
    margin-top: 15px;
  }
  @media (max-width: 575.98px) {
    #createreceipt-modal .modal-body{
      padding: 0;
    }
    .info-create-receipt .item, .info-create-receipt .item input, .box-check>.item:first-child, .box-check>.item{
      width: 100%;
    }
    .info-create-receipt .item:nth-child(even){
      margin-left: 0;
    }
    .info-create-receipt .item label{
      margin-bottom: 5px;
    }
    .info-create-receipt .item{
      display: inline-block;
    }
    #info-crc, #kiemduyet-crc{
      display: none;
      width: 100%;
      padding: 1rem;
    }
    #info-crc .box-payback-table{
      margin-bottom: 15px;
    }
    .title-check li:first-child, .detail-check>li:first-child,.table-custom tr td:first-child{
      display: none;
    }
    .title-list li{
      white-space: nowrap;
      overflow: scroll;
    }
    .box-check>.item:first-child .title-check li:last-child{
      text-align: center;
    }
    .box-check>.item:last-child .title-check li:first-child{
      display: block;
    }
    #info-crc .table-custom > tbody > tr > td:last-child a{
      display: -ms-grid;
      display: grid;
    }
    .all-price-receipt{
      margin-top: 0;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
    .title-list li{
      white-space: nowrap;
      overflow: scroll;
    }
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
    .title-list li{
      white-space: nowrap;
      overflow: scroll;
    }
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {  
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal super-large-modal" id="createreceipt-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Tạo phiếu nhập
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="tab-custom bg-black">
          <div class="item active">
            <a href="javascript:void(0)" data-id="info-crc" title="Thông tin">
              <i class="fa fa-sticky-note" aria-hidden="true"></i>
              <label>Thông tin</label>
            </a>
          </div>
          <div class="item">
            <a href="javascript:void(0)" data-id="kiemduyet-crc" title="Kiểm duyệt">
              <i class="fa fa-list" aria-hidden="true"></i>
              <label>Kiểm duyệt</label>
            </a>
          </div>
        </div>
        <div id="info-crc" class="tab-content item show-inline">
          <div class="info-create-receipt">
            <div class="item">
              <label>Nhà cung cấp:</label>
              <span>Hàng nhật HaLi (Chị Diệu Linh)</span>
            </div>
            <div class="item">
              <label>Kho hàng:</label>
              <span>Kho hàng bán</span>
            </div>
            <div class="item">
              <label>Vận chuyển:</label>
              <div class="custom-dropdown">
                <select class="form-control" name="id_transport" id="deliveryList">
                  <option value="-1">Chọn đơn vị vận chuyển</option>
                  <option value="0">Giao hàng tiết kiệm</option>
                  <option value="1">Tín tốc</option>
                  <option value="2">Giao hàng nhanh</option>
                </select>
              </div>
            </div>
            <div class="item">
              <label>Phí vận chuyển:</label>
              <input type="text" value="" id="cost" name="transport_fee" placeholder="Nhập phí vận chuyển" class="form-control" data-type="currency">
            </div>
            <div class="item">
              <label>Mã vận đơn:</label>
              <input type="text" value="" id="code" name="transport_code" placeholder="Nhập mã vận đơn" class="form-control" data-type="currency">
            </div>
          </div>
          <div class="box-payback-table">
            <div class="box-table">
              <table class="table table-custom table-addpro table-responsive">
                <thead>
                  <tr>
                    <th class="black-custom bold center-custom">STT</th>
                    <th class="black-custom bold">SKU</th>
                    <th class="black-custom bold">Tên sản phẩm</th>
                    <th class="black-custom bold center-custom">Quy cách</th>
                    <th class="black-custom bold right-custom">Giá bán</th>
                    <th class="black-custom bold right-custom">Giá mua</th>
                    <th class="black-custom bold center-custom">Lợi nhuận</th>
                    <th class="black-custom bold center-custom">Loại</th>
                    <th class="black-custom bold">Hạn sử dụng</th>
                    <th class="black-custom bold center-custom">Số lượng</th>
                    <th class="black-custom bold center-custom">Thành tiền</th>
                    <th class="black-custom bold center-custom">Phí vận chuyển</th>
                    <th class="black-custom bold right-custom">Tác vụ</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td data-title="STT" class="center-custom">1</td>
                    <td data-title="SKU">001EE631</td>
                    <td data-title="Tên sản phẩm">Kem dưỡng ẩm Naturie Skin Conditioning Gel 180g</td>
                    <td data-title="Quy cách" class="center-custom">Hộp</td>
                    <td data-title="Giá bán" class="right-custom">280,000 đ</td>
                    <td data-title="Giá mua" class="right-custom">190,000 đ</td>
                    <td data-title="Lợi nhuận" class="center-custom">31.4 %</td>
                    <td data-title="Loại" class="center-custom">Còn lại</td>
                    <td data-title="Hạn sử dụng">
                      <div class="box-time">
                        <input type="hidden" value="1" name="product[id][]">
                        <input type="hidden" value="1" name="product[qty][]">
                        <input autocomplete="off" onkeypress="return false;" type="text" name="product[date_expire][]" class="form-control date-receipt" placeholder="Chọn ngày hiển thị...">
                        <i class="fa fa-calendar icon-time"></i>
                      </div>
                    </td>
                    <td data-title="Số lượng" class="center-custom">1</td>
                    <td data-title="Thành tiền" class="center-custom">192,000 đ</td>
                    <td data-title="Phí vận chuyển" class="center-custom">
                      <label class="checkbox-custom checkbox-receipt">
                        <input value="" type="checkbox" disabled="">
                        <span class="checkmark"></span>
                      </label>
                    </td>
                    <td data-title="Tác vụ" class="right-custom">
                      <a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#tachlo-modal" class="link-custom black-custom" title="Tách lô">
                        <i class="fa fa-cubes" aria-hidden="true"></i> Tách lô
                      </a>
                      <a href="javascript:void(0);" class="link-custom black-custom" title="Xóa">
                        <i class="fa fa-trash-o"></i>
                      </a>
                      <a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#ghichu-modal" class="link-custom black-custom" title="Ghi chú">
                        <i class="fa fa-file-text"></i>
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <p class="custom-fright bold all-price-receipt">Tổng cộng: <span class="red-custom">192,000 đ</span></p>
          </div>
        </div>
        <div id="kiemduyet-crc" class="tab-content item">
          <div class="box-check">
            <div class="item">
              <ul class="title-check">
                <li class="center-custom bold">STT</li>
                <li class="bold">Tiêu chí</li>
                <li class="bold">KCS kiểm duyệt</li>
              </ul>
              <ul class="detail-check">
                <li>
                  <ul class="number-list">
                    <li class="center-custom">0</li>
                    <li class="center-custom">1</li>
                    <li class="center-custom">2</li>
                    <li class="center-custom">3</li>
                    <li class="center-custom">4</li>
                    <li class="center-custom">5</li>
                    <li class="center-custom">6</li>
                    <li class="center-custom">7</li>
                    <li class="center-custom">8</li>
                    <li class="center-custom">9</li>
                  </ul>
                </li>
                <li>
                  <ul class="title-list">
                    <li>0</li>
                    <li>All</li>
                    <li>Tên sản phẩm</li>
                    <li>Hình ảnh</li>
                    <li>Xuất xứ</li>
                    <li>Dung tích</li>
                    <li>Giấy CBSP checkcosmetic.net</li>
                    <li>SP chính hãng có tem nhãn phụ</li>
                    <li>Trên 50% hoặc ít nhất trước 8 tháng khi hết HSD</li>
                    <li>Ngoại quang</li>
                  </ul>
                </li>
                <li class="listpro-slider" id="kcs_listproslider">
                  <ul class="item">
                    <li class="center-custom">SP1</li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" id="checkAll1" onclick="checkall(1)">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[0][kcs][properties][]" class="chk1">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[1][kcs][properties][]" class="chk1">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[2][kcs][properties][]" class="chk1">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[3][kcs][properties][]" class="chk1">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[4][kcs][properties][]" class="chk1">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[5][kcs][properties][]" class="chk1">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[6][kcs][properties][]" class="chk1">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <div class="custom-dropdown">
                        <select class="form-control" name="check[0][kcs][quality]">
                          <option value="0">0%</option>
                          <option value="1">10%</option>
                          <option value="2">20%</option>
                          <option value="3">30%</option>
                          <option value="4">40%</option>
                          <option value="5">50%</option>
                          <option value="6">60%</option>
                          <option value="7">70%</option>
                          <option value="8">80%</option>
                          <option value="9">90%</option>
                          <option value="10">100%</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                  <ul class="item">
                    <li class="center-custom">SP2</li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" id="checkAll2" onclick="checkall(2)">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[0][kcs][properties][]" class="chk2">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[1][kcs][properties][]" class="chk2">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[2][kcs][properties][]" class="chk2">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[3][kcs][properties][]" class="chk2">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[4][kcs][properties][]" class="chk2">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[5][kcs][properties][]" class="chk2">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[6][kcs][properties][]" class="chk2">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <div class="custom-dropdown">
                        <select class="form-control" name="check[0][kcs][quality]">
                          <option value="0">0%</option>
                          <option value="1">10%</option>
                          <option value="2">20%</option>
                          <option value="3">30%</option>
                          <option value="4">40%</option>
                          <option value="5">50%</option>
                          <option value="6">60%</option>
                          <option value="7">70%</option>
                          <option value="8">80%</option>
                          <option value="9">90%</option>
                          <option value="10">100%</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                  <ul class="item">
                    <li class="center-custom">SP3</li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" id="checkAll3" onclick="checkall(3)">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[0][kcs][properties][]" class="chk3">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[1][kcs][properties][]" class="chk3">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[2][kcs][properties][]" class="chk3">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[3][kcs][properties][]" class="chk3">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[4][kcs][properties][]" class="chk3">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[5][kcs][properties][]" class="chk3">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[6][kcs][properties][]" class="chk3">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <div class="custom-dropdown">
                        <select class="form-control" name="check[0][kcs][quality]">
                          <option value="0">0%</option>
                          <option value="1">10%</option>
                          <option value="2">20%</option>
                          <option value="3">30%</option>
                          <option value="4">40%</option>
                          <option value="5">50%</option>
                          <option value="6">60%</option>
                          <option value="7">70%</option>
                          <option value="8">80%</option>
                          <option value="9">90%</option>
                          <option value="10">100%</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                  <ul class="item">
                    <li class="center-custom">SP4</li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" id="checkAll4" onclick="checkall(4)">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[0][kcs][properties][]" class="chk4">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[1][kcs][properties][]" class="chk4">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[2][kcs][properties][]" class="chk4">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[3][kcs][properties][]" class="chk4">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[4][kcs][properties][]" class="chk4">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[5][kcs][properties][]" class="chk4">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li class="center-custom">
                      <label class="checkbox-custom check-kcs">
                        <input value="" type="checkbox" name="check[6][kcs][properties][]" class="chk4">
                        <span class="checkmark"></span>
                      </label>
                    </li>
                    <li>
                      <div class="custom-dropdown">
                        <select class="form-control" name="check[0][kcs][quality]">
                          <option value="0">0%</option>
                          <option value="1">10%</option>
                          <option value="2">20%</option>
                          <option value="3">30%</option>
                          <option value="4">40%</option>
                          <option value="5">50%</option>
                          <option value="6">60%</option>
                          <option value="7">70%</option>
                          <option value="8">80%</option>
                          <option value="9">90%</option>
                          <option value="10">100%</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
            <div class="item">
              <ul class="title-check">
                <li class="bold">Ghi chú</li>
              </ul>
              <div class="box-cmt">
                <textarea id="comment" rows="3" class="form-control input-cmt"></textarea>
                <div class="upload-img">
                  <div class="input-group up-img">
                      <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
                      <label class="button bg-green custom-upload">
                          <input type="file" id="my-input" class="form-control" name="image" accept="image/*">Upload
                      </label>
                      <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
                  </div>
                </div>
                <div class="action-button">
                  <button type="button" class="button bg-green">Tạo phiếu</button>
                  <button type="button" data-dismiss="modal" class="button bg-red">Hủy</button>
                </div>
              </div>
              
            </div>
          </div>
        </div>  
      </div>
    </div>
  </div>
</div>
<?php include('include/goodsreceipt/note.php'); ?>
<?php include('include/goodsreceipt/tachlo.php'); ?>
<script>
  function checkall(id){
    var i;
    var checkBox = document.getElementById("checkAll"+id);
    var childcheck = document.getElementsByClassName("chk"+id);
    var childchecklen = childcheck.length;
    if (checkBox.checked == true){
        for (i = 0; i < childchecklen; i += 1) {
            childcheck[i].checked = true;
        }
    }
    else{
        for (i = 0; i < childchecklen; i += 1) {
            childcheck[i].checked = false;
        }
    }
  }
	jQuery(function(){
      var formatDate = {
          format: 'DD/MM/YYYY',
          daysOfWeek: [
              "CN",
              "T2",
              "T3",
              "T4",
              "T5",
              "T6",
              "T7"
          ],
          monthNames: [
              "Tháng 1",
              "Tháng 2",
              "Tháng 3",
              "Tháng 4",
              "Tháng 5",
              "Tháng 6",
              "Tháng 7",
              "Tháng 8",
              "Tháng 9",
              "Tháng 10",
              "Tháng 11",
              "Tháng 12"
          ],
      }
      jQuery('#cbo_username,#id_supplier').select2();
      if(window.innerWidth > 576) {
        var dropDate = 'down';
      }
      else{
        var dropDate = 'up';
      }
      jQuery('.date-receipt').daterangepicker({
        singleDatePicker: true,
        opens: 'left',
        drops: dropDate,
        autoApply: true,
        locale: formatDate 
      })
      jQuery('.listpro-slider').slick({
        autoplay: false,
        dots: false,
        infinite: false,
        centerMode: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        variableWidth: false,
        autoplaySpeed: 2000,
        nextArrow: '<span class="slick-next"><i class="ti-angle-right" aria-hidden="true"></i></span>',
        prevArrow: '<span class="slick-prev"><i class="ti-angle-left" aria-hidden="true"></i></span>',
        responsive: [
        {
            breakpoint: 1024,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false
          }
        },
        {
            breakpoint: 600,
            settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
      });
      jQuery(document).on('change', ':file', function() {
        var input = jQuery(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
      });
      jQuery(':file').on('fileselect', function(event, numFiles, label) {
        var input = jQuery(this).parents('.up-img').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        if( input.length ) {
            input.val(log);
            if(input.val(log)){
              jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
              jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
            }
        } else {
            if(log) ;
        }
      });
      jQuery('.delete-img').on('click', function(e){
        jQuery(this).parent().find('input[type=file]').val('');
        jQuery(this).parent().find('input[type=text]').val('');
        jQuery(this).parent().find('.custom-upload').css('display','flex');
        jQuery(this).parent().parent().next().css('display','none');
        jQuery(this).css('display','none');
      });

      jQuery('#createreceipt-modal .tab-custom .item a').click(function(){
        var data = jQuery(this).data('id');
        jQuery('#createreceipt-modal .tab-content').not('#' + data).removeClass('show-inline');
        jQuery(this).parent().addClass('active');
        jQuery('#createreceipt-modal .tab-custom .item a').not(this).parent().removeClass('active');
        jQuery('#createreceipt-modal #'+data).addClass('show-inline');
        jQuery('.listpro-slider').slick("refresh");
      });
    });
</script>