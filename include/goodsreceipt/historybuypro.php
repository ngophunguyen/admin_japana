<style>
  .info-history{
    display: inline-block;
    width: 100%;
  }
  .info-history .item{
    width: 30%;
    float: left;
  }
  .info-history .item:last-child{
    width: 70%;
    float: left;
  }
  .info-history .item label{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 5px;
    font-weight: 500;
  }
  .box-payback-table{
    display: inline-block;
    float: left;
    width: 100%;
  }
  @media (max-width: 575.98px) {
    .info-history .item, .info-history .item:last-child{
      width: 100%;
    }
    .table-custom tr td:first-child{
      display: none;
    }
    #historybuypro-modal .table-custom tr td:last-child{
      justify-content: initial;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {  
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal super-large-modal" id="historybuypro-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Lịch sử mua hàng
          <a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#editpro-modal" title="Thoát" class="close-modal white-custom">
          	<span class="ti-arrow-left" aria-hidden="true"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="info-history">
          <div class="item">
            <label>Mã sản phẩm:</label>
            <span>011GA112</span>
          </div>
          <div class="item">
            <label>Tên sản phẩm:</label>
            <span>Bàn chải đánh răng 360 độ Higuchi Nhật Bản (Cho trẻ từ 3 tuổi)</span>
          </div>
        </div>
      	<div class="box-payback-table">
          <div class="box-table">
            <table class="table table-custom table-addpro table-responsive">
              <thead>
                <tr>
                  <th class="black-custom bold center-custom">STT</th>
                  <th class="black-custom bold center-custom">Mã phiếu nhập</th>
                  <th class="black-custom bold width-auto">Ngày nhập</th>
                  <th class="black-custom bold">Nhà cung cấp</th>
                  <th class="black-custom bold center-custom">Số lượng</th>
                  <th class="black-custom bold right-custom">Giá mua</th>
                  <th class="black-custom bold right-custom">Giá bán</th>
                  <th class="black-custom bold center-custom">Chiết khấu</th>
                  <th class="black-custom bold right-custom">Tổng mua</th>
                  <th class="black-custom bold center-custom">Người mua hàng</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-title="STT" class="center-custom">1</td>
                  <td data-title="Mã phiếu nhập" class="center-custom">PN001</td>
                  <td data-title="Ngày nhập" class="width-auto">30-05-2019</td>
                  <td data-title="Nhà cung cấp">SBS (Anh Phi)</td>
                  <td data-title="Số lượng" class="center-custom">2</td>
                  <td data-title="Giá mua" class="right-custom">2.000 đ</td>
                  <td data-title="Giá bán" class="right-custom">4.000 đ</td>
                  <td data-title="Chiết khấu" class="center-custom">99.6 %</td>
                  <td data-title="Tổng mua" class="right-custom">4.000 đ</td>
                  <td data-title="Người mua hàng" class="center-custom">Hương mua hàng</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
	jQuery(function(){
		
	})
</script>