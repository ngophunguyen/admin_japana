<style>
  .info-tachlo{
    display: inline-block;
    width: 100%;
  }
  .info-tachlo .item{
    width: 30%;
    float: left;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    height: 38px;
    line-height: 1.5;
  }
  .info-tachlo .item input{
    width: 20%;
    margin-right: 10px;
  }
  .info-tachlo .item:last-child{
    width: 70%;
    float: left;
  }
  .info-tachlo .item label{
    font-size: 14px;
    line-height: 1.5;
    font-weight: 500;
    margin-right: 5px;
  }
  .box-tachlo-table{
    display: inline-block;
    float: left;
    width: 100%;
  }
  .box-tachlo-table tr td input{
    width: auto;
  }
  .box-tachlo-table table tr td .qty{
    width: 100px;
    max-width: 100px;
    min-width: 100px;
    white-space: normal;
  }
  #tachlo-modal .box-time input{
    width: inherit;
  }
  @media (max-width: 575.98px) {
    .table-custom tr td:first-child{
      display: none;
    }
    .info-tachlo .item,.info-tachlo .item:last-child{
      width: 100%;
    }
    .info-tachlo .item{
      display:inline-block;
      height: auto;
      margin-bottom: 5px;
    }
    .info-tachlo .item input{
      width: 100%;
      margin: 10px 0 15px;
    }
    #tachlo-modal .table-custom > tbody > tr > td:last-child{
      justify-content: initial;
    }
    #tachlo-modal .modal-body{
      padding-bottom: 0;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {  
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal super-large-modal" id="tachlo-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Tách lô sản phẩm
          <a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#createreceipt-modal" title="Thoát" class="close-modal white-custom">
          	<span class="ti-arrow-left" aria-hidden="true"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="info-tachlo">
          <div class="item">
            <label>Mã sản phẩm:</label>
            <span>011GA112</span>
          </div>
          <div class="item">
            <label>Tên sản phẩm:</label>
            <span>Bàn chải đánh răng 360 độ Higuchi Nhật Bản (Cho trẻ từ 3 tuổi)</span>
          </div>
        </div>
        <div class="info-tachlo">
          <div class="item">
            <label>Số lượng:</label>
            <span>3</span>
          </div>
          <div class="item">
            <label>Số lô cần tách:</label>
            <input type="text" value="" id="numberTachlo" name="count" placeholder="Nhập số lô" class="form-control" data-type="currency">
            <button type="button" class="button bg-black">Tách lô</button>
          </div>
        </div>
      	<div class="box-tachlo-table">
          <div class="box-table">
            <table class="table table-custom table-addpro table-responsive">
              <thead>
                <tr>
                  <th class="black-custom bold center-custom">STT</th>
                  <th class="black-custom bold">SKU</th>
                  <th class="black-custom bold">Tên sản phẩm</th>
                  <th class="black-custom bold center-custom">Quy cách</th>
                  <th class="black-custom bold right-custom">Giá mua</th>
                  <th class="black-custom bold right-custom">Giá bán</th>
                  <th class="black-custom bold center-custom">Lợi nhuận</th>
                  <th class="black-custom bold center-custom">Loại</th>
                  <th class="black-custom bold">Hạn sử dụng</th>
                  <th class="black-custom bold">Số lượng</th>
                  <th class="black-custom bold right-custom">Thành tiền</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-title="STT" class="center-custom">1</td>
                  <td data-title="SKU">001EAE57</td>
                  <td data-title="Tên sản phẩm">Viên uống trắng da, trị nám Transino White C Clear 120 viên</td>
                  <td data-title="Quy cách">Hộp</td>
                  <td data-title="Giá mua" class="right-custom">650,000 đ</td>
                  <td data-title="Giá bán" class="right-custom">490,000 đ</td>
                  <td data-title="Lợi nhuận" class="center-custom">24.6 %</td>
                  <td data-title="Loại" class="center-custom">Tập trung</td>
                  <td data-title="Hạn sử dụng">
                    <div class="box-time">
                      <input autocomplete="off" onkeypress="return false;" type="text" name="date_expire[]" class="form-control date-receipt" placeholder="Chọn ngày...">
                      <i class="fa fa-calendar icon-time"></i>
                    </div>
                  </td>
                  <td data-title="Số lượng">
                    <input type="text" name="qty[]" value="" placeholder="Nhập số lượng..." onblur="counttotaltachlo(0)" class="form-control qty qty_0">
                  </td>
                  <td data-title="Thành tiền" class="right-custom">1,140,000 đ</td>
                </tr>
                <tr>
                  <td data-title="STT" class="center-custom">1</td>
                  <td data-title="SKU">001EAE57</td>
                  <td data-title="Tên sản phẩm">Viên uống trắng da, trị nám Transino White C Clear 120 viên</td>
                  <td data-title="Quy cách">Hộp</td>
                  <td data-title="Giá mua" class="right-custom">650,000 đ</td>
                  <td data-title="Giá bán" class="right-custom">490,000 đ</td>
                  <td data-title="Lợi nhuận" class="center-custom">24.6 %</td>
                  <td data-title="Loại" class="center-custom">Tập trung</td>
                  <td data-title="Hạn sử dụng">
                    <div class="box-time">
                      <input autocomplete="off" onkeypress="return false;" type="text" name="date_expire[]" class="form-control date-receipt" placeholder="Chọn ngày...">
                      <i class="fa fa-calendar icon-time"></i>
                    </div>
                  </td>
                  <td data-title="Số lượng">
                    <input type="text" name="qty[]" value="" placeholder="Nhập số lượng..." onblur="counttotaltachlo(0)" class="form-control qty qty_0">
                  </td>
                  <td data-title="Thành tiền" class="right-custom">1,140,000 đ</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" data-toggle="modal" data-target="#createreceipt-modal" class="button bg-red">Hủy</button>
          <button type="button" class="button bg-green">Xác nhận</button>
      </div>
    </div>
  </div>
</div>
<script>
	jQuery(function(){
		
	})
</script>