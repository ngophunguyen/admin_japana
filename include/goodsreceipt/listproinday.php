<style>
	.info-ncc{
    display: inline-block;
    width: 100%;
  }
  .info-ncc .item{
    width: 50%;
    float: left;
  }
  .info-ncc .item label, .total-price label{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 5px;
    font-weight: 500;
  }
  .total-price span{
    font-size: 16px;
  }
  .box-payback-table{
      display: inline-block;
      float: left;
      width: 100%;
  }
  .box-payback-table .box-table tr td:last-child{
    -webkit-box-pack: center;
        -ms-flex-pack: center;
            justify-content: center;
  }
  #listproinday-modal .item{
    display: inline-block;
    width: 100%;
  }
  @media (max-width: 575.98px) {
    .modal-header .dropdown-collapse{
      -webkit-box-pack: justify;
          -ms-flex-pack: justify;
              justify-content: space-between;
    }
    .info-ncc .item{
      width: 100%;
    }
    .table-custom tr td:first-child{
      display: none;
    }
    #listproinday-modal .table-custom tr td:last-child{
      justify-content: initial;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {
  }
  @media (min-width: 768px) and (max-width: 991.98px) { 
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {
  }
  @media (min-width: 1200px) {
  }
</style>
<div class="modal large-modal" id="listproinday-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Hàng về ngày 28-05-2019
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="item">
          <div class="info-ncc">
            <div class="item">
              <label>Mã nhà cung cấp:</label>
              <span>NCC_001-1</span>
            </div>
            <div class="item">
              <label>Tên nhà cung cấp:</label>
              <span>SBS (Anh Phi)</span>
            </div>
          </div>
          <div class="box-payback-table">
            <div class="box-table">
              <table class="table table-custom table-addpro table-responsive">
                <thead>
                  <tr>
                    <th class="black-custom bold center-custom">STT</th>
                    <th class="black-custom bold">SKU</th>
                    <th class="black-custom bold">Tên sản phẩm</th>
                    <th class="black-custom bold center-custom">Quy cách</th>
                    <th class="black-custom bold right-custom">Giá bán</th>
                    <th class="black-custom bold right-custom">Giá mua</th>
                    <th class="black-custom bold center-custom">Chiết khấu</th>
                    <th class="black-custom bold center-custom">Loại</th>
                    <th class="black-custom bold center-custom">Số lượng</th>
                    <th class="black-custom bold right-custom">Thành tiền</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td data-title="STT" class="center-custom">1</td>
                    <td data-title="SKU">087AA9</td>
                    <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
                    <td data-title="Quy cách" class="center-custom">Hộp</td>
                    <td data-title="Giá bán" class="right-custom">420.000 đ</td>
                    <td data-title="Giá mua" class="right-custom">270.000 đ</td>
                    <td data-title="Chiết khấu" class="center-custom">35.7%</td>
                    <td data-title="Loại" class="center-custom">Còn lại</td>
                    <td data-title="Số lượng" class="center-custom">1</td>
                    <td data-title="Thành tiền" class="right-custom">270.000 đ</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="total-price custom-fright">
            <label>Tổng cộng:</label> <span class="red-custom">270.000 đ</span>
          </div>
        </div>
        <div class="item">
          <div class="info-ncc">
            <div class="item">
              <label>Mã nhà cung cấp:</label>
              <span>NCC_001-17</span>
            </div>
            <div class="item">
              <label>Tên nhà cung cấp:</label>
              <span>Hàng nhật HaLi (Chị Diệu Linh)</span>
            </div>
          </div>
          <div class="box-payback-table">
            <div class="box-table">
              <table class="table table-custom table-addpro table-responsive">
                <thead>
                  <tr>
                    <th class="black-custom bold center-custom">STT</th>
                    <th class="black-custom bold">SKU</th>
                    <th class="black-custom bold">Tên sản phẩm</th>
                    <th class="black-custom bold center-custom">Quy cách</th>
                    <th class="black-custom bold right-custom">Giá bán</th>
                    <th class="black-custom bold right-custom">Giá mua</th>
                    <th class="black-custom bold center-custom">Chiết khấu</th>
                    <th class="black-custom bold center-custom">Loại</th>
                    <th class="black-custom bold center-custom">Số lượng</th>
                    <th class="black-custom bold right-custom">Thành tiền</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td data-title="STT" class="center-custom">1</td>
                    <td data-title="SKU">087AA9</td>
                    <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
                    <td data-title="Quy cách" class="center-custom">Hộp</td>
                    <td data-title="Giá bán" class="right-custom">420.000 đ</td>
                    <td data-title="Giá mua" class="right-custom">270.000 đ</td>
                    <td data-title="Chiết khấu" class="center-custom">35.7%</td>
                    <td data-title="Loại" class="center-custom">Còn lại</td>
                    <td data-title="Số lượng" class="center-custom">1</td>
                    <td data-title="Thành tiền" class="right-custom">270.000 đ</td>
                  </tr>
                  <tr>
                    <td data-title="STT" class="center-custom">2</td>
                    <td data-title="SKU">001FAG259</td>
                    <td data-title="Tên sản phẩm">Gel vuốt tóc Gatsby 80g</td>
                    <td data-title="Quy cách" class="center-custom">Hộp</td>
                    <td data-title="Giá bán" class="right-custom">420.000 đ</td>
                    <td data-title="Giá mua" class="right-custom">270.000 đ</td>
                    <td data-title="Chiết khấu" class="center-custom">35.7%</td>
                    <td data-title="Loại" class="center-custom">Còn lại</td>
                    <td data-title="Số lượng" class="center-custom">1</td>
                    <td data-title="Thành tiền" class="right-custom">270.000 đ</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="total-price custom-fright">
            <label>Tổng cộng:</label> <span class="red-custom">540.000 đ</span>
          </div>
        </div>
        <div class="item">
          <div class="info-ncc">
            <div class="item">
              <label>Mã nhà cung cấp:</label>
              <span>NCC_001-1</span>
            </div>
            <div class="item">
              <label>Tên nhà cung cấp:</label>
              <span>SBS (Anh Phi)</span>
            </div>
          </div>
          <div class="box-payback-table">
            <div class="box-table">
              <table class="table table-custom table-addpro table-responsive">
                <thead>
                  <tr>
                    <th class="black-custom bold center-custom">STT</th>
                    <th class="black-custom bold">SKU</th>
                    <th class="black-custom bold">Tên sản phẩm</th>
                    <th class="black-custom bold center-custom">Quy cách</th>
                    <th class="black-custom bold right-custom">Giá bán</th>
                    <th class="black-custom bold right-custom">Giá mua</th>
                    <th class="black-custom bold center-custom">Chiết khấu</th>
                    <th class="black-custom bold center-custom">Loại</th>
                    <th class="black-custom bold center-custom">Số lượng</th>
                    <th class="black-custom bold right-custom">Thành tiền</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td data-title="STT" class="center-custom">1</td>
                    <td data-title="SKU">087AA9</td>
                    <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
                    <td data-title="Quy cách" class="center-custom">Hộp</td>
                    <td data-title="Giá bán" class="right-custom">420.000 đ</td>
                    <td data-title="Giá mua" class="right-custom">270.000 đ</td>
                    <td data-title="Chiết khấu" class="center-custom">35.7%</td>
                    <td data-title="Loại" class="center-custom">Còn lại</td>
                    <td data-title="Số lượng" class="center-custom">1</td>
                    <td data-title="Thành tiền" class="right-custom">270.000 đ</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="total-price custom-fright">
            <label>Tổng cộng:</label> <span class="red-custom">270.000 đ</span>
          </div>
        </div>
        <div class="item">
          <div class="info-ncc">
            <div class="item">
              <label>Mã nhà cung cấp:</label>
              <span>NCC_001-17</span>
            </div>
            <div class="item">
              <label>Tên nhà cung cấp:</label>
              <span>Hàng nhật HaLi (Chị Diệu Linh)</span>
            </div>
          </div>
          <div class="box-payback-table">
            <div class="box-table">
              <table class="table table-custom table-addpro table-responsive">
                <thead>
                  <tr>
                    <th class="black-custom bold center-custom">STT</th>
                    <th class="black-custom bold">SKU</th>
                    <th class="black-custom bold">Tên sản phẩm</th>
                    <th class="black-custom bold center-custom">Quy cách</th>
                    <th class="black-custom bold right-custom">Giá bán</th>
                    <th class="black-custom bold right-custom">Giá mua</th>
                    <th class="black-custom bold center-custom">Chiết khấu</th>
                    <th class="black-custom bold center-custom">Loại</th>
                    <th class="black-custom bold center-custom">Số lượng</th>
                    <th class="black-custom bold right-custom">Thành tiền</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td data-title="STT" class="center-custom">1</td>
                    <td data-title="SKU">087AA9</td>
                    <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
                    <td data-title="Quy cách" class="center-custom">Hộp</td>
                    <td data-title="Giá bán" class="right-custom">420.000 đ</td>
                    <td data-title="Giá mua" class="right-custom">270.000 đ</td>
                    <td data-title="Chiết khấu" class="center-custom">35.7%</td>
                    <td data-title="Loại" class="center-custom">Còn lại</td>
                    <td data-title="Số lượng" class="center-custom">1</td>
                    <td data-title="Thành tiền" class="right-custom">270.000 đ</td>
                  </tr>
                  <tr>
                    <td data-title="STT" class="center-custom">2</td>
                    <td data-title="SKU">001FAG259</td>
                    <td data-title="Tên sản phẩm">Gel vuốt tóc Gatsby 80g</td>
                    <td data-title="Quy cách" class="center-custom">Hộp</td>
                    <td data-title="Giá bán" class="right-custom">420.000 đ</td>
                    <td data-title="Giá mua" class="right-custom">270.000 đ</td>
                    <td data-title="Chiết khấu" class="center-custom">35.7%</td>
                    <td data-title="Loại" class="center-custom">Còn lại</td>
                    <td data-title="Số lượng" class="center-custom">1</td>
                    <td data-title="Thành tiền" class="right-custom">270.000 đ</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="total-price custom-fright">
            <label>Tổng cộng:</label> <span class="red-custom">540.000 đ</span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="button bg-red">Thoát</button>
        <button type="button" class="button bg-green" onclick="window.location.href = 'include/goodsreceipt/print.php';">In file</button>
      </div>
    </div>
  </div>
</div>
<script>
	jQuery(function(){

	})
</script>