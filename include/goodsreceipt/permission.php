<style>
  #huydon-modal .modal-body button{
    margin-top: 15px;
    float: right;
  }
  #xet_duyet_quan_ly{
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
  }
  #xet_duyet_quan_ly button{
    margin-left: 15px;
  }
  @media (max-width: 575.98px) {
    
  }
  @media (min-width: 576px) and (max-width: 767.98px) {

  }
  @media (min-width: 768px) and (max-width: 991.98px) {

  }
  @media (min-width: 992px) and (max-width: 1199.98px) {
    
  }
  @media (min-width: 1200px) {
    
  }
</style>
<div class="modal medium-modal" id="permission-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Quản lý xét duyệt
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <form action="http://test.japana.vn/admincp/goodsreceipt/activemanager" method="post" id="xet_duyet_quan_ly">
          <input type="text" onkeyup="initShowBtn.call(this);" name="note" id="log_note_admin_confirm" class="form-control cancel-order">
          <button type="button" onclick="managerActive()" class="button bg-gray" disabled="">Xác nhận</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  function initShowBtn(){
    if(jQuery(this).val() != '') {
      jQuery(this).next().prop('disabled', false);
      jQuery(this).next().removeClass('bg-gray');
      jQuery(this).next().addClass('bg-green');
    }
    else{
      jQuery(this).next().prop('disabled', true);
      jQuery(this).next().addClass('bg-gray');
      jQuery(this).next().removeClass('bg-green');
    }
  }
</script>