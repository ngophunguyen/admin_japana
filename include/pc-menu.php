<style>
	@media (max-width: 575.98px) {
		.title-menu,.menu-primary li.item>a>span>i{
			display: block;
		}
		.entry-header{
			z-index: 2
		}
		.bg-nav {
		    background: rgba(0,0,0,.3);
		    width: 100%;
		    height: 100%;
		    position: absolute;
		    right: 0;
		    z-index: -1;
		    display: block;
		}
		.closed-mb{
			background: transparent;
			color: rgba(255,255,255,0.8);
			border: 0;
			padding: 10px 0;
			width: 100%;
			text-align: right;
	    	padding-right: 3.5vw;
		}
		.closed-mb span{
			font-size: 8.5vw;
		}
		.back-pa{
			background: transparent;
			border: 0;
			color: rgba(255,255,255,0.2);
			position: absolute;
			left: 0;
			top: 50%;
			-webkit-transform: translate(0,-50%);
			    -ms-transform: translate(0,-50%);
			        transform: translate(0,-50%);
			display: none;
		}
		.nav-primary{
			position:fixed;
		    top:0;
		    width:100%;
		    height:100%;
		    z-index:999;
		    display: none;
		}
		.menu-primary{
			height:100%;
		    padding:0;
		    overflow-y:scroll;
		    overflow-x:hidden;
		    position:absolute;
		    width:85%;
		    background: #222D32;
		}
		.menu-primary .item, .menu-primary .title-menu, .dl-back, .dl-submenu li{
			padding: 2.3vh 1vh;
		    border-bottom: .7px solid rgba(255,255,255,0.2);
		    position: relative;
		}
		.menu-primary .item:last-child, .dl-submenu .item:last-child, .dl-subviewopen{
			border-bottom: 0;
		}
		.menu-primary .title-menu, .dl-back{
			text-align: center;
		}
		.dl-back{
			padding: 3.5vh!important;
		}
		.menu-primary .item a, 
		.menu-primary .title-menu a, 
		.dl-back a, .dl-back a>i,
		.menu-primary .item a span,
		.dl-submenu a>i{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}
		.menu-primary .item a, 
		.menu-primary .title-menu a, 
		.dl-back a,.dl-back a>i,
		.menu-primary .item a span,
		.dl-submenu a>i{
			-webkit-box-align: center;
	    	    -ms-flex-align: center;
	    	        align-items: center;
		}
		.menu-primary .item a, 
		.menu-primary .title-menu a, 
		.dl-back a,
		.dl-submenu a>i{
			-webkit-box-pack: center;
	    	    -ms-flex-pack: center;
	    	        justify-content: center;
		}
		.menu-primary .item a, .menu-primary .title-menu a, .dl-back a{
			color: rgba(255,255,255,0.8);
			font-weight: 300;
			font-size: 4vw;
			text-align: center;
		}
		.menu-primary .title-menu a, .dl-back a{
			color: rgba(255,255,255,0.3);
		}
		.menu-primary .item a>i, .menu-primary .item a>span i{
			width: 8.5vw;
			margin-right: 10px;
			margin-bottom: 0;
	    	font-size: initial;
		}
		.dl-back a>i{
			font-size: 4.5vw;
		}
		.menu-primary .item a>span i{
			margin-right: 0;
		}
		.menu-primary .item a span{
		    -webkit-box-pack: justify;
		        -ms-flex-pack: justify;
		            justify-content: space-between;
		    width: 100%;
		}
		.dl-subviewopen, .dl-subview{
			padding: 0!important;
		}
		.dl-submenu{
			display: none
		}
		.dl-submenu a>i{
			font-size: 3.5vw!important;
		}
		.menu-primary li.item:hover>a:before, .menu-primary li.item>a.active:before{
			content: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.nav-primary{
			width: 10%;
			float: left;
		}
		.entry-header .entry-title{
			padding: 15px;
			border-bottom: 1px solid #eee;
			text-align: center;
		}
		.entry-header {
		    left: 10%;
		    display: block;
		    height: auto;
		    padding: 0;
		    z-index: 99;
		}
		.entry-header ul li{
			margin-left: 0;
		}
		.entry-header ul li:last-of-type{
			float: right;
		}
		.entry-header ul{
			display: inline-block;
		    width: 100%;
		    padding: 5px 0;
		}
		.content-sidebar-wrap{
			width: 90%;
		}
		.customer-dropdown{
			position: static;
		}
		.menu-primary li.item{
			position: relative;
			padding: 7px 0;
		}
		.menu-primary li.item:first-child{
			padding-top: 15px;
		}
		.menu-primary li.item>a, 
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a{
			color: #fff;
			font-size: 12px;
			display: inline-block;
			width: 100%;
		}
		.menu-primary li.item>a{
			position: relative;
			text-align: center;
		}
		.menu-primary li.item>a>span>i{
			display: none;
		}
		.menu-primary li.item:hover>a:before, .menu-primary li.item>a.active:before{
			content: '';
			width: 3px;
			height: 100%;
			background: #fff;
			position: absolute;
			left: 0;
			top: 0;
			cursor: pointer;
		}
		.menu-primary li.item>a>i{
			font-size: 23px;
			margin-bottom: 12px;
			display: inline-block;
			width: 100%;
		}
		.menu-primary li.item ul.dl-submenu{
			position: absolute;
			left: 100%;
			top: 0;
			width: 240px;
			background: #222D32;
		}
		.menu-primary li.item ul.dl-submenu>li{
			border-bottom: 1px solid #374850;
		}

		.menu-primary li.item ul.dl-submenu>li>ul{
			background: #374850;
			position: static;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a,
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			display: -webkit-box;
		    display: -ms-flexbox;
		    display: flex;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a,
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			-webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a{
		    padding: 10px 15px;
		    width: 100%;
		}
		.menu-primary li.item ul.dl-submenu>li>a i,
		.menu-primary li.item ul.dl-submenu>li>ul>li a i{
		    font-size: 11px;
		    width: 10%;
		    float: left;
		}

		.menu-primary li.item ul.dl-submenu>li>ul>li a i{
			font-size: 15px;
		}

		.menu-primary li.item ul.dl-submenu>li a span i{
			width: auto;
		}

		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			width: 90%;
		    float: left;
		    -webkit-box-pack: justify;
		        -ms-flex-pack: justify;
		            justify-content: space-between;
		}
		.has-sub{
			display: none;
			z-index: 999;
		}
		.bg-nav,.title-menu{
			display: none;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.nav-primary{
			width: 10%;
			float: left;
		}
		.entry-header .entry-title{
			padding: 15px;
			border-bottom: 1px solid #eee;
			text-align: center;
		}
		.entry-header {
		    left: 10%;
		    display: block;
		    height: auto;
		    padding: 0;
		    z-index: 99;
		}
		.entry-header ul li{
			margin-left: 0;
		}
		.entry-header ul li:last-of-type{
			float: right;
		}
		.entry-header ul{
			display: inline-block;
		    width: 100%;
		    padding: 5px 0;
		}
		.content-sidebar-wrap{
			width: 90%;
		}
		.customer-dropdown{
			position: static;
		}
		.menu-primary li.item{
			position: relative;
			padding: 7px 0;
		}
		.menu-primary li.item:first-child{
			padding-top: 15px;
		}
		.menu-primary li.item>a, 
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a{
			color: #fff;
			font-size: 12px;
			display: inline-block;
			width: 100%;
		}
		.menu-primary li.item>a{
			position: relative;
			text-align: center;
		}
		.menu-primary li.item>a>span>i{
			display: none;
		}
		.menu-primary li.item:hover>a:before, .menu-primary li.item>a.active:before{
			content: '';
			width: 3px;
			height: 100%;
			background: #fff;
			position: absolute;
			left: 0;
			top: 0;
			cursor: pointer;
		}
		.menu-primary li.item>a>i{
			font-size: 23px;
			margin-bottom: 12px;
			display: inline-block;
			width: 100%;
		}
		.menu-primary li.item ul.dl-submenu{
			position: absolute;
			left: 100%;
			top: 0;
			width: 240px;
			background: #222D32;
		}
		.menu-primary li.item ul.dl-submenu>li{
			border-bottom: 1px solid #374850;
		}

		.menu-primary li.item ul.dl-submenu>li>ul{
			background: #374850;
			position: static;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a,
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			display: -webkit-box;
		    display: -ms-flexbox;
		    display: flex;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a,
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			-webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a{
		    padding: 10px 15px;
		    width: 100%;
		}
		.menu-primary li.item ul.dl-submenu>li>a i,
		.menu-primary li.item ul.dl-submenu>li>ul>li a i{
		    font-size: 11px;
		    width: 10%;
		    float: left;
		}

		.menu-primary li.item ul.dl-submenu>li>ul>li a i{
			font-size: 15px;
		}

		.menu-primary li.item ul.dl-submenu>li a span i{
			width: auto;
		}
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			width: 90%;
		    float: left;
		    -webkit-box-pack: justify;
		        -ms-flex-pack: justify;
		            justify-content: space-between;
		}
		.has-sub,.bg-nav,.title-menu{
			display: none;
		}
		.has-sub{
			z-index: 999;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.entry-header{
			left: 10%;
		}
		.content-sidebar-wrap{
			width: 90%;
		}
		.has-sub {
		    display: none;
		    z-index: 999;
		}
		.customer-dropdown{
			position: static;
		}
		.nav-primary{
			width: 10%;
			float: left;
		}
		.menu-primary li.item{
			position: relative;
			padding: 7px 0;
		}
		.menu-primary li.item:first-child{
			padding-top: 15px;
		}
		.menu-primary li.item>a, 
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a{
			color: #fff;
			font-size: 12px;
			display: inline-block;
			width: 100%;
		}
		.menu-primary li.item>a{
			position: relative;
			text-align: center;
		}
		.menu-primary li.item>a>span>i{
			display: none;
		}
		.menu-primary li.item:hover>a:before, .menu-primary li.item>a.active:before{
			content: '';
			width: 3px;
			height: 100%;
			background: #fff;
			position: absolute;
			left: 0;
			top: 0;
			cursor: pointer;
		}
		.menu-primary li.item>a>i{
			font-size: 23px;
			margin-bottom: 12px;
			display: inline-block;
			width: 100%;
		}
		.menu-primary li.item ul.dl-submenu{
			position: absolute;
			left: 100%;
			top: 0;
			width: 240px;
			background: #222D32;
		}
		.menu-primary li.item ul.dl-submenu>li{
			border-bottom: 1px solid #374850;
		}

		.menu-primary li.item ul.dl-submenu>li>ul{
			background: #374850;
			position: static;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a,
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			display: -webkit-box;
		    display: -ms-flexbox;
		    display: flex;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a,
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			-webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a{
		    padding: 10px 15px;
		    width: 100%;
		}
		.menu-primary li.item ul.dl-submenu>li>a i,
		.menu-primary li.item ul.dl-submenu>li>ul>li a i{
		    font-size: 11px;
		    width: 10%;
		    float: left;
		}

		.menu-primary li.item ul.dl-submenu>li>ul>li a i{
			font-size: 15px;
		}

		.menu-primary li.item ul.dl-submenu>li a span i{
			width: auto;
		}
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			width: 90%;
		    float: left;
		    -webkit-box-pack: justify;
		        -ms-flex-pack: justify;
		            justify-content: space-between;
		}
		.has-sub, 
		.bg-nav,
		.title-menu{
			display: none;
		}
		.has-sub{
			z-index: 999;
		}
	}
	@media (min-width: 1200px) {
		.nav-primary{
			width: 6%;
			height: 100%;
			float: left;
			z-index: 999;
		}
		.menu-primary li.item{
			position: relative;
			padding: 7px 0;
		}
		.menu-primary li.item:first-child{
			padding-top: 15px;
		}
		.menu-primary li.item>a, 
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a{
			color: #fff;
			font-size: 12px;
			display: inline-block;
			width: 100%;
		}
		.menu-primary li.item>a{
			position: relative;
			text-align: center;
		}
		.menu-primary li.item>a>span>i{
			display: none;
		}
		.menu-primary li.item:hover>a:before, .menu-primary li.item>a.active:before{
			content: '';
			width: 3px;
			height: 100%;
			background: #fff;
			position: absolute;
			left: 0;
			top: 0;
			cursor: pointer;
		}
		.menu-primary li.item>a>i{
			font-size: 23px;
			margin-bottom: 12px;
			display: inline-block;
			width: 100%;
		}
		.menu-primary li.item ul.dl-submenu{
			position: absolute;
			left: 100%;
			top: 0;
			width: 240px;
			background: #222D32;
		}
		.menu-primary li.item ul.dl-submenu>li{
			border-bottom: 1px solid #374850;
		}

		.menu-primary li.item ul.dl-submenu>li>ul{
			background: #374850;
			position: static;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a,
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			display: -webkit-box;
		    display: -ms-flexbox;
		    display: flex;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a,
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			-webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		}
		.menu-primary li.item ul.dl-submenu>li>a,
		.menu-primary li.item ul.dl-submenu>li>ul>li a{
		    padding: 10px 15px;
		    width: 100%;
		}
		.menu-primary li.item ul.dl-submenu>li>a i,
		.menu-primary li.item ul.dl-submenu>li>ul>li a i{
		    font-size: 11px;
		    width: 10%;
		    float: left;
		}
		.menu-primary li.item ul.dl-submenu>li>ul>li a i{
			font-size: 15px;
		}
		.menu-primary li.item ul.dl-submenu>li a span i{
			width: auto;
		}
		.menu-primary li.item ul.dl-submenu>li>a span,
		.menu-primary li.item ul.dl-submenu>li>ul>li a span{
			width: 90%;
		    float: left;
		    -webkit-box-pack: justify;
		        -ms-flex-pack: justify;
		            justify-content: space-between;
		}
		.has-sub, 
		.bg-nav,
		.title-menu{
			display: none;
		}
		.has-sub{
			z-index: 999;
		}
	}
</style>
<nav id="dl-menu" class="nav-primary bg-black">
	<div class="bg-nav"><button type="button" class="closed-mb"><span class="ti-close"></span></button></div>
	<ul class="menu genesis-nav-menu menu-primary dl-menu">
		<li class="title-menu">
			<a href="javascript:void(0);" title="Menu">
				<span>Menu</span>
			</a>
		</li>
		<li class="item">
			<a href="index.php" title="Dashboard">
				<i class="fa fa-database" aria-hidden="true"></i> <span>Dashboard</span>
			</a>
		</li>
		<li class="item">
			<a href="javascript:void(0);" title="Products">
				<i class="fa fa-cube" aria-hidden="true"></i> <span>Products <i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</a>
			<ul class="dl-submenu has-sub">
				<li>
					<a href="?action=category-product.php" title="Danh mục sản phẩm">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Danh mục sản phẩm</span>
					</a>
				</li>
				<li>
					<a href="?action=products.php" title="Danh sách sản phẩm">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Danh sách sản phẩm</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="item dl-menu">
			<a href="javascript:void(0);" title="Sales">
				<i class="fa fa-usd" aria-hidden="true"></i> <span>Sales <i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</a>
			<ul class="dl-submenu has-sub">
				<li>
					<a href="?action=order.php" title="Quản lý đơn hàng">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Quản lý đơn hàng</span>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" title="Quản lý nhập hàng">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Quản lý nhập hàng <i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</a>
					<ul class="dl-submenu has-sub">
						<li>
							<a href="?action=goodsreceipt.php" title="Đơn đặt hàng">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Đơn đặt hàng</span>
							</a>
						</li>
						<li>
							<a href="?action=refund.php" title="Danh sách đơn hàng trả">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Danh sách đơn hàng trả</span>
							</a>
						</li>
						<li>
							<a href="?action=listreceipt.php" title="Danh sách phiếu nhập">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Danh sách phiếu nhập</span>
							</a>
						</li>
						<li>
							<a href="?action=partner.php" title="Danh sách đối tác">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Danh sách đối tác</span>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0);" title="Quản lý kho">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Quản lý kho <i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</a>
					<ul class="dl-submenu has-sub">
						<li>
							<a href="?action=warehouses.php" title="Danh sách kho">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Danh sách kho</span>
							</a>
						</li>
						<li>
							<a href="?action=warehousemain.php" title="Danh sách sản phẩm kho">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Danh sách sản phẩm kho</span>
							</a>
						</li>
						<li>
							<a href="?action=listreceiptrefund.php" title="Quản lý trả hàng">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Quản lý trả hàng</span>
							</a>
						</li>
						<li>
							<a href="?action=warehousetemp.php" title="Danh sách kho tạm">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Danh sách kho tạm</span>
							</a>
						</li>
						<li>
							<a href="?action=warehousehistory.php" title="Lịch sử xuất/nhập kho">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Lịch sử xuất/nhập kho</span>
							</a>
						</li>
						<li>
							<a href="?action=checkwarehousemain.php" title="Kiểm kho">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Kiểm kho</span>
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="item">
			<a href="javascript:void(0);" title="Attributes">
				<i class="fa fa-puzzle-piece" aria-hidden="true"></i> <span>Attributes <i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</a>
			<ul class="dl-submenu has-sub">
				<li>
					<a href="javascript:void(0);" title="Mặc định">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Mặc định<i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</a>
					<ul class="dl-submenu has-sub">
						<li>
							<a href="?action=include/att/default/brand.php" title="Thương hiệu">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Thương hiệu</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/default/style.php" title="Quy cách">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Quy cách</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/default/city.php" title="Tỉnh/thành">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Tỉnh/thành</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/default/cityzone.php" title="Quận/huyện">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Quận/huyện</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/default/cityward.php" title="Phường/xã">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Phường/xã</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/default/country.php" title="Xuất xứ">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Xuất xứ</span>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="?action=include/att/create/set.php" title="Tự tạo">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Tự tạo</span>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" title="Thuộc tính đơn hàng">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Thuộc tính đơn hàng <i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</a>
					<ul class="dl-submenu has-sub">
						<li>
							<a href="?action=include/att/order/status.php" title="Trạng thái">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Trạng thái</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/order/statusarises.php" title="Trạng thái phát sinh">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Trạng thái phát sinh</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/order/othercosts.php" title="Chi phí khác">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Chi phí khác</span>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0);" title="Thuộc tính đơn vị vận chuyển">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Thuộc tính đơn vị vận chuyển <i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</a>
					<ul class="dl-submenu has-sub">
						<li>
							<a href="?action=include/att/shipper/delivery.php" title="Đơn vị vận chuyển">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Đơn vị vận chuyển</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/shipper/japanakey.php" title="Japana Key">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Japana Key</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/shipper/japanakeyrespon.php" title="Japana Key Respon">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Japana Key Respon</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/shipper/deliveryconvert.php" title="Chuyển đổi Key">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Chuyển đổi Key</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/shipper/deliveryconvertrespon.php" title="Chuyển đổi Key Respon">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Chuyển đổi Key Respon</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/shipper/deliveryjson.php" title="Json DVVC">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Json DVVC</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/shipper/databaseimport.php" title="Bảng dữ liệu Import/Export">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Bảng dữ liệu Import/Export</span>
							</a>
						</li>
						<li>
							<a href="?action=include/att/shipper/toolcity.php" title="Kiểm tra dữ liệu">
								<i class="fa fa-angle-right" aria-hidden="true"></i> <span>Kiểm tra dữ liệu</span>
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="item">
			<a href="javascript:void(0);" title="SEO">
				<i class="fa fa-line-chart" aria-hidden="true"></i> <span>SEO <i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</a>
			<ul class="dl-submenu has-sub">
				<li>
					<a href="?action=include/sitemap/sitemap.php" title="Sitemap">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Sitemap</span>
					</a>
				</li>
				<li>
					<a href="?action=include/sitemap/tags.php" title="Quản lý tags">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Quản lý tags</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="item">
			<a href="javascript:void(0);" title="Marketing">
				<i class="fa fa-bullhorn" aria-hidden="true"></i> <span>Marketing <i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</a>
			<ul class="dl-submenu has-sub">
				<li>
					<a href="?action=event.php" title="Danh sách khuyến mãi, sự kiện">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Danh sách khuyến mãi, sự kiện</span>
					</a>
				</li>
				<li>
					<a href="?action=include/promotion/group-products.php" title="Nhóm sản phẩm khuyến mãi">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Nhóm sản phẩm khuyến mãi</span>
					</a>
				</li>
				<li>
					<a href="?action=include/promotion/list-program.php" title="Các chương trình khuyến mãi">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Các chương trình khuyến mãi</span>
					</a>
				</li>
				<li>
					<a href="?action=include/promotion/list-promo.php" title="Danh sách mã khuyến mãi">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Danh sách mã khuyến mãi</span>
					</a>
				</li>
				<li>
					<a href="?action=email-register.php" title="Email đăng ký khuyến mãi">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Email đăng ký khuyến mãi</span>
					</a>
				</li>
				<li>
					<a href="?action=config-email.php" title="Config Email">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Config Email</span>
					</a>
				</li>
				<li>
					<a href="?action=banner.php" title="Banner chung">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Banner chung</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="item dl-menu">
			<a href="javascript:void(0);" title="News">
				<i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>News <i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</a>
			<ul class="dl-submenu has-sub">
				<li>
					<a href="?action=category-news.php" title="Danh mục tin tức">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Danh mục tin tức</span>
					</a>
				</li>
				<li>
					<a href="?action=news.php" title="Danh sách tin tức">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Danh sách tin tức</span>
					</a>
				</li>
				<li>
					<a href="?action=static.php" title="Quản lý tin tĩnh">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Danh sách tin tĩnh</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="item">
			<a href="javascript:void(0);" title="Pages/Block">
				<i class="fa fa-code" aria-hidden="true"></i> <span>Pages/Block <i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</a>
			<ul class="dl-submenu has-sub">
				<li>
					<a href="?action=pages.php" title="Pages">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Pages</span>
					</a>
				</li>
				<li>
					<a href="?action=block.php" title="Block">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Block</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="item">
			<a href="javascript:void(0);" title="Customners">
				<i class="fa fa-address-book" aria-hidden="true"></i> <span>Customners <i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</a>
			<ul class="dl-submenu has-sub">
				<li>
					<a href="?action=404.php" title="Danh sách khách hàng">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Danh sách khách hàng</span>
					</a>
				</li>
				<li>
					<a href="?action=level.php" title="Level">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Level</span>
					</a>
				</li>
				<li>
					<a href="?action=contact.php" title="Liên hệ">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Liên hệ</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="item">
			<a href="javascript:void(0);" title="Users">
				<i class="fa fa-users" aria-hidden="true"></i> <span>Users <i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</a>
			<ul class="dl-submenu has-sub">
				<li>
					<a href="?action=include/user/user-group.php" title="Nhóm user">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Nhóm user</span>
					</a>
				</li>
				<li>
					<a href="?action=include/user/list-user.php" title="Danh sách user">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Danh sách user</span>
					</a>
				</li>
				<li>
					<a href="?action=include/user/permission.php" title="Phân quyền nhóm user">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Phân quyền nhóm user</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="item">
			<a href="?action=keysearch.php" title="Keysearch">
				<i class="fa fa-key" aria-hidden="true"></i> <span>Keysearch</span>
			</a>
		</li>
		<li class="item">
			<a href="?action=404.php" title="Report">
				<i class="fa fa-file-text" aria-hidden="true"></i> <span>Report</span>
			</a>
		</li>
		<li class="item">
			<a href="javascript:void(0);" title="Settings">
				<i class="fa fa-cogs" aria-hidden="true"></i> <span>Settings <i class="fa fa-angle-right" aria-hidden="true"></i></span>
			</a>
			<ul class="dl-submenu has-sub">
				<li>
					<a href="javascript:void(0);" title="Menu">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Menu</span>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" title="Info website">
						<i class="fa fa-circle-thin" aria-hidden="true"></i> <span>Info website</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="item">
			<a href="?action=404.php" title="Document">
				<i class="fa fa-book" aria-hidden="true"></i> <span>Document</span>
			</a>
		</li>
	</ul>
</nav>

<script>
	jQuery(function() {
		if(window.innerWidth < 576) {
			jQuery( '#dl-menu' ).dlmenu({
			animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
			});
			jQuery( '#dl-menu' ).removeClass('bg-black');
			jQuery('.icon-nav').click(function(){
				jQuery('.nav-primary').toggle('slide', { direction: 'left' }, 300);
				jQuery('body').css('overflow','hidden');
			});
			jQuery('.closed-mb').click(function(){
				jQuery('.icon-nav').trigger('click', true);
				jQuery('body').css('overflow','auto');
			});
		}
		else{
			jQuery('.item>a').click(function(){
				jQuery("li.item>a").not(this).next('.has-sub').hide();
				jQuery("li.item>a").not(this).removeClass('active');
				jQuery(this).next('.dl-submenu').toggle();
				if(jQuery(this).next('.has-sub').css('display')=='block'){
					jQuery(this).addClass('active');
				}
				else{
					jQuery(this).removeClass('active');
				}
			});
			jQuery('.has-sub>li>a').click(function(){
				jQuery('.has-sub>li>a').not(this).next('.has-sub').hide();
				jQuery(this).next('.has-sub').toggle();
				if(jQuery(this).next('.has-sub').css('display')=='block'){
					jQuery(this).next('.has-sub').parent().find('span>i').addClass('fa-angle-down');
				}
				else{
					jQuery(this).next('.has-sub').parent().find('span>i').removeClass('fa-angle-down');
				}
			});
		}
	});
</script>