<style>
	.filter-pc{
		margin-top: 15px;
	}
	.filter-mobile{
		display: none;
	}
	.box-button-filter{
  		display: none;
  	}
  	.fixed-button{
  		position: fixed; 
  		bottom: 0;
  		right: 0;
	    width: calc(100% - 38px);
	    border-radius: 0;
	    z-index: 9999;
  	}
  	.button-filter{
	    font-size: 14px;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	    background: rgba(0,0,0,0.3);
	    border-radius: 10px 0 0 10px;
	    height: 38px;
	    width: 38px;
	    position: fixed;
	    right: 0;
	    top: 110px;
		z-index: 99
	}
	.button-filter.active{
		left: 0px;
	    border-radius: 0px;
	    right: auto;
	    z-index: 99;
	    top: 0px!important;
	    height: 40px;
	    background: #222D32!important;
	    color: #fff!important;
	}
	.button-filter:focus, .button-filter:hover{
		color: #fff;
	}
	.custom-collapse{
		display: none;
		width: 100%;
		padding: 0 15px;
	}
	.custom-collapse .item{
		display: inline-block;
		margin: 10px 2%;
		float: left;
	}
	.custom-collapse #advance_search{
		display: grid;
		grid-template-columns: repeat(3,1fr);
	}
	.custom-collapse .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.info-item{
		display: inline-block;
		width: 100%;
	}
	.info-item .item{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		float: left;
		margin: 20px 0 0;
		margin-right: 30px;
	}
	.info-item .item label{
		font-weight: 500;
		font-size: 14px;
		color: #000;
		margin-right: 5px;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    position: relative;
	}
	.table-custom{
		margin-bottom: 15px;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item button{
		float: left;
		margin-left: 15px;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	@media (max-width: 575.98px) {
		.box-button-filter {
		    display: block;
		}
		.info-item .item{
			margin: 15px 0 0;
			margin-right: 0;
			display: inline-block;
			line-height: 1.5;
		}
		.box-quick-search .item, .box-quick-search .item .search1{
			width: 100%;
			display: flex;
		}
	  	.dropdown-collapse{
	  		border-radius: 0;
	  		-webkit-box-pack: center;
	  		    -ms-flex-pack: center;
	  		        justify-content: center;
	  		display: none;
	  	}
	  	.dropdown-collapse i{
	  		display: none;
	  	}
	  	.filter-pc{
	  		display: block;
	  		position: fixed;
		    top: 0;
		    right: 0;
		    right: 0;
		    width: calc(100% - 38px);
		    height: 100%;
		    z-index: -1;
		    margin-top: 0!important;
	  	}
	  	.custom-collapse{
	  		height: 100%;
		    width: 100%;
		    padding: 0!important;
		    float: right;
		    overflow-x: hidden;
		    position: relative;
		    z-index: 999;
		    background: #fff;
		    border-left: 1px solid #eee;
	  	}
	  	.custom-collapse .item{
	  		width: 100%;
	  		margin: 10px 0 0!important;
	  	}
	  	.custom-collapse .item:last-child{
	  		margin-bottom: 10px;
	  	}
	  	.custom-collapse .item:last-child label{
	  		display: none;
	  	}
	  	.custom-collapse .item:last-child button{
	  		position: fixed;
	  		bottom: 0;
	  		right: 0;
	  		width: calc(100% - 38px);
	  		border-radius: 0;
	  	}
	  	.box-button-filter.active{
	  		background: rgba(0,0,0,.3);
		    width: 38px;
		    height: 100%;
		    position: fixed;
		    left: 0;
		    top: 0;
    		z-index: 999;
	  	}
	  	.custom-collapse #advance_search{
	  		display: inline-block;
	  		width: 100%;
	  		height: calc(100% - 38px);
		    background: #fff;
		    z-index: 999;
		    top: 0;
		    overflow-y: scroll;
		    overflow-x: hidden;
		    padding: 0 15px;
	  	}
	  	.custom-collapse #advance_search .button-submit{
	  		position: fixed;
	  		bottom: 0;
	  		right: 0;
	  		width: calc(100% - 38px);
	  		z-index: 999;
	  		border-radius: 0;
	  	}
	  	.custom-dropdown:after{
	  		width: 10%;
	  	}
	  	.custom-collapse .title{
	  		padding: 10px 12px;
	  		margin-bottom: 0;
	  		border-bottom: 1px solid #ccc;
	  		text-align: center;
	  	}
		.table-custom tr td:first-child{
			display: none;
		}
		.table-custom > tbody > tr > td{
			white-space: normal;
		}
		.table-custom > tbody > tr > td:last-child{
			justify-content: initial;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="warehousehistory-detail content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Chi tiết lịch sử xuất / nhập sản phẩm</h1>
			<ul>
				<li>
					<a href="?action=warehousehistory.php" class="link-custom black-custom" title="Trở về">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Trở về</label>
					</a>
				</li>
				<li>
					<a href="#" class="link-custom black-custom" title="Reset">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="info-item">
					<div class="item">
						<label>Mã sản phẩm:</label>
						<span>077GDD181</span>
					</div>
					<div class="item">
						<label>Tên sản phẩm:</label>
						<span>Khăn tay cotton cho bé ChuChuBaby 10 chiếc</span>
					</div>
				</div>
				<div class="box-button-filter">
					<a class="button-filter white-custom" href="javascript:void(0);">
						<i class="ti-search" aria-hidden="true"></i>
					</a>
				</div>
				<div class="filter-pc">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">Tìm kiếm nâng cao <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div id="filter-order" class="custom-collapse">
						<form id="advance_search" name="frm" method="post">
						    <div class="item">
						    	<label for="name">Mã kho</label>
						    	<input autocomplete="off" type="text" name="cusname" id="name" class="form-control" placeholder="Nhập mã kho sản phẩm...">
						    </div>
						    <div class="item">
						    	<label for="cbo_username">Nhà cung cấp</label>
						    	<select class="form-control" name="id_supplier" id="id_supplier">
						    		<option value="-1">Chọn nhà cung cấp</option>
								  	<option value="0">Công ty trách nhiệm hữu hạn Asahi-Nutifood</option>
								  	<option value="1">Giao nhận 365</option>
								  	<option value="2">SBS (Anh Phi)</option>
								</select>
						    </div>
						    <div class="item">
						    	<label for="skuname">SKU và tên sản phẩm</label>
						    	<input autocomplete="off" type="text" name="skuname" id="skuname" class="form-control" placeholder="SKU và tên sản phẩm">
						    </div>
						    <div class="item">
						    	<label for="cbo_username">Trạng thái</label>
						    	<div class="custom-dropdown">
						    		<select class="form-control" name="type">
							    		<option value="-1">Chọn trạng thái</option>
									  	<option value="0">Nhập</option>
									  	<option value="1">Xuất</option>
									</select>
						    	</div>
						    </div>
						    <div class="item">
						    	<label for="date-order">Khoảng thời gian</label>
						    	<div class="box-time">
						    		<input type="hidden" id="start-date3" name="from_date_ad" value="">
						    		<input type="hidden" id="end-date3" name="to_date_ad" value="">
		                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-order" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày...">
		                            <i class="fa fa-calendar icon-time"></i>
		                        </div>
						    </div>
						    <div class="item">
						    	<label class="visible-hidden">Xác nhận</label>
						    	<input type="hidden" name="method" value="3">
						    	<button type="submit" class="button bg-black">Xác nhận</button>
						    </div>
						</form>
					</div>
				</div>
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm kiếm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black center-custom">Mã kho</th>
					            <th class="bg-black center-custom">Trạng thái</th>
					            <th class="bg-black center-custom">Số lượng</th>
					            <th class="bg-black">Kho hàng</th>
					            <th class="bg-black">Tên NCC</th>
					            <th class="bg-black">Ngày cập nhật</th>
					            <th class="bg-black">Tên nhân viên</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Mã kho" class="center-custom">5763-2</td>
					            <td data-title="Trạng thái" class="center-custom">Nhập</td>
					            <td data-title="Số lượng" class="center-custom">2</td>
					            <td data-title="Kho hàng">Kho hàng hóa</td>
					            <td data-title="Tên NCC">Lê Mây (mỹ phẩm)</td>
					            <td data-title="Ngày cập nhật">21-09-2019|11:06</td>
					            <td data-title="Tên nhân viên">Hương thu mua</td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		if(window.innerWidth > 576){
		    jQuery('.dropdown-collapse').click(function(){
		    	if(jQuery('.custom-collapse').css('display')=='none'){
		    		jQuery('.custom-collapse').css('display','inline-block');
		    		jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
		    		jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
		    	}
		    	else{
		    		jQuery('.custom-collapse').css('display','none');
		    		jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
		    		jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
		    	}
		    });
		}else{
			jQuery('.button-filter').click(function(){
		    	if(jQuery('.custom-collapse').css('display')=='none'){
		    		jQuery('.custom-collapse').css('display','inline-block');
		    		jQuery('.dropdown-collapse').css('display','flex');
		    		jQuery(this).parent().addClass('active');
		    		jQuery(this).addClass('active');
		    		jQuery(this).find('i').addClass('ti-arrow-right');
		    		jQuery('body').css('overflow','hidden');
		    		jQuery('.filter-pc').css('z-index','999');
		    	}
		    	else{
		    		jQuery('.custom-collapse').css('display','none');
		    		jQuery('.dropdown-collapse').css('display','none');
		    		jQuery(this).parent().removeClass('active');
		    		jQuery(this).removeClass('active');
		    		jQuery(this).find('i').removeClass('ti-arrow-right');
		    		jQuery('body').css('overflow','inherit');
		    		jQuery('.filter-pc').css('z-index','-1');
		    	}
		    });
		}
		var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    jQuery('#id_supplier').select2();
	    if(window.innerWidth > 576) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
	    jQuery('input[name="date-order"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date3').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date3').val(end.format('DD-MM-YYYY'));
		});
	})
</script>