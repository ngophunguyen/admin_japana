<style>
	.box-cmt {
	    border: 1px solid #cbcbcb;
	    padding: 10px;
	    border-radius: 5px;
	    margin-bottom: 12px;
	    height: 164px;
	    overflow-y: scroll;
	    overflow-x: hidden;
	    background: #fff;
	    margin-top: 15px;
	    position: relative;
	}
	.box-cmt .item{
		display: inline-block;
		width: 100%;
		float: left;
		line-height: 1.5;
		margin-bottom: 15px;
	}
	.box-cmt .item p:first-child span:last-child{
		font-size: 13px;
	}
	.box-cmt .item p:last-child{
		font-size: 14px;
	}
	.box-input-cmt{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
	}
	.box-input-cmt .input-cmt{
		width: 84%;
	    float: left;
	    resize: none;
	}
	.box-input-cmt button{
		float: right;
		margin-left: 2%;
		height: 35px;
	}
	.cmt-staff{
		display: inline-block;
		width: 100%;
	}
	.cmt-staff .item:nth-child(even){
		text-align: right;
	}
	@media (max-width: 575.98px) {
		.box-cmt{
			height: auto;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {

	}
	@media (min-width: 1200px) {

	}
</style>
	<a class="dropdown-collapse customer-dropdown bg-black" href="javascript:void(0);">Ghi chú</a>
	<div id="comment_kh" class="box-cmt">
		<div class="item cmt-customer">
			<p class="red-custom bold">Khách hàng <span class="black-custom">- Nguyễn Văn A</span> <span class="gray-custom normal">(27-12-2018 | 10:40)</span></p>
			<p class="detail-cmt">Giao hàng nhanh, trước 14h hôm nay</p>
		</div>
		<div class="cmt-staff">
			<div class="item">
				<p class="blue-custom bold">Thu mua <span class="black-custom">- Hương</span> <span class="gray-custom normal">(27-12-2018 | 10:45)</span></p>
				<p class="detail-cmt">Giao hàng nhanh, trước 14h hôm nay</p>
			</div>
			<div class="item">
				<p class="blue-custom bold">Tư vấn bán hàng <span class="black-custom">- Hằng</span> <span class="gray-custom normal">(27-12-2018 | 10:45)</span></p>
				<p class="detail-cmt">Khách yêu cầu không giao nhanh</p>
			</div>
			<div class="item">
				<p class="blue-custom bold">Xử lý đơn hàng <span class="black-custom">- Lam</span> <span class="gray-custom normal">(27-12-2018 | 10:45)</span></p>
				<p class="detail-cmt">Giao nhanh cho chị</p>
			</div>
		</div>
	</div>
	<div class="box-input-cmt">
		<textarea id="comment" rows="1" data-min-rows='1' class="form-control input-cmt"></textarea>
		<button type="submit" class="button bg-black">Gửi</button>
	</div>
<script>
	jQuery(function(){
		new PerfectScrollbar('.box-cmt');
	    var messageBody = document.querySelector('#comment_kh');
	    messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight; 
	});
	jQuery(document).one('focus.input-cmt', 'textarea.input-cmt', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    }).on('input.input-cmt', 'textarea.input-cmt', function(){
        var minRows = this.getAttribute('data-min-rows')|0, rows;
        this.rows = minRows;
        rows = Math.floor((this.scrollHeight - this.baseScrollHeight)/16);
        this.rows = minRows + rows;
    });
</script>