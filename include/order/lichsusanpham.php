<style>
	.table-addpro tr th:nth-child(3), .table-addpro tr td:nth-child(3){
    	min-width: auto;
	 }
	.table-addpro tr th:nth-child(4), .table-addpro tr td:nth-child(4){
		max-width: 250px;
		white-space: normal;
	}
	.table-addpro thead {
	  	border-bottom: 1px solid #eee;
	}
	.box-table .table-addpro tr td:last-child{
		display: table-cell;
	}
	@media (max-width: 575.98px) {
		.table-addpro > tbody > tr td:first-child{
	      display:none;
	    }
	    .table-addpro > tbody > tr td:nth-child(2){
	      display: none;
	    }
	    .table-addpro tr th:nth-child(3), .table-addpro tr td:nth-child(3){
	      width: 100%;
	    }
	    .table-addpro > tbody > tr:last-child{
	      margin-bottom: 0;
	    }
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
	}
	@media (min-width: 1200px) {
	}
</style>
<div class="modal large-modal" id="lichsusanpham-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Lịch sử sản phẩm
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="box-payback-table">
          <div class="box-table">
            <table class="table table-custom table-addpro table-responsive">
              <thead>
                <tr>
                  <th class="black-custom bold center-custom">STT</th>
                  <th class="black-custom bold">SKU</th>
                  <th class="black-custom bold">Tên sản phẩm</th>
                  <th class="black-custom bold center-custom">Hình ảnh</th>
                  <th class="black-custom bold">Giá</th>
                  <th class="black-custom bold center-custom">Số lượng</th>
                  <th class="black-custom bold">Hành động</th>
                  <th class="black-custom bold">Ngày chỉnh sửa</th>
                  <th class="black-custom bold">Nhân viên</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-title="STT" class="center-custom">1</td>
                  <td data-title="SKU">087AA9</td>
                  <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
                  <td data-title="Hình ảnh" class="center-custom">
                    <img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
                  </td>
                  <td data-title="Giá">2.600.000 đ</td>
                  <td data-title="Số lượng" class="center-custom">
                    1
                  </td>
                  <td data-title="Hành động">Thêm sản phẩm</td>
                  <td data-title="Ngày chỉnh sửa">19/09/2019 | 10:41</td>
                  <td data-title="Nhân viên">Admin</td>
                </tr>
                <tr>
                  <td data-title="STT" class="center-custom">2</td>
                  <td data-title="SKU">087AA7</td>
                  <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml v2</td>
                  <td data-title="Hình ảnh" class="center-custom">
                    <img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
                  </td>
                  <td data-title="Giá">2.800.000 đ</td>
                  <td data-title="Số lượng" class="center-custom">
                    1
                  </td>
                  <td data-title="Hành động">Xóa sản phẩm</td>
                  <td data-title="Ngày chỉnh sửa">19/09/2019 | 10:40</td>
                  <td data-title="Nhân viên">Admin</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
	jQuery(function(){
		
	});
</script>