<!DOCTYPE html>
<html lang="vi">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="title" content="Admin Ver2">
		<meta name="description" content="Admin Ver2">
		<meta name="keywords" content="Admin Ver2">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui" />
		<title>In tem</title>
		<link rel="shortcut icon" href="assets/images/favicon.webp">

		<!--include CSS-->
		<link rel="stylesheet" href="../../assets/plugins/jquery-ui/jquery-ui.min.css">
		<link rel="stylesheet" href="../../assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="../../assets/plugins/themify-icons/themify-icons.css">
		<link rel="stylesheet" href="../../assets/plugins/normalize/normalize.css">
		<link rel="stylesheet" href="../../assets/plugins/normalize/libs.css">
		<!-- <link rel="stylesheet" href="../../assets/css/japana.css"> -->
		<!--/include CSS-->

		<!--include JS-->
		<script src="../../assets/plugins/jquery/jquery-3.3.1.min.js"></script>
		<script src="../../assets/plugins/jquery-ui/jquery-ui.min.js"></script>
		<!--/include JS-->

		<!--include Font-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=vietnamese" rel="stylesheet">
        <!--/.include Font-->
        <style>
	        @page {
		        margin: 0;
		        size: auto;
		    }
            html, body {
                width:100%;
                height:100%;
                font-size: 18pt;
                margin: 0 auto;
                -webkit-print-color-adjust: exact;
            }
	        .content-sidebar-wrap{
	    		width: 100%;
	    	}
	        .box-body-print{
	            padding: 30px 0;
	        }
	        .box-img-print img{
	            max-width: none;
	        }
	        .info-print ul li, .info-print ul li span{
	            font-size: 20pt;
	        }
	        .info-print ul li span{
	            font-weight: 500;
	        }
	        .info-cus{
	            float: left; 
	            width: 49%;
	            line-height: 1.5;
	            position: relative;
	            min-height: 950px;
	        }
	        .info-cus .info-cus-text{
	            font-size: 20pt;
	        }
	        .info-cus .info-cus-text span{
	            font-weight: 500; 
	            color: #000; 
	            margin-left: 0; 
	            max-width: none;
	        }
	        .info-cus .info-cus-text span:last-child{
	            max-width: 100%;
	        }
	        .info-cus .item:nth-child(2) .info-cus-text span:last-child{
	            text-transform: uppercase;
	            font-size: 22pt;
	        }
	        .info-cus .item:nth-child(7) .box-item{
				display: -webkit-box; 
	            display: -ms-flexbox; 
	            display: flex; 
	        }
	        .info-cus .item{
	        	margin-bottom: 15px;
	        }
	        .info-cus .item:last-child .box-item img{
	        	width: 100%;
	        }
	        .box-item{
	            -webkit-box-pack: baseline;
	            -ms-flex-pack: baseline;
	            justify-content: baseline;
	        }
	        .checkbox-print{
	            display: -webkit-box; 
	            display: -ms-flexbox; 
	            display: flex; 
	            -webkit-box-align: center; 
	            -ms-flex-align: center; 
	            align-items: center; 
	            margin: 0 15px 0 5px; 
	            font-size: 20pt; 
	            font-weight: 500;
	        }
	        .checkbox-print span{
	            width: 20px; 
	            height: 20px; 
	            display: -webkit-box; 
	            display: -ms-flexbox; 
	            display: flex; 
	            -webkit-box-align: center; 
	            -ms-flex-align: center; 
	            align-items: center; 
	            float: left; 
	            margin-right: 5px; 
	            font-weight: 500; 
	            background: #000; 
	            color: #fff;
	            border-radius: 4px; 
	            font-size: 16pt;
	            -webkit-box-pack: center;
	            -ms-flex-pack: center;
	            justify-content: center;
	        }
	        .info-cus:last-child{
	            float: left; 
	            width: 49%; 
	            margin-left: 2%;
	        }
	        .info-cus:last-child .item{
	            padding-bottom: 20px;
	            margin-bottom: 0;
	        }
	        .info-cus .item:last-child{
	        	position: absolute;
	        	bottom: 0;
	        	width: 100%;
	        }
	        .list-tb-item ul li{
	            float: left;
	            color: #000;
	        }
	        .list-tb-item ul li:first-child, .list-tb-item ul li:last-child{
	            -webkit-box-pack: center;
	                -ms-flex-pack: center;
	                    justify-content: center;
	        }
	        .box-img-print{
	            max-width: 24%;
	        }
	        .box-img-print img{
	            width: 100%;
	        }
	        .custom-body-print{
	            display: -webkit-box;
	            display: -ms-flexbox;
	            display: flex;
	            -webkit-box-align: center;
	                -ms-flex-align: center;
	                    align-items: center;
	            -webkit-box-pack: center;
	                -ms-flex-pack: center;
	                    justify-content: center;
	            padding: 15px;
	        }

	        .info-print{
	            max-width: 57%;
	            margin: 0 15px;
	        }

	        .info-print ul{
	            list-style-type: none;
	            padding: 0;
	            margin-bottom: 0;
	            line-height: 1.5
	        }
	        .qr-code{
	            max-width: 15%;
	        }
	        .qr-code img{
	            max-width: 100%;
	        }
	        .box-body-print{
	            display: -webkit-box;
	            display: -ms-flexbox;
	            display: flex;
	            -webkit-box-align: center;
	                -ms-flex-align: center;
	                    align-items: center;
	            -webkit-box-pack: center;
	                -ms-flex-pack: center;
	                    justify-content: center;
	        }
	        .custom-table tbody tr td{
	            padding: 0 8px;
	        }
	        .price-table{
	            font-family: 'Roboto',sans-serif;
	            font-weight: 500;
	            color: #e62e6b;
	            font-size: 16px;
	            display: inline-block;
	        }
	        .price-table p{
	            margin-bottom: 0
	        }
	        .price-table span{
	            font-weight: 500;
	            color: #333;
	        }
	        .list-tb-item{
	            display: inline-block;
	            width: 100%;
	            border: 1px solid #cbcbcb;
	        }
	        .list-tb-item ul{
	            list-style-type: none;
	            width: 100%;
	            padding: 0;
	            margin-bottom: 0;
	            display: -webkit-box;
	            display: -ms-flexbox;
	            display: flex;
	        }
	        .list-tb-item ul li{
	            float: left;
	            font-size: 20pt;
	            font-family: 'Roboto',sans-serif;
	            font-weight: 500;
	            height: 50px;
	            display: -webkit-box;
	            display: -ms-flexbox;
	            display: flex;
	            -webkit-box-align: center;
	                -ms-flex-align: center;
	                    align-items: center;
	        }
	        .list-tb-item ul:nth-child(odd){
	            background: #eee!important;
	        }
	        .list-tb-item ul:first-child{
	            background: #666666!important;
	        }
	        .list-tb-item ul:first-child li{
	        	color: #fff!important;
	        }
	        .list-tb-item ul li:first-child, .list-tb-item ul li:last-child{
	            width: 70px;
	            text-align: center;
	        }
	        .list-tb-item ul li:nth-child(2){
	            width: 470px;
	        }
	        .price-print{
	        	font-size: 20pt; 
	        	display: -webkit-box; 
	        	display: -ms-flexbox; 
	        	display: flex;
	        	-webkit-box-align: center;
	        	    -ms-flex-align: center;
	        	        align-items: center;
	        	-webkit-box-pack: justify;
	        	    -ms-flex-pack: justify;
	        	        justify-content: space-between; 
	        	width: 100%; 
	        }
	        .price-print span{
	        	font-weight: 500; 
	        	color: #000; 
	        	margin-left: 0; 
	        	max-width: none;
	        }
	        .price-print span:last-child{
	        	max-width: 100%; 
	        	font-weight: 500; 
	        	color: #000;
	        	float: right;
	        }
        </style>
	</head>

	<body>
		<div class="site-container">
			<div class="site-inner">
				<div class="content-sidebar-wrap">
					<main class="print content">
	                    <div class="box-body-print">
	                        <div class="box-img-print">
	                            <img src="../../assets/images/logo-tem.png" alt="logo">
	                        </div>
	                        <div class="info-print">
	                            <ul>
	                                <li><i class="fa fa-location-arrow" aria-hidden="true"></i> <span>Kanagawa Ken Ebina -Sbi Nakana, Japan</span></li>
	                                <li><i class="fa fa-location-arrow" aria-hidden="true"></i> <span>76 Nguyễn Háo Vĩnh, P. Tân Quý, Q. Tân Phú, TP. Hồ Chí Minh</span></li>
	                                <li><i class="fa fa-phone" aria-hidden="true"></i> <span>(028) 7108 8889 - 0935 600 800</span></li>
	                                <li><i class="fa fa-globe" aria-hidden="true"></i> <span>https://japana.vn</span></li>
	                            </ul>
	                        </div>
	                        <div class="qr-code">
	                            <img src="../../assets/images/br-code.png" alt="br-code">
	                        </div>
	                    </div>
	                    <div class="info-cus">
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Mã đơn hàng:</span> 
	                                    <span>JP_001</span>
	                                </p>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Đơn vị vận chuyển:</span> 
	                                    <span>Giao hàng tiết kiệm</span>
	                                </p>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Ngày xuất phiếu:</span> 
	                                    <span>13-02-2019</span>
	                                </p>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Mã vận đơn:</span> 
	                                    <span>GHTK-001</span>
	                                </p>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Tên khách hàng:</span> 
	                                    <span>JAPANA</span>
	                                </p>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Địa chỉ:</span> 
	                                    <span>76 Nguyễn Háo Vĩnh, P. Tân Quý, Q. Tân Phú, TP. Hồ Chí Minh</span>
	                                </p>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Lưu ý:</span> 
	                                </p>
	                                <p class="checkbox-print">
	                                    <span><i class="fa fa-check" aria-hidden="true"></i></span> Hàng dễ vỡ
	                                </p>
	                                <p class="checkbox-print">
	                                    <span><i class="fa fa-check" aria-hidden="true"></i></span> Được phép kiểm tra hàng
	                                </p>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="checkbox-print">
	                                    <span><i class="fa fa-check" aria-hidden="true"></i></span> Chỉ cho khách xem hàng - không được tự ý bóc seal.
	                                </p>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="checkbox-print">
	                                    <span><i class="fa fa-check" aria-hidden="true"></i></span> Không liên hệ được với khách, báo ngay cho siêu thị Nhật Bản Japana, KHÔNG TỰ Ý TRẢ HÀNG.
	                                </p>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <img src="../../assets/images/banner-tem.jpg" alt="banner-order">
	                            </div>
	                        </div>
	                    </div>
	                    <div class="info-cus">
	                        <div class="item">
	                            <div class="box-item">
	                                <div class="list-tb-item">
	                                    <ul>
	                                        <li>STT</li>
	                                        <li>Tên sản phẩm</li>
	                                        <li>SL</li>
	                                    </ul>
	                                    <ul>
	                                        <li>1</li>
	                                        <li>(SH-001) Sản phẩm 1</li>
	                                        <li>1</li>
	                                    </ul>
	                                    <ul>
	                                        <li>2</li>
	                                        <li>(SH-002) Sản phẩm 2</li>
	                                        <li>1</li>
	                                    </ul>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text price-print">
	                                    <span>TỔNG CỘNG:</span> 
	                                    <span>1,000,000 đ</span>
	                                </p>
	                            </div>
	                        </div>
							<div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text price-print">
	                                    <span>CHUYỂN KHOẢN: </span> 
	                                    <span>- 0 đ</span>
	                                </p>
	                            </div>
	                        </div>
							<div class="item">
	                            <div class="box-item">
									<p class="info-cus-text price-print">
	                                    <span>COD THU HỘ:</span> 
	                                    <span>0 đ</span>
	                                </p>
	    						</div>
	                        </div>
							<div class="item">
	                            <div class="box-item">
	                                <img src="../../assets/images/banner-tem.jpg" alt="banner-order">
	                            </div>
	                        </div>
	                    </div>
	                </main>
				</div>
			</div>
		</div>
		<script src="../../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function () {
	            window.print();
	        });
		    (function () {
		        var beforePrint = function () {
		            console.log('Đang chuẩn bị in.');
		        };
		        var afterPrint = function () {
		            window.close();
		        };

		        if (window.matchMedia) {
		            var mediaQueryList = window.matchMedia('print');
		            mediaQueryList.addListener(function (mql) {
		                if (mql.matches) {
		                    beforePrint();
		                } else {
		                    afterPrint();
		                }
		            });
		        }

		        window.onbeforeprint = beforePrint;
		        window.onafterprint = afterPrint;
		    }());
		</script>
	</body>
</html>