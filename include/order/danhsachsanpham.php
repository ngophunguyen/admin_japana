<style>
	#products-kh .dropdown-collapse{
		position: relative;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	}
	.table-custom{
		margin-top: 0;
		white-space: nowrap;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		height: auto;
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	img.gift-order{
		height: 20px!important; 
		bottom: 5px; 
		left: 0;  
		position: absolute;
	}
	.box-table tr td:nth-child(3){
		min-width: 250px;
    	white-space: normal;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.box-table tr td select{
		white-space: normal;
		width: auto;
	}
	.box-table tr td .box-time{
		width: 125px;
		white-space: normal;
	}
	.first-price{
		text-decoration: line-through;
	    font-size: 12px;
	    color: #333;
	    margin-bottom: 10px;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	    -ms-flex-align: center;
	    align-items: center;
	    padding: 15px 0;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.custom-dropdown{
		display: inline-block;
	}
	#products-kh .customer-dropdown span{
		padding: 12px 0;
		position: absolute;
		right: 1%;
	}
	#products-kh .customer-dropdown span:first-child{
		right: 11%;
	}
	.customer-dropdown span{
		padding: 12px 0;
	}
	.customer-dropdown span:first-child{
		right: 11%;
	}
	.coupon-box, .total-order-box{
		width: 49%;
		float: left;
	}
	.coupon-box .item p{
		display: inline-block;
		margin: 15px 0 0;
	}
	.coupon-box .item:last-child{
		border: 1px dashed #cbcbcb;
		padding: 15px 10px;
		border-radius: 10px;
		margin-top: 10px;
	}
	.total-order-box{
		float: right;
	}
	.coupon-box .item label{
		font-weight: 500;
    	font-size: 14px;
    	margin: 15px 0;
	}
	.coupon-box .item .box-input-coupon{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
	}
	.coupon-box .item .box-input-coupon #code{
		width: 75%;
		float: left;
	}
	.coupon-box .item .box-input-coupon .custom-dropdown, .coupon-box .item .box-input-coupon #coupon{
		width: 35%;
		float: left;
	}
	.coupon-box .item .box-input-coupon button{
		width: 20%;
		float: left;
	}
	.total-order-box p{
		font-weight: 500;
    	font-size: 14px;
    	margin: 15px 0;
    	display: -webkit-box;
    	display: -ms-flexbox;
    	display: flex;
    	-webkit-box-pack: justify;
    	    -ms-flex-pack: justify;
    	        justify-content: space-between;
	}
	.coupon-box .item:last-child p{
		width: 100%;
		margin: 0 0 15px;
	}
	.coupon-box .item:last-child p a{
		float: right;
		font-size: 20px;
	}
	.show-coupon-box{
		display: none;
	}
	.soluong{
		border: 0; 
		text-align: center;
		width: 90px;
		background-color: transparent!important;
	}
	.soluong-open{
		background: #fff;
		width: 90px;
		border: 1px solid #cfcfcf;
	}
	.add-pro-mobile{
		display: none;
	}
	.combo > td{
	    font-size: 12px!important;
	    font-style: italic;
	}
	.combo td:nth-child(2){
	    text-transform: uppercase;
	    display: flex;
    	align-items: center;
	}
	.combo td i{
		color: #cf0656;
	    font-size: 6px;
	    margin-right: 5px;
	}
	#p_info_code{
	    width: 30%;
	    padding-left: 5px;
	    padding-top: 0;
	    margin: 0;
	}
	#code-shipping{
		display: flex;
		align-items: center;
	}
	#code-shipping .edit-box {
	    display: none;
	    float: left;
	    width: 80%;
	    padding-left: 5px;
	}
	#code-shipping .edit-box input{
		width: 70%;
		float: left;
	}
	#p_info_code a{
		font-size: 14px;
		height: auto;
		padding: 0;
	}
	.code-mv {
	    background: #eeeeee;
	    font-size: 12px;
	    padding: 5px;
	    border-radius: 5px;
	    display: inline-block;
	    margin: 0 5px 5px 0;
	}
	@media (max-width: 575.98px) {
		.box-table tr td:nth-child(3){
			width: auto;
		}
		.table-responsive > tbody > tr td:first-child {
		    display: none;
		}
		img.gift-order{
			left: auto;
		}
		.first-price{
			margin-bottom: 0;
			width: auto;
		}
		.table-custom > tbody > tr > td .box-time, .table-custom > tbody > tr > td .custom-dropdown{
			width: 100%;
			padding: 5px 0;
		}
		.soluong{
			text-align: left;
		}
		.box-coupon-total{
			display: none;
			width: 100%;
			background: #fff;
			position: fixed;
		    left: 0;
		    right: 0;
		    padding: 0 15px;
		    height: calc(100% - 180px);
		    overflow: scroll;
		    bottom: 35px;
		    border-top: 1px solid #eee;
		    z-index: 2;
		}
		.coupon-box, .total-order-box{
			width: 100%;
		}
		.show-coupon-box{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		    -webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		    -webkit-box-pack: center;
		        -ms-flex-pack: center;
		            justify-content: center;
		    height: 35px;
			width: 100%;
			position: fixed;
			left: 0;
			right: 0;
			bottom: 0;
			z-index: 2;
		}
		.show-coupon-box a{
			color: #fff;
    		line-height: 1.5;
		}
		.coupon-box .item .box-input-coupon{
			display: block;
		}
		.coupon-box .item .box-input-coupon .custom-dropdown, .coupon-box .item .box-input-coupon #coupon{
			width: 100%;
			margin-bottom: 10px;
		}
		.coupon-box .item .box-input-coupon button{
			float: right;
		}
		
		.slide-top{
			-webkit-animation: slide-top 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) infinite both;
			        animation: slide-top 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) infinite both;
		}
		@-webkit-keyframes slide-top {
		  0% {
		    -webkit-transform: translateY(10px);
		            transform: translateY(10px);
		  }
		  100% {
		    -webkit-transform: translateY(-10px);
		            transform: translateY(-10px);
		  }
		}
		@keyframes slide-top {
		  0% {
		    -webkit-transform: translateY(10px);
		            transform: translateY(10px);
		  }
		  100% {
		    -webkit-transform: translateY(-10px);
		            transform: translateY(-10px);
		  }
		}
		#products-kh .add-pro-mobile{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}
		.customer-dropdown span{
			width: 100%;
		}
		#products-kh{
			height: 100vh;
		}
		#products-kh .table-custom > tbody > tr > td:last-child .link-custom i{
			margin-right: 0;
			margin-bottom: 0;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom {
		    white-space: nowrap;
		}
		.first-price{
			margin-right: 10px;
			width: auto;
		}
		.table-custom > tbody > tr > td:last-child{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
		.add-pro-mobile{
			display: none;
		}
		.coupon-box{
			width: 60%;
		}
		.total-order-box{
			width: 35%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.table-custom {
		    white-space: nowrap;
		}
		.first-price{
			margin-right: 10px;
			width: auto;
		}
		.table-custom > tbody > tr > td:last-child{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
		.add-pro-mobile{
			display: none;
		}
		.coupon-box{
			width: 60%;
		}
		.total-order-box{
			width: 35%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.scroll-order{
			width: 90%;
		}
		.scroll-order .custom-dropdown{
			width: auto;
		}
		.table-custom {
		    white-space: nowrap;
		}
		.first-price{
			margin-right: 10px;
			width: auto;
		}
		.table-custom > tbody > tr > td:last-child{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
	}
	@media (min-width: 1200px) {
		
	}
</style>
	<a class="dropdown-collapse customer-dropdown bg-black" href="javascript:void(0);">
		Danh sách sản phẩm 
		<span data-toggle="modal" data-target="#themsanpham-modal">
			<i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm sản phẩm
		</span>
		<span data-toggle="modal" data-target="#lichsusanpham-modal">
			<i class="fa fa-history" aria-hidden="true"></i> Lịch sử sản phẩm
		</span>
	</a>
	<div class="box-table">
		<table class="table table-custom table-responsive">
		    <thead>
		        <tr>
		            <th class="black-custom bold center-custom">STT</th>
		            <th class="black-custom bold">SKU</th>
		            <th class="black-custom bold">Tên sản phẩm</th>
		            <th class="black-custom bold center-custom">Hình ảnh</th>
		            <th class="black-custom bold">Giá</th>
		            <th class="black-custom bold center-custom">Nhập mã giảm</th>
		            <th class="black-custom bold center-custom">SL Kho</th>
		            <th class="black-custom bold center-custom">SL Chờ duyệt</th>
		            <th class="black-custom bold center-custom">SL Đã xác nhận</th>
		            <th class="black-custom bold center-custom">SL Còn lại</th>
		            <th class="black-custom bold center-custom">SL Quét</th>
		            <th class="black-custom bold center-custom">Mã hàng hóa</th>
		            <th class="black-custom bold center-custom">Kho kế toán</th>
		            <th class="black-custom bold center-custom">Số lượng</th>
		            <th class="black-custom bold">Thành tiền</th>
		            <th class="black-custom bold center-custom">Trạng thái</th>
		            <th class="black-custom bold">Ngày có hàng</th>
		            <th class="black-custom bold">Tác vụ</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td data-title="STT" class="center-custom">1</td>
		            <td data-title="SKU">087AA9</td>
		            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
		            <td data-title="Hình ảnh" class="center-custom">
		            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
		            </td>
		            <td data-title="Giá sản phẩm"><span class="first-price">2.600.000 đ</span> | <span>2.340.000 đ</span></td>
		            <td data-title="Nhập mã giảm" class="center-custom">0 đ</td>
		            <td data-title="SL Kho" class="center-custom">73</td>
		            <td data-title="SL Chờ duyệt" class="center-custom">17</td>
		            <td data-title="SL Đã xác nhận" class="center-custom">8</td>
		            <td data-title="SL Còn lại" class="center-custom">48</td>
		            <td data-title="SL Quét" class="center-custom"><span class="red-custom bold">0</span></td>
		            <td data-title="Mã hàng hóa" class="center-custom">
		            	<p class="code-mv gray-custom"><span class="red-custom bold">4693-1</span>:1</p>
		            	<p class="code-mv gray-custom"><span class="red-custom bold">4693-1</span>:1</p>
		            </td>
		            <td data-title="Kho kế toán" class="center-custom"><span class="green-custom">Đủ hàng</span></td>
		            <td data-title="Số lượng" class="center-custom">
			            <input min="1" maxlength="2" type="number" class="form-control soluong" value="1" name="qty_1" id="qty_1" disabled="disabled">
			        </td>
		            <td data-title="Thành tiền"><span>2.340.000 đ</span></td>
		            <td data-title="Trạng thái" class="center-custom">Còn hàng</td>
		            <td data-title="Ngày có hàng">
		            	<div class="box-time">
		            		<input type="hidden" id="date-order" name="date-order" class="form-control" value="">
				    		<input autocomplete="off" onkeypress="return false;" type="text" id="date-order" name="date-order" class="form-control" value="">
	                        <i class="fa fa-calendar icon-time"></i>
	                    </div>
		            </td>
		            <td data-title="Tác vụ">
		            	<a href="javascript:void(0);" onclick="clickOn();" class="link-custom black-custom" title="Chỉnh sửa">
		            		<i class="fa fa-pencil-square-o"></i>
		            	</a>
		            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
		            		<i class="fa fa-save"></i>
		            	</a>
		            	<a href="javascript:delete_product(1);" class="link-custom black-custom" title="Xoá">
		            		<i class="fa fa-trash-o"></i>
		            	</a>
		            </td>
		        </tr>
		        <tr>
		            <td data-title="STT" class="center-custom">2</td>
		            <td data-title="SKU">087AA7</td>
		            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml v2</td>
		            <td data-title="Hình ảnh" class="center-custom">
		            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
		            </td>
		            <td data-title="Giá sản phẩm"><span class="first-price">2.800.000 đ</span> | <span>3.000.000 đ</span></td>
		            <td data-title="Nhập mã giảm" class="center-custom">0 đ</td>
		            <td data-title="SL Kho" class="center-custom">73</td>
		            <td data-title="SL Chờ duyệt" class="center-custom">17</td>
		            <td data-title="SL Đã xác nhận" class="center-custom">8</td>
		            <td data-title="SL Còn lại" class="center-custom">48</td>
		            <td data-title="SL Quét" class="center-custom"><span class="red-custom bold">0</span></td>
		            <td data-title="Mã hàng hóa" class="center-custom">
		            	<p class="code-mv gray-custom"><span class="red-custom bold">4693-1</span>:1</p>
		            	<p class="code-mv gray-custom"><span class="red-custom bold">4693-1</span>:1</p>
		            </td>
		            <td data-title="Kho kế toán" class="center-custom"><span class="green-custom">Đủ hàng</span></td>
		            <td data-title="Số lượng" class="center-custom">
		            	<input min="1" maxlength="2" type="number" class="form-control soluong" value="5" name="qty_2" id="qty_2" disabled="disabled">
		            </td>
		            <td data-title="Thành tiền"><span>15.000.000 đ</span></td>
		            <td data-title="Trạng thái" class="center-custom">
		            	<div class="custom-dropdown">
					    	<select class="form-control">
					    		<option value="-1">Chờ hàng không ngày</option>
							  	<option value="0" selected>Chờ hàng có ngày</option>
							  	<option value="1">Hết hàng</option>
							</select>
						</div>
		            </td>
		            <td data-title="Ngày có hàng">
		            	<div class="box-time">
		            		<input type="hidden" id="date-order" name="date-order" class="form-control" value="">
				    		<input autocomplete="off" onkeypress="return false;" type="text" id="date-order" name="date-order" class="form-control" value="">
	                        <i class="fa fa-calendar icon-time"></i>
	                    </div>
		            </td>
		            <td data-title="Tác vụ">
		            	<a href="javascript:void(0);" onclick="clickOn();" class="link-custom black-custom" title="Chỉnh sửa">
		            		<i class="fa fa-pencil-square-o"></i>
		            	</a>
		            	<a href="javascript:update_product(2);" class="link-custom black-custom" title="Lưu">
		            		<i class="fa fa-save"></i>
		            	</a>
		            	<a href="javascript:delete_product(2);" class="link-custom black-custom" title="Xoá">
		            		<i class="fa fa-trash-o"></i>
		            	</a>
		            </td>
		        </tr>
		        <tr>
		            <td data-title="STT" class="center-custom">3</td>
		            <td data-title="SKU">KMNCC-079AA1</td>
		            <td data-title="Tên sản phẩm">Khuyến mãi Ly Sứ Cao Cấp Kinohimitsu</td>
		            <td data-title="Hình ảnh" class="center-custom">
		            	<img src="https://japana.vn/uploads/product/2018/08/23/1534988772-KMNCC-079AA1.jpg" alt="img">
		            	<img class="gift-order" src="https://japana.vn/assets/images/gift.png" alt="img">
		            </td>
		            <td data-title="Giá sản phẩm"><span>0 đ</span></td>
		            <td data-title="Nhập mã giảm" class="center-custom">0 đ</td>
		            <td data-title="SL Kho" class="center-custom">73</td>
		            <td data-title="SL Chờ duyệt" class="center-custom">17</td>
		            <td data-title="SL Đã xác nhận" class="center-custom">8</td>
		            <td data-title="SL Còn lại" class="center-custom">48</td>
		            <td data-title="SL Quét" class="center-custom"><span class="red-custom bold">0</span></td>
		            <td data-title="Mã hàng hóa" class="center-custom">
		            	<p class="code-mv gray-custom"><span class="red-custom bold">4693-1</span>:1</p>
		            	<p class="code-mv gray-custom"><span class="red-custom bold">4693-1</span>:1</p>
		            </td>
		            <td data-title="Kho kế toán" class="center-custom"><span class="green-custom">Đủ hàng</span></td>
		            <td data-title="Số lượng" class="center-custom">
		            	<input min="1" maxlength="2" type="number" class="form-control soluong" value="1" name="qty_0" id="qty_0" disabled="disabled">
		            </td>
		            <td data-title="Thành tiền"><span>0 đ</span></td>
		            <td data-title="Trạng thái" class="center-custom"></td>
		            <td data-title="Ngày có hàng"></td>
		            <td data-title="Tác vụ"></td>
		        </tr>
		        <tr>
		            <td data-title="STT" class="center-custom">4</td>
		            <td data-title="SKU">039AA2X3</td>
		            <td data-title="Tên sản phẩm">Combo 3 hộp Nước uống Venus Charge Collagen Peptide 20000mg Nhật Bản</td>
		            <td data-title="Hình ảnh" class="center-custom">
		            	<img src="https://japana.vn/uploads/product/2018/06/29/168x168-1530246170-combo-3-hop-nuoc-uong-venus-charge-collagen.jpg" alt="Combo 3 hộp Nước uống Venus Charge Collagen Peptide 20000mg Nhật Bản">
		            </td>
		            <td data-title="Giá sản phẩm"><span class="first-price">5.250.000 đ</span> | <span>4.460.000 đ</span></td>
		            <td data-title="Nhập mã giảm" class="center-custom">0 đ</td>
		            <td data-title="SL Kho" class="center-custom">0</td>
		            <td data-title="SL Chờ duyệt" class="center-custom">0</td>
		            <td data-title="SL Đã xác nhận" class="center-custom">0</td>
		            <td data-title="SL Còn lại" class="center-custom">1</td>
		            <td data-title="SL Quét" class="center-custom"><span class="red-custom bold">0</span></td>
		            <td data-title="Mã hàng hóa" class="center-custom">
		            	<p class="code-mv gray-custom"><span class="red-custom bold">4693-1</span>:1</p>
		            	<p class="code-mv gray-custom"><span class="red-custom bold">4693-1</span>:1</p>
		            </td>
		            <td data-title="Kho kế toán" class="center-custom"><span class="green-custom">Đủ hàng</span></td>
		            <td data-title="Số lượng" class="center-custom">
		            	<input min="1" maxlength="2" type="number" class="form-control soluong" value="1" name="qty_4" id="qty_4" disabled="disabled">
		            </td>
		            <td data-title="Thành tiền"><span>4.460.000 đ</span></td>
		            <td data-title="Trạng thái" class="center-custom">
		            	<div class="custom-dropdown">
					    	<select class="form-control">
					    		<option value="-1">Chờ hàng không ngày</option>
							  	<option value="0">Chờ hàng có ngày</option>
							  	<option value="1">Hết hàng</option>
							  	<option value="2" selected>Có hàng</option>
							</select>
						</div>
		            </td>
		            <td data-title="Ngày có hàng"></td>
		            <td data-title="Tác vụ">
		            	<a href="javascript:void(0);" onclick="clickOn();" class="link-custom black-custom" title="Chỉnh sửa">
		            		<i class="fa fa-pencil-square-o"></i>
		            	</a>
		            	<a href="javascript:update_product(4);" class="link-custom black-custom" title="Lưu">
		            		<i class="fa fa-save"></i>
		            	</a>
		            	<a href="javascript:delete_product(4);" class="link-custom black-custom" title="Xoá">
		            		<i class="fa fa-trash-o"></i>
		            	</a>
		            </td>
		        </tr>
		        <tr class="combo">
		            <td data-title="STT" class="center-custom"></td>
		            <td data-title="SKU">
		            	<i class="fa fa-circle" aria-hidden="true"></i> 039AA2
		            </td>
		            <td data-title="Tên sản phẩm">Nước uống Venus Charge Collagen Peptide 20000mg Nhật Bản</td>
		            <td data-title="Hình ảnh" class="center-custom">
		            	<img src="http://japana.local/uploads/product/2019/04/18/1555584939--nhat-ban-japana-sieu-thi-nhat-ban-japana-238.jpeg" alt="Nước uống Venus Charge Collagen Peptide 20000mg Nhật Bản">
		            </td>
		            <td data-title="Giá sản phẩm"><span>1,750,000 đ</span></td>
		            <td data-title="Nhập mã giảm" class="center-custom">0 đ</td>
		            <td data-title="SL Kho" class="center-custom">13</td>
		            <td data-title="SL Chờ duyệt" class="center-custom">9</td>
		            <td data-title="SL Đã xác nhận" class="center-custom">4</td>
		            <td data-title="SL Còn lại" class="center-custom">3</td>
		            <td data-title="SL Quét" class="center-custom"><span class="red-custom bold">0</span></td>
		            <td data-title="Mã hàng hóa" class="center-custom">
		            	<p class="code-mv gray-custom"><span class="red-custom bold">4693-1</span>:1</p>
		            	<p class="code-mv gray-custom"><span class="red-custom bold">4693-1</span>:1</p>
		            </td>
		            <td data-title="Kho kế toán" class="center-custom"><span class="green-custom">Đủ hàng</span></td>
		            <td data-title="Số lượng" class="center-custom">
		            	<input min="1" maxlength="2" type="number" class="form-control soluong" value="3" name="qty_5" id="qty_5" disabled="disabled">
		            </td>
		            <td data-title="Thành tiền"></td>
		            <td data-title="Trạng thái" class="center-custom"></td>
		            <td data-title="Ngày có hàng"></td>
		            <td data-title="Tác vụ"></td>
		        </tr>
		    </tbody>
		</table>
	</div>
	<div class="box-coupon-total">
		<a class="dropdown-collapse customer-dropdown add-pro-mobile bg-black" href="javascript:void(0);">
			<span data-toggle="modal" data-target="#themsanpham-modal">
				<i class="fa fa-plus-circle" aria-hidden="true"></i> Thêm sản phẩm
			</span>
		</a>
		<div class="coupon-box">
			<div class="item">
				<label for="code" class="black-custom">
					Mã khuyến mãi
				</label>
				<div class="box-input-coupon">
					<input type="text" class="form-control" id="code" name="code" placeholder="Nhập mã">
					<button type="submit" class="button bg-green">Thêm</button>
				</div>
				<p class="black-custom bold">KM-001 <span class="gray normal">(-20,000 đ)</span></p>
			</div>
			<div class="item">
				<label for="id_chiphikhac" class="black-custom">
					Chi phí khác
				</label>
				<div class="box-input-coupon">
					<div class="custom-dropdown">
				    	<select class="form-control" id="id_chiphikhac">
				    		<option value="-1">Chọn loại chi phí</option>
						  	<option value="0">Khuyến mãi</option>
						  	<option value="1">Phí phát sinh</option>
						  	<option value="2">Phí V.I.P</option>
						</select>
					</div>
					<input type="text" class="form-control" id="coupon" name="coupon" placeholder="Nhập số tiền">
					<button type="submit" class="button bg-green">Thêm</button>
				</div>
				<p class="black-custom bold">Chi phí khác <span class="gray normal">(-20,000 đ)</span><a href="javascript:void(0);" title="Xoá" class="red-custom"> <i class="fa fa-times-circle" aria-hidden="true"></i></a></p>
			</div>
			<div class="item">
				<p class="black-custom bold">Đơn vị vận chuyển: <span class="gray normal">Tín tốc</span></p>
				<p class="black-custom bold">Phí vận chuyển và COD: <span class="gray normal">45.550 đ</span></p>
				<div id="code-shipping" class="black-custom bold">Mã vận đơn: 
					<p id="p_info_code">
						<span class="gray normal">TTA0410718</span>
						<a href="javascript:void(0);" class="open-form-edit link-custom black-custom" title="Chỉnh sửa">
							<i class="fa fa-pencil-square-o"></i> Edit
						</a>
					</p>
					<div class="edit-box">
						<input type="text" value="TTA0410718" id="info_code" name="info_code" class="form-control">
						<div class="box-button">
							<button type="button" class="button button-header green-custom">
								<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu
							</button>
							<button type="button" class="button button-header red-custom" onclick="hideform('p_info_code');">
								<i class="ti-close" aria-hidden="true"></i> Xoá
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="total-order-box">
			<p class="black-custom">
				Tạm tính: <span>1,000,000 đ</span>
			</p>
			<p class="black-custom">
				Giảm V.I.P: <span>- 50,000 đ</span>
			</p>
			<p class="black-custom">
				Vận chuyển: <span>+ 40,000 đ</span>
			</p>
			<hr>
			<p class="black-custom">
				Tổng đơn: <span class="bold blue-custom">950,000 đ</span>
			</p>
			<p class="green-custom">
				Chuyển khoản: <span>- 0 đ</span>
			</p>
			<p class="black-custom">
				COD thu hộ: <span>950,000 đ</span>
			</p>
		</div>
	</div>
	<div class="show-coupon-box bg-black">
		<a href="javascript:void(0);" title="Show">
			Mở nhập mã khuyến mãi <i class="fa fa-angle-double-up slide-top"></i>
		</a>
	</div>
<script>
	function clickOn(){
		jQuery(this).parent().css('display','block');
		jQuery(".soluong").prop("disabled", false);
        jQuery(".soluong").addClass('soluong-open');
		jQuery(".soluong").removeClass('soluong');
	}
	jQuery(function(){
		jQuery('.show-coupon-box a').click(function(){
			if(jQuery('.box-coupon-total').css('display')=='none'){
				jQuery('.box-coupon-total').css('display','inline-block');
				jQuery(this).text("");
				jQuery(this).append("Đóng nhập mã khuyến mãi <i class='fa fa-angle-double-down slide-top'></i>");
				showBackgroundPopup();
			}
			else{
				jQuery('.box-coupon-total').css('display','none');
				deleteBackgroundPopup();
				jQuery(this).text("");
				jQuery(this).append("Mở nhập mã khuyến mãi <i class='fa fa-angle-double-up slide-top'></i>");
			}
		});
		var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    if(window.innerWidth > 500) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
	    jQuery('input[name="date-order"]').daterangepicker({
			singleDatePicker: true,
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			locale: formatDate
		},function(start) {
			jQuery('#date-order').val(start.format('DD-MM-YYYY'));
		});
	});
</script>