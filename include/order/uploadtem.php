<style>
	.upload-img{
		margin-bottom: 15px;
	}
	.box-upload{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-upload .item{
		display: inline-block;
		width: 40%;
		float: left;
	}
	.box-upload>.item:last-child{
		margin-left: 2%;
		border: 1px solid #ccc;
		padding: 15px;
		width: 58%;
	}
	.box-img-meta{
		display: none;
		margin-bottom: 10px;
	}
	.box-img-meta img{
		width: 100%;
	}
    .info-print ul li{
    	margin-bottom: 5px;
    }
    .info-print ul li, .info-print ul li span{
        font-size: 14px;
    }
    .info-cus{
        float: left; 
        width: 50%;
        height: 100%;
        position: relative;
    }
    .info-cus .item{
    	width: 100%;
    	margin-bottom: 10px;
    	line-height: 1.5;
    }
    /*.info-cus .item:last-child{
    	margin-bottom: 0;
    }*/
    .info-cus:first-child .item:last-child{
    	position: absolute;
    	bottom: 0;
    	right: 0;

    }
    .info-cus .info-cus-text{
        font-size: 14px;
        line-height: 1.5;
    }
    .info-cus .info-cus-text span{
        font-weight: 500; 
        color: #000; 
        margin-left: 0; 
        max-width: none;
    }
    .info-cus .info-cus-text span:first-child{
    	white-space: nowrap;
    }
    .info-cus .info-cus-text span:last-child{
        max-width: 100%;
    }
    .info-cus:nth-child(2) .item .box-item-tem .info-cus-text{
    	display: flex;
    }
    .info-cus .item:nth-child(2) .info-cus-text span:last-child{
        text-transform: uppercase;
        font-size: 14px;
    }
    .box-item-tem{
    	display: -webkit-box;
    	display: -ms-flexbox;
    	display: flex;
        -webkit-box-pack: baseline;
        -ms-flex-pack: baseline;
        justify-content: baseline;
    }
    .checkbox-print{
        font-size: 14px; 
        font-weight: 500;
        padding-left: 8px;
    }
    .checkbox-print span{
        width: 13px; 
        height: 13px; 
        display: -webkit-box; 
        display: -ms-flexbox; 
        display: flex; 
        -webkit-box-align: center; 
        -ms-flex-align: center; 
        align-items: center; 
        float: left; 
        margin-right: 5px; 
        font-weight: 500; 
        background: #000; 
        color: #fff; 
        border-radius: 4px; 
        font-size: 10px;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center
    }
    .info-cus:last-child{
        float: left; 
        width: 48%; 
        margin-left: 2%;
    }
    .info-cus:last-child .item{
        width: 100%;
    }
    .info-cus .item:last-child, .info-cus .item:last-child .box-img-meta{
    	margin-bottom: 0;
    }
    .info-cus:last-child .item:last-child{
    	position: absolute;
    	bottom: 0;
    	right: 0;
    }
    .list-tb-item ul li{
        float: left;
        color: #000;
    }
    .list-tb-item ul li:first-child, .list-tb-item ul li:last-child{
        -webkit-box-pack: center;
            -ms-flex-pack: center;
                justify-content: center;
    }
    .box-img-print{
        max-width: 27%;
    }

    .box-img-print img{
        width: 100%;
    }
    .custom-body-print{
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
            -ms-flex-align: center;
                align-items: center;
        -webkit-box-pack: center;
            -ms-flex-pack: center;
                justify-content: center;
        padding: 15px;
    }

    .info-print{
        max-width: 57%;
        padding: 0 5px;
    }

    .info-print ul{
        list-style-type: none;
        padding: 0;
        margin-bottom: 0;
    }
    .info-print ul li span{
        font-family: 'Roboto', sans-serif;
        font-weight: 500;
        font-size: 14px;
        margin-left: 10px;
    }
    .qr-code{
        max-width: 15%;
    }
    .qr-code img{
        max-width: 100%;
    }

    .box-body-print{
    	padding: 0 0 15px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
            -ms-flex-align: center;
                align-items: center;
        -webkit-box-pack: justify!important;
            -ms-flex-pack: justify!important;
                justify-content: space-between!important;
    }

    .custom-table tbody tr td{
        padding: 0 8px;
    }

    .price-table{
        font-family: 'Roboto',sans-serif;
        font-weight: 500;
        color: #e62e6b;
        font-size: 16px;
        display: inline-block;
    }
    .price-table p{
        margin-bottom: 0
    }

    .price-table span{
        font-weight: 500;
        color: #333;
    }

    .custom-tb-print>tbody>tr:nth-of-type(odd) {
        background-color: #fff!important;
    }

    .custom-tb-print>tbody>tr:nth-of-type(even) {
        background: #eeeeee;
    }

    .list-tb-item{
        display: inline-block;
        width: 100%;
        border: 1px solid #cbcbcb;
    }

    .list-tb-item ul{
        list-style-type: none;
        width: 100%;
        padding: 0;
        margin-bottom: 0;
        display: inline-block;
    }

    .list-tb-item ul li{
        float: left;
        width: 25%;
        font-size: 15px;
        font-family: 'Roboto',sans-serif;
        font-weight: 500;
        height: 35px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
            -ms-flex-align: center;
                align-items: center;
    }

    .list-tb-item ul:nth-child(odd){
        background: #eee;
    }
    .list-tb-item ul:first-child{
        color: #fff;
    }
    .list-tb-item ul li:first-child, .list-tb-item ul li:last-child{
        width: 70px;
        text-align: center;
    }
    .list-tb-item ul li:nth-child(2){
        width: 50%;
    }
    .info-cus-text{
    	font-size: 15px; 
    	display: inline-block; 
    	width: 100%;
    }
    .info-cus-text span{
    	font-weight: 500; 
    	color: #000; 
    	margin-left: 0; 
    	max-width: none;
    }
    .info-cus-text span:last-child{
    	max-width: 100%; 
    	font-weight: 500; 
    	color: #000; 
    	float: right;
    }
	@media (max-width: 575.98px) {
		.box-upload>.item{
			width: 100%;
		}
		.box-upload>.item:last-child{
			margin-left: 0;
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-upload>.item{
			width: 100%;
		}
		.box-upload>.item:last-child{
			margin-left: 0;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.box-upload>.item{
			width: 100%;
		}
		.box-upload>.item:last-child{
			margin-left: 0;
		}
	}
	@media (min-width: 1200px) {

	}
</style>
<main class="bannertem content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Banner tem đơn hàng</h1>
			<ul>
				<li>
					<a href="?action=order.php" class="link-custom black-custom" title="Thoát">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Trở về</label>
					</a>
				</li>
				<li>
					<button type="button" class="button button-header link-custom black-custom">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
                    </button>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-upload">
					<div class="item">
						<div class="upload-img">
	                        <div class="input-group up-img">
	                            <input autocomplete="off" type="text" id="ipt-img-1" class="form-control" placeholder="Chưa có hình ảnh nào được chọn (Hình 1)" readonly="">
	                            <label class="button bg-green custom-upload">
	                                <input type="file" id="ipt-img1" class="form-control" name="imgs1" onchange="readURL(this,1);" accept="image/*">Upload
	                            </label>
	                            <button type="button" id="delete-img-1" onclick="removeImg(1)" class="button bg-red custom-upload delete-img">Xóa</button>
	                        </div>
	                    </div>
	                    <div id="box-img-1" class="box-img-meta">
	                    	<img src="assets/images/collagen-sp.jpg" alt="collagen" class="photo1">
	                    </div>
	                    <div class="upload-img">
	                        <div class="input-group up-img">
	                            <input autocomplete="off" type="text" id="ipt-img-2" class="form-control" placeholder="Chưa có hình ảnh nào được chọn (Hình 2)" readonly="">
	                            <label class="button bg-green custom-upload">
	                                <input type="file" id="ipt-img2" class="form-control" name="imgs2" onchange="readURL(this,2);" accept="image/*">Upload
	                            </label>
	                            <button type="button" id="delete-img-2" onclick="removeImg(2)" class="button bg-red custom-upload delete-img">Xóa</button>
	                        </div>
	                    </div>
	                    <div id="box-img-2" class="box-img-meta">
	                    	<img src="assets/images/collagen-sp.jpg" alt="collagen" class="photo2">
	                    </div>
					</div>
					<div class="item">
						<div class="box-body-print">
	                        <div class="box-img-print">
	                            <img src="../assets/images/logo-tem.png" alt="logo">
	                        </div>
	                        <div class="info-print">
	                            <ul>
	                                <li><i class="fa fa-location-arrow" aria-hidden="true"></i> <span>Kanagawa Ken Ebina -Sbi Nakana, Japan</span></li>
	                                <li><i class="fa fa-location-arrow" aria-hidden="true"></i> <span>76 Nguyễn Háo Vĩnh, P. Tân Quý, Q. Tân Phú, TP. Hồ Chí Minh</span></li>
	                                <li><i class="fa fa-phone" aria-hidden="true"></i> <span>(028) 7108 8889 - 0935 600 800</span></li>
	                                <li><i class="fa fa-globe" aria-hidden="true"></i> <span>https://japana.vn</span></li>
	                            </ul>
	                        </div>
	                        <div class="qr-code">
	                            <img src="../assets/images/br-code.png" alt="br-code">
	                        </div>
	                    </div>
	                    <div class="info-cus">
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Mã đơn hàng:</span> 
	                                    <span>JP_001</span>
	                                </p>
	                            </div>
	                        </div>

	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Đơn vị vận chuyển:</span> 
	                                    <span>Giao hàng tiết kiệm</span>
	                                </p>
	                            </div>
	                        </div>

	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Ngày xuất phiếu:</span> 
	                                    <span>13-02-2019</span>
	                                </p>
	                            </div>
	                        </div>

	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Mã vận đơn:</span> 
	                                    <span>GHTK-001</span>
	                                </p>
	                            </div>
	                        </div>

	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Tên khách hàng:</span> 
	                                    <span>JAPANA</span>
	                                </p>
	                            </div>
	                        </div>

	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Địa chỉ:</span> 
	                                    <span>76 Nguyễn Háo Vĩnh, P. Tân Quý, Q. Tân Phú, TP. Hồ Chí Minh</span>
	                                </p>
	                            </div>
	                        </div>

	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text" style="width: auto;">
	                                    <span>Lưu ý:</span> 
	                                </p>
	                                <p class="checkbox-print">
	                                    <span><i class="fa fa-check" aria-hidden="true"></i></span> Hàng dễ vỡ
	                                </p>
	                                <p class="checkbox-print">
	                                    <span><i class="fa fa-check" aria-hidden="true"></i></span> Được phép kiểm tra hàng
	                                </p>
	                            </div>
	                        </div>
							<div class="item">
								<p class="checkbox-print">
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span> Chỉ cho khách xem hàng - không được tự ý bóc seal.
                                </p>
							</div>
							<div class="item">
								<p class="checkbox-print">
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span> Không liên hệ được với khách, báo ngay cho siêu thị Nhật Bản Japana, KHÔNG TỰ Ý TRẢ HÀNG.
                                </p>
							</div>
	                        <div class="item">
	                            <div class="box-img-meta">
			                    	<img src="assets/images/no-img.png" alt="images" class="photo1">
			                    </div>
	                        </div>
	                    </div>
	                    <div class="info-cus">
	                        <div class="item">
	                            <div class="box-item">
	                                <div class="list-tb-item">
	                                    <ul>
	                                        <li>STT</li>
	                                        <li>Tên sản phẩm</li>
	                                        <li>SL</li>
	                                    </ul>
	                                    
	                                    <ul>
	                                        <li>1</li>
	                                        <li>(SH-001) Sản phẩm 1</li>
	                                        <li>1</li>
	                                    </ul>
	                                    <ul>
	                                        <li>2</li>
	                                        <li>(SH-002) Sản phẩm 2</li>
	                                        <li>1</li>
	                                    </ul>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text">
	                                    <span>Tổng cộng:</span> 
	                                    <span>1,000,000 đ</span>
										
	                                </p>
	                            </div>
	                        </div>
							<div class="item">
	                            <div class="box-item">
	                                <p class="info-cus-text" style="">
	                                    <span>Chuyển khoản: </span> 
	                                    <span>- 0 đ</span>
	                                </p>
	                            </div>
	                        </div>
							<div class="item">
	                            <div class="box-item">
									<p class="info-cus-text">
	                                    <span>COD thu hộ:</span> 
	                                    <span>0  đ</span>
	                                </p>
	    						</div>
	                        </div>
	                        <div class="item">
	                            <div class="box-img-meta">
			                    	<img src="assets/images/no-img.png" alt="images" class="photo2">
			                    </div>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	function readURL(input,id) {
	    if (input.files && input.files[0]) {
	    	var first = jQuery('.info-cus').first().outerHeight()
	        var reader = new FileReader();
	        reader.onload = function (e) {
	        	jQuery('.photo'+id).parent().css('display','block');
	        	if(jQuery('.content-sidebar-wrap').outerHeight() < jQuery('.nav-primary').outerHeight()){
					jQuery('.nav-primary').css('height','auto');	
				}else if(jQuery('.content-sidebar-wrap').outerHeight() > jQuery('body').outerHeight()){
					jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
				}
				else {
					jQuery('.nav-primary').css('height',jQuery('body').outerHeight());	
				}
	          	jQuery('.photo'+id).attr('src', e.target.result);
	          	jQuery('.info-cus').last().css('height',first);
	        };
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	function removeImg(id){
		jQuery('#ipt-img'+id).parent().find('input[type=file]').val('');
	    jQuery('#ipt-img-'+id).parent().find('input[type=text]').val('');
	    jQuery('#box-img-'+id).css('display','none');
	    jQuery('#delete-img-'+id).css('display','none');
	    jQuery('#delete-img-'+id).prev().css('display','flex');
	    jQuery('.photo'+id).parent().css('display','none');
	    jQuery('.photo'+id).attr('src','');
	}
	jQuery(function(){
		jQuery(document).on('change', ':file', function() {
		    var input = jQuery(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		});
		jQuery(':file').on('fileselect', function(event, numFiles, label) {
          	var input = jQuery(this).parents('.up-img').find(':text'),
              	log = numFiles > 1 ? numFiles + ' files selected' : label;
          	if( input.length ) {
              	input.val(log);
              	if(input.val(log)){
                	jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
                	jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
              	}
          	} else {
              	if(log) ;
          	}
      	});
	})
</script>