<style>
  .timeline {
    position: relative;
    max-width: 1200px;
    margin: 0 auto;
  }
  .timeline::after {
    content: '';
    position: absolute;
    width: 2px;
    background-color: white;
    top: 0;
    bottom: 0;
    left: 50%;
    margin-left: -1px;
  }
  .container-timeline {
    padding: 10px 40px;
    position: relative;
    background-color: inherit;
    width: 50%;
  }
  .container-timeline::after {
    content: '';
    position: absolute;
    width: 20px;
    height: 20px;
    right: -10px;
    background-color: white;
    top: 22px;
    border-radius: 100%;
    z-index: 1;
    border: 2px solid green;
  }
  .left-timeline {
    left: 0;
  }
  .right-timeline {
    left: 50%;
  }
  .left-timeline::before {
    content: " ";
    height: 0;
    position: absolute;
    top: 22px;
    width: 0;
    z-index: 1;
    right: 30px;
    border: medium solid white;
    border-width: 10px 0 10px 10px;
    border-color: transparent transparent transparent white;
  }
  .right-timeline::before {
    content: " ";
    height: 0;
    position: absolute;
    top: 22px;
    width: 0;
    z-index: 1;
    left: 30px;
    border: medium solid white;
    border-width: 10px 10px 10px 0;
    border-color: transparent white transparent transparent;
  }
  .right-timeline::after {
    left: -10px;
  }
  .content-timeline {
    padding: 20px 30px;
    background-color: white;
    position: relative;
    border-radius: 6px;
    line-height: 1.5;
  }
  .content-timeline .date-timeline{
    display: inline-block;
    margin-bottom: 5px;
    font-size: 12px;
  }
  @media (max-width: 575.98px) {
    .timeline::after {
      left: 31px;
    }
    .container-timeline {
      width: 100%;
      padding-left: 70px;
      padding-right: 25px;
    }
    .container-timeline::before {
      left: 60px;
      border: medium solid white;
      border-width: 10px 10px 10px 0;
      border-color: transparent white transparent transparent;
    }
    .left-timeline::after, .right-timeline::after {
      left: 21px;
    }
    .right-timeline {
      left: 0%;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {

  }
  @media (min-width: 768px) and (max-width: 991.98px) {

  }
  @media (min-width: 992px) and (max-width: 1199.98px) {
    
  }
  @media (min-width: 1200px) {
    
  }
</style>
<div class="modal large-modal" id="log-cart-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Lịch sử thay đổi trạng thái
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body bg-gray">
        <div class="timeline">
          <div class="container-timeline left-timeline">
            <div class="content-timeline">
              <h2 class="green-custom">Hoàn thành</h2>
              <span class="date-timeline">17-09-2019 | 09:55:08</span>
              <p>Nhân viên đã thay đổi trạng thái.</p>
            </div>
          </div>
          <div class="container-timeline right-timeline">
            <div class="content-timeline">
              <h2 class="green-custom">Đang giao hàng</h2>
              <span class="date-timeline">17-09-2019 | 09:55:08</span>
              <p>Nhân viên <span class="green-custom">Kho</span> : <span class="red-custom"> Nguyễn Thái Hưng Thượng </span> đã thay đổi trạng thái.</p>
            </div>
          </div>
          <div class="container-timeline left-timeline">
            <div class="content-timeline">
              <h2 class="green-custom">Lập lệnh xuất</h2>
              <span class="date-timeline">17-09-2019 | 09:55:08</span>
              <p>Nhân viên <span class="green-custom">Kế toán</span> : <span class="red-custom"> Phan Thị Anh Nguyệt </span>  đã thay đổi trạng thái.</p>
            </div>
          </div>
          <div class="container-timeline right-timeline">
            <div class="content-timeline">
              <h2 class="green-custom">Chờ duyệt</h2>
              <span class="date-timeline">17-09-2019 | 09:55:08</span>
              <p>Nhân viên <span class="green-custom">Xử lý đơn hàng</span> : <span class="red-custom"> Bùi Tuấn Anh </span>  đã thay đổi trạng thái.</p>
            </div>
          </div>
          <div class="container-timeline left-timeline">
            <div class="content-timeline">
              <h2 class="yellow-custom">Chờ hàng có ngày</h2>
              <span class="date-timeline">17-09-2019 | 09:55:08</span>
              <p>Nhân viên <span class="green-custom">Thu mua</span> : <span class="red-custom"> Diễm Hương </span>  đã thay đổi trạng thái.</p>
            </div>
          </div>
          <div class="container-timeline right-timeline">
            <div class="content-timeline">
              <h2 class="yellow-custom">Chờ hàng không ngày</h2>
              <span class="date-timeline">17-09-2019 | 09:55:08</span>
              <p>Nhân viên <span class="green-custom">Thu mua</span> : <span class="red-custom"> Diễm Hương </span>  đã thay đổi trạng thái.</p>
            </div>
          </div>
          <div class="container-timeline left-timeline">
            <div class="content-timeline">
              <h2 class="blue-custom">Đã xác nhận</h2>
              <span class="date-timeline">17-09-2019 | 09:55:08</span>
              <p>Nhân viên <span class="green-custom">Tư vấn bán hàng</span> : <span class="red-custom"> Nguyễn Hà My </span>  đã thay đổi trạng thái.</p>
            </div>
          </div>
          <div class="container-timeline right-timeline">
            <div class="content-timeline">
              <h2 class="green-custom">Mới đặt</h2>
              <span class="date-timeline">17-09-2019 | 09:55:08</span>
              <p>Khách hàng <span class="green-custom">Japana</span> vừa đặt hàng.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  
</script>