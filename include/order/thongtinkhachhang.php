<style>
	.customer-dropdown{
		margin-top: 15px;
	}
	.edit-box .custom-dropdown{
		display: block;
	}
	.box-item-customer .item{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		width: 100%;
		margin-top: 10px;
		min-height: 38px;
	}
	.box-item-customer>.item:nth-child(4){
		-webkit-box-align: initial;
		    -ms-flex-align: initial;
		        align-items: initial;
	}
	.box-item-customer .item p{
		display: inline-block;
		width: 22%;
		float: left;
		padding-left: 15px;
	}
	.box-item-customer .item p span{
		line-height: 1.5;
	}
	.box-item-customer>.item:nth-child(4) p{
		padding-top: 12px;
	}
	.box-item-customer .item p:last-of-type{
		float: left;
	    width: 78%;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: justify;
	        -ms-flex-pack: justify;
	            justify-content: space-between;
		padding-left: 0;
		padding-top: 0;
	}
	.box-item-customer .item:last-child{
		height: 38px;
	}
	.box-item-customer .item a{
		float: left;
	}
	.box-item-customer .item .edit-box{
		display: none;
		float: left;
		width: 100%;
	}
	.box-item-customer .item .edit-box input{
		width: 70%;
		float: left;
	}
	.box-button{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    width: 30%;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	    float: left;
	}
	.item-addr{
		width: 70%;
		display: inline-block;
    	float: left;
	}
	.item-addr .item{
		display: inline-block;
		width: 100%;
		margin-top: 0;
		float: left;
		margin-top: 10px;
	}
	.item-addr .item:first-child{
		margin-top: 0;
	}
	.item-addr .item:last-child input{
		width: 100%;
	}
	@media (max-width: 575.98px) {
		.box-item-customer .item p{
	  		padding-left: 0;
	  	}
	  	#note-kh{
	  		margin-left: 0;
	  	}
	  	.box-item-customer .item{
	  		display: inline-block;
	  		margin-top: 15px;
	  		border-bottom: 1px solid #eee;
	  	}
	  	.box-item-customer .item .edit-box{
	  		margin-bottom: 10px;
	  	}
	  	.box-item-customer .item p{
	  		width: 100%;
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
    		-webkit-box-align: center;
    		    -ms-flex-align: center;
    		        align-items: center;
    		margin-bottom: 5px;
	  	}
	  	.box-item-customer .item p:last-of-type{
	  		width: 100%;
	  		-webkit-box-align: baseline;
	  		    -ms-flex-align: baseline;
	  		        align-items: baseline;
	  	}
	  	.box-item-customer .item .link-custom{
	  		height: auto;
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
    		-webkit-box-align: center;
    		    -ms-flex-align: center;
    		        align-items: center;
    		-webkit-box-pack: end;
    		    -ms-flex-pack: end;
    		        justify-content: flex-end;
    		float: right;
    		font-size: inherit;
	  		width: 20%;
	  	}
	  	.box-item-customer .item .link-custom i{
	  		width: auto;
	  		margin-bottom: 0;
	  		margin-right: 5px;
	  	}
	  	.box-item-customer .item span{
	  		line-height: 1.5;
		    display: inline-block;
		    float: left;
		    width: 78%;
	  	}
	  	.box-item-customer>.item:nth-child(4) p{
	  		padding-top: 0;
	  	}
	  	.box-item-customer .item:last-child{
	  		border-bottom: 0;
	  	}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-info-cart>.item:nth-child(2){
	  		margin-left: 0;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.box-info-cart>.item:nth-child(2){
	  		margin-left: 0;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
	<a class="dropdown-collapse customer-dropdown bg-black" href="javascript:void(0);">Thông tin khách hàng - Vip Platium</a>
	<div class="custom-collapse">
		<div class="box-item-customer">
			<div class="item">
				<p class="bold">Họ và tên: </p>
				<p id="p_info_name" class="normal">
					<span>Nguyễn Văn A</span>
					<a href="javascript:void(0);" class="open-form-edit link-custom black-custom" title="Chỉnh sửa">
						<i class="fa fa-pencil-square-o"></i> Edit
					</a>
				</p>
				<div class="edit-box">
					<input type="text" id="info_name" value="Nguyễn Văn A" placeholder="Họ và tên..." name="info_name" class="form-control">
					<div class="box-button">
						<button onclick="thongtinkh();" type="button" class="button button-header green-custom">
							<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu
						</button>
						<button type="button" class="button button-header red-custom" onclick="hideform('p_info_name');">
							<i class="ti-close" aria-hidden="true"></i> Xoá
						</button>
					</div>
				</div>
			</div>
			<div class="item">
				<p class="bold">Số điện thoại: </p>
				<p id="p_info_mobile" class="normal">
					<span>0987654321</span>
					<a href="javascript:void(0);" class="open-form-edit link-custom black-custom" title="Chỉnh sửa">
						<i class="fa fa-pencil-square-o"></i> Edit
					</a>
				</p>
				<div class="edit-box">
					<input type="number" value="0987654321" id="info_mobile" name="info_mobile" class="form-control">
					<div class="box-button">
						<button onclick="thongtinkh();" type="button" class="button button-header green-custom">
							<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu
						</button>
						<button type="button" class="button button-header red-custom" onclick="hideform('p_info_mobile');">
							<i class="ti-close" aria-hidden="true"></i> Xoá
						</button>
					</div>
				</div>
			</div>
			<div class="item">
				<p class="bold">Email: </p>
				<p id="p_info_email" class="normal">
					<span>info@japana.vn</span>
					<a href="javascript:void(0);" class="open-form-edit link-custom black-custom" title="Chỉnh sửa">
						<i class="fa fa-pencil-square-o"></i> Edit
					</a>
				</p>
				<div class="edit-box">
					<input type="email" value="info@japana.vn" id="info_email" name="info_email" class="form-control">
					<div class="box-button">
						<button onclick="thongtinkh();" type="button" class="button button-header green-custom">
							<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu
						</button>
						<button type="button" class="button button-header red-custom" onclick="hideform('p_info_email');">
							<i class="ti-close" aria-hidden="true"></i> Xoá
						</button>
					</div>
				</div>
			</div>
			<div class="item">
				<p class="bold">Địa chỉ: </p>
				<p id="p_info_address" class="normal">
					<span>76 Nguyễn Háo Vĩnh, P. Tân Quý, Q. Tân Phú, TP. HCM</span>
					<a href="javascript:void(0);" class="open-form-edit link-custom black-custom" title="Chỉnh sửa">
						<i class="fa fa-pencil-square-o"></i> Edit
					</a>
				</p>
				<div class="edit-box">
					<div class="item-addr">
						<div class="item">
							<div class="custom-dropdown">
								<select class="form-control" onchange="thanhpho()">
						    		<option value="-1">Chọn thành phố</option>
								  	<option value="0">TP.HCM</option>
								  	<option value="1">Hà Nội</option>
								</select>
							</div>
						</div>
						<div class="item">
							<div class="custom-dropdown">
								<select class="form-control" onchange="quanhuyen()">
						    		<option value="-1">Chọn quận</option>
								  	<option value="0">Quận 1</option>
								  	<option value="1">Quận 2</option>
								</select>
							</div>
						</div>
						<div class="item">
							<div class="custom-dropdown">
								<select class="form-control">
						    		<option value="-1">Chọn phường</option>
								  	<option value="0">Phú Thạnh</option>
								  	<option value="1">Phú Trung</option>
								</select>
							</div>
						</div>
						<div class="item">
							<input type="text" value="76 Nguyễn Háo Vĩnh" id="info_address" name="info_address" class="form-control">
						</div>
					</div>
					<div class="box-button">
						<button onclick="addressSave();" type="button" class="button button-header green-custom">
							<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu
						</button>
						<button type="button" class="button button-header red-custom" onclick="hideform('p_info_address');">
							<i class="ti-close" aria-hidden="true"></i> Xoá
						</button>
					</div>
				</div>
			</div>
			<div class="item">
				<p class="bold green-custom">Chuyển khoản: </p>
				<p id="p_info_cod">
					<span class="bold green-custom">0 đ</span>
					<a href="javascript:void(0);" class="open-form-edit link-custom black-custom" title="Chỉnh sửa">
						<i class="fa fa-pencil-square-o"></i> Edit
					</a>
				</p>
				<div class="edit-box">
					<input type="number" name="money" id="money_payment1" onchange="txt_onkeypress()" class="form-control" value="0">
					<div class="box-button">
						<button onclick="thongtinkh();" type="button" class="button button-header green-custom">
							<i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu
						</button>
						<button type="button" class="button button-header red-custom" onclick="hideform('p_info_cod');">
							<i class="ti-close" aria-hidden="true"></i> Xoá
						</button>
					</div>
				</div>
			</div>
			<div class="item">
				<p class="bold">Loại khách hàng: </p>
				<p id="p_info_customer" class="normal">
					Mới
				</p>
			</div>
			<div class="item">
				<p class="bold">Mã theo dõi: </p>
				<p id="p_info_follow" class="normal">
					DUTpgz
				</p>
			</div>
			<div class="item">
				<p class="bold">Hình thức thanh toán: </p>
				<p id="p_info_payment" class="normal">
					Tiền mặt
				</p>
			</div>
		</div>
	</div>

<script>
	jQuery(function(){
		jQuery('.open-form-edit').click(function(){
			jQuery(this).parent().hide();
			jQuery(this).parent().next().show();
			jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
		});
		
	});
	function hideform(id){
		jQuery("#"+id).show();
		jQuery("#"+id).next().hide();
		jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
	}
</script>