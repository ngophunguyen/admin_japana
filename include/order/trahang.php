<style>
  #huydon-modal .modal-body button{
    margin-top: 15px;
    float: right;
  }
  .box-radio,.box-payback{
    display: inline-block;
    width: 100%;
    float: left;
  }
  .box-radio .item{
    float: left;
    width: 30%;
  }
  .box-payback{
    margin-bottom: 15px;    
  }
  .box-payback .item{
    display: inline-block;
    width: 32%;
    float: left;
  }
  .box-payback .item:nth-child(2){
    margin: 0 2%;
  }
  .box-payback .item .custom-dropdown{
    display: block;
  }
  .box-ghichu-trahang label,.box-payback .item label {
    font-weight: 500;
    font-size: 14px;
    margin: 15px 0;
  }
  .box-ghichu-trahang label{
    margin-top: 0;
  }
  .box-ghichu-trahang{
    display: inline-block;
    float: left;
    width: 49%;
    margin-bottom: 15px;
    margin-left: 2%;
  }
  .table-payback tr td img{
    width: auto;
    height: 60px;
  }
  .table-payback tr th:nth-child(2), .table-payback tr td:nth-child(2){
    width: 130px;
  }
  .table-payback{
    display: none;
  }
  .table-payback thead {
      border-bottom: 1px solid #eee;
  }
  .box-payback-table{
    display: inline-block;
    float: left;
    width: 100%;
  }
  #trahang-modal tr td:last-child{
    max-width: auto;
  }
  @media (max-width: 575.98px) {
    .box-payback .item{
      width: 100%;
    }
    .box-payback .item:nth-child(2){
      margin: 0;
    }
    .box-ghichu-trahang{
      width: 100%;
    }
    .table-payback tr th:nth-child(2), .table-payback tr td:nth-child(2){
      width: 100%;
    }
    .table-custom tr td:first-child{
      display: none;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {

  }
  @media (min-width: 768px) and (max-width: 991.98px) {
    .box-ghichu-trahang{
        width: 60%;
    }
  }
  @media (min-width: 992px) and (max-width: 1199.98px) {
    
  }
  @media (min-width: 1200px) {
    
  }
</style>
<div class="modal large-modal" id="trahang-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Trả hàng
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="box-radio">
          <div class="item">
            <label class="radio-custom">
              <input value="1" id="radio1" type="radio" name="trahang[radio]" checked="checked">
              <span class="checkmark"></span>
              Trả toàn bộ đơn
            </label>
          </div>
          <div class="item">
            <label for="radiojp" class="checkbox-custom">Hàng còn trong kho
              <input id="radiojp" name="trahang[kho]" type="checkbox" onclick="checkKho();">
              <span class="checkmark"></span>
            </label>
          </div>
          <div class="item">
            <label class="radio-custom">
              <input value="2" id="radio2" type="radio" name="trahang[radio]">
              <span class="checkmark"></span>
              Trả 1 phần đơn
            </label>
          </div>
        </div>
        <div class="box-payback">
          <div class="item">
            <label class="black-custom">
              Chọn đơn vị vận chuyển
            </label>
            <div class="custom-dropdown">
              <select class="form-control" name="trahang[dvvc]" id="cbo_transport">
                <option value="-1">Chọn đơn vị...</option>
                <option value="0">Giao hàng tiết kiệm</option>
                <option value="1">Giao hàng nhanh</option>
                <option value="2">Ship an toàn</option>
              </select>
            </div>          
          </div>
          <div class="item">
            <label class="black-custom">
              Phí ship trả
            </label>
            <input autocomplete="off" value="" class="form-control" placeholder="Nhập số tiền..." id="cost_transport" name="trahang[tiendvvc]" type="text">
          </div>
          <div class="item">
            <label class="black-custom">
              Nhập số tiền
            </label>
            <input autocomplete="off" value="" class="form-control" placeholder="Nhập số tiền..." id="cost_transport" name="trahang[tientra]" type="text">
          </div>
        </div>
        <div class="box-payback-table">
          <div class="box-table">
            <table class="table table-custom table-payback table-responsive">
              <thead>
                  <tr>
                      <th class="black-custom bold center-custom">STT</th>
                      <th class="black-custom bold">SKU</th>
                      <th class="black-custom bold">Tên sản phẩm</th>
                      <th class="black-custom bold center-custom">Hình ảnh</th>
                      <th class="black-custom bold">Giá</th>
                      <th class="black-custom bold center-custom">Số lượng</th>
                      <th class="black-custom bold center-custom">Tổng tiền trả</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td data-title="STT" class="center-custom">1</td>
                      <td data-title="SKU">087AA9</td>
                      <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
                      <td data-title="Hình ảnh" class="center-custom">
                        <img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
                      </td>
                      <td data-title="Giá"><span class="first-price">2.600.000 đ</span> <span>2.340.000 đ</span></td>
                      <td data-title="Số lượng" class="center-custom">
                        <input type="text" value="0" name="trahang" class="form-control">
                      </td>
                      <td data-title="Tổng tiền trả" class="center-custom">
                        <input type="text" value="0" name="trahang" data-qty="" min="0" max="" class="form-control">
                      </td>
                  </tr>
                  <tr>
                      <td data-title="STT" class="center-custom">2</td>
                      <td data-title="SKU">087AA7</td>
                      <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml v2</td>
                      <td data-title="Hình ảnh" class="center-custom">
                        <img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
                      </td>
                      <td data-title="Giá"><span class="first-price">2.800.000 đ</span> <span>3.000.000 đ</span></td>
                      <td data-title="Số lượng" class="center-custom">
                        <input type="text" value="0" name="trahang" class="form-control">
                      </td>
                      <td data-title="Tổng tiền trả" class="center-custom">
                        <input type="text" value="0" name="trahang" data-qty="" min="0" max="" class="form-control">
                      </td>
                  </tr>
                  <tr>
                      <td data-title="STT" class="center-custom">3</td>
                      <td data-title="SKU">KMNCC-079AA1 </td>
                      <td data-title="Tên sản phẩm">Khuyến mãi Ly Sứ Cao Cấp Kinohimitsu</td>
                      <td data-title="Hình ảnh" class="center-custom">
                        <img src="https://japana.vn/uploads/product/2018/08/23/1534988772-KMNCC-079AA1.jpg" alt="img">
                        <img class="gift-order" src="https://japana.vn/assets/images/gift.png" alt="img">
                      </td>
                      <td data-title="Giá"><span>0 đ</span></td>
                      <td data-title="Số lượng" class="center-custom">
                        <input type="text" value="0" name="trahang" class="form-control">
                      </td>
                      <td data-title="Tổng tiền trả" class="center-custom">
                        <input type="text" value="0" name="trahang" data-qty="" min="0" max="" class="form-control">
                      </td>
                  </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="total-order-box custom-fleft">
          <p class="black-custom">
            Tổng COD: <span>1,000,000 đ</span>
          </p>
          <p class="black-custom">
           Giá trị trả: <span>500,000 đ</span>
          </p>
          <p class="black-custom">
            COD còn lại: <span>500,000 đ</span>
          </p>
        </div>
        <div class="box-ghichu-trahang">
          <label class="black-custom">
            Lý do trả hàng
          </label>
          <textarea required="" rows="2" class="form-control"></textarea>
        </div>
        <button type="button" class="button bg-red custom-fright">Trả hàng</button>
      </div>
    </div>
  </div>
</div>
<script>
  var op = document.getElementById("cbo_transport").getElementsByTagName("option");
    for (var i = 0; i < op.length; i++) {
        if(op[i].value === '17'){op[i].disabled = true};
    }

    function checkKho() {
        if (document.getElementById('radiojp').checked) {
            document.getElementById('cbo_transport').value = $('#radiojp').val();
            document.getElementById('cbo_transport').disabled = true;
            var op = document.getElementById("cbo_transport").getElementsByTagName("option");
            for (var i = 0; i < op.length; i++) {
                if(op[i].value === '17')
                {op[i].disabled = false};
            }

        } else {
            document.getElementById('cbo_transport').disabled = false;
            document.getElementById('cbo_transport').value = '';

            var op = document.getElementById("cbo_transport").getElementsByTagName("option");
            for (var i = 0; i < op.length; i++) {
                if(op[i].value === '17')
                {op[i].disabled = true};
            }

        }
    }

    var tra = 0;

    function cal_qty() {
      if ($("#cbo_transport").val()=="" ) {
          alert('Cần chọn đơn vị vận chuyển!');
          return;
      }
      if (document.getElementById('radio2') != null && document.getElementById('radio2').checked) {
          var qty_return_input = document.getElementsByClassName("qty_return");
          var i;
          var qty = 0;
          var qty_return = 0;

          for (i = 0; i < qty_return_input.length; i++) {
              qty += Number(qty_return_input[i].getAttribute("data-qty"));
              qty_return += Number(qty_return_input[i].value);
          }
          if (qty_return == 0) {
              alert('Số lượng trả tối thiểu 1 sản phẩm');
              return false;
          }
          if (qty == qty_return) {
              return confirm('Bạn có chắc trả toàn bộ đơn?');
          }
          if (qty < qty_return) {
              return confirm('Nhập quá số lượng');
          }
      }
      if($("#textNote").val()==""){
          alert('Cần nhập lý do trả hàng!');
          return;
      }

      document.getElementById("trahang_submit").submit();
  }
  jQuery(function(){
    jQuery('.radio-custom').change(function() {
      if (jQuery('#radio2').prop('checked')) {
        jQuery('.box-payback .item:last-child').hide();
        jQuery('.table-payback').show();
      } else {
        jQuery('.box-payback .item:last-child').show();
        jQuery('.table-payback').hide();
      }
    });
  });
</script>