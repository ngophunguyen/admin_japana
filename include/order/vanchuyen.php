<style>
	.vanchuyen-api, .more-info-dvvc, .ship-box-other, .api-shipper, .api-shipper .item{
		display: inline-block;
		width: 100%;
	}
	.vanchuyen-api>.item{
		width: 49%;
		float: left;
	}
	.vanchuyen-api>.item:last-child{
		margin-left: 2%;
	}
	.ship-box .item, .ship-box-other .item{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		margin-left: 0;
		margin-top: 5px;
	}
	.api-shipper .item .box-input-shipper{
		margin-bottom: 20px;
	}
	.ship-box .item label, .vanchuyen-api .item .title, .ship-box-other .item label, .api-shipper .item>label{
		font-weight: 500;
    	font-size: 14px;
    	margin: 15px 0;
    	width: 30%;
    	float: left;
	}
	.api-shipper .item>label{
		width: 100%;
		margin-top: 0;
	}
	.ship-box-other .item label{
		width: 30%;
	}
	.vanchuyen-api .item .title{
		width: 100%;
		border-bottom: 1px solid #ccc;
		font-size: 15px;
		float: left;
	}
	.btn-cancel-dvvc, .btn-close-dvvc, #dvvc_khoiluong, .more-info-dvvc, .ship-box-other{
	    display: none;
	}
	.box-input-shipper, .button-action{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
	}
	.box-input-shipper{
		width: 70%;
		float: left;
	}
	.api-shipper .item .box-input-shipper{
		padding-left: 30px;
		padding-right: 30px;
		width: 100%;
	}
	.box-input-shipper label{
		width: auto;
	}
	.active-api {
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    width: 100%;
	    border: 2px solid #ff0000;
	    padding: 12px 5px;
	}
	.default-ship .item:nth-child(6){
		margin-bottom: 15px;
	}
	@media (max-width: 575.98px) {
		.ship-box .item, .ship-box-other .item, .ship-box .item label, .vanchuyen-api>.item, .ship-box-other .item label{
			display: inline-block;
			width: 100%;
		}
		.box-input-shipper{
			width: 100%;
		}
		.ship-box .item label, .ship-box-other .item label{
			margin: 10px 0;
		}
		.btn-edit-dvvc, .btn-show-dvvc{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}
		.box-input-shipper .link-custom{
			font-size: inherit;
			margin-left: 10px;
		}
		.link-custom i{
			margin-right: 5px;
		}
		.more-info-dvvc .item label{
			margin-bottom: 15px;
		}
		.ship-box button, .ship-box-other button{
			margin-top: 15px;
		}
		.default-ship .item{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}
		.api-shipper .item .box-input-shipper{
			padding-left: 15px;
			padding-right: 15px;
		}
		.default-ship .item:nth-child(6){
			margin-bottom: 0;
		}
		.button-action a{
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
		}
		.ship-box button{
			margin-top: 0;
			margin-bottom: 15px;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.ship-box .item, .ship-box-other .item, .ship-box .item label, .vanchuyen-api>.item, .ship-box-other .item label{
			display: inline-block;
			width: 100%;
		}
		.box-input-shipper{
			width: 100%;
		}
		.ship-box .item label, .ship-box-other .item label{
			margin: 10px 0;
		}
		.btn-edit-dvvc, .btn-show-dvvc{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}
		.link-custom i{
			margin-right: 5px;
		}
		.more-info-dvvc .item label{
			margin-bottom: 15px;
		}
		.ship-box button, .ship-box-other button{
			margin-top: 15px;
		}
		.default-ship .item{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}
		.api-shipper .item .box-input-shipper{
			padding-left: 15px;
			padding-right: 15px;
		}
		.default-ship .item:nth-child(6){
			margin-bottom: 0;
		}
		.vanchuyen-api>.item:last-child{
			margin-left: 0;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.ship-box .item, .ship-box-other .item, .ship-box .item label, .ship-box-other .item label{
			display: inline-block;
			width: 100%;
		}
		.box-input-shipper{
			width: 100%;
		}
		.ship-box .item label{
			margin: 5px 0;
		}
		.ship-box button, .ship-box-other button{
			margin-top: 15px;
		}
		.default-ship .item{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.default-ship .item:nth-child(6){
			margin-bottom: 0;
		}

	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<div class="vanchuyen-api">
	<div class="item">
		<a class="dropdown-collapse customer-dropdown bg-black" href="javascript:void(0);">
			Thông tin gói hàng
		</a>
		<div class="ship-box">
			<div class="item">
				<label class="black-custom">
					Khối lượng:
				</label>
				<div class="box-input-shipper">
					<span>500 gram</span>
					<input type="text" class="form-control" id="dvvc_khoiluong" name="dvvc_khoiluong" placeholder="Nhập khối lượng">
					<div class="button-action">
						<a href="javascript:void(0);" onclick="hideform3('dvvc_khoiluong');" class="link-custom black-custom btn-cancel-dvvc">
	                        <i class="fa fa-times" aria-hidden="true"></i> Hủy
	                    </a>
	                    <a href="javascript:void(0);" onclick="showform3('dvvc_khoiluong');" class="link-custom black-custom btn-edit-dvvc">
	                        <i class="fa fa-pencil-square-o"></i> Edit
	                    </a>
	                    <a href="javascript:void(0);" onclick="showform2();" class="link-custom black-custom btn-show-dvvc">
	                        <i class="fa fa-caret-down" aria-hidden="true"></i> Thêm 
	                    </a>
	                    <a href="javascript:void(0);" onclick="hideform2();" class="link-custom black-custom btn-close-dvvc">
	                        <i class="fa fa-caret-up" aria-hidden="true"></i> Đóng 
	                    </a>
					</div>
				</div>
			</div>
			<div class="more-info-dvvc">
				<div class="item">
					<label class="black-custom">
						Mã khuyến mãi:
					</label>
					<div class="box-input-shipper">
						<input type="text" class="form-control" id="dvvc_makm" name="dvvc_makm" placeholder="Nhập mã khuyến mãi">
					</div>
				</div>
				<div class="item">
					<label class="black-custom">
						COD thu hộ:
					</label>
					<div class="box-input-shipper">
						<span>1.950.000 đ</span>
					</div>
				</div>
				<div class="item">
					<label class="black-custom">
						Ghi chú:
					</label>
					<div class="box-input-shipper">
						<input type="text" class="form-control" id="dvvc_ghichu" name="dvvc_ghichu" placeholder="Nhập ghi chú">
					</div>
				</div>
			</div>
			<button type="submit" class="button bg-black">Tính phí</button>
		</div>
		<h3 class="title red-custom">Cước phí</h3>
		<div class="api-shipper">
			<div class="item">
				<label class="black-custom">
					Giao hàng nhanh:
				</label>
				<div class="box-input-shipper">
					<label class="radio-custom" for="radiodvvc_3">
		              	<input value="1N" id="radiodvvc_3" type="radio" name="dvvc_service" checked="">
		              	<span class="checkmark"></span>
		              	1 ngày
		            </label>
		            <span>50.000 đ</span>
				</div>
			</div>
			<div class="item">
				<label class="black-custom">
					Tín tốc:
				</label>
				<div class="box-input-shipper">
					<label class="radio-custom" for="radiodvvc_2">
		              	<input value="HG" id="radiodvvc_2" type="radio" name="dvvc_service" checked="">
		              	<span class="checkmark"></span>
		              	Hẹn giờ
		            </label>
		            <span>25.000 đ</span>
				</div>
				<div class="box-input-shipper active-api">
					<label class="radio-custom" for="radiodvvc_1">
		              	<input value="TC" id="radiodvvc_1" type="radio" name="dvvc_service" checked="">
		              	<span class="checkmark"></span>
		              	Tiêu chuẩn
		            </label>
		            <span>18.000 đ</span>
				</div>
			</div>
			<div class="item">
				<label class="black-custom">
					Chọn đơn vị vận chuyển khác:
				</label>
				<div class="box-input-shipper">
					<label class="checkbox-custom" for="other-input"> Chọn
                      <input value="0" id="other-input" name="other-input" type="checkbox" onclick="showOtherDvvc('dvvc-default');">
                      <span class="checkmark"></span>
                    </label>
				</div>
			</div>
		</div>
		<button id="Dangdondvvc" onclick="dangdonhang();this.disabled=true;" type="button" class="button bg-green">Đăng đơn</button>
		<div id="dvvc-default" class="ship-box-other">
			<div class="item">
				<label class="black-custom">
					Chọn đơn vị vận chuyển:
				</label>
				<div class="box-input-shipper custom-dropdown">
					<select class="form-control" id="id_transport">
			    		<option value="-1">Chọn đơn vị vận chuyển</option>
					  	<option value="0">Giao hàng tiết kiệm</option>
					  	<option value="1">Giao hàng nhanh</option>
					  	<option value="2">Ahamove</option>
					</select>
				</div>
			</div>
			<div class="item">
				<label class="black-custom">
					Phí vận chuyển và COD:
				</label>
				<div class="box-input-shipper">
					<input type="text" class="form-control" id="cost_transport" name="cost_transport" placeholder="Nhập phí">
				</div>
			</div>
			<div class="item">
				<label class="black-custom">
					Mã vận chuyển:
				</label>
				<div class="box-input-shipper">
					<input type="text" class="form-control" id="code_transport" name="code_transport" placeholder="Nhập mã vận đơn">
				</div>
			</div>
			<button type="submit" class="button bg-black">Lưu lại</button>
		</div>
	</div>
	<div class="item">
		<a class="dropdown-collapse customer-dropdown bg-black" href="javascript:void(0);">
			Chọn đơn vị vận chuyển mặc định
		</a>
		<div class="ship-box default-ship">
			<div class="item">
				<label class="black-custom">
					Đơn vị vận chuyển:
				</label>
				<div class="box-input-shipper">
					<span>Giao hàng nhanh</span>
				</div>
			</div>
			<div class="item">
				<label class="black-custom">
					Phí vận chuyển:
				</label>
				<div class="box-input-shipper">
					<span>16.500 đ</span>
				</div>
			</div>
			<div class="item">
				<label class="black-custom">
					Khối lượng:
				</label>
				<div class="box-input-shipper">
					<span>500 gram</span>
				</div>
			</div>
			<div class="item">
				<label class="black-custom">
					Mã vận đơn:
				</label>
				<div class="box-input-shipper">
					<span>EEHY5R5H9</span>
				</div>
			</div>
			<div class="item">
				<label class="black-custom">
					Loại đăng đơn:
				</label>
				<div class="box-input-shipper">
					<span>Có API</span>
				</div>
			</div>
			<div class="item">
				<label class="black-custom">
					Trạng thái đơn:
				</label>
				<div class="box-input-shipper">
					<span>Chưa tiếp nhận</span>
				</div>
			</div>
			<button type="submit" class="button bg-red">Hủy đơn</button>
			<button type="submit" class="button bg-green">Đồng bộ</button>
		</div>
	</div>
</div>
	

<script>
	function showOtherDvvc(id) {
        if (jQuery('#other-input').prop('checked') == true) {
            jQuery('#' + id).css('display', 'block');
            jQuery("input[name='dvvc_service']").attr('disabled', true);
            jQuery('#Dangdondvvc').css('display', 'none');
            jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
        } else {
            jQuery('#' + id).css('display', 'none');
            jQuery("input[name='dvvc_service']").attr('disabled', false);
            jQuery('#Dangdondvvc').css('display', 'block');
            jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
        }
    }
	function showform3(id) {
        jQuery('.btn-edit-dvvc').css('display', 'none');
        jQuery('#' + id).prev().css('display', 'none');
        jQuery('#' + id).css('display', 'block');
        jQuery('.btn-cancel-dvvc').css('display', 'flex');
        jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
    }
    function hideform3(id) {
        jQuery('.btn-edit-dvvc').css('display', 'flex');
        jQuery('#' + id).prev().css('display', 'block');
        jQuery('#' + id).css('display', 'none');
        jQuery('.btn-cancel-dvvc').css('display', 'none');
        jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
    }
    function showform2() {
        jQuery('.more-info-dvvc').css('display', 'block');
        jQuery('.btn-show-dvvc').css('display','none');
        jQuery('.btn-close-dvvc').css('display','flex');
        jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
    }
    function hideform2() {
        jQuery('.more-info-dvvc').css('display', 'none');
        jQuery('.btn-close-dvvc').css('display','none');
        jQuery('.btn-show-dvvc').css('display','flex');
        jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
    }

	jQuery(function(){

	});
</script>