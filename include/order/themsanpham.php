<style>
  .checkbox-custom{
    cursor: pointer;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    height: 20px;
  }
  .checkbox-custom label{
    color: #ccc;
  }
  .box-search-products .item{
    width: 48%;
    float: left;
  }
  .box-search-products .item:last-child{
    margin-left: 4%;
  }
  .box-search-products .item label {
    font-weight: 500;
    font-size: 14px;
    margin: 0 0 15px;
  }
  .box-search-products .item input{
    width: 75%;
    float: left;
  }
  .box-search-products .item button{
    width: 20%;
    float: right;
  }
  .table-addpro tr th:nth-child(3), .table-addpro tr td:nth-child(3){
    min-width: auto;
  }
  .table-addpro tr th:nth-child(4), .table-addpro tr td:nth-child(4){
    max-width: 250px;
    white-space: normal;
  }
  .table-addpro thead {
      border-bottom: 1px solid #eee;
  }
  .box-table tr td:last-child{
    width: auto;
    white-space: normal;
    max-width: 95px;
    margin: 0 auto;
  }
  @media (max-width: 575.98px) {
    .box-search-products .item{
      width: 100%;
      margin-bottom: 10px;
    }
    .box-search-products .item:last-child{
      margin-left: 0;
    }
    .box-search-products .item label{
      margin-bottom: 5px;
    }
    .box-search-products .item input{
      width: 65%;
    }
    .box-search-products .item button{
      width: 30%;
    }
    .table-addpro > tbody > tr td:first-child{
      display:none;
    }
    .table-addpro > tbody > tr td:nth-child(2){
      display: none;
    }
    .table-addpro tr th:nth-child(3), .table-addpro tr td:nth-child(3){
      width: 100%;
    }
    .table-addpro > tbody > tr:last-child{
      margin-bottom: 0;
    }
  }
  @media (min-width: 576px) and (max-width: 767.98px) {

  }
  @media (min-width: 768px) and (max-width: 991.98px) {

  }
  @media (min-width: 992px) and (max-width: 1199.98px) {
    
  }
  @media (min-width: 1200px) {
    
  }
</style>
<div class="modal large-modal" id="themsanpham-modal" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="dropdown-collapse customer-dropdown bg-black">
          Thêm sản phẩm
          <a href="javascript:void(0);" class="close-modal white-custom" title="Thoát">
            <span class="ti-close" data-dismiss="modal"></span>
          </a>
        </div>
      </div>
      <div class="modal-body">
        <div class="box-search-products">
          <div class="item">
            <label for="sku" class="black-custom">
              Nhập 1 hoặc nhiều SKU
            </label>
            <div class="box-input-coupon">
              <input type="text" name="sku" id="sku" class="form-control" placeholder="Nhập một hoặc nhiều SKU...">
              <button type="submit" class="button bg-black">Tìm kiếm</button>
            </div>
          </div>
          <div class="item">
            <label for="name_vi" class="black-custom">
              Nhập tên sản phẩm
            </label>
            <div class="box-input-coupon">
              <input type="text" name="name_vi" id="name_vi" class="form-control" placeholder="Nhập tên sản phẩm...">
              <button type="submit" class="button bg-black">Tìm kiếm</button>
            </div>
          </div>
        </div>
        <div class="box-payback-table">
          <div class="box-table">
            <table class="table table-custom table-addpro table-responsive">
              <thead>
                <tr>
                  <th class="black-custom bold center-custom">
                    <label class="checkbox-custom">
                      <input value="0" id="checkAll" type="checkbox" onclick="checkall()">
                      <span class="checkmark"></span>
                    </label>
                  </th>
                  <th class="black-custom bold center-custom">STT</th>
                  <th class="black-custom bold">SKU</th>
                  <th class="black-custom bold">Tên sản phẩm</th>
                  <th class="black-custom bold center-custom">Hình ảnh</th>
                  <th class="black-custom bold">Giá</th>
                  <th class="black-custom bold center-custom">Số lượng</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">1</td>
                  <td data-title="SKU">087AA9</td>
                  <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
                  <td data-title="Hình ảnh" class="center-custom">
                    <img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
                  </td>
                  <td data-title="Giá"><span class="first-price">2.600.000 đ</span> | <span>2.340.000 đ</span></td>
                  <td data-title="Số lượng" class="center-custom">
                    <input type="text" value="0" name="trahang" class="form-control">
                  </td>
                </tr>
                <tr>
                  <td data-title="Checkbox" class="center-custom">
                    <label class="checkbox-custom">
                      <input value="" type="checkbox" class="chk2">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td data-title="STT" class="center-custom">2</td>
                  <td data-title="SKU">087AA7</td>
                  <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml v2</td>
                  <td data-title="Hình ảnh" class="center-custom">
                    <img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
                  </td>
                  <td data-title="Giá"><span class="first-price">2.800.000 đ</span> | <span>3.000.000 đ</span></td>
                  <td data-title="Số lượng" class="center-custom">
                    <input type="text" value="0" name="trahang" class="form-control">
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <button type="button" class="button bg-green custom-fright">Thêm</button>
      </div>
    </div>
  </div>
</div>
<script>
  function checkall(){
    var i;
    var checkBox = document.getElementById("checkAll");
    var childcheck = document.getElementsByClassName("chk2");
    var childchecklen = childcheck.length;
    if (checkBox.checked == true){
        for (i = 0; i < childchecklen; i += 1) {
            childcheck[i].checked = true;
        }
    }
    else{
        for (i = 0; i < childchecklen; i += 1) {
            childcheck[i].checked = false;
        }
    }
  }
</script>