<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item button{
		float: left;
		margin-left: 15px;
	}
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item, 
	  	.box-quick-search .item:first-child input{
	  		width: 100%;
	  	}
		.search1{
	  		display: inline-block;
	  		width: 100%;
	  	}
	  	.box-quick-search .item:first-child button{
	  		margin-left: 0;
	  		margin-top: 15px;
	  	}
		.refund .table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="refund content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách kho</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#themkho-modal" class="link-custom black-custom" title="Tạo mới">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Tạo mới</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm mã kho hoặc tên kho...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Mã kho</th>
					            <th class="bg-black">Tên kho</th>
					            <th class="bg-black">Địa chỉ</th>
					            <th class="bg-black center-custom">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Mã kho">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#suakho-modal" title="Kho 001">Kho 001</a>
					            </td>
					            <td data-title="Tên kho">Kho 001</td>
					            <td data-title="Địa chỉ">76 Nguyễn Háo Vĩnh</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#suakho-modal" title="Chỉnh sửa kho" style="cursor: pointer;">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Mã kho">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#suakho-modal" title="Kho 002">Kho 002</a>
					            </td>
					            <td data-title="Tên kho">Kho 002</td>
					            <td data-title="Địa chỉ">76 Nguyễn Háo Vĩnh</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#suakho-modal" title="Chỉnh sửa kho" style="cursor: pointer;">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Mã kho">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#suakho-modal" title="Kho 003">Kho 003</a>
					            </td>
					            <td data-title="Tên kho">Kho 003</td>
					            <td data-title="Địa chỉ">76 Nguyễn Háo Vĩnh</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#suakho-modal" title="Chỉnh sửa kho" style="cursor: pointer;">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Mã kho">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#suakho-modal" title="Kho 004">Kho 004</a>
					            </td>
					            <td data-title="Tên kho">Kho 004</td>
					            <td data-title="Địa chỉ">76 Nguyễn Háo Vĩnh</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#suakho-modal" title="Chỉnh sửa kho" style="cursor: pointer;">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Mã kho">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#suakho-modal" title="Kho 005">Kho 005</a>
					            </td>
					            <td data-title="Tên kho">Kho 005</td>
					            <td data-title="Địa chỉ">76 Nguyễn Háo Vĩnh</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#suakho-modal" title="Chỉnh sửa kho" style="cursor: pointer;">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/warehouses/add.php'); ?>
<?php include('include/warehouses/edit.php'); ?>
<script>
	jQuery(function(){
		
	})
</script>