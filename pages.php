<style>
	.box-quick-search {
	    display: inline-block;
	    width: 100%;
	    margin-top: 15px;
	}
	.box-quick-search .item {
	    display: inline-block;
	    width: 50%;
	    float: left;
	}
	.box-quick-search .item input{
		width: 50%;
    	float: left;
	}
	.box-quick-search .item button {
	    float: left;
	    margin-left: 15px;
	}
	.custom-dropdown {
	    display: inline-block;
	    width: 50%;
	    float: right;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin: 15px 0;
	    position: relative;
	}
	.table-custom{
		margin-top: 0;
		white-space: nowrap;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		position: relative;
	}
	.table-custom tr th:nth-child(4),
	.table-custom tr td:nth-child(4){
		text-align: right;
	}
	.table-custom tr th:nth-child(5),
	.table-custom tr td:nth-child(5),
	.table-custom tr th:last-child,
	.table-custom tr td:last-child{
		text-align: center;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.box-table tr td:nth-child(2) img{
		width: 100px;
	}
	
	@media (max-width: 575.98px) {
		.box-quick-search .item{
	  		width: 100%;
	  	}
	  	.box-quick-search .item input{
	  		width: 100%;
	  	}
	  	.box-quick-search .item button{
	  		margin-left: 0;
	  		margin-top: 15px;
	  	}
	  	.custom-dropdown {
	  		width: 100%;
	  		margin-top: 15px;
	  	}
	  	.table-custom > tbody > tr > td{
			min-height: 40px;
		}
		.table-responsive > tbody > tr td:first-child {
		    display: none;
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-quick-search .item input{
			width: 65%;
		}
		.box-quick-search .item button{
			width: 30%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.box-quick-search .item input{
			width: 65%;
		}
		.box-quick-search .item button{
			width: 30%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {

	}
</style>
<main class="block content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách pages</h1>
			<ul>
				<li>
					<a href="?action=include/pages/add.php" class="link-custom black-custom" title="Tạo page">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Tạo page</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post" class="search1">
	                       <input autocomplete="off" name="search" value="" type="text" class="form-control custom-ipt" placeholder="Tìm trang...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<form name="status_search" id="frm" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="type" name="type" onchange="document.getElementById('frm').submit();">
						    		<option value="-1">Chọn</option>
								  	<option value="0">Tất cả</option>
								  	<option value="1">Event</option>
								  	<option value="2">Mặc định</option>
								  	<option value="3">Thương hiệu</option>
								  	<option value="4">Danh mục sản phẩm</option>
								</select>
							</div>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Tên page</th>
					            <th class="bg-black">Ngày tạo</th>
					            <th class="bg-black">Nhân viên tạo</th>
					            <th class="bg-black">Trạng thái</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Tên page">
					            	<a href="?action=include/pages/edit.php" title="Chỉnh sửa">Home Page</a>
					            </td>
					            <td data-title="Ngày tạo">18/03/2019 - 13:03</td>
					            <td data-title="Nhân viên tạo">dev_admin</td>
					            <td data-title="Trạng thái"><i class="green-custom fa fa-check" aria-hidden="true"></i></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/pages/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Tên page">
					            	<a href="?action=include/pages/edit.php" title="Chỉnh sửa">tags 01</a>
					            </td>
					            <td data-title="Ngày tạo">18/03/2019 - 13:03</td>
					            <td data-title="Nhân viên tạo">dev_admin</td>
					            <td data-title="Trạng thái"><i class="gray-custom fa fa-file-text" aria-hidden="true"></i></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/pages/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Tên page">
					            	<a href="?action=include/pages/edit.php" title="Chỉnh sửa">Add to cart</a>
					            </td>
					            <td data-title="Ngày tạo">18/03/2019 - 13:03</td>
					            <td data-title="Nhân viên tạo">dev_admin</td>
					            <td data-title="Trạng thái"><i class="green-custom fa fa-check" aria-hidden="true"></i></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/pages/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Tên page">
					            	<a href="?action=include/pages/edit.php" title="Chỉnh sửa">News index</a>
					            </td>
					            <td data-title="Ngày tạo">18/03/2019 - 13:03</td>
					            <td data-title="Nhân viên tạo">dev_admin</td>
					            <td data-title="Trạng thái"><i class="green-custom fa fa-check" aria-hidden="true"></i></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/pages/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Tên page">
					            	<a href="?action=include/pages/edit.php" title="Chỉnh sửa">page to follow cart</a>
					            </td>
					            <td data-title="Ngày tạo">18/03/2019 - 13:03</td>
					            <td data-title="Nhân viên tạo">dev_admin</td>
					            <td data-title="Trạng thái"><i class="green-custom fa fa-check" aria-hidden="true"></i></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/pages/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Tên page">
					            	<a href="?action=include/pages/edit.php" title="Chỉnh sửa">brand</a>
					            </td>
					            <td data-title="Ngày tạo">18/03/2019 - 13:03</td>
					            <td data-title="Nhân viên tạo">dev_admin</td>
					            <td data-title="Trạng thái"><i class="green-custom fa fa-check" aria-hidden="true"></i></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/pages/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Tên page">
					            	<a href="?action=include/pages/edit.php" title="Chỉnh sửa">product detail</a>
					            </td>
					            <td data-title="Ngày tạo">18/03/2019 - 13:03</td>
					            <td data-title="Nhân viên tạo">dev_admin</td>
					            <td data-title="Trạng thái"><i class="green-custom fa fa-check" aria-hidden="true"></i></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/pages/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Tên page">
					            	<a href="?action=include/pages/edit.php" title="Chỉnh sửa">Product Category</a>
					            </td>
					            <td data-title="Ngày tạo">18/03/2019 - 13:03</td>
					            <td data-title="Nhân viên tạo">dev_admin</td>
					            <td data-title="Trạng thái"><i class="green-custom fa fa-check" aria-hidden="true"></i></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/pages/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Tên page">
					            	<a href="?action=include/pages/edit.php" title="Chỉnh sửa">Checkout</a>
					            </td>
					            <td data-title="Ngày tạo">18/03/2019 - 13:03</td>
					            <td data-title="Nhân viên tạo">dev_admin</td>
					            <td data-title="Trạng thái"><i class="green-custom fa fa-check" aria-hidden="true"></i></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/pages/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Tên page">
					            	<a href="?action=include/pages/edit.php" title="Chỉnh sửa">Page statis</a>
					            </td>
					            <td data-title="Ngày tạo">18/03/2019 - 13:03</td>
					            <td data-title="Nhân viên tạo">dev_admin</td>
					            <td data-title="Trạng thái"><i class="green-custom fa fa-check" aria-hidden="true"></i></td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/pages/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){

	})
</script>