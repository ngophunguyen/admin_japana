<style>
	.listreceiptrefund .filter-pc{
		margin-top: 15px;
	}
	.filter-mobile{
		display: none;
	}
	.custom-collapse{
		display: none;
		width: 100%;
		padding: 0 15px;
	}
	.custom-collapse .item{
		display: inline-block;
		width: 32%;
		margin: 10px 2%;
		float: left;
	}
	.custom-collapse .item:nth-child(1), 
	.custom-collapse .item:nth-child(3), 
	.custom-collapse .item:nth-child(4),
	.custom-collapse .item:nth-child(6),
	.custom-collapse .item:nth-child(7),
	.custom-collapse .item:nth-child(9){
		margin: 10px 0;
	}
	.custom-collapse .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}

	.table-custom tr td:nth-child(6) span{
		font-weight: 500;
		line-height: 1.5;
	}
	.table-custom tr td strong{
		color: red;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:last-child{
		width: 55%;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	}
	.box-quick-search .item:first-child input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .box-time{
		width: 40%;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
		float: right;
	}
	.search2, .search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.search1{
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.box-button-filter{
  		display: none;
  	}
  	.fixed-button{
  		position: fixed; 
  		bottom: 0;
  		right: 0;
	    width: calc(100% - 38px);
	    border-radius: 0;
	    z-index: 9999;
  	}
  	.button-filter{
	    font-size: 14px;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	    background: rgba(0,0,0,0.3);
	    border-radius: 10px 0 0 10px;
	    height: 38px;
	    width: 38px;
	    position: fixed;
	    right: 0;
	    top: 220px;
		z-index: 99
	}
	.button-filter.active{
		left: 0px;
	    border-radius: 0px;
	    right: auto;
	    z-index: 99;
	    top: 0px!important;
	    height: 40px;
	    background: #222D32!important;
	    color: #fff!important;
	}
	.button-filter:focus, .button-filter:hover{
		color: #fff;
	}
	@media (max-width: 575.98px) {
	  	.group-category{
	  		display: none;
	  	}
	  	.dropdown-collapse{
	  		border-radius: 0;
	  		-webkit-box-pack: center;
	  		    -ms-flex-pack: center;
	  		        justify-content: center;
	  		display: none;
	  	}
	  	.dropdown-collapse i{
	  		display: none;
	  	}
	  	.filter-pc{
	  		display: block;
	  		position: fixed;
		    top: 0;
		    right: 0;
		    right: 0;
		    width: calc(100% - 38px);
		    height: 100%;
		    z-index: -1;
		    margin-top: 0!important;
	  	}
	  	.custom-collapse{
	  		height: 100%;
		    width: 100%;
		    padding: 0!important;
		    float: right;
		    overflow-x: hidden;
		    position: relative;
		    z-index: 999;
		    background: #fff;
		    border-left: 1px solid #eee;
	  	}
	  	.custom-collapse .item{
	  		width: 100%;
	  		margin: 10px 0 0!important;
	  	}
	  	.custom-collapse .item:last-child{
	  		margin-bottom: 10px;
	  	}
	  	.custom-collapse .item:last-child label{
	  		display: none;
	  	}
	  	.custom-collapse .item:last-child button{
	  		position: fixed;
	  		bottom: 0;
	  		right: 0;
	  		width: calc(100% - 38px);
	  		border-radius: 0;
	  	}
	  	.box-button-filter.active{
	  		background: rgba(0,0,0,.3);
		    width: 38px;
		    height: 100%;
		    position: fixed;
		    left: 0;
		    top: 0;
    		z-index: 999;
	  	}
	  	#advance_search{
	  		display: inline-block;
	  		width: 100%;
	  		height: calc(100% - 38px);
		    background: #fff;
		    z-index: 999;
		    top: 0;
		    overflow-y: scroll;
		    overflow-x: hidden;
		    padding: 0 15px;
	  	}
	  	#advance_search .button-submit{
	  		position: fixed;
	  		bottom: 0;
	  		right: 0;
	  		width: calc(100% - 38px);
	  		z-index: 999;
	  		border-radius: 0;
	  	}
	  	.custom-dropdown:after{
	  		width: 10%;
	  	}
	  	.custom-collapse .title{
	  		padding: 10px 12px;
	  		margin-bottom: 0;
	  		border-bottom: 1px solid #ccc;
	  		text-align: center;
	  	}
	  	.box-quick-search .item, .box-quick-search .item:first-child input, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child,.box-quick-search .item:last-child button{
	  		margin-top: 15px;
	  	}
	  	.box-quick-search .item:last-child,.search2{
	  		display: inline-block;
	  	}
	  	.box-quick-search .item:last-child .custom-dropdown{
	  		width: 40%;
	  	}
	  	.box-quick-search .item:last-child .box-time{
	  		width: 55%;
	  		margin-left: 5%;
	  	}
	  	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
	  		float: left;
	  	}
	  	.box-quick-search .item:last-child button{
	  		margin-left: 0;
	  		clear: both;
	  	}
		.table-custom tr th:first-child, 
		.table-custom tr th:last-child,
		.table-custom tr th:nth-child(8),
		.table-custom tr th:nth-child(9),
		.table-custom tr td:first-child, 
		.table-custom tr td:last-child,
		.table-custom tr td:nth-child(8),
		.table-custom tr td:nth-child(9){
			display: none;
		}

		.table-custom tr td:nth-child(6){
			display: -ms-grid;
			display: grid;
		}
		.table-custom tr td:nth-child(6) span{
			font-weight: 500;
			line-height: 1.5;
			display: inline-block;
			width: 100%;
		}
		.box-button-filter{
	  		display: block;
	  	}
	  	.custom-collapse .item:nth-child(9){
	  		margin-bottom: 53px!important;
	  	}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	  	.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.box-quick-search .item, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child{
	  	 	margin-top: 15px;
	  	}
	  	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 80%;
	  	}
	  	.box-quick-search .item:last-child .box-time{
	  		width: 54%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}

		.table-custom tr th:first-child, 
		.table-custom tr th:last-child,
		.table-custom tr th:nth-child(8),
		.table-custom tr th:nth-child(9),
		.table-custom tr td:first-child, 
		.table-custom tr td:last-child,
		.table-custom tr td:nth-child(8),
		.table-custom tr td:nth-child(9){
			display: none;
		}
		.table-custom tr td:nth-child(6) span{
			margin-right: 5px;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
	  	.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.box-quick-search .item, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child{
	  	 	margin-top: 15px;
	  	}
	  	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 80%;
	  	}
	  	.box-quick-search .item:last-child .box-time{
	  		width: 54%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
	  	.table-custom{
	  		white-space: nowrap;
	  	}

		.table-custom tr th:first-child, 
		.table-custom tr th:last-child,
		.table-custom tr th:nth-child(8),
		.table-custom tr th:nth-child(9),
		.table-custom tr td:first-child, 
		.table-custom tr td:last-child,
		.table-custom tr td:nth-child(8),
		.table-custom tr td:nth-child(9){
			display: none;
		}
		.table-custom tr td:nth-child(6) span{
			margin-right: 5px;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 1200px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
</style>
<main class="listreceiptrefund content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách phiếu trả hàng</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="filter-pc">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">Tìm kiếm nâng cao <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div id="filter-order" class="custom-collapse">
						<form id="advance_search" name="frm advance_search" method="post">
						    <div class="item">
						    	<label for="cbo_username">Nhân viên</label>
						    	<select class="form-control" name="username" id="cbo_username">
						    		<option value="-1">Chọn nhân viên</option>
								  	<option value="0">Admin</option>
								  	<option value="1">Nguyễn Văn A</option>
								  	<option value="2">Trần Thị B</option>
								</select>
						    </div>
						    <div class="item">
						    	<label for="id_supplier">Nhà cung cấp</label>
						    	<select class="form-control" name="id_supplier" id="id_supplier">
						    		<option value="-1">Chọn nhà cung cấp</option>
								  	<option value="0">Giao nhận 365</option>
								  	<option value="1">SBS (Anh Phi)</option>
								  	<option value="2">Jenny Food</option>
								</select>
						    </div>
						    <div class="item">
						    	<label for="skuname">SKU và tên sản phẩm</label>
						    	<input autocomplete="off" type="text" name="skuname" id="skuname" class="form-control" placeholder="SKU và tên sản phẩm">
						    </div>
						    <div class="item">
						    	<label for="id_status">Trạng thái phiếu trả</label>
						    	<div class="custom-dropdown">
							    	<select class="form-control" name="id_status" id="id_status">
							    		<option value="-1">Chọn trạng thái</option>
									  	<option value="0">Phiếu mới</option>
									  	<option value="1">Đồng ý xuất kho</option>
									  	<option value="2">Không đồng ý xuất kho</option>
									  	<option value="1">Đã xuất kho</option>
									  	<option value="2">Hoàn thành</option>
									</select>
								</div>
						    </div>
						    <div class="item">
						    	<label for="type">Loại phiếu</label>
						    	<div class="custom-dropdown">
							    	<select class="form-control" name="type" id="type">
							    		<option value="-1">Chọn loại phiếu</option>
									  	<option value="0">Phiếu trả</option>
									  	<option value="1">Phiếu trả NCC không nhận</option>
									</select>
								</div>
						    </div>
						    <div class="item">
						    	<label for="id_warehouse">Chọn kho</label>
						    	<select class="form-control" name="id_warehouse" id="id_warehouse">
						    		<option value="-1">Chọn kho</option>
								  	<option value="0">Kho 001</option>
								  	<option value="1">Kho 002</option>
								  	<option value="2">Kho 003</option>
								</select>
						    </div>
						    <div class="item">
						    	<label for="date-order">Khoảng thời gian</label>
						    	<div class="box-time">
						    		<input type="hidden" id="start-date3" name="from_date_ad" value="">
						    		<input type="hidden" id="end-date3" name="to_date_ad" value="">
		                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-order" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày...">
		                            <i class="fa fa-calendar icon-time"></i>
		                        </div>
						    </div>
						    <div class="item">
						    	<label class="visible-hidden">Xác nhận</label>
						    	<input type="hidden" name="method" value="3">
						    	<button type="submit" class="button bg-black">Xác nhận</button>
						    </div>
						</form>
					</div>
				</div>
				<div class="box-button-filter">
					<a class="button-filter white-custom" href="javascript:void(0);">
						<i class="ti-search" aria-hidden="true"></i>
					</a>
				</div>
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm mã phiếu, nhà cung cấp, SKU...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<form name="status_search" id="frm" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="status_cart" name="status_cart">
						    		<option value="-1">Chọn trạng thái</option>
								  	<option value="0">Phiếu mới</option>
								  	<option value="1">Đồng ý xuất kho</option>
								  	<option value="2">Không đồng ý xuất kho</option>
								  	<option value="3">Đã xuất kho</option>
								  	<option value="4">Hoàn thành</option>
								</select>
							</div>
							<div class="box-time">
	                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-order" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày...">
	                            <i class="fa fa-calendar icon-time"></i>
	                        </div>
	                        <button type="submit" class="button bg-black">Tìm kiếm</button>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black center-custom">Mã phiếu trả</th>
					            <th class="bg-black">Ngày tạo phiếu</th>
					            <th class="bg-black">Nhà cung cấp</th>
					            <th class="bg-black">Trạng thái</th>
					            <th class="bg-black">Người tạo phiếu</th>
					            <th class="bg-black">Người yêu cầu</th>
					            <th class="bg-black center-custom">Ngày chuyển trạng thái</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Mã phiếu trả" class="center-custom"><a href="?action=include/listreceiptrefund/edit.php" title="PTH-01">PTH-01</a></td>
					            <td data-title="Ngày tạo phiếu">20-09-2019|11:48</td>
					            <td data-title="Nhà cung cấp">Kizuna</td>
					            <td data-title="Trạng thái"><span class="green-custom">Phiếu mới</span></td>
					            <td data-title="Người tạo phiếu">Trần Thị Ngọc Trân</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển trạng thái" class="center-custom">20-09-2019|11:48</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Mã phiếu trả" class="center-custom"><a href="?action=include/listreceiptrefund/edit.php" title="PTH-02">PTH-02</a></td>
					            <td data-title="Ngày tạo phiếu">20-09-2019|11:48</td>
					            <td data-title="Nhà cung cấp">Chị Lan (Kaij)</td>
					            <td data-title="Trạng thái"><span class="blue-custom">Đồng ý xuất kho</span></td>
					            <td data-title="Người tạo phiếu">Trần Thị Ngọc Trân</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển trạng thái" class="center-custom">20-09-2019|11:48</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Mã phiếu trả" class="center-custom"><a href="?action=include/listreceiptrefund/edit.php" title="PTH-03">PTH-03</a></td>
					            <td data-title="Ngày tạo phiếu">20-09-2019|11:48</td>
					            <td data-title="Nhà cung cấp">Kaicho Enzym,e</td>
					            <td data-title="Trạng thái"><span class="yellow-custom"><i class="fa fa-flag" aria-hidden="true"></i> Không đồng ý xuất kho</span></td>
					            <td data-title="Người tạo phiếu">Trần Thị Ngọc Trân</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển trạng thái" class="center-custom">20-09-2019|11:48</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Mã phiếu trả" class="center-custom"><a href="?action=include/listreceiptrefund/edit.php" title="PTH-04">PTH-04</a></td>
					            <td data-title="Ngày tạo phiếu">20-09-2019|11:48</td>
					            <td data-title="Nhà cung cấp">Công ty Đức Thịnh</td>
					            <td data-title="Trạng thái"><span class="yellow-custom"><i class="fa fa-flag" aria-hidden="true"></i> Đã xuất kho</span></td>
					            <td data-title="Người tạo phiếu">Trần Thị Ngọc Trân</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển trạng thái" class="center-custom">20-09-2019|11:48</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Mã phiếu trả" class="center-custom"><a href="?action=include/listreceiptrefund/edit.php" title="PTH-05">PTH-05</a></td>
					            <td data-title="Ngày tạo phiếu">20-09-2019|11:48</td>
					            <td data-title="Nhà cung cấp">Nobita Shop (Orihiro)</td>
					            <td data-title="Trạng thái"><span class="green-custom">Hoàn thành</span></td>
					            <td data-title="Người tạo phiếu">Trần Thị Ngọc Trân</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển trạng thái" class="center-custom">20-09-2019|11:48</td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		var heightBottomPagination = jQuery('.pagination-custom').outerHeight();
		if(window.innerWidth < 576){
			jQuery('.box-table').css('padding-bottom',heightBottomPagination);
		}
		if(window.innerWidth > 576){
		    jQuery('.dropdown-collapse').click(function(){
		    	if(jQuery('.custom-collapse').css('display')=='none'){
		    		jQuery('.custom-collapse').css('display','inline-block');
		    		jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
		    		jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
		    	}
		    	else{
		    		jQuery('.custom-collapse').css('display','none');
		    		jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
		    		jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
		    	}
		    });
		}else{
			jQuery('.button-filter').click(function(){
		    	if(jQuery('.custom-collapse').css('display')=='none'){
		    		jQuery('.custom-collapse').css('display','inline-block');
		    		jQuery('.dropdown-collapse').css('display','flex');
		    		jQuery(this).parent().addClass('active');
		    		jQuery(this).addClass('active');
		    		jQuery(this).find('i').addClass('ti-arrow-right');
		    		jQuery('body').css('overflow','hidden');
		    		jQuery('.filter-pc').css('z-index','999');
		    	}
		    	else{
		    		jQuery('.custom-collapse').css('display','none');
		    		jQuery('.dropdown-collapse').css('display','none');
		    		jQuery(this).parent().removeClass('active');
		    		jQuery(this).removeClass('active');
		    		jQuery(this).find('i').removeClass('ti-arrow-right');
		    		jQuery('body').css('overflow','inherit');
		    		jQuery('.filter-pc').css('z-index','-1');
		    	}
		    });
		}
	    
	    var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    jQuery('#cbo_username,#id_supplier,#id_warehouse').select2();
	    if(window.innerWidth > 576) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
	    jQuery('input[name="date-order"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date3').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date3').val(end.format('DD-MM-YYYY'));
		});
		jQuery('input[name="from_date_ad"]').daterangepicker({
			singleDatePicker: true,
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start) {
			jQuery('#start-date3').val(start.format('DD-MM-YYYY'));
		});
		jQuery('input[name="to_date_ad"]').daterangepicker({
			singleDatePicker: true,
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(end) {
			jQuery('#end-date3').val(end.format('DD-MM-YYYY'));
		});
	});
</script>