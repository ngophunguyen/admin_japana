<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin: 15px 0;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:last-child{
		width: 55%;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	}
	.box-quick-search .item:first-child input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .box-time{
		width: 40%;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
		float: right;
	}
	.search2, .search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.search1{
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
	}
	.box-table {
	    position: relative;
	    margin-bottom: 15px;
	}
	.table-custom{
		margin-top: 0;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item, 
	  	.box-quick-search .item:first-child input, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
		.search1{
	  		display: inline-block;
	  		width: 100%;
	  	}
	  	.box-quick-search .item:first-child button{
	  		margin-left: 0;
	  		margin-top: 15px;
	  	}
	  	.box-quick-search .item:last-child{
	  		margin-top: 15px;
	  	}
	  	.box-quick-search .item:last-child .box-time{
			margin-left: 0;
			width: 70%;
	  	}
		.table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="warehousemain content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách sản phẩm kho</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#yeucautrahang-modal" class="link-custom black-custom" title="Yêu cầu trả hàng">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Yêu cầu trả hàng</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm sku hoặc tên sản phẩm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<div class="box-time">
							<input type="hidden" id="start-date" name="date_from" value="">
						    <input type="hidden" id="end-date" name="date_to" value="">
                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-product" id="date-product" class="form-control ipt-date" placeholder="Chọn ngày hết hạn sản phẩm...">
                            <i class="fa fa-calendar icon-time"></i>
                        </div>
                        <button type="submit" class="button bg-black">Tìm kiếm</button>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">SKU</th>
					            <th class="bg-black">Tên sản phẩm</th>
					            <th class="bg-black center-custom">Số lượng</th>
					            <th class="bg-black">Ngày hết hạn gần nhất</th>
					            <th class="bg-black center-custom">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewproduct-modal" title="KMNCC-037GDA04">KMNCC-037GDA04</a>
					            </td>
					            <td data-title="Tên sản phẩm">Đồ chơi lắp ráp thông minh</td>
					            <td data-title="Số lượng" class="center-custom">4</td>
					            <td data-title="Ngày hết hạn">10-08-2020</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#viewproduct-modal" title="Xem chi tiết" style="cursor: pointer;">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewproduct-modal" title="043DC11">043DC11</a>
					            </td>
					            <td data-title="Tên sản phẩm">Đồ chơi lắp ráp thông minh</td>
					            <td data-title="Số lượng" class="center-custom">4</td>
					            <td data-title="Ngày hết hạn">10-08-2020</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#viewproduct-modal" title="Xem chi tiết" style="cursor: pointer;">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewproduct-modal" title="046EE5">046EE5</a>
					            </td>
					            <td data-title="Tên sản phẩm">Viên uống hỗ trợ điều trị ung thư Umeken Beta Glucan Ball (Hộp 30 gói x 2g)</td>
					            <td data-title="Số lượng" class="center-custom">4</td>
					            <td data-title="Ngày hết hạn">10-08-2020</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#viewproduct-modal" title="Xem chi tiết" style="cursor: pointer;">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewproduct-modal" title="039AA2">039AA2</a>
					            </td>
					            <td data-title="Tên sản phẩm">Kem làm sáng da - Astalift White Cream</td>
					            <td data-title="Số lượng" class="center-custom">4</td>
					            <td data-title="Ngày hết hạn">10-08-2020</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#viewproduct-modal" title="Xem chi tiết" style="cursor: pointer;">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewproduct-modal" title="001DD54">001DD54</a>
					            </td>
					            <td data-title="Tên sản phẩm">Nước uống Venus Charge Collagen Peptide 20000mg Nhật Bản</td>
					            <td data-title="Số lượng" class="center-custom">4</td>
					            <td data-title="Ngày hết hạn">10-08-2020</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#viewproduct-modal" title="Xem chi tiết" style="cursor: pointer;">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewproduct-modal" title="020EG15">020EG15</a>
					            </td>
					            <td data-title="Tên sản phẩm">Viên uống bổ xương khớp Glucosamine Orihiro 900 viên</td>
					            <td data-title="Số lượng" class="center-custom">4</td>
					            <td data-title="Ngày hết hạn">10-08-2020</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#viewproduct-modal" title="Xem chi tiết" style="cursor: pointer;">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewproduct-modal" title="068EG1">068EG1</a>
					            </td>
					            <td data-title="Tên sản phẩm">Sữa rửa mặt Sakura Sensitive Gentle Cleansing Foam</td>
					            <td data-title="Số lượng" class="center-custom">4</td>
					            <td data-title="Ngày hết hạn">10-08-2020</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#viewproduct-modal" title="Xem chi tiết" style="cursor: pointer;">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewproduct-modal" title="001EF413">001EF413</a>
					            </td>
					            <td data-title="Tên sản phẩm">Kem chống lão hóa da SK-II R.N.A Power Radical New Age 15g</td>
					            <td data-title="Số lượng" class="center-custom">4</td>
					            <td data-title="Ngày hết hạn">10-08-2020</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#viewproduct-modal" title="Xem chi tiết" style="cursor: pointer;">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewproduct-modal" title="001EE427">001EE427</a>
					            </td>
					            <td data-title="Tên sản phẩm">Kem dưỡng trắng da SK-II Cellumination Deep Surge EX 50g</td>
					            <td data-title="Số lượng" class="center-custom">4</td>
					            <td data-title="Ngày hết hạn">10-08-2020</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#viewproduct-modal" title="Xem chi tiết" style="cursor: pointer;">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewproduct-modal" title="001ECD428">001ECD428</a>
					            </td>
					            <td data-title="Tên sản phẩm">Kem trang điểm SK-II Atmosphere CC Cream 30g</td>
					            <td data-title="Số lượng" class="center-custom">4</td>
					            <td data-title="Ngày hết hạn">10-08-2020</td>
					            <td data-title="Tác vụ" class="center-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#viewproduct-modal" title="Xem chi tiết" style="cursor: pointer;">
					            		<i class="fa fa-eye"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<?php include('include/warehousemain/refund.php'); ?>
<?php include('include/warehousemain/view.php'); ?>
<script>
	jQuery(function(){
		var heightBottomPagination = jQuery('.pagination-custom').outerHeight();
		var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    if(window.innerWidth > 576) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    	//jQuery('.box-table').css('padding-bottom',heightBottomPagination);
	    }
	    jQuery('input[name="date-product"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date').val(end.format('DD-MM-YYYY'));
		});
	})
</script>