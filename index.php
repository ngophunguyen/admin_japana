<!DOCTYPE html>
<html lang="vi">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
  		<meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="title" content="Admin Ver2">
		<meta name="description" content="Admin Ver2">
		<meta name="keywords" content="Admin Ver2">
		<title>Admin JAPANA Ver2</title>
		<link rel="shortcut icon" href="assets/images/favicon.png">

		<!--include CSS-->
		<link rel="stylesheet" href="assets/plugins/slick/slick.css">
		<link rel="stylesheet" href="assets/plugins/tag/jquery.tagit.css">
		<link rel="stylesheet" href="assets/plugins/tag/tagit.ui-zendesk.css">
		<link rel="stylesheet" href="assets/plugins/jquery-ui/jquery-ui.min.css">
		<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/plugins/themify-icons/themify-icons.css">
		<link rel="stylesheet" href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css">
		<link rel="stylesheet" href="assets/plugins/menu/css/component.css">
		<link rel="stylesheet" href="assets/plugins/select2/select2.min.css">
		<link rel="stylesheet" href="assets/plugins/ios-checkbox/iosCheckbox.min.css">
		<link rel="stylesheet" href="assets/plugins/normalize/normalize.css">
		<link rel="stylesheet" href="assets/plugins/normalize/libs.css">

		<?php include('assets/css/css_allpage.php'); ?>
		<!--/include CSS-->

		<!--include JS-->
		<script src="assets/plugins/jquery/jquery-3.3.1.min.js"></script>
		<script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
		<script src="assets/plugins/ckeditor/ckeditor.js"></script>
		<script src="assets/plugins/tag/tag-it.min.js"></script>
		<script src="assets/plugins/daterangepicker/moment.min.js"></script>
		<script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/chartjs/chart.min.js"></script>
        <script src="assets/plugins/chartjs/utils.js"></script>
        <script src="assets/plugins/menu/js/modernizr.custom.js"></script>
        <script src="assets/plugins/select2/select2.min.js"></script>
        <script src="assets/plugins/ios-checkbox/iosCheckbox.min.js"></script>
		<!--/include JS-->

		<!--include Font-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=vietnamese" rel="stylesheet">
        <!--/.include Font-->
	</head>

	<body>
		<div class="site-container">
			<?php include('include/mb-header.php'); ?>
			<?php include('include/pc-menu.php'); ?>
			<div class="site-inner">
				<div class="content-sidebar-wrap">
					<?php if(!isset($_GET['action']) || $_GET['action'] == 'home.php' ) include 'home.php';
					else include $_GET['action']; ?>
					
					<?php include('include/pc-footer.php')?>
				</div>
			</div>
		</div>
		<!--include JS-->
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/plugins/slick/slick.min.js"></script>

		<?php include('assets/js/js_allpage.php')?>
		
		<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="assets/plugins/drag-on/drag-on.js"></script>
        <script src="assets/plugins/menu/js/jquery.dlmenu.js"></script>
		<!--/include JS-->
	</body>
</html>