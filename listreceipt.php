<style>
	.filter-pc{
		margin-top: 15px;
	}
	.filter-mobile{
		display: none;
	}
	.custom-collapse{
		display: none;
		width: 100%;
		padding: 0 15px;
	}
	.custom-collapse .item{
		display: inline-block;
		width: 24%;
		margin: 10px 0;
		float: left;
	}
	.custom-collapse .item:nth-child(2), 
	.custom-collapse .item:nth-child(3), 
	.custom-collapse .item:nth-child(4),
	.custom-collapse .item:nth-child(6), 
	.custom-collapse .item:nth-child(7), 
	.custom-collapse .item:nth-child(8){
		margin-left: 1%;
	}
	.custom-collapse .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.show-filter-order{
		color: #fff;
	    font-size: 14px;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	    border: 1px solid #222D32;
	    height: 38px;
	    width: 38px;
	    position: fixed;
	    right: 0;
		top: 160px;
		z-index: 99;
	}
	.show-filter-order:focus, .show-filter-order:hover{
		color: #fff;
	}
	.table-custom tr td strong{
		color: red;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:last-child{
		width: 55%;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	}
	.box-quick-search .item:first-child input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .box-time{
		width: 40%;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
		float: right;
	}
	.search2, .search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.search1{
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.total-order{
		display: inline-block;
		float: left;
	}
	.total-order p{
		color: #222D32;
		font-size: 16px;
		font-weight: 500;
	}
	.total-order span{
		color: #ff0000;
		font-weight: 500;
		font-size: 20px;
	}
	.box-button-filter{
  		display: none;
  	}
  	.fixed-button{
  		position: fixed; 
  		bottom: 0;
  		right: 0;
	    width: calc(100% - 38px);
	    border-radius: 0;
	    z-index: 9999;
  	}
	@media (max-width: 575.98px) {
	  	.group-category{
	  		display: none;
	  	}
	  	.dropdown-collapse{
	  		border-radius: 0;
	  		-webkit-box-pack: center;
	  		    -ms-flex-pack: center;
	  		        justify-content: center;
	  		display: none;
	  	}
	  	.dropdown-collapse i{
	  		display: none;
	  	}
	  	.filter-pc{
	  		display: block;
	  		position: fixed;
		    top: 0;
		    right: 0;
		    right: 0;
		    width: calc(100% - 38px);
		    height: 100%;
		    z-index: -1;
		    margin-top: 0!important;
	  	}
	  	.custom-collapse{
	  		height: 100%;
		    width: 100%;
		    padding: 0!important;
		    float: right;
		    overflow-x: hidden;
		    position: relative;
		    z-index: 999;
		    background: #fff;
		    border-left: 1px solid #eee;
	  	}
	  	.custom-collapse .item{
	  		width: 100%;
	  		margin: 10px 0 0!important;
	  	}
	  	.custom-collapse .item:last-child{
	  		margin-bottom: 53px!important;

	  	}
	  	.custom-collapse .item:last-child label{
	  		display: none;
	  	}
	  	.custom-collapse .item:last-child button{
	  		position: fixed;
	  		bottom: 0;
	  		right: 0;
	  		width: calc(100% - 38px);
	  		border-radius: 0;
	  	}
	  	.box-button-filter.active{
	  		background: rgba(0,0,0,.3);
		    width: 38px;
		    height: 100%;
		    position: fixed;
		    left: 0;
		    top: 0;
    		z-index: 999;
	  	}
	  	#advance_search{
	  		display: inline-block;
	  		width: 100%;
	  		height: calc(100% - 38px);
		    background: #fff;
		    z-index: 999;
		    top: 0;
		    overflow-y: scroll;
		    overflow-x: hidden;
		    padding: 0 15px;
	  	}
	  	.box-quick-search .item,
	  	.box-quick-search .item:first-child input{
	  		width: 100%;
	  	}
	  	.search1{
	  		display: inline-block;
	  		width: 100%;
	  	}

	  	.box-quick-search .item:first-child button{
	  		margin-left: 0;
	  		margin-top: 15px;
	  	}
	  	.box-quick-search .item:last-child{
	  		display: none;
	  	}
	  	.custom-dropdown:after{
	  		width: 10%;
	  	}
	  	.custom-collapse .title{
	  		padding: 10px 12px;
	  		margin-bottom: 0;
	  		border-bottom: 1px solid #ccc;
	  		text-align: center;
	  	}
		.total-order{
			position: fixed;
			bottom: 0;
			left: 0;
			right: 0;
		}
		.total-order{
			width: 100%;
		    margin-bottom: 38px;
		    background: #fff;
		    display: -webkit-box;
		    display: -ms-flexbox;
		    display: flex;
		    -webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		    -webkit-box-pack: center;
		        -ms-flex-pack: center;
		            justify-content: center;
		    border-top: 1px solid #ccc;
		    height: 41px;
		}
		
		.box-button-filter{
	  		display: block;
	  	}
	  	.custom-collapse .item:nth-child(9){
	  		margin-bottom: 53px!important;
	  	}
	  	.table-custom tr th:first-child,
		.table-custom tr td:first-child,
		.table-custom tr th:nth-child(3),
		.table-custom tr td:nth-child(3),
		.table-custom tr th:nth-child(7),
		.table-custom tr td:nth-child(7){
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	  	.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.box-quick-search .item, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child{
	  	 	margin-top: 15px;
	  	}
	  	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 80%;
	  	}
	  	.box-quick-search .item:last-child .box-time{
	  		width: 54%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
	  	.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.box-quick-search .item, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child{
	  	 	margin-top: 15px;
	  	}
	  	.custom-collapse .item:nth-child(7){
	  		width: 49%;
	  	}
	  	.custom-collapse .item:last-child{
	  		margin-left: 0;
	  	}
	  	.custom-collapse .item:last-child label{
	  		display: none;
	  	}
	  	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 80%;
	  	}
	  	.box-quick-search .item:last-child .box-time{
	  		width: 54%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
	  	.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 1200px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
</style>
<main class="listreceipt content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách phiếu nhập</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="filter-pc">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">Tìm kiếm nâng cao <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div id="filter-order" class="custom-collapse">
						<form id="advance_search" name="frm advance_search" method="post">
						    <div class="item">
						    	<label for="cbo_username">Nhân viên</label>
						    	<select class="form-control" name="username" id="cbo_username">
						    		<option value="-1">Chọn nhân viên</option>
								  	<option value="0">Admin</option>
								  	<option value="1">Nguyễn Văn A</option>
								  	<option value="2">Trần Thị B</option>
								</select>
						    </div>
						    <div class="item">
						    	<label for="name">Tên khách hàng</label>
						    	<input autocomplete="off" type="text" name="cusname" id="name" class="form-control" placeholder="Tên khách hàng">
						    </div>
						    <div class="item">
						    	<label for="skuname">SKU và tên sản phẩm</label>
						    	<input autocomplete="off" type="text" name="skuname" id="skuname" class="form-control" placeholder="SKU và tên sản phẩm">
						    </div>
						    <div class="item">
						    	<label for="cbo_username">Trạng thái phiếu nhập</label>
						    	<div class="custom-dropdown">
						    		<select class="form-control" name="id_status">
							    		<option value="-1">Chọn trạng thái</option>
									  	<option value="0">Chờ hàng</option>
									  	<option value="1">Có hàng</option>
									  	<option value="2">Đang giao hàng</option>
									</select>
						    	</div>
						    </div>
						    <div class="item">
						    	<label for="cbo_username">Loại phiếu nhập</label>
						    	<div class="custom-dropdown">
						    		<select class="form-control" name="type">
							    		<option value="-1">Chọn loại phiếu</option>
									  	<option value="0">Phiếu nhập</option>
									  	<option value="1">Phiếu trả</option>
									</select>
						    	</div>
						    </div>
						    <div class="item">
						    	<label for="cbo_username">Loại kho</label>
						    	<div class="custom-dropdown">
						    		<select class="form-control" name="id_warehouse">
							    		<option value="-1">Chọn kho</option>
									  	<option value="0">Kho hàng bán</option>
									  	<option value="1">Kho ký gửi</option>
									  	<option value="2">Kho gameshow</option>
									  	<option value="3">Kho trưng bày</option>
									</select>
						    	</div>
						    </div>
						    <div class="item">
						    	<label for="date-order">Khoảng thời gian</label>
						    	<div class="box-time">
						    		<input type="hidden" id="start-date3" name="from_date" value="">
						    		<input type="hidden" id="end-date3" name="to_date" value="">
		                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-order" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày...">
		                            <i class="fa fa-calendar icon-time"></i>
		                        </div>
						    </div>
						    <div class="item">
						    	<label class="visible-hidden">Tìm kiếm</label>
						    	<input type="hidden" name="method" value="3">
						    	<button type="submit" class="button bg-black">Tìm kiếm</button>
						    </div>
						</form>
					</div>
				</div>
				<div class="box-button-filter">
					<a class="show-filter-order bg-black" href="javascript:void(0);"><i class="ti-search" aria-hidden="true"></i></a>
				</div>
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm kiếm phiếu nhập...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<form name="status_search" id="frm" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="status_cart" name="status_cart">
						    		<option value="-1">Chọn trạng thái</option>
								  	<option value="0">Phiếu mới</option>
								  	<option value="1">Quản lý xác nhận</option>
								  	<option value="2">Nhập kho</option>
								  	<option value="3">Yêu cầu đàm phán</option>
								  	<option value="4">Yêu cầu trả hàng</option>
								  	<option value="5">Đã đàm phán</option>
								  	<option value="6">Đã trả hàng</option>
								  	<option value="7">Hoàn thành (Trả hàng)</option>
								  	<option value="8">Hoàn thành</option>
								  	<option value="9">Hủy phiếu</option>
								</select>
							</div>
							<div class="box-time">
	                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-order" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày...">
	                            <i class="fa fa-calendar icon-time"></i>
	                        </div>
	                        <button type="submit" class="button bg-black">Tìm kiếm</button>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black center-custom">Mã phiếu</th>
					            <th class="bg-black">Khách hàng</th>
					            <th class="bg-black">Ngày tạo</th>
					            <th class="bg-black right-custom">Tổng tiền</th>
					            <th class="bg-black">Trạng thái</th>
					            <th class="bg-black">Loại phiếu</th>
					            <th class="bg-black">Người tạo</th>
					            <th class="bg-black">Người yêu cầu</th>
					            <th class="bg-black">Ngày chuyển trạng thái</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-935">PN-935</a>
					            </td>
					            <td data-title="Khách hàng">Cty VNJP</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="green-custom"> Phiếu mới</span></td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-934">PN-934</a>
					            </td>
					            <td data-title="Khách hàng">Cty Happy Life Tea</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="red-custom"><span class="green-custom"> Phiếu mới (Đã in mã vạch)</span></td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-898">PN-898</a>
					            </td>
					            <td data-title="Khách hàng">Menard Việt Nam</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="blue-custom"> Quản lý xác nhận</span></td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-503">PN-503</a>
					            </td>
					            <td data-title="Khách hàng">Công ty TNHH Sản xuất và thương mại Phong Vinh</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="yellow-custom"><i class="fa fa-flag" aria-hidden="true"></i></span> Nhập kho</td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-555">PN-555</a>
					            </td>
					            <td data-title="Khách hàng">Công ty Cố phần Vang Tinh Hoa</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="yellow-custom"><i class="fa fa-flag" aria-hidden="true"></i></span> Nhập kho (Đã đàm phán)</td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-674">PN-674</a>
					            </td>
					            <td data-title="Khách hàng">Công ty TNHH Khoruou</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="yellow-custom"><i class="fa fa-flag" aria-hidden="true"></i></span> Nhập kho (Trả hàng)</td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-802">PN-802</a>
					            </td>
					            <td data-title="Khách hàng">Công ty TNHH PA VN Advertisement</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="red-custom"><i class="fa fa-flag" aria-hidden="true"></i></span> Yêu cầu đàm phán</td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-560">PN-560</a>
					            </td>
					            <td data-title="Khách hàng">Công ty TNHH Dược Mỹ Phẩm Innopha</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="red-custom"><i class="fa fa-flag" aria-hidden="true"></i></span> Yêu cầu trả hàng</td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-504">PN-504</a>
					            </td>
					            <td data-title="Khách hàng">Chị Hà (mỹ phẩm SK-II)</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="yellow-custom">Đã đàm phán</span></td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-474">PN-474</a>
					            </td>
					            <td data-title="Khách hàng">Bubas House</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="green-custom">Hoàn thành (Trả hàng)</span></td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Mã phiếu" class="center-custom">
					            	<a href="?action=include/listreceipt/edit.php" title="PN-474">PN-474</a>
					            </td>
					            <td data-title="Khách hàng">Bubas House</td>
					            <td data-title="Ngày tạo">21-02-2019 <strong>|</strong> 11:46:38</td>
					            <td data-title="Tổng tiền" class="right-custom">389,400 đ</td>
					            <td data-title="Trạng thái"><span class="red-custom">Hủy phiếu</span></td>
					            <td data-title="Loại phiếu">Phiếu nhập</td>
					            <td data-title="Người tạo">Thới Thị Bích Khiêm</td>
					            <td data-title="Người yêu cầu">Nguyễn Ngọc Anh Vũ</td>
					            <td data-title="Ngày chuyển">21-02-2019 <strong>|</strong> 11:46:38</td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<div class="total-order">
					<p>Tổng tiền: <span>100.000.000 VNĐ</span></p>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		var heightBottomTotal = jQuery('.total-order').outerHeight();
		var heightBottomPagination = jQuery('.pagination-custom').outerHeight();
		if(window.innerWidth < 576){
			jQuery('.box-table').css('margin-bottom',heightBottomPagination+heightBottomTotal);
		}
		if(window.innerWidth > 576){
		    jQuery('.dropdown-collapse').click(function(){
		    	if(jQuery('.custom-collapse').css('display')=='none'){
		    		jQuery('.custom-collapse').css('display','inline-block');
		    		jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
		    	}
		    	else{
		    		jQuery('.custom-collapse').css('display','none');
		    		jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
		    	}
		    });
		}else{
			jQuery('.show-filter-order').click(function(){
		    	if(jQuery('.custom-collapse').css('display')=='none'){
		    		jQuery('.custom-collapse').css('display','inline-block');
		    		jQuery('.dropdown-collapse').css('display','flex');
		    		jQuery(this).css('left',0);
		    		jQuery(this).css('top',38);
		    		jQuery(this).css('right','auto');
		    		jQuery(this).css('z-index','99');
		    		jQuery(this).parent().addClass('active');
		    		jQuery(this).find('i').removeClass('ti-search').addClass('ti-arrow-right');
		    		jQuery('body').css('overflow','hidden');
		    		jQuery('.filter-pc').css('z-index','999');

		    	}
		    	else{
		    		jQuery('.custom-collapse').css('display','none');
		    		jQuery('.dropdown-collapse').css('display','none');
		    		jQuery(this).css('left','auto');
		    		jQuery(this).css('right',0);
		    		jQuery(this).css('top',160);
		    		jQuery(this).parent().removeClass('active');
		    		jQuery(this).find('i').removeClass('ti-arrow-right').addClass('ti-search');
		    		jQuery('body').css('overflow','inherit');
		    		jQuery('.filter-pc').css('z-index','-1');
		    	}
		    });
		}
	    
	    var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    jQuery('#cbo_username,#cbo_username2').select2();
	    if(window.innerWidth > 576) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
	    jQuery('input[name="date-order"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date3').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date3').val(end.format('DD-MM-YYYY'));
		});
		jQuery('input[name="from_date_ad"]').daterangepicker({
			singleDatePicker: true,
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start) {
			jQuery('#start-date3').val(start.format('DD-MM-YYYY'));
		});
		jQuery('input[name="to_date_ad"]').daterangepicker({
			singleDatePicker: true,
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(end) {
			jQuery('#end-date3').val(end.format('DD-MM-YYYY'));
		});
	});
</script>