<style>
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	}
	.table-custom > tbody > tr > td a{
		float: left;
	}
	.table-custom > tbody > tr > td input{
		display: none;
		width: 250px;
	}
	@media (max-width: 575.98px) {
		.entry-header ul{
			display: none;
		}
		.table-custom > tbody > tr > td input{
			width: 100%;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="contact content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Khách hàng liên hệ</h1>
			<ul>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Họ và tên</th>
					            <th class="bg-black">Email</th>
					            <th class="bg-black">Điện thoại</th>
					            <th class="bg-black">Nội dung</th>
					            <th class="bg-black">Ngày gửi</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Họ và tên">tung</td>
					            <td data-title="Email">tung@gmail.com</td>
					            <td data-title="Điện thoại">0906324061</td>
					            <td data-title="Nội dung">test</td>
					            <td data-title="Ngày gửi">2018-03-16 11:06:13</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Họ và tên">tung2</td>
					            <td data-title="Email">tung@gmail.com</td>
					            <td data-title="Điện thoại">0906324061</td>
					            <td data-title="Nội dung">test</td>
					            <td data-title="Ngày gửi">2018-03-16 11:06:13</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Họ và tên">tung3</td>
					            <td data-title="Email">tung@gmail.com</td>
					            <td data-title="Điện thoại">0906324061</td>
					            <td data-title="Nội dung">test</td>
					            <td data-title="Ngày gửi">2018-03-16 11:06:13</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Họ và tên">tung4</td>
					            <td data-title="Email">tung@gmail.com</td>
					            <td data-title="Điện thoại">0906324061</td>
					            <td data-title="Nội dung">Chào bạn, ngày trước mình cũng như bạn đấy, bây giờ tháng mình kiếm 1000USD, nhà lầu, xe hơi</td>
					            <td data-title="Ngày gửi">2018-03-16 11:06:13</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Họ và tên">tung5</td>
					            <td data-title="Email">tung@gmail.com</td>
					            <td data-title="Điện thoại">0906324061</td>
					            <td data-title="Nội dung">test</td>
					            <td data-title="Ngày gửi">2018-03-16 11:06:13</td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		if(window.innerWidth < 576){
			jQuery('.entry-content').css('margin-bottom','15px')
		}
	})
</script>