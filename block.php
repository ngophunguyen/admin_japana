<style>
	.box-quick-search {
	    display: inline-block;
	    width: 100%;
	    margin-top: 15px;
	}
	.box-quick-search .item {
	    display: inline-block;
	    width: 50%;
	    float: left;
	}
	.box-quick-search .item input{
		width: 50%;
    	float: left;
	}
	.box-quick-search .item button {
	    float: left;
	    margin-left: 15px;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin: 15px 0;
	    position: relative;
	}
	.table-custom{
		margin-top: 0;
		white-space: nowrap;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		height: auto;
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.box-table tr td:nth-child(2) img{
		width: 100px;
	}
	
	@media (max-width: 575.98px) {
		.box-quick-search .item{
	  		width: 100%;
	  	}
	  	.box-quick-search .item input{
	  		width: 100%;
	  	}
	  	.box-quick-search .item button{
	  		margin-left: 0;
	  		margin-top: 15px;
	  	}
	  	.table-custom > tbody > tr > td{
			min-height: 40px;
		}
		.table-responsive > tbody > tr td:first-child {
		    display: none;
		}
		.table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: initial;
			    -ms-flex-pack: initial;
			        justify-content: initial;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-quick-search .item input{
			width: 65%;
		}
		.box-quick-search .item button{
			width: 30%;
		}

	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.box-quick-search .item input{
			width: 65%;
		}
		.box-quick-search .item button{
			width: 30%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {

	}
</style>
<main class="block content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách block</h1>
			<ul>
				<li>
					<a href="?action=include/block/add.php" class="link-custom black-custom" title="Tạo block">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Tạo block</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post" class="search1">
	                       <input autocomplete="off" name="search" value="" type="text" class="form-control custom-ipt" placeholder="Tìm block...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black center-custom">Hình ảnh</th>
					            <th class="bg-black">Tên block</th>
					            <th class="bg-black">Tên code</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="assets/images/no-img2.png" alt="Không có hình ảnh">
					            </td>
					            <td data-title="Tên block">
					            	<a href="?action=include/block/edit.php" title="Chỉnh sửa">Countdown banner 4 hình</a>
					            </td>
					            <td data-title="Tên code">Countdown1</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="assets/images/no-img2.png" alt="Không có hình ảnh">
					            </td>
					            <td data-title="Tên block">
					            	<a href="?action=include/block/edit.php" title="Chỉnh sửa">Countdown theo SKUs</a>
					            </td>
					            <td data-title="Tên code">Countdown2</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="assets/images/no-img2.png" alt="Không có hình ảnh">
					            </td>
					            <td data-title="Tên block">
					            	<a href="?action=include/block/edit.php" title="Chỉnh sửa">banner landingpage</a>
					            </td>
					            <td data-title="Tên code">banner2</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="assets/images/no-img2.png" alt="Không có hình ảnh">
					            </td>
					            <td data-title="Tên block">
					            	<a href="?action=include/block/edit.php" title="Chỉnh sửa">4.4.4 banner</a>
					            </td>
					            <td data-title="Tên code">banner4</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="assets/images/no-img2.png" alt="Không có hình ảnh">
					            </td>
					            <td data-title="Tên block">
					            	<a href="?action=include/block/edit.php" title="Chỉnh sửa">1 banner multiple products</a>
					            </td>
					            <td data-title="Tên code">product_full_width</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="assets/images/no-img2.png" alt="Không có hình ảnh">
					            </td>
					            <td data-title="Tên block">
					            	<a href="?action=include/block/edit.php" title="Chỉnh sửa">1 banner 6 products</a>
					            </td>
					            <td data-title="Tên code">banner_pro3</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="assets/images/no-img2.png" alt="Không có hình ảnh">
					            </td>
					            <td data-title="Tên block">
					            	<a href="?action=include/block/edit.php" title="Chỉnh sửa">5.1.1 Banner</a>
					            </td>
					            <td data-title="Tên code">banner3</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="assets/images/no-img2.png" alt="Không có hình ảnh">
					            </td>
					            <td data-title="Tên block">
					            	<a href="?action=include/block/edit.php" title="Chỉnh sửa">product_filter1</a>
					            </td>
					            <td data-title="Tên code">product_filter1</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="assets/images/no-img2.png" alt="Không có hình ảnh">
					            </td>
					            <td data-title="Tên block">
					            	<a href="?action=include/block/edit.php" title="Chỉnh sửa">product_3_column</a>
					            </td>
					            <td data-title="Tên code">product_3_column</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="assets/images/no-img2.png" alt="Không có hình ảnh">
					            </td>
					            <td data-title="Tên block">
					            	<a href="?action=include/block/edit.php" title="Chỉnh sửa">product_detail</a>
					            </td>
					            <td data-title="Tên code">product_detail</td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){

	})
</script>