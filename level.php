<style>
	.table-custom > tbody > tr > td input{
		display: none;
		width: 250px;
	}
	.table-custom tbody tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	@media (max-width: 575.98px) {
		.table-custom > tbody > tr > td input{
			width: 100%;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="level content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">VIP</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#addvip-modal" class="link-custom black-custom open-receipt" title="Thêm VIP">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm VIP</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Cấp level</th>
					            <th class="bg-black">Điều kiện</th>
					            <th class="bg-black center-custom">Ưu đãi</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Cấp level">
					            	<input autocomplete="off" type="text" name="hide_name1" id="hide_name1" class="form-control" value="VIP Diamond">
									<span>VIP Diamond</span>
					            </td>
					            <td data-title="Điều kiện">
					            	<input autocomplete="off" type="text" name="hide_condition1" id="hide_condition1" class="form-control" value="50000000">
									<span>50.000.000</span>
					            </td>
					            <td data-title="Ưu đãi" class="center-custom">
					            	<input autocomplete="off" type="text" name="hide_discount1" id="hide_discount1" class="form-control" value="5">
									<span>5</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(1);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Cấp level">
					            	<input autocomplete="off" type="text" name="hide_name2" id="hide_name2" class="form-control" value="VIP Platinum">
									<span>VIP Platinum</span>
					            </td>
					            <td data-title="Điều kiện">
					            	<input autocomplete="off" type="text" name="hide_condition2" id="hide_condition2" class="form-control" value="30000000">
									<span>30.000.000</span>
					            </td>
					            <td data-title="Ưu đãi" class="center-custom">
					            	<input autocomplete="off" type="text" name="hide_discount2" id="hide_discount2" class="form-control" value="4">
									<span>4</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(2);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Cấp level">
					            	<input autocomplete="off" type="text" name="hide_name3" id="hide_name3" class="form-control" value="VIP Gold">
									<span>VIP Gold</span>
					            </td>
					            <td data-title="Điều kiện">
					            	<input autocomplete="off" type="text" name="hide_condition3" id="hide_condition3" class="form-control" value="10000000">
									<span>10.000.000</span>
					            </td>
					            <td data-title="Ưu đãi" class="center-custom">
					            	<input autocomplete="off" type="text" name="hide_discount3" id="hide_discount3" class="form-control" value="3">
									<span>3</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(3);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Cấp level">
					            	<input autocomplete="off" type="text" name="hide_name4" id="hide_name4" class="form-control" value="Thành viên mới">
									<span>Thành viên mới</span>
					            </td>
					            <td data-title="Điều kiện">
					            	<input autocomplete="off" type="text" name="hide_condition4" id="hide_condition4" class="form-control" value="0">
									<span>0</span>
					            </td>
					            <td data-title="Ưu đãi" class="center-custom">
					            	<input autocomplete="off" type="text" name="hide_discount4" id="hide_discount4" class="form-control" value="0">
									<span>0</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(4);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Cấp level">
					            	<input autocomplete="off" type="text" name="hide_name5" id="hide_name5" class="form-control" value="Chưa đăng ký">
									<span>Chưa đăng ký</span>
					            </td>
					            <td data-title="Điều kiện">
					            	<input autocomplete="off" type="text" name="hide_condition5" id="hide_condition5" class="form-control" value="0">
									<span>0</span>
					            </td>
					            <td data-title="Ưu đãi" class="center-custom">
					            	<input autocomplete="off" type="text" name="hide_discount5" id="hide_discount5" class="form-control" value="0">
									<span>0</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(5);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Cấp level">
					            	<input autocomplete="off" type="text" name="hide_name6" id="hide_name6" class="form-control" value="Nhân viên công ty">
									<span>Nhân viên công ty</span>
					            </td>
					            <td data-title="Điều kiện">
					            	<input autocomplete="off" type="text" name="hide_condition6" id="hide_condition6" class="form-control" value="0">
									<span>0</span>
					            </td>
					            <td data-title="Ưu đãi" class="center-custom">
					            	<input autocomplete="off" type="text" name="hide_discount6" id="hide_discount6" class="form-control" value="0">
									<span>0</span>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" onclick="editItem(6);" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/level/add.php'); ?>
<script>
	function editItem(id) {
	    jQuery('#hide_name' + id).css('display', 'block');
	    jQuery('#hide_name' + id).next().css('display', 'none');
	    jQuery('#hide_condition' + id).css('display', 'block');
	    jQuery('#hide_condition' + id).next().css('display', 'none');
	    jQuery('#hide_discount' + id).css('display', 'block');
	    jQuery('#hide_discount' + id).next().css('display', 'none');
	}
	jQuery(function(){
		
	})
</script>