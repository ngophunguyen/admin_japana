<style>
	.custom-dropdown:after{
		padding-right: 15px;
	}
	.show-cate{
		margin: 15px 0 0;
	}
	.list-cate-ingroup{
		display: none;
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:last-child{
		width: 55%;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	}
	.box-quick-search .item:first-child input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child button{
		float: right;
	}
	.box-quick-search .item:last-child .custom-dropdown{
		width: 40%;
	}
	.search2, .search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.search1{
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.box-table tr td:last-child {
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	    -ms-flex-align: center;
	    align-items: center;
	    padding: 15px 0;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item, .box-quick-search .item:first-child input, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child,.box-quick-search .item:last-child button{
	  		margin-top: 15px;
	  	}
	  	.box-quick-search .item:last-child,.search2{
	  		display: inline-block;
	  	}
	  	.box-quick-search .item:last-child .custom-dropdown{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child button{
	  		float: left;
	  	}
	  	.box-quick-search .item:last-child button{
	  		margin-left: 0;
	  		clear: both;
	  	}
		.table-custom > tbody > tr > td:last-child {
		    -ms-flex-pack: distribute;
		    justify-content: space-around;
		    padding: 0;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.box-quick-search .item, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child{
	  	 	margin-top: 15px;
	  	}
	  	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 80%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.custom-dropdown:after{
	  		padding: 12px 15px;
	  	}
	  	.box-quick-search .item, .box-quick-search .item:last-child{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:last-child{
	  	 	margin-top: 15px;
	  	}
	  	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 80%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 1200px) {	
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
</style>
<main class="banner content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách banner</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#addbanner-modal" class="link-custom black-custom" title="Thêm banner">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm banner</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<a class="show-cate dropdown-collapse bg-black" href="javascript:void(0);">Danh mục banner <i class="fa fa-caret-down" aria-hidden="true"></i></a>
				<div class="list-cate-ingroup">
					<div class="box-table">
						<table class="table table-custom table-striped table-responsive">
						    <thead class="bg-black">
						        <tr class="bg-black">
						            <th class="bg-black center-custom">STT</th>
						            <th class="bg-black center-custom">ID</th>
						            <th class="bg-black">Tên danh mục</th>
						            <th class="bg-black center-custom">Trạng thái</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td data-title="STT" class="center-custom">1</td>
						            <td data-title="ID" class="center-custom">1</td>
						            <td data-title="Tên danh mục">Banner trên</td>
						            <td data-title="Trạng thái" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">2</td>
						            <td data-title="ID" class="center-custom">2</td>
						            <td data-title="Tên danh mục">Banner dưới</td>
						            <td data-title="Trạng thái" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" checked="checked" />
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">3</td>
						            <td data-title="ID" class="center-custom">3</td>
						            <td data-title="Tên danh mục">Banner trái</td>
						            <td data-title="Trạng thái" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" />
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">4</td>
						            <td data-title="ID" class="center-custom">4</td>
						            <td data-title="Tên danh mục">Banner phải</td>
						            <td data-title="Trạng thái" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" />
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">5</td>
						            <td data-title="ID" class="center-custom">5</td>
						            <td data-title="Tên danh mục">Banner popup</td>
						            <td data-title="Trạng thái" class="center-custom">
						            	<input type="checkbox" class="checkbox-ios" />
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
				</div>
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Nhập tên banner...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<form name="status_search" id="frm" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="id_banner_cate" name="id_banner_cate">
						    		<option value="-1">Chọn danh mục</option>
								  	<option value="0">Banner trên</option>
								  	<option value="1">Banner dưới</option>
								</select>
							</div>
	                        <button type="submit" class="button bg-black">Tìm kiếm</button>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Danh mục</th>
					            <th class="bg-black">Tên banner</th>
					            <th class="bg-black">Url</th>
					            <th class="bg-black center-custom">Trạng thái</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Danh mục">Banner trên</td>
					            <td data-title="Tên banner">Lì xì may mắn</td>
					            <td data-title="Url">https://japana.vn</td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewbanner-modal" class="link-custom black-custom" title="Xem trước">
					            		<i class="fa fa-eye" aria-hidden="true"></i>
					            	</a>
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#editbanner-modal" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Danh mục">Banner dưới</td>
					            <td data-title="Tên banner">Chúc tết</td>
					            <td data-title="Url">https://japana.vn</td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewbanner-modal" class="link-custom black-custom" title="Xem trước">
					            		<i class="fa fa-eye" aria-hidden="true"></i>
					            	</a>
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#editbanner-modal" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Danh mục">Banner trên</td>
					            <td data-title="Tên banner">Japana</td>
					            <td data-title="Url">https://japana.vn</td>
					            <td data-title="Trạng thái" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#viewbanner-modal" class="link-custom black-custom" title="Xem trước">
					            		<i class="fa fa-eye" aria-hidden="true"></i>
					            	</a>
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#editbanner-modal" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/banner/add.php'); ?>
<?php include('include/banner/edit.php'); ?>
<?php include('include/banner/view.php'); ?>
<script>
	jQuery(".checkbox-ios").iosCheckbox();
	jQuery(function(){
		jQuery('.show-cate').click(function(){
	    	if(jQuery('.list-cate-ingroup').css('display')=='none'){
	    		jQuery('.list-cate-ingroup').css('display','block');
	    		jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
	    	}
	    	else{
	    		jQuery('.list-cate-ingroup').css('display','none');
	    		jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
	    	}
	    });
	})
</script>