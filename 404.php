<style>
	.entry-content{
		margin-top: 0;
		height: 100vh;
		position: relative;
	}
	.box-alert{
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%,-50%);
		text-align: center;
	}
	.box-alert p{
		font-weight: 600;
		font-size: 20vw;
		color: #ccc;
	}
	.box-alert span{
		font-size: 1.8vw;
		font-weight: 500;
		color: #ccc;
		line-height: 1.5;
	}
	@media (max-width: 575.98px) {
		.box-alert p{
			font-size: 45vw;
		}
		.box-alert span{
			font-size: 5.5vw;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.box-alert p{
			font-size: 45vw;
		}
		.box-alert span{
			font-size: 5.5vw;
		}	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="404 content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Lỗi !!!</h1>
			<ul>
				<li>
					<a href="?action=index.php" class="link-custom red-custom" title="Thoát">
                        <i class="ti-close" aria-hidden="true"></i> <label>Thoát</label>
                    </a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-alert">
					<p>404</p>
					<span>Trang không tìm thấy hoặc tính năng đang phát triển</span>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){

	})
</script>