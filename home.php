<style>
	[class$="-legend"] {
		list-style: none;
		cursor: pointer;
		padding-left: 0;
	}

	[class$="-legend"] li {
		display: inline-block;
		padding: 0 5px;
		margin-right: 25px;
		font-size: 12px;
		color: #4D4F5C;
	}

	[class$="-legend"] li.hidden {
		text-decoration: line-through;
	}

	[class$="-legend"] li span {
		border-radius: 5px;
		display: inline-block;
		height: 10px;
		margin-right: 10px;
		width: 20px;
	}
	.box-revenue,
	.time-graph{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	}
	.box-revenue{
		-webkit-box-pack: justify;
		    -ms-flex-pack: justify;
		        justify-content: space-between;
		width: 100%;
	}
	.box-revenue .item{
		float: left;
		line-height: 1.5;
		width: 19%;
		border-radius: 5px;
    	padding: 15px;
		border: 2px solid rgba(0,0,0,0.1);
		position: relative;
	}

	.box-revenue .item .title{
		font-size: 14px;
		font-weight: 400;
		margin-bottom: 20px;
	}

	.box-revenue .item .price{
		font-size: 22px;
		font-weight: 500;
		color: #B8B7C1;
		margin-bottom: 0;
	}

	.box-revenue .item .count-order{
		font-size: 13px;
		color: #333333;
		margin-bottom: 0;
	}
	.time-graph{
    	-webkit-box-pack: end;
    	    -ms-flex-pack: end;
    	        justify-content: flex-end;
    	margin: 15px 0;
	}
	.time-graph .box-time{
		margin-right: 10px; 
		width: auto;
	}
	.time-graph span{
		font-family:'Roboto',sans-serif; 
		font-weight: 400; 
		font-size: 14px; 
		text-align: right; 
		margin-bottom: 0; 
		width: auto;
	}
	.chartWrapper {
	  position: relative;
	  overflow: hidden;
	}

	.chartWrapper > canvas {
	  position: absolute;
	  left: 0;
	  top: 0;
	  pointer-events: none;
	}
	.chartAreaWrapper {
	  width: 100%;
	  overflow-x: scroll;
	}
	#legend{
		margin: 15px 0;
	}
	@media (max-width: 575.98px) {
		.entry-header ul{
			display: none;
		}
		.entry-content{
			margin-top: 48px!important;
		}
		.button{
			font-size: 14px;
		    display: inline-block;
		    padding: 0 5px;
		}
		.form-control{
			font-size: 14px;
		}
	  	.box-revenue{
	  		display: inline-block;
	  		width: 100%;
		    margin: 0 auto;
	  	}
	  	.box-revenue .item{
	  		width: 49%;
	  		display: inline-block;
	  		text-align: center;
	  		padding: 2vh 0;
	  		margin-bottom: 2%;
	  	}
	  	.box-revenue .item:nth-child(odd){
	  		margin-left: 2%;
	  	}
	  	.box-revenue .item:first-child{
	  		width: 100%;
	  		margin-left: 0;
	  	}

	  	.box-graph>.title{
	  		font-size: 18px;
	  		margin: 2.5vh 0;
	  	}
	  	.box-revenue .item .title{
	  		font-size: 14px;
	  		margin-bottom: 2vh;
	  	}
	  	.box-revenue .item .count-order{
	  		font-size: 14px;
	  	}
	  	.box-revenue .item .price{
	  		font-size: 15px;
	  	}

	  	.time-graph, .time-graph .box-time, .ipt-date{
	  		display: inline-block;
	  		width: 100%;
	  	}
	  	.time-graph .box-time{
	  		margin-right: 0;
	  		margin-bottom: 3%;
	  	}
	  	.chart-container{
	  		position: relative; 
	  		overflow: scroll;
	  	}
	  	[class$="-legend"] li{
	  		margin-right: 0;
	  		width: 50%;
	  		font-size: 14px;
	  		margin: 5px 0;
	  	}
	  	.content {
		    margin-bottom: 15px;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	  	.box-revenue .item{
	  		padding: 5px;
	  	}
	  	.box-revenue .item .title{
	  		font-size: 12px;
	  	}
	  	.box-revenue .item .price{
	  		font-size: 2vw;
	  	}
	  	[class$="-legend"] li{
	  		margin-right: 10px;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
	  	.box-revenue .item{
	  		padding: 5px;
	  	}
	  	.box-revenue .item .price{
	  		font-size: 2vw;
	  	}
	  	[class$="-legend"] li{
	  		margin-right: 10px;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
	  	[class$="-legend"] li{
	  		margin-right: 15px;
	  	}
	}
	@media (min-width: 1200px) {

	}
</style>
<main class="dashboard content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Dashboard</h1>
			<ul>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-graph">
		            <form method="POST" action="">
	                    <div class="time-graph">
	                        <div class="box-time">
	                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-chart" id="date-chart" class="form-control ipt-date" placeholder="Chọn ngày...">
	                            <i class="fa fa-calendar icon-time"></i>
	                        </div>
	                        <button type="submit" class="button bg-black">Xác nhận</button>
	                    </div>
	                </form>
	                <div class="chartWrapper">
  						<div class="chartAreaWrapper">
  							<div class="chartAreaWrapper2">
				        		<canvas id="canvas" height="110"></canvas>
				    		</div>
					  	</div>
					</div>
		            <div id="legend"></div>
		            <div class="box-revenue">
		                <div class="item">
		                    <h3 class="title">Tổng doanh thu</h3>
		                    <p class="price" style="color: #FF8000;">
		                    189.070.070 đ</p>
		                    <p class="count-order">135 đơn hàng</p>
		                </div>
		                <div class="item">
		                    <h3 class="title">Tổng doanh số</h3>
		                    <p class="price" style="color: #d21762;">
		                    593.955.550 đ</p>
		                    <p class="count-order">475 đơn hàng</p>
		                </div>
		                <div class="item">
		                    <h3 class="title">Tổng đơn mới</h3>
		                    <p class="price" style="color: #04B404;">
		                    713.552.340 đ</p>
		                    <p class="count-order">499 đơn hàng</p>
		                </div>
		                <div class="item">
		                    <h3 class="title">Tổng hủy đơn</h3>
		                    <p class="price" style="color: #ffd425;">
		                    184.121.400 đ</p>
		                    <p class="count-order">112 đơn hàng</p>
		                </div>
		                <div class="item">
		                    <h3 class="title">Tổng trả đơn</h3>
		                    <p class="price" style="color: #10bbda;">
		                    688.000 đ</p>
		                    <p class="count-order">10 đơn hàng</p>
		                </div>
		            </div>
		        </div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		new PerfectScrollbar('.chartAreaWrapper');
		var formatDate = {
		      	format: 'DD/MM/YYYY',
		      	daysOfWeek: [
		            "CN",
		            "T2",
		            "T3",
		            "T4",
		            "T5",
		            "T6",
		            "T7"
		        ],
		        monthNames: [
		            "Tháng 1",
		            "Tháng 2",
		            "Tháng 3",
		            "Tháng 4",
		            "Tháng 5",
		            "Tháng 6",
		            "Tháng 7",
		            "Tháng 8",
		            "Tháng 9",
		            "Tháng 10",
		            "Tháng 11",
		            "Tháng 12"
		        ],
		    }
		jQuery('input[name="date-chart"]').daterangepicker({
			opens: 'left',
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		});
	});
	function addData(chart){
		if(window.innerWidth < 576) {
			var newwidth = jQuery('.chartAreaWrapper2').width() + 200;
		}
		else{
			var newwidth = jQuery('.chartAreaWrapper2').width();
		}
		jQuery('.chartAreaWrapper2').width(newwidth);
	}
	var chartData = {
	  	labels: ['14/02','15/02','16/02','17/02','18/02','19/02','20/02','21/02'],
        datasets: [{
            label: 'Tổng doanh thu',
            fill: false,
            hidden: true,
            pointRadius: 4,
            pointHoverRadius: 7,
            borderWidth: 2,
            backgroundColor: "#FF8000",
            borderColor: "#FF8000",
            data: [96507150,0,0,0,184918350,0,0,4261280],
            data1: [74,0,0,0,130,0,0,1],
        }, {
            label: 'Tổng doanh số',
            fill: false,
            pointRadius: 4,
            pointHoverRadius: 7,
            borderWidth: 2,
            backgroundColor: "#d21762",
            borderColor: "#d21762",
            data: [112110150,131347550,63998200,0,185606350,99472930,95238800,25485720],
            data1: [79,110,52,0,131,91,77,20],
        }, {
            label: 'Tổng đơn mới',
            fill: false,
            pointRadius: 4,
            pointHoverRadius: 7,
            borderWidth: 2,
            backgroundColor: "#04B404",
            borderColor: "#04B404",
            data: [116080750,99396890,85493660,127502500,125432040,119911430,98662100,67338720],
            data1: [66,82,70,59,94,83,77,42],
        }, {
            label: 'Tổng hủy đơn',
            fill: false,
            hidden: true,
            pointRadius: 4,
            pointHoverRadius: 7,
            borderWidth: 2,
            backgroundColor: "#ffd425",
            borderColor: "#ffd425",
            data: [8388000,19647000,1170000,0,95192400,49613000,0,21552000],
            data1: [4,14,2,0,59,30,0,11],
        }, {
            label: 'Tổng trả đơn',
            fill: false,
            hidden: true,
            pointRadius: 4,
            pointHoverRadius: 7,
            borderWidth: 2,
            backgroundColor: "#10bbda",
            borderColor: "#10bbda",
            data: [15603000,0,0,0,688000,0,0,29747000],
            data1: [5,0,0,0,1,0,0,21],
        }]
	};
	jQuery(function() {
	  	var canvasChart = jQuery('#canvas');
	  	if(window.innerWidth < 576) {
			canvasChart.height = 150;
		}
	  	var fullChart = new Chart(canvasChart, {
		    type: 'line',
	        data: chartData,
	        options: {
	            responsive: true,
	            elements: {
	                line: {
	                    tension: 0,
	                }
	            },
	            legendCallback: function(chart) {
				    var text = [];
				    text.push('<ul class="' + chart.id + '-legend">');
				    for (var i = 0; i < chart.data.datasets.length; i++) {
			     		text.push('<li id="'+ i +'" onclick="updateDataset(event, ' + '\'' + i + '\'' + ')"><span style="background-color:' + chart.data.datasets[i].backgroundColor + '">');
				      	text.push('</span>');
				      	if (chart.data.datasets[i].label) {
		                    text.push(chart.data.datasets[i].label);
		                }
				      	text.push('</li>');
				    }
				    text.push('</ul>');
				    return text.join("");
				},
				legend: {
	                display: false,
	            },
	            title: {
	                display: false,
	                text: 'Thống kê doanh số'
	            },
	            tooltips: {
	                mode: 'index',
	                intersect: false,
	                bodySpacing: 10,
	                callbacks: {
	                    label: function(t, d) {
	                       var xLabel = d.datasets[t.datasetIndex].label;
	                       var count = d.datasets[t.datasetIndex].data1[t.index];
	                       var yLabel = t.yLabel >= 1000 ? t.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' đ' : t.yLabel+ ' đ';
	                       return xLabel + ': ' + yLabel + ' - ' + count + ' đơn hàng';
	                    }
	                }
	            },
	            hover: {
	                mode: 'nearest',
	                intersect: true
	            },
	            scales: {
	                xAxes: [{
	                    display: true,
	                    scaleLabel: {
	                        display: true,
	                        labelString: '',
	                    },
	                    ticks: {
	                        callback: function(value, index, values) {
	                            return value;
	                            
	                        }
	                    }
	                }],
	                yAxes: [{
	                    display: true,
	                    scaleLabel: {
	                        display: false,
	                        labelString: 'Triệu'
	                    },
	                    ticks: {
	                        beginAtZero: true,
	                        callback: function(value, index, values) {
	                            if (parseInt(value) >= 1000) {
	                                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' đ';
	                            } else {
	                                return value + ' đ';
	                            }
	                        }
	                    }
	                }]
	            },
	            animation: {
			        onComplete: function() {
			        	if(jQuery('#canvas').length) {
				          	if(window.innerWidth > 576) {
								setTimeout(function(){
									if(jQuery('.nav-primary').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight() && jQuery('.nav-primary').outerHeight() > jQuery('body').outerHeight()){
										jQuery('.nav-primary').css('height','auto');
									}else if(jQuery('body').outerHeight() > jQuery('.nav-primary').outerHeight() && jQuery('body').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight()){
										jQuery('.nav-primary').css('height',jQuery('body').outerHeight());
									}
									else{
										jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
									}
								},10)
							}
				        }
			        }
			    }
	        }
	  	});
	  	addData(fullChart);
		updateDataset = function(e, datasetIndex) {    
			var index = datasetIndex;
	        var ci = fullChart;
	        var meta = ci.getDatasetMeta(index);
	        var hiddenDatasets = [];
	       	meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;
		    var result= (meta.data[datasetIndex].hidden == true) ? false : true;
		    if(result==true)
		    {
		        jQuery('#' + e.path[0].id).css("text-decoration", "line-through");
		        meta.data[datasetIndex].hidden = true;
		    }else{
		        jQuery('#' + e.path[0].id).css("text-decoration","");
		        meta.data[datasetIndex].hidden = false;
		    }
		    for(var i=0; i<fullChart.data.datasets.length; i++) {
			    if (!fullChart.isDatasetVisible(i)) {
			        jQuery('#' + i).css("text-decoration", "line-through");
			        hiddenDatasets.push(fullChart.data.datasets[i]);
			    }
			    else{
			    	jQuery('#' + i).css("text-decoration","");
			    }
			}
		    ci.update();   
		};
	  	document.getElementById('legend').innerHTML = fullChart.generateLegend();
	  	var hiddenDatasets = [];
		for(var i=0; i<fullChart.data.datasets.length; i++) {
		    if (!fullChart.isDatasetVisible(i)) {
		        jQuery('#' + i).css("text-decoration", "line-through");
		        hiddenDatasets.push(fullChart.data.datasets[i]);
		    }
		}
	});
</script>