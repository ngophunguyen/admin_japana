<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item button{
		float: left;
		margin-left: 15px;
	}
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item, 
	  	.box-quick-search .item:first-child input{
	  		width: 100%;
	  	}
		.search1{
	  		display: inline-block;
	  		width: 100%;
	  	}
	  	.box-quick-search .item:first-child button{
	  		margin-left: 0;
	  		margin-top: 15px;
	  	}
		.refund .table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="refund content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách trả hàng</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#listproinday-modal" class="link-custom black-custom" title="Hàng trả trong ngày">
						<i class="fa fa-calendar-o" aria-hidden="true"></i> <label>Hàng trả trong ngày</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Nhập thông tin cần tìm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">Mã DVVC</th>
					            <th class="bg-black">Tên DVVC</th>
					            <th class="bg-black center-custom">Số lượng trả</th>
					            <th class="bg-black right-custom">Tổng tiền trả</th>
					            <th class="bg-black right-custom">Ngày cập nhật</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Mã DVVC">
					            	<a href="?action=include/refund/edit.php" title="GHTK">GHTK</a>
					            </td>
					            <td data-title="Tên DVVC">Giao hàng tiết kiệm</td>
					            <td data-title="Số lượng trả" class="center-custom">1</td>
					            <td data-title="Tổng tiền trả" class="right-custom">210.000 đ</td>
					            <td data-title="Ngày cập nhật" class="right-custom">27-05-2019 | 13:56</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Mã DVVC">
					            	<a href="?action=include/refund/edit.php" title="GHN">GHN</a>
					            </td>
					            <td data-title="Tên DVVC">Giao hàng nhanh</td>
					            <td data-title="Số lượng trả" class="center-custom">5</td>
					            <td data-title="Tổng tiền trả" class="right-custom">210.000 đ</td>
					            <td data-title="Ngày cập nhật" class="right-custom">27-05-2019 | 13:56</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Mã DVVC">
					            	<a href="?action=include/refund/edit.php" title="SAT">SAT</a>
					            </td>
					            <td data-title="Tên DVVC">Ship an toàn</td>
					            <td data-title="Số lượng trả" class="center-custom">0</td>
					            <td data-title="Tổng tiền trả" class="right-custom">0 đ</td>
					            <td data-title="Ngày cập nhật" class="right-custom"></td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/refund/listproinday.php'); ?>
<script>
	jQuery(function(){
		
	})
</script>