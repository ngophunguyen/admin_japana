<!DOCTYPE html>
<html lang="vi">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="title" content="Admin Ver2">
		<meta name="description" content="Admin Ver2">
		<meta name="keywords" content="Admin Ver2">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui" />
		<title>Admin JAPANA Ver2</title>
		<link rel="shortcut icon" href="assets/images/favicon.webp">

		<!--include CSS-->
		<link rel="stylesheet" href="assets/plugins/jquery-ui/jquery-ui.min.css">
		<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/plugins/themify-icons/themify-icons.css">
		<link rel="stylesheet" href="assets/plugins/normalize/normalize.css">
		<link rel="stylesheet" href="assets/plugins/normalize/libs.css">
		<link rel="stylesheet" href="assets/css/japana.css">
		<!--/include CSS-->

		<!--include JS-->
		<script src="assets/plugins/jquery/jquery-3.3.1.min.js"></script>
		<script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
		<!--/include JS-->

		<!--include Font-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=vietnamese" rel="stylesheet">
        <!--/.include Font-->

        <style>
        	.site-container, .site-inner{
        		width: 100%;
        		margin: 0 auto;
        	}
        	.content-sidebar-wrap{
        		width: 100%;
			    min-height: 100vh;
			    display: -webkit-box;
			    display: -ms-flexbox;
			    display: flex;
			    -ms-flex-wrap: wrap;
			        flex-wrap: wrap;
			    -webkit-box-pack: center;
			        -ms-flex-pack: center;
			            justify-content: center;
			    -webkit-box-align: center;
			        -ms-flex-align: center;
			            align-items: center;
			    padding: 15px;
			    background-color: #ebebeb;
        	}
        	.login{
        		margin-bottom: 0;
        	}
        	.box-login{
        		width: 500px;
			    background: #fff;
			    border-radius: 10px;
			    position: relative;
			   	padding: 25px 60px;
    			display: inline-block;
        	}
        	.logo{
        		text-align: center;
        		margin-bottom: 10px;
        	}
        	.logo img{
        		width: 200px;
        	}
        	.box-login .title{
        		font-size: 15px;
        		font-family: 'Roboto', sans-serif;
        		font-weight: 400;
        		display: inline-block;
    			width: 100%;
    			color: #333
        	}
        	.box-login input{
        		margin: 10px 0;
        		height: 40px;
        	}
        	.container{
        		margin-bottom: 10px;
        		cursor: pointer;
			    display: -webkit-box;
			    display: -ms-flexbox;
			    display: flex;
			    -webkit-box-align: center;
			        -ms-flex-align: center;
			            align-items: center;
			    height: 20px;
        	}
        	.container label{
        		color: #ccc;
        	}
        	.box-login .item{
        		position: relative;
        	}
        	.box-login .item a{
        		position: absolute;
			    top: 50%;
			    right: 0;
			    -webkit-transform: translate(-50%,-50%);
			        -ms-transform: translate(-50%,-50%);
			            transform: translate(-50%,-50%);
			    color: #999;
			    font-size: 16px;
			    width: 20px;
			    display: -webkit-box;
			    display: -ms-flexbox;
			    display: flex;
			    -webkit-box-align: center;
			        -ms-flex-align: center;
			            align-items: center;
			    -webkit-box-pack: center;
			        -ms-flex-pack: center;
			            justify-content: center;
        	}
        	.box-login .item a:hover{
        		color: #333;
        	}
        	#login-submit{
        		margin-top: 10px;
        	}
        	@media (max-width: 575.98px) {
			  	.login, .box-login{
			  		width: 100%;
			  	}
			  	.box-login{
			  		padding: 20px;
			  	}
			  	.logo img{
			  		width: 65%;
			  	}
			}
			@media (min-width: 576px) and (max-width: 767.98px) {
			  	
			}
        	@media (min-width: 768px) and (max-width: 991.98px) {
			  	
			}
			@media (min-width: 992px) and (max-width: 1199.98px) {

			}
			@media (min-width: 1200px) {
				
			}
        </style>
	</head>

	<body>
		<div class="site-container">
			<div class="site-inner">
				<div class="content-sidebar-wrap">
					<main class="login content">
						<div class="box-login">
							<form method="GET" action="/" class="form-group nomargin">
								<div class="logo">
									<a href="https://japana.vn" title="JAPANA">
										<img src="assets/images/logo.svg" alt="logo">
									</a>
								</div>
								<span class="title">
									Tên đăng nhập
								</span>
								<div class="item">
									<input autocomplete="off" id="username" type="text" name="username" placeholder="Tên đăng nhập" class="form-control">
									<a href="javascript:void(0);" title="Username">
										<i class="fa fa-user" aria-hidden="true"></i>
									</a>
								</div>
								<span class="title">
									Mật khẩu
								</span>
								<div class="item">
									<input autocomplete="off" id="pwd" type="password" name="password" placeholder="Mật khẩu" class="form-control">
									<a href="javascript:void(0);" title="Mật khẩu">
										<i class="fa fa-unlock-alt" aria-hidden="true"></i>
									</a>
								</div>
								<button id="login-submit" type="button" class="button bg-black custom-fright button-submit">Đăng nhập</button>
							</form>
						</div>
					</main>
				</div>
			</div>
		</div>
		<div id="myModal" class="modal danger-modal fade" role="dialog">
		  	<div class="modal-dialog">
			    <div class="modal-content custom-modal">
			    	<a href="javascript:void(0);" class="close-modal" title="Thoát">
				    	<span class="ti-close" data-dismiss="modal"></span>
				    </a>
			      	<div class="modal-body">
			        	<p>Bạn chưa nhập tên đăng nhập.</p>
			      	</div>
			    </div>
		  	</div>
		</div>
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script>
		  	jQuery("#login-submit").click(function(){
				if(jQuery('#username').val()==''){
		  			jQuery('#myModal').modal('show');
		  			jQuery('#myModal').find('p').text('Tên đăng nhập không được để rỗng!!!');
		  		}
		  		if(jQuery('#pwd').val()==''){
		  			jQuery('#myModal').modal('show');
		  			jQuery('#myModal').find('p').text('Mật khẩu không được để rỗng!!!');
		  		}
		  		if(jQuery('#username').val()=='' && jQuery('#pwd').val()==''){
		  			jQuery('#myModal').modal('show');
		  			jQuery('#myModal').find('p').text('Tên đăng nhập và mật khẩu không được để rỗng!!!');
		  		}
		  		else{
		  			location.href = "index.php";
		  		}
		  	});
		  	
		</script>
	</body>
</html>