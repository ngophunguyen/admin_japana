<style>
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin: 15px 0;
	    position: relative;
	}
	.table-custom{
		margin-top: 0;
		white-space: nowrap;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		height: auto;
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
	}
	.box-table tr td .checkbox-custom{
		padding: 0;
	}
	.box-table tr td .checkbox-custom .checkmark{
		left: 50%;
		top: 50%;
	    -webkit-transform: translate(-50%,-50%);
	        -ms-transform: translate(-50%,-50%);
	            transform: translate(-50%,-50%);
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item button{
		float: left;
		margin-left: 15px;
	}
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item{
			width: 100%;
		}
	  	.table-custom > tbody > tr > td{
			min-height: 40px;
		}
		.table-responsive > tbody > tr td:first-child {
		    display: none;
		}
		.box-table tr td:last-child{
			padding: 0;
		}
		.table-custom > tbody > tr > td a{
			white-space: normal;
		}
		.box-table tr td .checkbox-custom .checkmark{
			-webkit-transform: translate(-0%,-50%);
			    -ms-transform: translate(-0%,-50%);
			        transform: translate(-0%,-50%);
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	  	.box-quick-search .item{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:first-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 80%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
	  	.box-quick-search .item{
	  		width: 100%;
	  	}
	  	.box-quick-search .item:first-child button{
	  		width: 20%;
	  	}
	  	.box-quick-search .item:first-child input{
	  		width: 80%;
	  	}
	  	.box-quick-search .item form{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		-webkit-box-align: center;
	  		    -ms-flex-align: center;
	  		        align-items: center;
	  		-webkit-box-pack: justify;
	  		    -ms-flex-pack: justify;
	  		        justify-content: space-between;
	  	}
	  	.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
	  	.table-custom{
	  		white-space: nowrap;
	  	}
	}
	@media (min-width: 1200px) {
		.table-custom{
	  		white-space: nowrap;
	  	}
	}
</style>
<main class="static content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách tin tĩnh</h1>
			<ul>
				<li>
					<a href="?action=include/static/add.php" class="link-custom black-custom" title="Thêm bài viết">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="keyword" value="" type="text" class="form-control custom-ipt" placeholder="Tìm kiếm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black center-custom">ID</th>
					            <th class="bg-black">Tên tin</th>
					            <th class="bg-black center-custom">Hình ảnh</th>
					            <th class="bg-black">Ngày tạo</th>
					            <th class="bg-black center-custom">Publish</th>
					            <th class="bg-black center-custom">HOT</th>
					            <th class="bg-black">Người tạo</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="ID" class="center-custom">13</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/static/edit.php" title="Quy định bán hàng trên website Japana">
					            		Quy định bán hàng trên website Japana
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">Chưa có hình</td>
					            <td data-title="Ngày Publish">21-02-2019 | 08:00:00</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="HOT" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" checked="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="ID" class="center-custom">13</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/static/edit.php" title="Quy định bán hàng trên website Japana">
					            		Quy định bán hàng trên website Japana
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">Chưa có hình</td>
					            <td data-title="Ngày Publish">21-02-2019 | 08:00:00</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="HOT" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" checked="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="ID" class="center-custom">13</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/static/edit.php" title="Quy định bán hàng trên website Japana">
					            		Quy định bán hàng trên website Japana
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">Chưa có hình</td>
					            <td data-title="Ngày Publish">21-02-2019 | 08:00:00</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="HOT" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" checked="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="ID" class="center-custom">13</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/static/edit.php" title="Quy định bán hàng trên website Japana">
					            		Quy định bán hàng trên website Japana
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">Chưa có hình</td>
					            <td data-title="Ngày Publish">21-02-2019 | 08:00:00</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="HOT" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" checked="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="ID" class="center-custom">13</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/static/edit.php" title="Quy định bán hàng trên website Japana">
					            		Quy định bán hàng trên website Japana
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">Chưa có hình</td>
					            <td data-title="Ngày Publish">21-02-2019 | 08:00:00</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="HOT" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" checked="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="ID" class="center-custom">13</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/static/edit.php" title="Quy định bán hàng trên website Japana">
					            		Quy định bán hàng trên website Japana
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">Chưa có hình</td>
					            <td data-title="Ngày Publish">21-02-2019 | 08:00:00</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" />
					            </td>
					            <td data-title="HOT" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="ID" class="center-custom">13</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/static/edit.php" title="Quy định bán hàng trên website Japana">
					            		Quy định bán hàng trên website Japana
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">Chưa có hình</td>
					            <td data-title="Ngày Publish">21-02-2019 | 08:00:00</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" />
					            </td>
					            <td data-title="HOT" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="ID" class="center-custom">13</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/static/edit.php" title="Quy định bán hàng trên website Japana">
					            		Quy định bán hàng trên website Japana
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">Chưa có hình</td>
					            <td data-title="Ngày Publish">21-02-2019 | 08:00:00</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios" />
					            </td>
					            <td data-title="HOT" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="ID" class="center-custom">13</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/static/edit.php" title="Quy định bán hàng trên website Japana">
					            		Quy định bán hàng trên website Japana
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">Chưa có hình</td>
					            <td data-title="Ngày Publish">21-02-2019 | 08:00:00</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios"/>
					            </td>
					            <td data-title="HOT" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="ID" class="center-custom">13</td>
					            <td data-title="Tên bài viết">
					            	<a href="?action=include/static/edit.php" title="Quy định bán hàng trên website Japana">
					            		Quy định bán hàng trên website Japana
					            	</a>
					            </td>
					            <td data-title="Hình ảnh" class="center-custom">Chưa có hình</td>
					            <td data-title="Ngày Publish">21-02-2019 | 08:00:00</td>
					            <td data-title="Publish" class="center-custom">
					            	<input type="checkbox" class="checkbox-ios"/>
					            </td>
					            <td data-title="HOT" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Người tạo">Sương Content</td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		var heightBottomTotal = jQuery('.total-order').outerHeight();
		var heightBottomPagination = jQuery('.pagination-custom').outerHeight();
		if(window.innerWidth < 576){
			jQuery('.box-table').css('margin-bottom',heightBottomPagination+heightBottomTotal);
		}
		if(window.innerWidth > 576){
		    jQuery('.dropdown-collapse').click(function(){
		    	if(jQuery('.custom-collapse').css('display')=='none'){
		    		jQuery('.custom-collapse').css('display','inline-block');
		    		jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
		    	}
		    	else{
		    		jQuery('.custom-collapse').css('display','none');
		    		jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
		    	}
		    });
		}else{
			jQuery('.show-filter-order').click(function(){
		    	if(jQuery('.custom-collapse').css('display')=='none'){
		    		jQuery('.custom-collapse').css('display','inline-block');
		    		jQuery('.dropdown-collapse').css('display','flex');
		    		jQuery(this).css('left',0);
		    		jQuery(this).css('right','auto');
		    		jQuery(this).css('z-index','99');
		    		jQuery(this).parent().addClass('active');
		    		jQuery(this).find('i').removeClass('fa-search').addClass('fa-caret-right');
		    		jQuery('body').css('overflow','hidden');
		    		jQuery('.filter-pc').css('z-index','999');

		    	}
		    	else{
		    		jQuery('.custom-collapse').css('display','none');
		    		jQuery('.dropdown-collapse').css('display','none');
		    		jQuery(this).css('left','auto');
		    		jQuery(this).css('right',0);
		    		jQuery(this).parent().removeClass('active');
		    		jQuery(this).find('i').removeClass('fa-caret-right').addClass('fa-search');
		    		jQuery('body').css('overflow','inherit');
		    		jQuery('.filter-pc').css('z-index','-1');
		    	}
		    });
		}
	    
	    var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    jQuery('#cbo_username,#cbo_catenews').select2();
	    if(window.innerWidth > 576) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
	    jQuery('input[name="date-news"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date3').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date3').val(end.format('DD-MM-YYYY'));
		});
		jQuery('input[name="date-news-fast"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date2').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date2').val(end.format('DD-MM-YYYY'));
		});
		jQuery('input[name="date-news-table"]').daterangepicker({
			singleDatePicker: true,
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		});
	});
</script>