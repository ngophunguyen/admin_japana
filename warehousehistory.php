<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin: 15px 0;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:last-child{
		width: 55%;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	}
	.box-quick-search .item:first-child input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .box-time{
		width: 40%;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
		float: right;
	}
	.search2, .search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.search1{
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
	}
	.box-table {
	    position: relative;
	    margin-bottom: 15px;
	}
	.table-custom{
		margin-top: 0;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item, 
	  	.box-quick-search .item:first-child input, .box-quick-search .item:last-child, .box-quick-search .item:last-child .box-time{
	  		width: 100%;
	  		margin-left: 0;
	  	}
	  	.box-quick-search .item:last-child{
	  		margin-top: 15px;
	  	}
		.search1{
	  		display: inline-block;
	  		width: 100%;
	  	}
	  	.box-quick-search .item:first-child button{
	  		margin-left: 0;
	  		margin-top: 15px;
	  	}
		.refund .table-custom > tbody > tr > td:last-child{
			-webkit-box-pack: justify;
			    -ms-flex-pack: justify;
			        justify-content: space-between;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="refund content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Lịch sử xuất/nhập kho</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm sku hoặc tên sản phẩm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<div class="box-time">
							<input type="hidden" id="start-date" name="date_from" value="">
						    <input type="hidden" id="end-date" name="date_to" value="">
                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-product" id="date-product" class="form-control ipt-date" placeholder="Chọn ngày hết hạn sản phẩm...">
                            <i class="fa fa-calendar icon-time"></i>
                        </div>
                        <button type="submit" class="button bg-black">Tìm kiếm</button>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">SKU</th>
					            <th class="bg-black">Tên sản phẩm</th>
					            <th class="bg-black center-custom">SL Đầu kỳ</th>
					            <th class="bg-black center-custom">SL Nhập trong kỳ</th>
					            <th class="bg-black center-custom">SL Xuất trong kỳ</th>
					            <th class="bg-black center-custom">SL Cuối kỳ</th>
					            <th class="bg-black">Ngày</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="SKU">
					            	<a href="?action=include/warehousehistory/edit.php" title="KMNCC-037GDA04">
						            	KMNCC-037GDA04
						            </a>
					            </td>
					            <td data-title="Tên sản phẩm">Đồ chơi lắp ráp thông minh</td>
					            <td data-title="SL Đầu kỳ" class="center-custom">0</td>
					            <td data-title="SL Nhập trong kỳ" class="center-custom">2</td>
					            <td data-title="SL Xuất trong kỳ" class="center-custom">4</td>
					            <td data-title="SL Cuối kỳ" class="center-custom">4</td>
					            <td data-title="Ngày">10-08-2020|11:06</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="SKU">
					            	<a href="?action=include/warehousehistory/edit.php" title="077GDB177">
						            	077GDB177
						            </a>
					            </td>
					            <td data-title="Tên sản phẩm">Viên ngậm lợi khuẩn ChuChuBaby (Vị sữa chua)</td>
					            <td data-title="SL Đầu kỳ" class="center-custom">0</td>
					            <td data-title="SL Nhập trong kỳ" class="center-custom">2</td>
					            <td data-title="SL Xuất trong kỳ" class="center-custom">4</td>
					            <td data-title="SL Cuối kỳ" class="center-custom">4</td>
					            <td data-title="Ngày">10-08-2020|11:06</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="SKU">
					            	<a href="?action=include/warehousehistory/edit.php" title="027HE189">
						            	027HE189
						            </a>
					            </td>
					            <td data-title="Tên sản phẩm">Bình thủy bơm rót Zojirushi ZOBT-AB-RB22-FC 2.2L</td>
					            <td data-title="SL Đầu kỳ" class="center-custom">0</td>
					            <td data-title="SL Nhập trong kỳ" class="center-custom">2</td>
					            <td data-title="SL Xuất trong kỳ" class="center-custom">4</td>
					            <td data-title="SL Cuối kỳ" class="center-custom">4</td>
					            <td data-title="Ngày">10-08-2020|11:06</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="SKU">
					            	<a href="?action=include/warehousehistory/edit.php" title="027HI152">
						            	027HI152
						            </a>
					            </td>
					            <td data-title="Tên sản phẩm">Máy ép trái cây Panasonic MJ-H100WRA</td>
					            <td data-title="SL Đầu kỳ" class="center-custom">0</td>
					            <td data-title="SL Nhập trong kỳ" class="center-custom">2</td>
					            <td data-title="SL Xuất trong kỳ" class="center-custom">4</td>
					            <td data-title="SL Cuối kỳ" class="center-custom">4</td>
					            <td data-title="Ngày">10-08-2020|11:06</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="SKU">
					            	<a href="?action=include/warehousehistory/edit.php" title="027HC57-YA">
						            	027HC57-YA
						            </a>
					            </td>
					            <td data-title="Tên sản phẩm">Bình nước lưỡng tính Zojirushi SC-MC60YA (Màu Yellow)</td>
					            <td data-title="SL Đầu kỳ" class="center-custom">0</td>
					            <td data-title="SL Nhập trong kỳ" class="center-custom">2</td>
					            <td data-title="SL Xuất trong kỳ" class="center-custom">4</td>
					            <td data-title="SL Cuối kỳ" class="center-custom">4</td>
					            <td data-title="Ngày">10-08-2020|11:06</td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    if(window.innerWidth > 576) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
	    jQuery('input[name="date-product"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date').val(end.format('DD-MM-YYYY'));
		});
	})
</script>