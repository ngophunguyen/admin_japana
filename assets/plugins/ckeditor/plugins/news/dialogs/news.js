CKEDITOR.dialog.add( 'newsDialog', function( editor ) {
    return {
        title: 'Thêm ID bài viết tin tức',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'ID bài viết Tin tức',
                elements: [
                    {
                        type: 'text',
                        id: 'news',
                        label: 'ID bài viết Tin tức',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Cần nhập ID bài viết." ),
                        validate: CKEDITOR.dialog.validate.number( "Yêu cầu số ID bài viết." )
                    }
                ]
            }
        ],
        onOk: function() {
            var dialog = this;
            var news = editor.document.createElement('div');
            news.setAttribute("class", "title_id_new"+dialog.getValueOf('tab-basic', 'news'));
            news.setHtml("<div class='hidden'>ID TIN TỨC:<div class='id_new'>" + dialog.getValueOf('tab-basic', 'news') + "</div></div>");
            editor.insertElement(news);
        }
    };
});