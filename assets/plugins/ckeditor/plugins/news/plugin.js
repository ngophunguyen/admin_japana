CKEDITOR.plugins.add( 'news', {
    icons: 'news',
    init: function( editor ) {
        editor.addCommand( 'news', new CKEDITOR.dialogCommand( 'newsDialog' ) );
        editor.ui.addButton( 'News', {
            label: 'Thêm bài viết tin tức',
            command: 'news',
            toolbar: 'product_new'
        });
        CKEDITOR.dialog.add( 'newsDialog', this.path + 'dialogs/news.js' );
    }
});
