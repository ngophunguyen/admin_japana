CKEDITOR.dialog.add('productDialog', function (editor) {
    return {
        title: 'Thêm sản phẩm vào bài viết',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'ID sản phẩm',
                elements: [
                    {
                        type: "text",
                        id: 'product',
                        label: 'ID sản phẩm',
                        validate: CKEDITOR.dialog.validate.notEmpty("Cần nhập ID sản phẩm."),
                        validate: CKEDITOR.dialog.validate.number("Yêu cầu số ID Sản phẩm.")
                    }
                ]
            }
        ],
        onOk: function () {
            var dialog = this;
            var product = editor.document.createElement('div');
            product.setAttribute("class", "title_id_sp"+dialog.getValueOf('tab-basic', 'product'));
            product.setHtml("<div class='hidden'>ID SẢN PHẨM:<div class='id_sp'>" + dialog.getValueOf('tab-basic', 'product') + "</div></div>");
            editor.insertElement(product);
        }
    };
});