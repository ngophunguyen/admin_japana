/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// Define changes to default configuration here. For example:
	config.language = 'vi';

	//Add plugin to ckeditor
	config.extraPlugins = 'product,news';
	// Enable a limited set of text formats:
	config.format_tags = 'p;h2;h3;h4';

	// config.uiColor = '#AADC6E';
	config.toolbarCanCollapse = true;
	config.toolbar = [
	               	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source'] },
	               	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
	               	
	               	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline' ] },
	               	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Indent', '-', 'CreateDiv',  'JustifyCenter' ] },
	               	{ name: 'links', items: [ 'Link', 'Unlink' ] },
	               	{ name: 'insert', items: [ 'Image', 'PageBreak', 'Iframe' ] },

	               	{ name: 'styles', items: [ 'Format'] },
	               	{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
	               	{ name: 'others', items: [ '-' ] },

		 			{ name: 'product_new', items: [ 'Product','News' ] }
	               ];
			
	config.filebrowserBrowseUrl = 'assets/plugins/ckfinder/ckfinder.html';
	config.filebrowserFlashBrowseUrl = 'assets/plugins/ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'; 
	config.filebrowserFlashUploadUrl = 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
	config.filebrowserImageBrowseUrl = 'assets/plugins/ckfinder/ckfinder.html?type=Images';
	config.filebrowserImageUploadUrl = 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
};
