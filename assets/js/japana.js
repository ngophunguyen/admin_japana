jQuery(function(){
	 jQuery("input[data-type='currency']").on({
        keyup: function() {
            formatCurrency(jQuery(this));
        },
        blur: function() {
            formatCurrency(jQuery(this), "blur");
        }
    });
	if(window.innerWidth > 1024 && jQuery('.box-table').length) {
		jQuery('.box-table').addClass('dragon');
	}else{
		jQuery('.box-table').removeClass('dragon');
	}
	if(window.innerWidth > 576) {
		setTimeout(function(){
			if(jQuery('.nav-primary').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight() && jQuery('.nav-primary').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height','auto');
			}else if(jQuery('body').outerHeight() > jQuery('.nav-primary').outerHeight() && jQuery('body').outerHeight() > jQuery('.content-sidebar-wrap').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
		},10)
		
	}
	
	if(jQuery('.box-table').length){
		jQuery('.box-table').each(function(){
			new PerfectScrollbar(this);
		});
	}
	if(jQuery('.checkbox-ios').length){
		jQuery(".checkbox-ios").iosCheckbox();
	}
	
	var heightTopheader = jQuery('.entry-header').outerHeight();
	var heightTopButton = jQuery('.entry-header ul').outerHeight();
	var heightBottomPagination = jQuery('.pagination-custom').outerHeight();
	var heightTopSearch = jQuery('.box-quick-search').outerHeight();
	if(window.innerWidth < 767){
		jQuery('.entry-content').css('margin-top',heightTopheader+heightTopButton);
	}else{
		jQuery('.entry-content').css('margin-top',heightTopheader);
	}
}).resize();

function numberFormat(x) {
    if (x !== "") {
        var parts = x.toString().split(",");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(",");
    }
}
function formatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}
function formatCurrency(input, blur) {
    var input_val = input.val();
    if (input_val === "") { return; }
    var original_len = input_val.length;
    var caret_pos = input.prop("selectionStart");
    if (input_val.indexOf(".") >= 0) {
        var decimal_pos = input_val.indexOf(".");
        var left_side = input_val.substring(0, decimal_pos);
        left_side = formatNumber(left_side);
        input_val = left_side;

    } else {
        input_val = formatNumber(input_val);
    }
    input.val(input_val);
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
}