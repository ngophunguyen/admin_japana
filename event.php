<style>
	.box-event{
		display: inline-block;
		margin-top: 15px;
		width: 100%;
	}
	.box-event .item{
		width: 49%;
		margin-right: 2%;
		float: left;
	}
	.box-event .item:last-child{
		margin-right: 0;
	}
	.item .seo-input{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: baseline;
		    -ms-flex-align: baseline;
		        align-items: baseline;
		-webkit-box-pack: end;
		    -ms-flex-pack: end;
		        justify-content: flex-end;
		margin-bottom: 15px;
	}
	.item .seo-input input, .item .seo-input textarea, .item .seo-input .upload-img, .box-img-meta{
		margin-left: 15px;
		width: 80%;
		float: right;
	}
	.item .seo-input .upload-img input{
		margin-left: 0;
		width: auto;
	}
	.box-img{
		display: none;
	}
	@media (max-width: 575.98px) {
		.box-event .item{
			width: 100%;
			margin-right: 0;
		}
		.upload-img, .item .seo-input{
			display: inline-block;
			width: 100%;
		}
		.item .seo-input label{
			margin-bottom: 10px;
		}
		.item .seo-input label, .item .seo-input input, .item .seo-input .upload-img, .box-img-meta{
			width: 100%;
			margin-left: 0;
		}
		.item .seo-input label.custom-upload{
			width: 100px;
		}
		.box-table tr td:last-child{
			-webkit-box-orient: initial;
			-webkit-box-direction: initial;
			    -ms-flex-direction: initial;
			        flex-direction: initial;
			-webkit-box-pack: unset!important;
			    -ms-flex-pack: unset!important;
			        justify-content: unset!important;
		    padding: 0;
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.box-event .item{
			width: 100%;
			margin-left: 0;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {	
		.box-event .item{
			width: 100%;
			margin-left: 0;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.box-event .item{
			width: 100%;
			margin-left: 0;
		}	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="event content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Tạo danh sách</h1>
			<ul>
				<li>
					<button type="button" class="button button-header link-custom black-custom">
	                    <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Tạo danh sách</label>
	                </button>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-event">
					<div class="item">
						<span class="seo-input">
							<label>Tên danh sách:</label>
							<input autocomplete="off" type="text" name="meta_web_title" id="meta_web_title" placeholder="Nhập title" value="" class="form-control">
						</span>
						<span class="seo-input">
							<label>Icon:</label>
							<div class="upload-img">
	                            <div class="input-group up-img">
	                                <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
	                                <label class="button bg-green custom-upload">
	                                    <input type="file" class="form-control" name="icon" id="icon" accept="image/*">Upload
	                                </label>
	                                <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
	                            </div>
	                        </div>
						</span>
						<div class="box-img">
                        	<img src="assets/images/no-img2.png" alt="no-img2" id="photo">
                        </div>
					</div>
					<div class="item">
						<div class="box-table">
							<table class="table table-custom table-striped table-responsive nomargin">
							    <thead class="bg-black">
							        <tr class="bg-black">
							            <th class="bg-black center-custom">STT</th>
							            <th class="bg-black">Tên danh sách</th>
							            <th class="bg-black">Kiểu danh sách</th>
							            <th class="bg-black center-custom">Tác vụ</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							            <td data-title="STT" class="center-custom">1</td>
							            <td data-title="Tên danh sách">
							            	<a href="?action=include/event/add-sk.php" title="Sản phẩm khuyến mãi và quà tặng">
							            	Sản phẩm hot
							            	</a>
							            </td>
							            <td data-title="Kiểu danh sách">Sự kiện</td>
							            <td data-title="Tác vụ">
							            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
							            		<i class="fa fa-trash-o"></i>
							            	</a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">2</td>
							            <td data-title="Tên danh sách">
							            	<a href="?action=include/event/add-km.php" title="Sản phẩm khuyến mãi và quà tặng">
							            	Sản phẩm mới
							            	</a>
							            </td>
							            <td data-title="Kiểu danh sách">Khuyến mãi</td>
							            <td data-title="Tác vụ">
							            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
							            		<i class="fa fa-trash-o"></i>
							            	</a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">3</td>
							            <td data-title="Tên danh sách">
							            	<a href="?action=include/event/add-sk.php" title="Sản phẩm khuyến mãi và quà tặng">
							            	Bảo hành 12 tháng
							            	</a>
							            </td>
							            <td data-title="Kiểu danh sách">Sự kiện</td>
							            <td data-title="Tác vụ">
							            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
							            		<i class="fa fa-trash-o"></i>
							            	</a>
							            </td>
							        </tr>
							        <tr>
							            <td data-title="STT" class="center-custom">4</td>
							            <td data-title="Tên danh sách">
							            	<a href="?action=include/event/add-sk.php" title="Sản phẩm khuyến mãi và quà tặng">
							            	Giao hàng 60'
							            	</a>
							            </td>
							            <td data-title="Kiểu danh sách">Sự kiện</td>
							            <td data-title="Tác vụ">
							            	<a href="javascript:void(0);" class="link-custom black-custom" title="Xoá">
							            		<i class="fa fa-trash-o"></i>
							            	</a>
							            </td>
							        </tr>
							    </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		jQuery(document).on('change', ':file', function() {
		    var input = jQuery(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		});
		jQuery(':file').on('fileselect', function(event, numFiles, label) {
          	var input = jQuery(this).parents('.up-img').find(':text'),
              	log = numFiles > 1 ? numFiles + ' files selected' : label;
          	if( input.length ) {
              	input.val(log);
              	if(input.val(log)){
                	jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
                	jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
              	}
          	} else {
              	if(log) ;
          	}
      	});
      	jQuery('.delete-img').on('click', function(e){
		    jQuery(this).parent().find('input[type=file]').val('');
		    jQuery(this).parent().find('input[type=text]').val('');
		    jQuery(this).parent().find('.custom-upload').css('display','flex');
		    jQuery(this).parent().parent().next().css('display','none');
		    jQuery(this).css('display','none');
		});
	})
</script>