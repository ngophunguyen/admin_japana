<style>
	.dropdown-collapse{
		margin-top: 15px;
	}
	.custom-collapse .item{
		display: inline-block;
		width: 20%;
		float: left;
		margin-top: 10px;
		padding: 0 15px;
	}
	.custom-collapse .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.custom-dropdown:after{
		width: 8%;
	}
	#show1,#show2{
		display: none;
	}
	#show2 .item{
		float: left;
		width: auto;
	}
	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin: 15px 0;
	    position: relative;
	}
	.table-custom{
		margin-top: 0;
		white-space: nowrap;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		height: auto;
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	img.gift-order{
		height: 20px!important; 
		bottom: 5px; 
		left: 0;  
		position: absolute;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		padding: 15px 0;
	}
	.box-quick-search {
	    display: inline-block;
	    width: 100%;
	    margin-top: 15px;
	}
	.box-quick-search .item {
	    display: inline-block;
	    width: 50%;
	    float: left;
	}
	.box-quick-search .item:first-child input{
		width: 50%;
    	float: left;
	}
	.box-quick-search .item:first-child button {
	    float: left;
	    margin-left: 15px;
	}
	.box-button-filter{
  		display: none;
  	}
  	.button-filter{
	    font-size: 14px;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	    background: rgba(0,0,0,0.3);
	    border-radius: 10px 0 0 10px;
	    height: 38px;
	    width: 38px;
	    position: fixed;
	    right: 0;
	    top: 145px;
		z-index: 99
	}
	.button-filter.active{
		left: 0px;
	    border-radius: 0px;
	    right: auto;
	    z-index: 99;
	    top: 0px!important;
	    height: 40px;
	    background: #222D32!important;
	    color: #fff!important;
	}
	#button-filter-2{
		top: 189px;
	}
	.button-filter:focus, .button-filter:hover{
		color: #fff;
	}
	#name-mobile-1,#name-mobile-2{
		display: none;
	}
	.search1{
		display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	}
	@media (max-width: 575.98px) {
		.entry-header ul{
			white-space: nowrap;
			overflow: auto;
		}
		.filter-pc{
	  		display: none;
	  		position: fixed;
		    top: 0;
		    right: 0;
		    width: calc(100% - 38px);
		    height: 100%;
		    z-index: -1;
		    margin-top: 0!important;
	  	}
	  	.dropdown-collapse{
	  		display: none;
	  	}
	  	.box-filter{
	  		position: relative;
	  		height: 100%;
	  	}
	  	.button-tab a{
	  		width: 100%;
		    display: -webkit-box;
		    display: -ms-flexbox;
		    display: flex;
		    height: 40px;
		    color: #fff;
		    background: #222D32;
		    -webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		    -webkit-box-pack: center;
		        -ms-flex-pack: center;
		            justify-content: center;
		    float: left;
	  	}
	  	.custom-collapse{
	  		height: 100%;
		    width: 100%;
		    padding: 0!important;
		    float: right;
		    overflow-x: hidden;
		    position: relative;
		    z-index: 999;
		    background: #fff;
		    border-left: 1px solid #eee;
	  	}
	  	#show1{
	  		height: calc(100% - 80px);
	  	}
	  	.custom-collapse .item:last-child{
	  		padding: 0;
	  	}
	  	.custom-collapse .item:last-child button {
		    position: fixed;
		    bottom: 0;
		    right: 0;
		    width: calc(100% - 38px);
		    border-radius: 0;
		}
	  	.custom-collapse .item{
	  		width: 100%;
	  		margin: 10px 0 0!important;
	  	}
	  	.custom-collapse .item:first-child{
	  		width: 100%;
	  	}
	  	.custom-collapse .item:last-child label{
	  		display: none;
	  	}
	  	.custom-collapse .item label{
	  		font-size: 13px;
	  	}
	  	.box-button-filter.active{
	  		background: rgba(0,0,0,.3);
		    width: 38px;
		    height: 100%;
		    position: fixed;
		    left: 0;
		    top: 0;
    		z-index: 999;
	  	}
	  	.box-button-filter{
	  		display: block;
	  	}
	  	.box-quick-search .item,.box-quick-search .item:first-child input{
	  		width: 100%;
	  	}
	  	.table-custom > tbody > tr > td{
			min-height: 40px;
		}
		.table-responsive > tbody > tr td:first-child {
		    display: none;
		}
		img.gift-order{
			left: auto;
		}
		.first-price{
			margin-bottom: 0;
			width: auto;
		}
		.table-custom > tbody > tr > td{
			white-space: normal;
		}
		.table-custom > tbody > tr > td .box-time, .table-custom > tbody > tr > td .custom-dropdown{
			width: 100%;
			padding: 5px 0;
		}
		.box-table tr td:last-child{
			padding: 0;
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
		#show2 .item{
			width: 100%;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.custom-collapse .item{
			padding: 0 5px;
		}
		.box-quick-search .item:first-child input{
			width: 65%;
		}
		.box-quick-search .item:first-child button{
			width: 30%;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.custom-collapse .item{
			padding: 0 5px;
		}
		.box-quick-search .item:first-child input{
			width: 65%;
		}
		.box-quick-search .item:first-child button{
			width: 30%;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {

	}
</style>
<main class="products content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Sản phẩm</h1>
			<ul>
				<li>
					<a href="?action=include/products/add.php" class="link-custom black-custom" title="Thêm sản phẩm">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#addproduct-modal" title="Thêm sản phẩm bằng file Excel">
						<i class="fa fa-upload" aria-hidden="true"></i> <label>Thêm bằng file Excel</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#updateproduct-modal" title="Cập nhật sản phẩm bằng file Excel">
						<i class="fa fa-upload" aria-hidden="true"></i> <label>Cập nhật bằng file Excel</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div id="mobile-filter-1" class="filter-pc">
					<div class="box-filter">
						<div class="button-tab">
							<a href="javascript:void(0);" id="name-mobile-1" title="Lọc sản phẩm">Lọc sản phẩm</a>
						</div>
						<a class="dropdown-collapse bg-black" onclick="showFilter(1)" href="javascript:void(0);">Lọc sản phẩm <i class="fa fa-caret-down" aria-hidden="true"></i></a>
						<div id="show1" class="custom-collapse">
							<form id="frm" name="frm" method="post">
							    <div class="item">
							    	<label for="list_id_category">Danh mục</label>
							    	<select class="form-control" name="list_id_category_search[]" id="list_id_category">
							    		<option value="-1">Chọn danh mục</option>
									  	<option value="0">Collagen</option>
									  	<option value="1">Dành cho bé</option>
									  	<option value="2">Dành cho mẹ</option>
									</select>
							    </div>
							    <div class="item">
							    	<label for="id_ncc">Nhà cung cấp</label>
							    	<select class="form-control" name="id_ncc" id="id_ncc">
							    		<option value="-1">Chọn nhà cung cấp</option>
									  	<option value="0">031-Công Ty Trách Nhiệm Hữu Hạn Asahi-Nutifood</option>
									  	<option value="1">NCC-002-Giao nhận 365</option>
									  	<option value="2">NCC_001-10-JP Shop Nhật (Chị Thu)</option>
									</select>
							    </div>
							    <div class="item">
							    	<label for="id_brand">Thương hiệu</label>
							    	<select class="form-control" name="id_brand" id="id_brand">
							    		<option value="-1">Chọn thương hiệu</option>
									  	<option value="0">Panasonic</option>
									  	<option value="1">Orihiro</option>
									  	<option value="2">Ogaland</option>
									</select>
							    </div>
							    <div class="item">
							    	<label for="status_num">Tình trạng</label>
							    	<div class="custom-dropdown">
								    	<select class="form-control" name="status_num" id="status_num">
								    		<option value="-1">Chọn tình trạng</option>
										  	<option value="0">Hết hàng</option>
										  	<option value="1">Còn hàng</option>
										</select>
									</div>
							    </div>
							    <div class="item">
							    	<label for="status">Publish</label>
							    	<div class="custom-dropdown">
								    	<select class="form-control" name="status" id="status">
								    		<option value="-1">Publish/Unpublish</option>
										  	<option value="0">Publish</option>
										  	<option value="1">Unpublish</option>
										</select>
									</div>
							    </div>
							    <div class="item">
							    	<label for="price_from">Từ giá</label>
							    	<input autocomplete="off" type="text" name="price_from" id="price_from" data-type='currency' class="form-control" placeholder="Nhập giá">
							    </div>
							    <div class="item">
							    	<label for="price_to">Đến giá</label>
							    	<input autocomplete="off" type="text" name="price_to" id="price_to" data-type='currency' class="form-control" placeholder="Nhập giá">
							    </div>
							    <div class="item">
							    	<label for="id_style">Quy cách</label>
						    		<select class="form-control" name="id_style" id="id_style">
							    		<option value="-1">Chọn quy cách</option>
									  	<option value="0">Hộp</option>
									  	<option value="1">Tuýp</option>
									  	<option value="2">Chai</option>
									</select>
							    </div>
							    <div class="item">
							    	<label for="id_country">Xuất xứ</label>
						    		<select class="form-control" name="id_country" id="id_country">
							    		<option value="-1">Chọn xuất xứ</option>
									  	<option value="0">Việt Nam</option>
									  	<option value="1">Pháp</option>
									  	<option value="2">Nhật</option>
									</select>
							    </div>
							    <div class="item">
							    	<label for="name">Khối lượng</label>
							    	<input autocomplete="off" type="number" name="kg" id="kg" data-type='currency' class="form-control" placeholder="Khối lượng">
							    </div>
							    <div class="item">
							    	<label for="username">Người tạo</label>
							    	<select class="form-control" name="username" id="username">
							    		<option value="-1">Chọn người tạo</option>
									  	<option value="0">Deverloper</option>
									  	<option value="1">Thu Sương</option>
									  	<option value="2">Thới Thị Bích Khiêm</option>
									</select>
							    </div>
							    <div class="item">
							    	<label for="id_user_showview">Người publish</label>
							    	<select class="form-control" name="id_user_showview" id="id_user_showview">
							    		<option value="-1">Chọn người publish</option>
									  	<option value="0">Deverloper</option>
									  	<option value="1">Thu Sương</option>
									  	<option value="2">Thới Thị Bích Khiêm</option>
									</select>
							    </div>
							    <div class="item">
							    	<label for="id_product_type">Loại sản phẩm</label>
						    		<select class="form-control" name="id_product_type" id="id_product_type">
							    		<option value="-1">Chọn loại sản phẩm</option>
									  	<option value="0">Sản phẩm chính</option>
									  	<option value="1">Sản phẩm phụ</option>
									  	<option value="2">Sản phẩm khuyến mãi</option>
									</select>
							    </div>
							    <div class="item">
							    	<label for="date-order">Khoảng thời gian</label>
							    	<div class="box-time">
							    		<input type="hidden" id="start-date3" name="from_date_ad" value="">
							    		<input type="hidden" id="end-date3" name="to_date_ad" value="">
			                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-order" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày...">
			                            <i class="fa fa-calendar icon-time"></i>
			                        </div>
		                        </div>
							    <div class="item">
							    	<label class="visible-hidden">Xác nhận</label>
							    	<button type="submit" class="button bg-black">Xác nhận</button>
							    </div>
							</form>
						</div>
					</div>
				</div>
				<div id="mobile-filter-2" class="filter-pc">
					<div class="box-filter">
						<div class="button-tab">
							<a href="javascript:void(0);" id="name-mobile-2" title="Thiết lập hiển thị">Thiết lập hiển thị</a>
						</div>
						<a class="dropdown-collapse bg-black" onclick="showFilter(2)" href="javascript:void(0);">Thiết lập hiển thị <i class="fa fa-caret-down" aria-hidden="true"></i></a>
						<div id="show2" class="custom-collapse">
							<div class="item">
								<label for="checkbox_kg" class="checkbox-custom">Khối lượng
				                  	<input id="checkbox_kg" name="checkbox_kg" type="checkbox" onclick="setview('checkbox_kg','kg');">
				                  	<span class="checkmark"></span>
				                </label>
							</div>
							<div class="item">
								<label for="checkbox_brand" class="checkbox-custom">Thương hiệu
								  	<input id="checkbox_brand" name="checkbox_brand" type="checkbox" onclick="setview('checkbox_brand','brand');">
				                  	<span class="checkmark"></span>
								</label>
							</div>
							<div class="item">
								<label for="checkbox_country" class="checkbox-custom">Xuất xứ
				                  	<input id="checkbox_country" name="checkbox_country" type="checkbox" onclick="setview('checkbox_country','country');">
				                  	<span class="checkmark"></span>
				                </label>
							</div>
							<div class="item">
								<label for="checkbox_style" class="checkbox-custom">Quy cách
								  	<input id="checkbox_style" name="checkbox_style" type="checkbox" onclick="setview('checkbox_style','style');">
				                  	<span class="checkmark"></span>
								</label>
							</div>
							<div class="item">
								<label for="checkbox_pricekm" class="checkbox-custom">Giá khuyến mãi
				                  	<input id="checkbox_pricekm" name="checkbox_pricekm" type="checkbox" onclick="setview('checkbox_pricekm','pricekm');">
				                  	<span class="checkmark"></span>
				                </label>
							</div>
							<div class="item">
								<label for="checkbox_approve" class="checkbox-custom">Duyệt/Chưa duyệt
								  	<input id="checkbox_approve" name="checkbox_approve" type="checkbox" onclick="setview('checkbox_approve','approve');">
				                  	<span class="checkmark"></span>
								</label>
							</div>
							<div class="item">
								<label for="checkbox_times" class="checkbox-custom">Thời gian sử dụng
				                  	<input id="checkbox_times" name="checkbox_times" type="checkbox" onclick="setview('checkbox_times','times');">
				                  	<span class="checkmark"></span>
				                </label>
							</div>
							<div class="item">
								<label for="checkbox_status_num" class="checkbox-custom">Tình trạng hết hàng
								  	<input id="checkbox_status_num" name="checkbox_status_num" type="checkbox" onclick="setview('checkbox_status_num','status_num');">
				                  	<span class="checkmark"></span>
								</label>
							</div>
							<div class="item">
								<label for="checkbox_date_showview" class="checkbox-custom">Ngày publish
				                  	<input id="checkbox_date_showview" name="checkbox_date_showview" type="checkbox" onclick="setview('checkbox_date_showview','date_showview');">
				                  	<span class="checkmark"></span>
				                </label>
							</div>
							<div class="item">
								<label for="checkbox_user_showview" class="checkbox-custom">Người publish
								  	<input id="checkbox_user_showview" name="checkbox_user_showview" type="checkbox" onclick="setview('checkbox_user_showview','user_showview');">
				                  	<span class="checkmark"></span>
								</label>
							</div>
							<div class="item">
								<label for="checkbox_date_update" class="checkbox-custom">Ngày cập nhật
				                  	<input id="checkbox_date_update" name="checkbox_date_update" type="checkbox" onclick="setview('checkbox_date_update','date_update');">
				                  	<span class="checkmark"></span>
				                </label>
							</div>
							<div class="item">
								<label for="checkbox_user_update" class="checkbox-custom">Người cập nhật
								  	<input id="checkbox_user_update" name="checkbox_user_update" type="checkbox" onclick="setview('checkbox_user_update','user_update');">
				                  	<span class="checkmark"></span>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="box-button-filter">
					<a id="button-filter-1" class="button-filter white-custom" onclick="openFilterMobile(1);" href="javascript:void(0);"><i class="ti-search" aria-hidden="true"></i></a>
					<a id="button-filter-2" class="button-filter white-custom" onclick="openFilterMobile(2);" href="javascript:void(0);"><i class="fa fa-eye" aria-hidden="true"></i></a>
				</div>
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post" class="search1">
	                       <input autocomplete="off" name="search" value="" type="text" class="form-control custom-ipt" placeholder="Tìm thông tin sản phẩm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<?php include('include/pagination.php')?>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black center-custom">Hình ảnh</th>
					            <th class="bg-black center-custom">SKU</th>
					            <th class="bg-black">Tên sản phẩm</th>
					            <th class="bg-black">Giá</th>
					            <th class="bg-black center-custom">Xem trước</th>
					            <th class="bg-black">Người tạo</th>
                    			<th class="bg-black center-custom">Ngày tạo</th>
					            <th id="th_user_showview" class="bg-black hidden">Người publish</th>
			                    <th id="th_date_showview" class="bg-black center-custom hidden">Ngày publish</th>   
			                    <th id="th_user_update" class="bg-black hidden">Người cập nhật</th>
			                    <th id="th_date_update" class="bg-black center-custom hidden">Ngày cập nhật</th>    
					            <th id="th_kg" class="bg-black hidden">Khối lượng</th>
			                    <th id="th_brand" class="bg-black hidden">Thương hiệu</th>
			                    <th id="th_country" class="bg-black hidden">Xuất sứ</th>
			                    <th id="th_style" class="bg-black hidden">Quy cách</th>
			                    <th id="th_pricekm" class="bg-black hidden">Giá khuyến mãi</th>
                                <th id="th_approve" class="bg-black hidden">Duyệt/Chưa duyệt</th>
                                <th id="th_times" class="bg-black hidden">Thời gian sử dụng</th>
			                    <th id="th_status_num" class="bg-black hidden">Hết hàng?</th>
					            <th class="bg-black center-custom">Publish</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="SKU" class="center-custom"><a href="?action=include/products/edit.php" title="Chỉnh sửa">087AA9</a></td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giá sản phẩm"><span>2,340,000 đ</span></td>
					            <td data-title="Xem trước" class="center-custom"><a href="#" title="Xem">Xem</a></td>
								<td data-title="Người tạo">Deverloper</td>
								<td data-title="Ngày tạo" class="center-custom">15-08-2019</td>
								<td data-title="Người publish" class="td_user_showview hidden">Deverloper</td>
								<td data-title="Ngày publish" class="center-custom td_date_showview hidden">15-08-2019</td>
								<td data-title="Người cập nhật" class="td_user_update hidden">Deverloper</td>
								<td data-title="Ngày cập nhật" class="center-custom td_date_update hidden">15-08-2019</td>
					            <td data-title="Khối lượng" class="center-custom td_kg hidden">1000</td>
					            <td data-title="Thương hiệu" class="center-custom td_brand hidden">Orihiro</td>
					            <td data-title="Xuất sứ" class="center-custom td_country hidden">Nhật Bản</td>
					            <td data-title="Quy cách" class="center-custom td_style hidden">Chai</td>
					            <td data-title="Giá KM" class="center-custom td_pricekm hidden">2,000,000 đ</td>
					            <td data-title="Duyệt?" class="center-custom td_approve hidden">Duyệt</td>
					            <td data-title="Hạn sử dụng" class="center-custom td_times hidden">1</td>
					            <td data-title="Hết hàng?" class="center-custom td_status_num hidden">Còn hàng</td>
					            <td data-title="Publish">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/products/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="SKU" class="center-custom"><a href="?action=include/products/edit.php" title="Chỉnh sửa">087AA9</a></td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giá sản phẩm"><span>2.340.000 đ</span></td>
					            <td data-title="Xem trước" class="center-custom"><a href="#" title="Xem">Xem</a></td>
								<td data-title="Người tạo">Deverloper</td>
								<td data-title="Ngày tạo" class="center-custom">15-08-2019</td>
								<td data-title="Người publish" class="td_user_showview hidden">Deverloper</td>
								<td data-title="Ngày publish" class="center-custom td_date_showview hidden">15-08-2019</td>
								<td data-title="Người cập nhật" class="td_user_update hidden">Deverloper</td>
								<td data-title="Ngày cập nhật" class="center-custom td_date_update hidden">15-08-2019</td>
					            <td data-title="Khối lượng" class="center-custom td_kg hidden">1000</td>
					            <td data-title="Thương hiệu" class="center-custom td_brand hidden">Orihiro</td>
					            <td data-title="Xuất sứ" class="center-custom td_country hidden">Nhật Bản</td>
					            <td data-title="Quy cách" class="center-custom td_style hidden">Chai</td>
					            <td data-title="Giá KM" class="center-custom td_pricekm hidden">2,000,000 đ</td>
					            <td data-title="Duyệt?" class="center-custom td_approve hidden">Duyệt</td>
					            <td data-title="Hạn sử dụng" class="center-custom td_times hidden">1</td>
					            <td data-title="Hết hàng?" class="center-custom td_status_num hidden">Còn hàng</td>

					            <td data-title="Publish">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/products/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="SKU" class="center-custom"><a href="?action=include/products/edit.php" title="Chỉnh sửa">087AA9</a></td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giá sản phẩm"><span>2.340.000 đ</span></td>
					            <td data-title="Xem trước" class="center-custom"><a href="#" title="Xem">Xem</a></td>
								<td data-title="Người tạo">Deverloper</td>
								<td data-title="Ngày tạo" class="center-custom">15-08-2019</td>
								<td data-title="Người publish" class="td_user_showview hidden">Deverloper</td>
								<td data-title="Ngày publish" class="center-custom td_date_showview hidden">15-08-2019</td>
								<td data-title="Người cập nhật" class="td_user_update hidden">Deverloper</td>
								<td data-title="Ngày cập nhật" class="center-custom td_date_update hidden">15-08-2019</td>
					            <td data-title="Khối lượng" class="center-custom td_kg hidden">1000</td>
					            <td data-title="Thương hiệu" class="center-custom td_brand hidden">Orihiro</td>
					            <td data-title="Xuất sứ" class="center-custom td_country hidden">Nhật Bản</td>
					            <td data-title="Quy cách" class="center-custom td_style hidden">Chai</td>
					            <td data-title="Giá KM" class="center-custom td_pricekm hidden">2,000,000 đ</td>
					            <td data-title="Duyệt?" class="center-custom td_approve hidden">Duyệt</td>
					            <td data-title="Hạn sử dụng" class="center-custom td_times hidden">1</td>
					            <td data-title="Hết hàng?" class="center-custom td_status_num hidden">Còn hàng</td>

					            <td data-title="Publish">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/products/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="SKU" class="center-custom"><a href="?action=include/products/edit.php" title="Chỉnh sửa">087AA9</a></td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giá sản phẩm"><span>2.340.000 đ</span></td>
					            <td data-title="Xem trước" class="center-custom"><a href="#" title="Xem">Xem</a></td>
								<td data-title="Người tạo">Deverloper</td>
								<td data-title="Ngày tạo" class="center-custom">15-08-2019</td>
								<td data-title="Người publish" class="td_user_showview hidden">Deverloper</td>
								<td data-title="Ngày publish" class="center-custom td_date_showview hidden">15-08-2019</td>
								<td data-title="Người cập nhật" class="td_user_update hidden">Deverloper</td>
								<td data-title="Ngày cập nhật" class="center-custom td_date_update hidden">15-08-2019</td>
					            <td data-title="Khối lượng" class="center-custom td_kg hidden">1000</td>
					            <td data-title="Thương hiệu" class="center-custom td_brand hidden">Orihiro</td>
					            <td data-title="Xuất sứ" class="center-custom td_country hidden">Nhật Bản</td>
					            <td data-title="Quy cách" class="center-custom td_style hidden">Chai</td>
					            <td data-title="Giá KM" class="center-custom td_pricekm hidden">2,000,000 đ</td>
					            <td data-title="Duyệt?" class="center-custom td_approve hidden">Duyệt</td>
					            <td data-title="Hạn sử dụng" class="center-custom td_times hidden">1</td>
					            <td data-title="Hết hàng?" class="center-custom td_status_num hidden">Còn hàng</td>

					            <td data-title="Publish">
					            	<input type="checkbox" class="checkbox-ios" checked="checked" />
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/products/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="SKU" class="center-custom"><a href="?action=include/products/edit.php" title="Chỉnh sửa">087AA9</a></td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giá sản phẩm"><span>2.340.000 đ</span></td>
					            <td data-title="Xem trước" class="center-custom"><a href="#" title="Xem">Xem</a></td>
								<td data-title="Người tạo">Deverloper</td>
								<td data-title="Ngày tạo" class="center-custom">15-08-2019</td>
								<td data-title="Người publish" class="td_user_showview hidden">Deverloper</td>
								<td data-title="Ngày publish" class="center-custom td_date_showview hidden">15-08-2019</td>
								<td data-title="Người cập nhật" class="td_user_update hidden">Deverloper</td>
								<td data-title="Ngày cập nhật" class="center-custom td_date_update hidden">15-08-2019</td>
					            <td data-title="Khối lượng" class="center-custom td_kg hidden">1000</td>
					            <td data-title="Thương hiệu" class="center-custom td_brand hidden">Orihiro</td>
					            <td data-title="Xuất sứ" class="center-custom td_country hidden">Nhật Bản</td>
					            <td data-title="Quy cách" class="center-custom td_style hidden">Chai</td>
					            <td data-title="Giá KM" class="center-custom td_pricekm hidden">2,000,000 đ</td>
					            <td data-title="Duyệt?" class="center-custom td_approve hidden">Duyệt</td>
					            <td data-title="Hạn sử dụng" class="center-custom td_times hidden">1</td>
					            <td data-title="Hết hàng?" class="center-custom td_status_num hidden">Còn hàng</td>

					            <td data-title="Publish">
					            	<input type="checkbox" class="checkbox-ios"/>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/products/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="SKU" class="center-custom"><a href="?action=include/products/edit.php" title="Chỉnh sửa">087AA9</a></td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giá sản phẩm"><span>2.340.000 đ</span></td>
					            <td data-title="Xem trước" class="center-custom"><a href="#" title="Xem">Xem</a></td>
								<td data-title="Người tạo">Deverloper</td>
								<td data-title="Ngày tạo" class="center-custom">15-08-2019</td>
								<td data-title="Người publish" class="td_user_showview hidden">Deverloper</td>
								<td data-title="Ngày publish" class="center-custom td_date_showview hidden">15-08-2019</td>
								<td data-title="Người cập nhật" class="td_user_update hidden">Deverloper</td>
								<td data-title="Ngày cập nhật" class="center-custom td_date_update hidden">15-08-2019</td>
					            <td data-title="Khối lượng" class="center-custom td_kg hidden">1000</td>
					            <td data-title="Thương hiệu" class="center-custom td_brand hidden">Orihiro</td>
					            <td data-title="Xuất sứ" class="center-custom td_country hidden">Nhật Bản</td>
					            <td data-title="Quy cách" class="center-custom td_style hidden">Chai</td>
					            <td data-title="Giá KM" class="center-custom td_pricekm hidden">2,000,000 đ</td>
					            <td data-title="Duyệt?" class="center-custom td_approve hidden">Duyệt</td>
					            <td data-title="Hạn sử dụng" class="center-custom td_times hidden">1</td>
					            <td data-title="Hết hàng?" class="center-custom td_status_num hidden">Còn hàng</td>

					            <td data-title="Publish">
					            	<input type="checkbox" class="checkbox-ios"/>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/products/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="SKU" class="center-custom"><a href="?action=include/products/edit.php" title="Chỉnh sửa">087AA9</a></td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giá sản phẩm"><span>2.340.000 đ</span></td>
					            <td data-title="Xem trước" class="center-custom"><a href="#" title="Xem">Xem</a></td>
								<td data-title="Người tạo">Deverloper</td>
								<td data-title="Ngày tạo" class="center-custom">15-08-2019</td>
								<td data-title="Người publish" class="td_user_showview hidden">Deverloper</td>
								<td data-title="Ngày publish" class="center-custom td_date_showview hidden">15-08-2019</td>
								<td data-title="Người cập nhật" class="td_user_update hidden">Deverloper</td>
								<td data-title="Ngày cập nhật" class="center-custom td_date_update hidden">15-08-2019</td>
					            <td data-title="Khối lượng" class="center-custom td_kg hidden">1000</td>
					            <td data-title="Thương hiệu" class="center-custom td_brand hidden">Orihiro</td>
					            <td data-title="Xuất sứ" class="center-custom td_country hidden">Nhật Bản</td>
					            <td data-title="Quy cách" class="center-custom td_style hidden">Chai</td>
					            <td data-title="Giá KM" class="center-custom td_pricekm hidden">2,000,000 đ</td>
					            <td data-title="Duyệt?" class="center-custom td_approve hidden">Duyệt</td>
					            <td data-title="Hạn sử dụng" class="center-custom td_times hidden">1</td>
					            <td data-title="Hết hàng?" class="center-custom td_status_num hidden">Còn hàng</td>

					            <td data-title="Publish">
					            	<input type="checkbox" class="checkbox-ios"/>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/products/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="SKU" class="center-custom"><a href="?action=include/products/edit.php" title="Chỉnh sửa">087AA9</a></td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giá sản phẩm"><span>2.340.000 đ</span></td>
					            <td data-title="Xem trước" class="center-custom"><a href="#" title="Xem">Xem</a></td>
								<td data-title="Người tạo">Deverloper</td>
								<td data-title="Ngày tạo" class="center-custom">15-08-2019</td>
								<td data-title="Người publish" class="td_user_showview hidden">Deverloper</td>
								<td data-title="Ngày publish" class="center-custom td_date_showview hidden">15-08-2019</td>
								<td data-title="Người cập nhật" class="td_user_update hidden">Deverloper</td>
								<td data-title="Ngày cập nhật" class="center-custom td_date_update hidden">15-08-2019</td>
					            <td data-title="Khối lượng" class="center-custom td_kg hidden">1000</td>
					            <td data-title="Thương hiệu" class="center-custom td_brand hidden">Orihiro</td>
					            <td data-title="Xuất sứ" class="center-custom td_country hidden">Nhật Bản</td>
					            <td data-title="Quy cách" class="center-custom td_style hidden">Chai</td>
					            <td data-title="Giá KM" class="center-custom td_pricekm hidden">2,000,000 đ</td>
					            <td data-title="Duyệt?" class="center-custom td_approve hidden">Duyệt</td>
					            <td data-title="Hạn sử dụng" class="center-custom td_times hidden">1</td>
					            <td data-title="Hết hàng?" class="center-custom td_status_num hidden">Còn hàng</td>

					            <td data-title="Publish">
					            	<input type="checkbox" class="checkbox-ios"/>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/products/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="SKU" class="center-custom"><a href="?action=include/products/edit.php" title="Chỉnh sửa">087AA9</a></td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giá sản phẩm"><span>2.340.000 đ</span></td>
					            <td data-title="Xem trước" class="center-custom"><a href="#" title="Xem">Xem</a></td>
								<td data-title="Người tạo">Deverloper</td>
								<td data-title="Ngày tạo" class="center-custom">15-08-2019</td>
								<td data-title="Người publish" class="td_user_showview hidden">Deverloper</td>
								<td data-title="Ngày publish" class="center-custom td_date_showview hidden">15-08-2019</td>
								<td data-title="Người cập nhật" class="td_user_update hidden">Deverloper</td>
								<td data-title="Ngày cập nhật" class="center-custom td_date_update hidden">15-08-2019</td>
					            <td data-title="Khối lượng" class="center-custom td_kg hidden">1000</td>
					            <td data-title="Thương hiệu" class="center-custom td_brand hidden">Orihiro</td>
					            <td data-title="Xuất sứ" class="center-custom td_country hidden">Nhật Bản</td>
					            <td data-title="Quy cách" class="center-custom td_style hidden">Chai</td>
					            <td data-title="Giá KM" class="center-custom td_pricekm hidden">2,000,000 đ</td>
					            <td data-title="Duyệt?" class="center-custom td_approve hidden">Duyệt</td>
					            <td data-title="Hạn sử dụng" class="center-custom td_times hidden">1</td>
					            <td data-title="Hết hàng?" class="center-custom td_status_num hidden">Còn hàng</td>

					            <td data-title="Publish">
					            	<input type="checkbox" class="checkbox-ios"/>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/products/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="Hình ảnh" class="center-custom">
					            	<img src="https://japana.vn/uploads/product/2018/03/19/1521470586-tinh-chat-nhau-thai-chong-lao-hoa-placenta-82x-450000mg.jpg" alt="Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml">
					            </td>
					            <td data-title="SKU" class="center-custom"><a href="?action=include/products/edit.php" title="Chỉnh sửa">087AA9</a></td>
					            <td data-title="Tên sản phẩm">Tinh chất nhau thai Placenta 82x 450000mg Nhật Bản 500ml</td>
					            <td data-title="Giá sản phẩm"><span>2.340.000 đ</span></td>
					            <td data-title="Xem trước" class="center-custom"><a href="#" title="Xem">Xem</a></td>
								<td data-title="Người tạo">Deverloper</td>
								<td data-title="Ngày tạo" class="center-custom">15-08-2019</td>
								<td data-title="Người publish" class="td_user_showview hidden">Deverloper</td>
								<td data-title="Ngày publish" class="center-custom td_date_showview hidden">15-08-2019</td>
								<td data-title="Người cập nhật" class="td_user_update hidden">Deverloper</td>
								<td data-title="Ngày cập nhật" class="center-custom td_date_update hidden">15-08-2019</td>
					            <td data-title="Khối lượng" class="center-custom td_kg hidden">1000</td>
					            <td data-title="Thương hiệu" class="center-custom td_brand hidden">Orihiro</td>
					            <td data-title="Xuất sứ" class="center-custom td_country hidden">Nhật Bản</td>
					            <td data-title="Quy cách" class="center-custom td_style hidden">Chai</td>
					            <td data-title="Giá KM" class="center-custom td_pricekm hidden">2,000,000 đ</td>
					            <td data-title="Duyệt?" class="center-custom td_approve hidden">Duyệt</td>
					            <td data-title="Hạn sử dụng" class="center-custom td_times hidden">1</td>
					            <td data-title="Hết hàng?" class="center-custom td_status_num hidden">Còn hàng</td>

					            <td data-title="Publish">
					            	<input type="checkbox" class="checkbox-ios"/>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="?action=include/products/edit.php" class="link-custom black-custom" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:update_product(1);" class="link-custom black-custom" title="Lưu">
					            		<i class="fa fa-save"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<?php include('include/products/addproductbyexcel.php')?>
<?php include('include/products/updateproductbyexcel.php')?>
<script>
	function showFilter(id){
    	if(jQuery('#show'+id).css('display')=='none'){
    		jQuery('#show'+id).css('display','inline-block');
    		jQuery('#show'+id).prev().find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
    		if(jQuery('.content-sidebar-wrap').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());	
			}
    	}
    	else{
    		jQuery('#show'+id).css('display','none');
    		jQuery('#show'+id).prev().find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
    		if(jQuery('.content-sidebar-wrap').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());	
			}
    	}
	}

	function setview(idcontrol,name){
        var bool = jQuery('#' + idcontrol).is(":checked");
        if(bool){
            jQuery('#th_' + name).removeClass('hidden');
            jQuery('.td_' + name).removeClass('hidden');
        }else{
            jQuery('#th_' + name).addClass('hidden');
            jQuery('.td_' + name).addClass('hidden');
        }
    }

    function openFilterMobile(id){
    	if(jQuery('#mobile-filter-'+id).css('display')=='none'){
    		jQuery('#mobile-filter-'+id).css('display','inline-block');
    		jQuery('#show'+id).css('display','inline-block');
	    	jQuery('#mobile-filter-'+id).css('z-index','999');
	    	jQuery('#name-mobile-'+ id).css('display','flex');
	    	jQuery('body').css('overflow','hidden');
	    	jQuery('.button-filter').parent().addClass('active');
	    	jQuery('#button-filter-'+id).find('i').addClass('ti-arrow-right');
	    	jQuery('#button-filter-'+id).addClass('active');
    		jQuery('#button-filter-'+id).siblings().css('display','none');
    	}else{
    		jQuery('body').css('overflow','inherit');
    		jQuery('#mobile-filter-'+id).css('display','none');
    		jQuery('#mobile-filter-'+id).css('z-index','-1');
    		jQuery('#name-mobile-'+ id).css('display','none');
    		jQuery('#show'+id).css('display','none');
    		jQuery('.button-filter').parent().removeClass('active');
    		jQuery('#button-filter-'+id).find('i').removeClass('ti-arrow-right');
    		jQuery('#button-filter-'+id).removeClass('active');
    		jQuery('#button-filter-'+id).siblings().css('display','flex');
    	}
    }
	jQuery(function(){
		var heightBottomPagination = jQuery('.pagination-custom').outerHeight();
		if(window.innerWidth < 576){
			jQuery('.box-table').css('margin-bottom',heightBottomPagination);
		}
		jQuery('#list_id_category,#id_brand,#id_style,#id_country,#id_ncc,#id_style,#id_product_type,#id_user_showview,#username').select2();
	})
</script>