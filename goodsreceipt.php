<style>
	.dropdown-collapse{
		margin-top: 15px;
	}
	.custom-collapse .item{
		display: inline-block;
		width: 20%;
		float: left;
		margin-top: 10px;
		padding: 0 15px;
	}
	.custom-collapse .item label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.custom-dropdown:after{
		width: 8%;
	}
	#show1,#show2{
		display: none;
	}
	#show2 .item{
		float: left;
		width: auto;
	}
	.box-table{
		position: relative;
	}
	.table-custom{
		white-space: nowrap;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	img.gift-order{
		height: 20px!important; 
		bottom: 5px; 
		left: 0;  
		position: absolute;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.box-table tr td .checkbox-custom {
	    cursor: pointer;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	    -ms-flex-align: center;
	    align-items: center;
	    height: 20px;
	}
	table tr th:nth-child(3), table tr td:nth-child(3){
	    width: 250px;
	    max-width: 250px;
	    min-width: 250px;
	    white-space: normal;
	}
	.box-table tr td:nth-child(9) .checkbox-custom .checkmark{
		left: auto;
	}

	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		padding: 15px 0;
	}
	.box-quick-search {
	    display: inline-block;
	    width: 100%;
	    margin-top: 15px;
	}
	.box-quick-search .item {
	    display: inline-block;
	    width: 50%;
	    float: left;
	}
	.box-quick-search .item:first-child input{
		width: 50%;
    	float: left;
	}
	.box-quick-search .item:first-child button {
	    float: left;
	    margin-left: 15px;
	}
	.box-button-filter{
  		display: none;
  	}
  	.button-filter{
	    font-size: 14px;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: center;
	        -ms-flex-pack: center;
	            justify-content: center;
	    background: rgba(0,0,0,0.3);
	    border-radius: 10px 0 0 10px;
	    height: 38px;
	    width: 38px;
	    position: fixed;
	    right: 0;
	    top: 145px;
		z-index: 99
	}
	.button-filter.active{
		left: 0px;
	    border-radius: 0px;
	    right: auto;
	    z-index: 99;
	    top: 0px!important;
	    height: 40px;
	    background: #222D32!important;
	    color: #fff!important;
	}
	#button-filter-2{
		top: 189px;
	}
	.button-filter:focus, .button-filter:hover{
		color: #fff;
	}
	#name-mobile-1,#name-mobile-2{
		display: none;
	}
	.search2, .search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.search1{
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.action-button{
		padding-top: 15px;
	}
	@media (max-width: 575.98px) {
		.filter-pc{
	  		display: none;
	  		position: fixed;
		    top: 0;
		    right: 0;
		    width: calc(100% - 38px);
		    height: 100%;
		    z-index: -1;
		    margin-top: 0!important;
	  	}
	  	.dropdown-collapse{
	  		display: none;
	  	}
	  	.box-filter{
	  		position: relative;
	  		height: 100%;
	  	}
	  	.button-tab a{
	  		width: 100%;
		    display: -webkit-box;
		    display: -ms-flexbox;
		    display: flex;
		    height: 40px;
		    color: #fff;
		    background: #222D32;
		    -webkit-box-align: center;
		        -ms-flex-align: center;
		            align-items: center;
		    -webkit-box-pack: center;
		        -ms-flex-pack: center;
		            justify-content: center;
		    float: left;
	  	}
	  	.custom-collapse{
	  		height: 100%;
		    width: 100%;
		    padding: 0!important;
		    float: right;
		    overflow-x: hidden;
		    position: relative;
		    z-index: 999;
		    background: #fff;
		    border-left: 1px solid #eee;
	  	}
	  	#show1{
	  		height: calc(100% - 80px);
	  	}
	  	#show2 .item {
		    width: 100%;
		}
	  	.custom-collapse .item:last-child{
	  		padding: 0;
	  		position: fixed;
		    bottom: 0;
		    right: 0;
		    width: calc(100% - 38px);
	  	}
	  	.custom-collapse .item:last-child button {
		    width: 50%;
		    border-radius: 0;
		    float: left;
		    border-right: 1px solid #eee;
		}
		.custom-collapse .item:last-child button:last-child{
			border-right: 0;
		}
	  	.custom-collapse .item{
	  		width: 100%;
	  		margin: 10px 0 0!important;
	  	}
	  	.custom-collapse .item:first-child{
	  		width: 100%;
	  	}
	  	.custom-collapse .item:last-child label{
	  		display: none;
	  	}
	  	.custom-collapse .item label{
	  		font-size: 13px;
	  	}
	  	.box-button-filter.active{
	  		background: rgba(0,0,0,.3);
		    width: 38px;
		    height: 100%;
		    position: fixed;
		    left: 0;
		    top: 0;
    		z-index: 999;
	  	}
	  	.box-button-filter{
	  		display: block;
	  	}
	  	.box-quick-search{
	  		margin-top: 0;
	  	}
	  	.box-quick-search .item{
	  		margin-top: 15px;
	  	}
	  	.box-quick-search .item,.box-quick-search .item:first-child input{
	  		width: 100%;
	  	}
	  	.search2{
	  		display: inline-block;
	  		width: auto;
	  	}
	  	.entry-header ul{
	  		white-space: nowrap;
    		overflow: scroll;
	  	}
	  	.box-table tr td .checkbox-custom{
	  		padding-left: 0;
	  	}
	  	.table-responsive > tbody > tr td:first-child{
	  		display: none;
	  	}
	  	.table-custom > tbody > tr > td{
			white-space: normal;
		}
		.box-table tr td:last-child{
			padding: 0;
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.custom-collapse .item:last-child{
			width: 100%
		}
		.custom-collapse .item:last-child button{
			float: left;
			margin-right: 15px;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.custom-collapse .item:last-child{
			width: 100%
		}
		.custom-collapse .item:last-child button{
			float: left;
			margin-right: 15px;
		}	
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {	
	}
	@media (min-width: 1200px) {
	}
</style>
<main class="goodsreceipt content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Sản phẩm đã đặt</h1>
			<ul>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#listproinday-modal" class="link-custom black-custom" title="Hàng về trong ngày">
						<i class="fa fa-calendar-o" aria-hidden="true"></i> <label>Hàng về trong ngày</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#addpro-modal" class="link-custom black-custom" title="Thêm sản phẩm">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm sản phẩm</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#createreceipt-modal" class="link-custom black-custom open-receipt" title="Tạo phiếu nhập">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Tạo phiếu nhập</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div id="mobile-filter-1" class="filter-pc">
					<div class="box-filter">
						<div class="button-tab">
							<a href="javascript:void(0);" id="name-mobile-1" title="Tìm kiếm nâng cao">Tìm kiếm nâng cao</a>
						</div>
						<a class="dropdown-collapse bg-black" onclick="showFilter(1)" href="javascript:void(0);">Tìm kiếm nâng cao <i class="fa fa-caret-down" aria-hidden="true"></i></a>
						<div id="show1" class="custom-collapse">
							<form id="advance_search" name="frm advance_search" method="post">
							    <div class="item">
							    	<label for="cbo_username">Nhân viên</label>
							    	<select class="form-control" name="username" id="cbo_username">
							    		<option value="-1">Chọn nhân viên</option>
									  	<option value="0">Admin</option>
									  	<option value="1">Nguyễn Văn A</option>
									  	<option value="2">Trần Thị B</option>
									</select>
							    </div>
							    <div class="item">
							    	<label for="id_supplier">Nhà cung cấp</label>
							    	<select class="form-control" name="id_supplier" id="id_supplier">
							    		<option value="-1">Chọn nhà cung cấp</option>
									  	<option value="0">SBS (Anh Phi)</option>
									  	<option value="1">JP Shop Nhật (Chị Thu)</option>
									  	<option value="2">Chị Châu SK-II</option>
									</select>
							    </div>
							    <div class="item">
							    	<label for="keywordavanced">SKU và tên sản phẩm</label>
							    	<input autocomplete="off" type="text" name="keywordavanced" id="keywordavanced" class="form-control" placeholder="Nhập SKU và tên sản phẩm">
							    </div>
							    <div class="item">
							    	<label for="product_status">Trạng thái sản phẩm</label>
							    	<div class="custom-dropdown">
								    	<select class="form-control" name="product_status" id="product_status">
								    		<option value="-1">Chọn trạng thái</option>
										  	<option value="0">Đạt</option>
										  	<option value="1">Không đạt</option>
										</select>
									</div>
							    </div>
							    <div class="item">
							    	<label for="product_type">Loại sản phẩm</label>
							    	<div class="custom-dropdown">
								    	<select class="form-control" name="product_type" id="product_type">
								    		<option value="-1">Chọn loại sản phẩm</option>
										  	<option value="0">Chiến lược</option>
										  	<option value="1">Tập trung</option>
										  	<option value="2">Còn lại</option>
										</select>
									</div>
							    </div>
							    <div class="item">
							    	<label for="date-order">Khoảng thời gian</label>
							    	<div class="time-graph">
				                        <div class="box-time">
				                        	<input type="hidden" name="from_date_ad" id="start-date3" value="">
									        <input type="hidden" name="to_date_ad" id="end-date3" value="">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-order" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày hiển thị...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
				                    </div>
							    </div>
							    <div class="item">
							    	<label class="visible-hidden">Xác nhận</label>
							    	<button type="submit" name="advanced-search" class="button bg-black">Tìm kiếm</button>
							    	<button type="button" onclick="gotolink('http://japana.vn/admincp/goodsreceipt/index?clear=true');" name="advanced-search" class="button bg-red">Reset</button>
							    </div>
							</form>
						</div>
					</div>
				</div>
				<div id="mobile-filter-2" class="filter-pc">
					<div class="box-filter">
						<div class="button-tab">
							<a href="javascript:void(0);" id="name-mobile-2" title="Thiết lập hiển thị">Thiết lập hiển thị</a>
						</div>
						<a class="dropdown-collapse bg-black" onclick="showFilter(2)" href="javascript:void(0);">Thiết lập hiển thị <i class="fa fa-caret-down" aria-hidden="true"></i></a>
						<div id="show2" class="custom-collapse">
							<div class="item">
								<label for="checkbox_kg" class="checkbox-custom">Loại
				                  	<input id="checkbox_kg" name="checkbox_kg" type="checkbox" onclick="setview('checkbox_type','type');">
				                  	<span class="checkmark"></span>
				                </label>
							</div>
							<div class="item">
								<label for="checkbox_brand" class="checkbox-custom">Ngày cập nhật
								  	<input id="checkbox_brand" name="checkbox_brand" type="checkbox" onclick="setview('checkbox_date_update','date_update');">
				                  	<span class="checkmark"></span>
								</label>
							</div>
							<div class="item">
								<label for="checkbox_country" class="checkbox-custom">Ngày hàng về
				                  	<input id="checkbox_country" name="checkbox_country" type="checkbox" onclick="setview('checkbox_date_available','date_available');">
				                  	<span class="checkmark"></span>
				                </label>
							</div>
							<div class="item">
								<label for="checkbox_style" class="checkbox-custom">Nhà cung cấp
								  	<input id="checkbox_style" name="checkbox_style" type="checkbox" onclick="setview('checkbox_supplier','supplier');">
				                  	<span class="checkmark"></span>
								</label>
							</div>
							<div class="item">
								<label for="checkbox_pricekm" class="checkbox-custom">Kho hàng
				                  	<input id="checkbox_pricekm" name="checkbox_pricekm" type="checkbox" onclick="setview('checkbox_user_create','user_create');">
				                  	<span class="checkmark"></span>
				                </label>
							</div>
							<div class="item">
								<label for="checkbox_approve" class="checkbox-custom">Người tạo
								  	<input id="checkbox_approve" name="checkbox_approve" type="checkbox" onclick="setview('checkbox_approve','approve');">
				                  	<span class="checkmark"></span>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="box-button-filter">
					<a id="button-filter-1" class="button-filter white-custom" onclick="openFilterMobile(1);" href="javascript:void(0);"><i class="ti-search" aria-hidden="true"></i></a>
					<a id="button-filter-2" class="button-filter white-custom" onclick="openFilterMobile(2);" href="javascript:void(0);"><i class="fa fa-eye" aria-hidden="true"></i></a>
				</div>
				<div class="box-quick-search">
					<div class="item">
						<form name="quick_search" id="frm" action="" method="post" class="search1">
	                       <input name="value" value="" type="text" class="form-control custom-ipt" placeholder="Nhập thông tin cần tìm...">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<form name="status_search" id="frm" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select class="form-control" id="oderByGoodSreceipt" name="orderBy">
						    		<option value="-1">Trạng thái</option>
								  	<option value="0">Mặc định</option>
								  	<option value="1">Ngày cập nhật</option>
								  	<option value="2">Ngày hàng về</option>
								  	<option value="3">Tên sản phẩm</option>
								</select>
							</div>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black">SKU</th>
					            <th class="bg-black">Tên sản phẩm</th>
					            <th class="bg-black right-custom">Giá mua</th>
					            <th class="bg-black right-custom">Giá bán</th>
					            <th class="bg-black center-custom">Chiết khấu</th>
					            <th class="bg-black center-custom">Số lượng</th>
					            <th class="bg-black center-custom">Loại</th>
					            <th class="bg-black center-custom">Phí vận chuyển</th>
					            <th class="bg-black">Ngày cập nhật</th>
					            <th class="bg-black">Ngày về hàng</th>
					            <th class="bg-black">Nhà cung cấp</th>
					            <th class="bg-black">Kho hàng</th>
					            <th class="bg-black">Người tạo</th>
					            <th class="bg-black center-custom">Thực nhập</th>
					            <th class="bg-black center-custom">Check</th>
					            <th class="bg-black">Tác vụ</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="SKU">
					            	<a href="javascript:void(0)" data-toggle="modal" data-target="#editpro-modal" title="001FAC582">001FAC582</a>
					            </td>
					            <td data-title="Tên sản phẩm">Nước súc miệng Propolinse màu cam 600ml</td>
					            <td data-title="Giá mua" class="right-custom">140.000 đ</td>
					            <td data-title="Giá bán" class="right-custom">210.000 đ</td>
					            <td data-title="Chiết khấu" class="center-custom">33.3%</td>
					            <td data-title="Số lượng" class="center-custom">2</td>
					            <td data-title="Loại" class="center-custom">Còn lại</td>
					            <td data-title="Phí vận chuyển" class="center-custom">
				                    <label class="checkbox-custom">
				                      <input value="" type="checkbox" disabled="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Ngày cập nhật">21-02-2019 <strong>|</strong> 08:59</td>
					            <td data-title="Ngày về hàng">23-02-2019</td>
					            <td data-title="Nhà cung cấp">SBS (Anh Phi)</td>
					            <td data-title="Kho hàng">Kho hàng bán</td>
					            <td data-title="Người tạo">Hương mua hàng</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[1]" id="qty1" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" id="chk_receipt1" onclick="checkedandopen(1)" class="chk2">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#editpro-modal" title="Chỉnh sửa sản phẩm">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(1,'http://test.japana.vn/admincp/goodsreceipt/del/id=1')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom red-custom">2</td>
					            <td data-title="SKU" class="red-custom"><a href="javascript:void(0)" class="red-custom" data-toggle="modal" data-target="#editpro-modal" title="001FAC2">001FAC2</a></td>
					            <td data-title="Tên sản phẩm" class="red-custom">Nước súc miệng Propolinse</td>
					            <td data-title="Giá mua" class="right-custom red-custom">140.000 đ</td>
					            <td data-title="Giá bán" class="right-custom red-custom">210.000 đ</td>
					            <td data-title="Chiết khấu" class="center-custom red-custom">33.3%</td>
					            <td data-title="Số lượng" class="center-custom red-custom">20</td>
					            <td data-title="Loại" class="center-custom red-custom">Còn lại</td>
					            <td data-title="Phí vận chuyển" class="center-custom red-custom">
				                    <label class="checkbox-custom">
				                      <input value="" type="checkbox" disabled="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Ngày cập nhật" class="red-custom">21-02-2019 <strong>|</strong> 08:59</td>
					            <td data-title="Ngày về hàng" class="red-custom">23-02-2019</td>
					            <td data-title="Nhà cung cấp" class="red-custom">SBS (Anh Phi)</td>
					            <td data-title="Kho hàng" class="red-custom">Kho hàng bán</td>
					            <td data-title="Người tạo" class="red-custom">Hương mua hàng</td>
					            <td data-title="Thực nhập" class="center-custom red-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[2]" id="qty2" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom red-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" id="chk_receipt2" onclick="checkedandopen(2)" class="chk2">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Tác vụ" class="red-custom">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#editpro-modal" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(2,'http://test.japana.vn/admincp/goodsreceipt/del/id=2')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="SKU"><a href="javascript:void(0)" data-toggle="modal" data-target="#editpro-modal" title="001FAC582">001FAC582</a></td>
					            <td data-title="Tên sản phẩm">Propolinse màu cam 600ml</td>
					            <td data-title="Giá mua" class="right-custom">140.000 đ</td>
					            <td data-title="Giá bán" class="right-custom">210.000 đ</td>
					            <td data-title="Chiết khấu" class="center-custom">33.3%</td>
					            <td data-title="Số lượng" class="center-custom">12</td>
					            <td data-title="Loại" class="center-custom">Còn lại</td>
					            <td data-title="Phí vận chuyển" class="center-custom">
				                    <label class="checkbox-custom">
				                      <input value="" type="checkbox" disabled="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Ngày cập nhật">21-02-2019 <strong>|</strong> 08:59</td>
					            <td data-title="Ngày về hàng">23-02-2019</td>
					            <td data-title="Nhà cung cấp">SBS (Anh Phi)</td>
					            <td data-title="Kho hàng">Kho hàng bán</td>
					            <td data-title="Người tạo">Hương mua hàng</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[3]" id="qty3" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" id="chk_receipt3" onclick="checkedandopen(3)" class="chk2">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#editpro-modal" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(3,'http://test.japana.vn/admincp/goodsreceipt/del/id=3')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="SKU"><a href="javascript:void(0)" data-toggle="modal" data-target="#editpro-modal" title="001FA582">001FA582</a></td>
					            <td data-title="Tên sản phẩm">Nước súc miệng màu cam 600ml</td>
					            <td data-title="Giá mua" class="right-custom">140.000 đ</td>
					            <td data-title="Giá bán" class="right-custom">210.000 đ</td>
					            <td data-title="Chiết khấu" class="center-custom">33.3%</td>
					            <td data-title="Số lượng" class="center-custom">2</td>
					            <td data-title="Loại" class="center-custom">Còn lại</td>
					            <td data-title="Phí vận chuyển" class="center-custom">
				                    <label class="checkbox-custom">
				                      <input value="" type="checkbox" disabled="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Ngày cập nhật">21-02-2019 <strong>|</strong> 08:59</td>
					            <td data-title="Ngày về hàng">23-02-2019</td>
					            <td data-title="Nhà cung cấp">SBS (Anh Phi)</td>
					            <td data-title="Kho hàng">Kho hàng bán</td>
					            <td data-title="Người tạo">Hương mua hàng</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[4]" id="qty4" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" id="chk_receipt4" onclick="checkedandopen(4)" class="chk2">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#editpro-modal" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(4,'http://test.japana.vn/admincp/goodsreceipt/del/id=4')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="SKU"><a href="javascript:void(0)" data-toggle="modal" data-target="#editpro-modal" title="001AC582">001AC582</a></td>
					            <td data-title="Tên sản phẩm">Nước Propolinse màu cam 600ml</td>
					            <td data-title="Giá mua" class="right-custom">140.000 đ</td>
					            <td data-title="Giá bán" class="right-custom">210.000 đ</td>
					            <td data-title="Chiết khấu" class="center-custom">33.3%</td>
					            <td data-title="Số lượng" class="center-custom">2</td>
					            <td data-title="Loại" class="center-custom">Còn lại</td>
					            <td data-title="Phí vận chuyển" class="center-custom">
				                    <label class="checkbox-custom">
				                      <input value="" type="checkbox" disabled="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Ngày cập nhật">21-02-2019 <strong>|</strong> 08:59</td>
					            <td data-title="Ngày về hàng">23-02-2019</td>
					            <td data-title="Nhà cung cấp">SBS (Anh Phi)</td>
					            <td data-title="Kho hàng">Kho hàng bán</td>
					            <td data-title="Người tạo">Hương mua hàng</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[5]" id="qty5" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<label class="checkbox-custom">
				                      <input value="" type="checkbox" id="chk_receipt5" onclick="checkedandopen(5)" class="chk2">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#editpro-modal" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(5,'http://test.japana.vn/admincp/goodsreceipt/del/id=5')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">6</td>
					            <td data-title="SKU"><a href="javascript:void(0)" data-toggle="modal" data-target="#editpro-modal" title="001FA582">001FA582</a></td>
					            <td data-title="Tên sản phẩm">Nước súc miệng Propolinse 600ml</td>
					            <td data-title="Giá mua" class="right-custom">140.000 đ</td>
					            <td data-title="Giá bán" class="right-custom">210.000 đ</td>
					            <td data-title="Chiết khấu" class="center-custom">33.3%</td>
					            <td data-title="Số lượng" class="center-custom">2</td>
					            <td data-title="Loại" class="center-custom">Còn lại</td>
					            <td data-title="Phí vận chuyển" class="center-custom">
				                    <label class="checkbox-custom">
				                      <input value="" type="checkbox" checked="" disabled="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Ngày cập nhật">21-02-2019 <strong>|</strong> 08:59</td>
					            <td data-title="Ngày về hàng">23-02-2019</td>
					            <td data-title="Nhà cung cấp">SBS (Anh Phi)</td>
					            <td data-title="Kho hàng">Kho hàng ký gửi</td>
					            <td data-title="Người tạo">Hương mua hàng</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[6]" id="qty6" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#permission-modal" title="Duyệt">Duyệt</a>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#editpro-modal" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(6,'http://test.japana.vn/admincp/goodsreceipt/del/id=6')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">7</td>
					            <td data-title="SKU"><a href="javascript:void(0)" data-toggle="modal" data-target="#editpro-modal" title="001FAC582">001FAC582</a></td>
					            <td data-title="Tên sản phẩm">Nước súc Propolinse màu cam 600ml</td>
					            <td data-title="Giá mua" class="right-custom">140.000 đ</td>
					            <td data-title="Giá bán" class="right-custom">210.000 đ</td>
					            <td data-title="Chiết khấu" class="center-custom">33.3%</td>
					            <td data-title="Số lượng" class="center-custom">2</td>
					            <td data-title="Loại" class="center-custom">Còn lại</td>
					            <td data-title="Phí vận chuyển" class="center-custom">
				                    <label class="checkbox-custom">
				                      <input value="" type="checkbox" checked="" disabled="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Ngày cập nhật">21-02-2019 <strong>|</strong> 08:59</td>
					            <td data-title="Ngày về hàng">23-02-2019</td>
					            <td data-title="Nhà cung cấp">SBS (Anh Phi)</td>
					            <td data-title="Kho hàng">Kho hàng ký gửi</td>
					            <td data-title="Người tạo" class="center-custom">Hương mua hàng</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[7]" id="qty7" disabled="">
					            </td>
					            <td data-title="Check">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#permission-modal" title="Duyệt">Duyệt</a>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#editpro-modal" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(7,'http://test.japana.vn/admincp/goodsreceipt/del/id=7')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">8</td>
					            <td data-title="SKU"><a href="javascript:void(0)" data-toggle="modal" data-target="#editpro-modal" title="001C582">001C582</a></td>
					            <td data-title="Tên sản phẩm">Nước súc miệng Propolinse màu cam</td>
					            <td data-title="Giá mua" class="right-custom">140.000 đ</td>
					            <td data-title="Giá bán" class="right-custom">210.000 đ</td>
					            <td data-title="Chiết khấu" class="center-custom">33.3%</td>
					            <td data-title="Số lượng" class="center-custom">2</td>
					            <td data-title="Loại" class="center-custom">Còn lại</td>
					            <td data-title="Phí vận chuyển" class="center-custom">
				                    <label class="checkbox-custom">
				                      <input value="" type="checkbox" checked="" disabled="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Ngày cập nhật">21-02-2019 <strong>|</strong> 08:59</td>
					            <td data-title="Ngày về hàng">23-02-2019</td>
					            <td data-title="Nhà cung cấp">SBS (Anh Phi)</td>
					            <td data-title="Kho hàng">Kho hàng ký gửi</td>
					            <td data-title="Người tạo">Hương mua hàng</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[8]" id="qty8" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#permission-modal" title="Duyệt">Duyệt</a>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#editpro-modal" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(8,'http://test.japana.vn/admincp/goodsreceipt/del/id=8')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">9</td>
					            <td data-title="SKU"><a href="javascript:void(0)" data-toggle="modal" data-target="#editpro-modal" title="001FAC582">001FA82</a></td>
					            <td data-title="Tên sản phẩm">Nước súc màu cam 600ml</td>
					            <td data-title="Giá mua" class="right-custom">140.000 đ</td>
					            <td data-title="Giá bán" class="right-custom">210.000 đ</td>
					            <td data-title="Chiết khấu" class="center-custom">33.3%</td>
					            <td data-title="Số lượng" class="center-custom">2</td>
					            <td data-title="Loại" class="center-custom">Còn lại</td>
					            <td data-title="Phí vận chuyển" class="center-custom">
				                    <label class="checkbox-custom">
				                      <input value="" type="checkbox" checked="" disabled="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Ngày cập nhật">21-02-2019 <strong>|</strong> 08:59</td>
					            <td data-title="Ngày về hàng">23-02-2019</td>
					            <td data-title="Nhà cung cấp">SBS (Anh Phi)</td>
					            <td data-title="Kho hàng">Kho hàng ký gửi</td>
					            <td data-title="Người tạo">Hương mua hàng</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[9]" id="qty9" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#permission-modal" title="Duyệt">Duyệt</a>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#editpro-modal" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(9,'http://test.japana.vn/admincp/goodsreceipt/del/id=9')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">10</td>
					            <td data-title="SKU"><a href="javascript:void(0)" data-toggle="modal" data-target="#editpro-modal" title="001FAC582">FAC582</a></td>
					            <td data-title="Tên sản phẩm">Propolinse màu cam 600ml</td>
					            <td data-title="Giá mua" class="right-custom">140.000 đ</td>
					            <td data-title="Giá bán" class="right-custom">210.000 đ</td>
					            <td data-title="Chiết khấu" class="center-custom">33.3%</td>
					            <td data-title="Số lượng" class="center-custom">2</td>
					            <td data-title="Loại" class="center-custom">Còn lại</td>
					            <td data-title="Phí vận chuyển" class="center-custom">
				                    <label class="checkbox-custom">
				                      <input value="" type="checkbox" checked="" disabled="">
				                      <span class="checkmark"></span>
				                    </label>
					            </td>
					            <td data-title="Ngày cập nhật">21-02-2019 <strong>|</strong> 08:59</td>
					            <td data-title="Ngày về hàng">23-02-2019</td>
					            <td data-title="Nhà cung cấp">SBS (Anh Phi)</td>
					            <td data-title="Kho hàng">Kho hàng ký gửi</td>
					            <td data-title="Người tạo">Hương mua hàng</td>
					            <td data-title="Thực nhập" class="center-custom">
					            	<input type="number" class="form-control soluong-open" value="0" name="probill[10]" id="qty10" disabled="">
					            </td>
					            <td data-title="Check" class="center-custom">
					            	<a href="javascript:void(0);" data-toggle="modal" data-target="#permission-modal" title="Duyệt">Duyệt</a>
					            </td>
					            <td data-title="Tác vụ">
					            	<a href="javascript:void(0);" class="link-custom black-custom" data-toggle="modal" data-target="#editpro-modal" title="Chỉnh sửa">
					            		<i class="fa fa-pencil-square-o"></i>
					            	</a>
					            	<a href="javascript:void(0);" onclick="removeProOrder(10,'http://test.japana.vn/admincp/goodsreceipt/del/id=10')" class="link-custom black-custom" title="Xóa">
					            		<i class="fa fa-trash-o"></i>
					            	</a>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
				<?php include('include/pagination.php')?>
			</div>
		</div>
	</article>
</main>
<?php include('include/goodsreceipt/listproinday.php'); ?>
<?php include('include/goodsreceipt/addpro.php'); ?>
<?php include('include/goodsreceipt/permission.php'); ?>
<?php include('include/goodsreceipt/create-receipt.php'); ?>
<?php include('include/goodsreceipt/editpro.php'); ?>
<?php include('include/goodsreceipt/historybuy.php'); ?>
<?php include('include/goodsreceipt/historybuypro.php'); ?>
<script>
	function checkedandopen(id){
	  if(jQuery('#chk_receipt'+id).prop('checked')==false){
	    jQuery('#qty'+id).prop('disabled',true);
	    jQuery('#qty'+id).addClass('bg-gray');
	  }else{
	    jQuery('#qty'+id).prop('disabled',false);
	    jQuery('#qty'+id).removeClass('bg-gray');
	  }
	}
	function showFilter(id){
    	if(jQuery('#show'+id).css('display')=='none'){
    		jQuery('#show'+id).css('display','inline-block');
    		jQuery('#show'+id).prev().find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
    		if(jQuery('.content-sidebar-wrap').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());	
			}
    	}
    	else{
    		jQuery('#show'+id).css('display','none');
    		jQuery('#show'+id).prev().find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
    		if(jQuery('.content-sidebar-wrap').outerHeight() > jQuery('body').outerHeight()){
				jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
			}
			else{
				jQuery('.nav-primary').css('height',jQuery('body').outerHeight());	
			}
    	}
	}

	function setview(idcontrol,name){
        var bool = jQuery('#' + idcontrol).is(":checked");
        if(bool){
            jQuery('#th_' + name).removeClass('hidden');
            jQuery('.td_' + name).removeClass('hidden');
        }else{
            jQuery('#th_' + name).addClass('hidden');
            jQuery('.td_' + name).addClass('hidden');
        }
    }

    function openFilterMobile(id){
    	if(jQuery('#mobile-filter-'+id).css('display')=='none'){
    		jQuery('#mobile-filter-'+id).css('display','inline-block');
    		jQuery('#show'+id).css('display','inline-block');
	    	jQuery('#mobile-filter-'+id).css('z-index','999');
	    	jQuery('#name-mobile-'+ id).css('display','flex');
	    	jQuery('body').css('overflow','hidden');
	    	jQuery('.button-filter').parent().addClass('active');
	    	jQuery('#button-filter-'+id).find('i').addClass('ti-arrow-right');
	    	jQuery('#button-filter-'+id).addClass('active');
    		jQuery('#button-filter-'+id).siblings().css('display','none');
    	}else{
    		jQuery('body').css('overflow','inherit');
    		jQuery('#mobile-filter-'+id).css('display','none');
    		jQuery('#mobile-filter-'+id).css('z-index','-1');
    		jQuery('#name-mobile-'+ id).css('display','none');
    		jQuery('#show'+id).css('display','none');
    		jQuery('.button-filter').parent().removeClass('active');
    		jQuery('#button-filter-'+id).find('i').removeClass('ti-arrow-right');
    		jQuery('#button-filter-'+id).removeClass('active');
    		jQuery('#button-filter-'+id).siblings().css('display','flex');
    	}
    }
	jQuery(function(){
		var heightPagination = jQuery('.pagination-custom').outerHeight();
		jQuery('.open-receipt').click(function(e){
			jQuery(window).trigger('resize');
		});
		var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    jQuery('#cbo_username,#id_supplier').select2();
	    if(window.innerWidth > 576) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    	jQuery('.goodsreceipt .box-table').css('margin-bottom',heightPagination + 15);
	    }
		jQuery('input[name="date-order"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date3').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date3').val(end.format('DD-MM-YYYY'));
		});
	})
</script>