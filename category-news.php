<style>
	.group-category{
		display: inline-block;
		float: left;
		width: 24%;
		border: 1px solid #eee;
		margin-top: 15px;
		overflow-y: scroll;
		overflow-x: hidden;
		position: relative;
	}
	.group-category .item{
		position: relative;
		height: 40px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-orient: horizontal;
		-webkit-box-direction: reverse;
		    -ms-flex-direction: row-reverse;
		        flex-direction: row-reverse;
		padding-right: 15px;
		border-bottom: 1px solid #eee;
	}
	.group-category .item a{
		position: absolute;
		top: 50%;
		left: 15px;
		-webkit-transform: translate(0,-50%);
		    -ms-transform: translate(0,-50%);
		        transform: translate(0,-50%);
	}
	.group-category .item a.active{
		color: #ff0000;
	}
	.group-category .item .show-category{
		display: inline-block;
	    width: 100%;
	    text-align: right;
	    cursor: pointer;
	}
	.group-custom-cate, .group-box-seo{
		display: inline-block;
		float: left;
		width: 36%;
		margin-top: 15px;
		margin-left: 2%;
		margin-bottom: 15px;
	}
	.box-cate-custom .item, .box-seo .item{
		display: inline-block;
		width: 31%;
		margin: 10px 0;
		float: left;
	}
	.box-cate-custom .item:last-child, .box-seo .item .seo-input:last-child{
		margin-bottom: 0;
	}
	.box-cate-custom .item:first-child{
		width: 100%;
	}
	.box-cate-custom .item:nth-child(2){
		width: 60%;
	}
	.box-cate-custom .item:nth-child(3){
		width: 20%;
		padding: 0 10px;
	}
	.box-cate-custom .item:nth-child(4){
		width: 20%;
	}
	.box-cate-custom .item:nth-child(5), .box-cate-custom .item:nth-child(6){
		width: 100%;
	}
	.input-group label.custom-upload, .input-group label.custom-upload2{
		width: 100px!important;
	}
	.box-cate-custom .item label, .box-seo .item label, .box-ckeditor label{
		font-size: 14px;
		line-height: 1.5;
		margin-bottom: 5px;
		font-weight: 500;
		width: 100%
	}
	.box-ckeditor label{
		margin-top: 5px;
	}
	.custom-dropdown:after{
		padding-right: 15px;
	}
	.box-img-meta{
		max-height: 180px;
		height: 180px;
		display: none;
		margin-top: 15px;
		display: none;
	}
	.box-img-meta img{
		height: 100%;
		width: auto;
	}
	.dropdown-collapse{
		float: left;
	}

	.box-seo{
		display: inline-block;
		width: 100%;
	}

	.box-seo .item{
		width: 100%;
	}
	.box-seo .item:last-child{
		margin-bottom: 0;
	}

	.box-seo .item .seo-input{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: baseline;
		    -ms-flex-align: baseline;
		        align-items: baseline;
		-webkit-box-pack: end;
		    -ms-flex-pack: end;
		        justify-content: flex-end;
		margin-bottom: 15px;
	}
	
	.box-seo .item .seo-input input, .box-seo .item .seo-input textarea, .box-seo .item .seo-input .upload-img{
		margin-left: 15px;
		width: 80%;
		float: right;
	}
	.box-seo .item .seo-input .upload-img input{
		margin-left: 0;
		width: auto
	}

	.box-table{
		width: 100%;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	    float: left;
	}
	.table-custom{
		margin-top: 0;
	}
	.table-custom > thead > tr > th, .table-custom > tbody > tr > td{
		padding: 0 5px;
	}
	.table-custom > tbody > tr > td{
		height: auto;
		position: relative;
	}
	.table-custom tr td img{
		width: auto;
		height: 60px;
	}
	img.gift-order{
		height: 20px!important; 
		bottom: 5px; 
		left: 0;  
		position: absolute;
	}
	.box-table tr td:nth-child(3){
		width: 250px;
	}
	.box-table thead{
		border-bottom: 1px solid #eee;
	}
	.first-price{
		text-decoration: line-through;
	    font-size: 12px;
	    color: #333;
	    width: 100%;
	    display: inline-block;
	    margin-bottom: 10px;
	}
	.box-table tr td:last-child a{
		float: left;
		padding: 5px;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-pack: center;
		    -ms-flex-pack: center;
		        justify-content: center;
	}
	.checkbox-custom{
	    cursor: pointer;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    height: 20px;
	}
	.checkbox-custom label{
	    color: #ccc;
	}
	.box-table tr td:nth-child(8), .box-table tr th:nth-child(8){
		width: 100px;
	}
	.box-table tr td:nth-child(10), .box-table tr th:nth-child(10){
		width: 230px;
	}
	.box-table tr td:last-child{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		    -ms-flex-align: center;
		        align-items: center;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		    -ms-flex-direction: column;
		        flex-direction: column;
		padding: 10px 0;
	}
	.checkmark{
		left: 30%;
    	-webkit-transform: translate(0,0);
    	    -ms-transform: translate(0,0);
    	        transform: translate(0,0);
	}
	.box-ckeditor{
		display: none;
		padding-top: 0;
	}
	.content-product{
		margin-bottom: 15px;
	}
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 45%;
		float: left;
	}
	.box-quick-search .item:last-child{
		width: 55%;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
	}
	.box-quick-search .item:first-child input{
		width: 70%;
		float: left;
	}
	.box-quick-search .item:first-child button, .box-quick-search .item:last-child button{
		float: left;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .box-time{
		width: 40%;
		margin-left: 15px;
	}
	.box-quick-search .item:last-child .custom-dropdown, .box-quick-search .item:last-child .box-time, .box-quick-search .item:last-child button{
		float: right;
	}
	.search2, .search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: end;
	        -ms-flex-pack: end;
	            justify-content: flex-end;
		width: 100%;
	}
	.search1{
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
	}
	.box-quick-search .item:last-child .custom-dropdown:after{
		padding: 12px 16px;
	}
	.show-inline{
		display: block!important;
	}
	.box-button-info-order{
  		display: none;
  		text-align: center;
  	}
  	.box-button-info-order a:hover{
  		color: #fff;
  	}
  	.show-info-order{
		color: #fff;
		z-index: 999;
	}
	.dropdown-table {
		width: auto;
	    text-align: center;
	    right: 25px;
	    background: #fff;
	    border: 1px solid #cbcbcb;
	    z-index: 1;
	    padding: 10px;
    	position: absolute;
    	display: none;
    	top: 38px;
	}
	.dropdown-table::after, .dropdown-table::before {
	    content: '';
	    position: absolute;
	    top: -8px;
	    right: 50%;
	    -webkit-transform: translateX(50%);
	    -ms-transform: translateX(50%);
	    transform: translateX(50%);
	    width: 0;
	    height: 0;
	    border-left: 7px solid transparent;
	    border-right: 7px solid transparent;
	    border-bottom: 8px solid #fff;
	}
	.dropdown-table::before {
	    border-left: 8px solid transparent;
	    border-right: 8px solid transparent;
	    border-bottom: 7px solid #cbcbcb;
	}
	.dropdown-table li{
		padding-bottom: 10px;
	}
	.dropdown-table li.active a {
	    color: #ff0000;
	}
	.dropdown-table li:last-child{
	    padding-bottom: 0;
	}
	.soluong-open {
	    background: #fff;
	    width: 90px;
	    border: 1px solid #cfcfcf;
	}
	#listsp-dm{
		display: inline-block;
		position: relative;
		width: 100%;
	}
	@media (max-width: 575.98px) {
		.show-info-order{
			display: block;
		}
	  	.group-category{
	  		display: none;
	  	}
	  	.group-category{
	  		position: fixed;
	  		height: auto!important;
		    width: 100%;
		    display: none;
		    top: 92px;
		    left: 0;
		    right: 0;
		    border-bottom: 2px solid #eee;
		    z-index: 99;
		    background: #fff;
		    margin-top: 0;
	  	}
	  	.dropdown-collapse{
	  		display: none;
	  	}
	  	.box-button-info-order{
	  		display: block;
	  	}
	  	.upload-multi-img{
	  		display: -webkit-box;
	  		display: -ms-flexbox;
	  		display: flex;
	  		margin-top: 15px;
	  	}
		#info-dm, #seo-dm, #note-dm, #listsp-dm{
			display: none;
			width: 100%;
		}
		.group-custom-cate, .group-box-seo{
			width: 100%;
			margin: 0;
		}
		.box-cate-custom .item, 
		.box-seo .item .seo-input .upload-img, .box-seo .item .seo-input,.box-img-block{
			display: inline-block;
			width: 100%;
		}
		.box-img-meta img{
			width: 100%;
			height: auto;
		}
		.box-seo .item .seo-input .upload-img{
			margin: 0;
			margin-top: 10px;
		}
		.box-seo .item .seo-input .upload-img input{
			margin-top: 0;
		}
		.box-cate-custom .item:nth-child(odd), .box-img-meta{
			margin-left: 0;
		}
		.box-cate-custom .item:last-child{
			margin-bottom: 15px;
		}
		.box-cate-custom .item:nth-child(2) {
		    width: 100%;
		}
		.box-cate-custom .item:nth-child(3),.box-cate-custom .item:nth-child(4){
			width: 49%;
			padding: 0;
		}
		.box-cate-custom .item:nth-child(3){
			margin-right: 2%;
		}
		.box-seo .item .seo-input input, .box-seo .item .seo-input textarea{
			margin-left: 0;
			width: 100%;
			margin-top: 10px;
		}
		.box-cate-custom .item, .box-seo .item{
			margin: 0;
			margin-top: 10px;
		}
		.box-ckeditor{
			display: block;
			margin-bottom: 15px;
			padding: 0;
		}
		.box-img-block{
			white-space: nowrap;
    		overflow-x: scroll;
    		overflow-y: hidden;
		}
		.box-img-block .item{
			width: 45%;
			margin: 0;
			margin-right: 5%;
		}
		.box-img-block .item:last-child{
			margin-right: 0;
		}
		.box-img-block .img-upbox{
			white-space: initial;
		}
		.img-upbox .btn-del-img{
			top: 0;
			left: auto;
    		right: -10px;
		}
		.box-quick-search .item{
			width: 100%;
			margin-bottom: 15px;
		}
		.box-quick-search .item:last-child{
			display: none;
		}
		.search1 input{
			width: 68%;
		}
		.search1 button{
			width: 30%;
		}
		.table-custom > tbody > tr > td{
		    min-height: 40px;
		}
		.box-table tr td:nth-child(3),
		.box-table tr td:nth-child(8), 
		.box-table tr th:nth-child(8), 
		.box-table tr td:nth-child(10), 
		.box-table tr th:nth-child(10){
			width: auto;
		}
		.checkbox-custom{
			padding-left: 0;
		}
		.box-table tr td:last-child{
			-webkit-box-orient: initial;
			-webkit-box-direction: initial;
			    -ms-flex-direction: initial;
			        flex-direction: initial;
			-webkit-box-pack: unset!important;
			    -ms-flex-pack: unset!important;
			        justify-content: unset!important;
		    padding: 0;
		}
		.box-table tr td:last-child .link-custom i{
			margin-bottom: 0;
		}
		.table-custom tr td:first-child{
			display: none;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {
		.group-category{
			width: 40%;
			margin-top: 15px;
		}
		.group-custom-cate{
			width: 58%;
			margin-top: 15px;
		}
		.group-box-seo{
			width: 100%;
			margin-left: 0;
		}
		.table-custom {
		    white-space: nowrap;
		}
		.table-custom > tbody > tr > td:last-child{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
		.box-table tr td:nth-child(8) input, .box-table tr th:nth-child(8) input{
			width: 100px;
		}
		.box-table tr td:nth-child(10) .box-time, .box-table tr th:nth-child(10) .box-time{
			width: 230px;
		}
	}
	@media (min-width: 768px) and (max-width: 991.98px) {
		.group-category{
			width: 40%;
			margin-top: 15px;
		}
		.group-custom-cate{
			width: 58%;
			margin-top: 15px;
		}
		.group-box-seo{
			width: 100%;
			margin-left: 0;
		}
		.table-custom {
		    white-space: nowrap;
		}
		.box-quick-search .item:last-child{
			display: none;
		}
		.table-custom > tbody > tr > td:last-child{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
		.box-table tr td:nth-child(8) input, .box-table tr th:nth-child(8) input{
			width: 100px;
		}
		.box-table tr td:nth-child(10) .box-time, .box-table tr th:nth-child(10) .box-time{
			width: 230px;
		}
	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		.box-seo .item .seo-input input, .box-seo .item .seo-input textarea{
			width: 70%;
		}
		.table-custom {
		    white-space: nowrap;
		}
		.first-price{
			margin-right: 10px;
			width: auto;
		}
		.table-custom > tbody > tr > td:last-child{
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			    -ms-flex-align: center;
			        align-items: center;
			-webkit-box-pack: center;
			    -ms-flex-pack: center;
			        justify-content: center;
			padding: 15px 0;
		}
		.box-table tr td:nth-child(8) input, .box-table tr th:nth-child(8) input{
			width: 100px;
		}
		.box-table tr td:nth-child(10) .box-time, .box-table tr th:nth-child(10) .box-time{
			width: 230px;
		}
		.box-seo .item .seo-input .upload-img input{
			width: 55%;
		}
	}
	@media (min-width: 1200px) {

	}
</style>
<main class="categorynews content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh mục tin tức</h1>
			<ul>
				<li>
					<a href="javascript:void(0)" data-toggle="modal" data-target="#themdanhmuc-modal" class="link-custom black-custom" title="Thêm danh mục">
						<i class="fa fa-plus-circle" aria-hidden="true"></i> <label>Thêm mới</label>
					</a>
				</li>
				<li>
					<button type="button" class="button button-header link-custom black-custom">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <label>Lưu lại</label>
                    </button>
				</li>
				<li class="box-button-info-order">
					<a class="show-info-order link-custom black-custom" href="javascript:void(0);">
						<i class="fa fa-list" aria-hidden="true"></i> <label>Danh mục</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="tab-custom bg-black">
				<div class="item active">
					<a href="javascript:void(0)" data-id="info-dm" title="Chi tiết danh mục">
						<i class="fa fa-sticky-note" aria-hidden="true"></i>
						<label>Chi tiết</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="seo-dm" title="SEO">
						<i class="fa fa-bullhorn" aria-hidden="true"></i>
						<label>SEO</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="note-dm" title="Nội dung">
						<i class="fa fa-book" aria-hidden="true"></i>
						<label>Nội dung</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="listsp-dm" title="Danh sách tin tức">
						<i class="fa fa-cube" aria-hidden="true"></i>
						<label>Tin tức</label>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<div class="group-category">
					<div class="item show-child-1 show-all-1">
						<input id="show-2" class="show-all-1" type="hidden" value="0">
						<a href="#" title="Thông tin tiêu dùng">Thông tin tiêu dùng </a>
						<span class="show-category" onclick="showChild(2)"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</div>
					<div style="display: none;" class="item show-child-2 show-all-1 show-all-2">
						<input id="show-3" class=" show-all-1 show-all-2" type="hidden" value="0">
						<a href="#" title="Tiêu dùng 1" style="padding-left: 20px">Tiêu dùng 1 </a>
						<span class="show-category" onclick="showChild(3)"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</div>
					<div style="display: none;" class="item show-child-2 show-all-1 show-all-2">
						<input id="show-4" class=" show-all-1 show-all-2" type="hidden" value="0">
						<a href="#" title="Tiêu dùng 2" style="padding-left: 20px">Tiêu dùng 2 </a>
						<span class="show-category" onclick="showChild(4)"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</div>
					<div style="display: none;" class="item show-child-2 show-all-1 show-all-2">
						<input id="show-5" class=" show-all-1 show-all-2" type="hidden" value="0">
						<a href="#" title="Tiêu dùng 3" style="padding-left: 20px">Tiêu dùng 3 </a>
						<span class="show-category" onclick="showChild(5)"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</div>
					<div class="item show-child-1 show-all-1">
						<input id="show-6" class="show-all-1" type="hidden" value="0">
						<a href="#" title="Thực phẩm làm đẹp">Tin tức công ty </a>
						<span class="show-category" onclick="showChild(6)"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</div>
					<div style="display: none;" class="item show-child-6 show-all-1 show-all-6">
						<input id="show-7" class=" show-all-1 show-all-6" type="hidden" value="0">
						<a href="#" title="Tin tức công ty 1" style="padding-left: 20px">Tin tức công ty 1 </a>
						<span class="show-category" onclick="showChild(7)"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</div>
					<div style="display: none;" class="item show-child-6 show-all-1 show-all-6">
						<input id="show-8" class=" show-all-1 show-all-6" type="hidden" value="0">
						<a href="#" title="Tin tức công ty 2" style="padding-left: 20px">Tin tức công ty 2 </a>
						<span class="show-category" onclick="showChild(8)"></span>
					</div>
					<div style="display: none;" class="item show-child-6 show-all-1 show-all-6">
						<input id="show-9" class=" show-all-1 show-all-6" type="hidden" value="0">
						<a href="#" title="Tin tức công ty 3" style="padding-left: 20px">Tin tức công ty 3 </a>
						<span class="show-category" onclick="showChild(9)"></span>
					</div>
				</div>
				<div id="info-dm" class="group-custom-cate tab-content item show-inline">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">Chi tiết danh mục</a>
					<div class="box-cate-custom">
						<div class="item">
					    	<label for="id_parent1">Chọn nhóm cha:</label>
				    		<select class="form-control" name="id_parent1" id="id_parent1">
					    		<option value="-1" selected="selected">Chọn danh mục cha</option>
							  	<option value="0">Thông tin tiêu dùng</option>
							  	<option value="1">Tin tức công ty</option>
							</select>
					    </div>
					    <div class="item">
					    	<label for="fullname">Tên nhóm:</label>
					    	<input autocomplete="off" type="text" name="name_vi" id="name_vi" placeholder="Nhập tên nhóm" value="Collagen" class="form-control" required="">
					    </div>
					    <div class="item">
					    	<label for="email">STT:</label>
					    	<input autocomplete="off" type="text" name="sort" id="sort" value="3" class="form-control" required="">
					    </div>
					    <div class="item">
					    	<label for="showview">Ẩn / hiện:</label>
					    	<div class="custom-dropdown">
					    		<select class="form-control" name="showview" id="showview">
						    		<option value="-1">Chọn</option>
								  	<option value="0">Ẩn</option>
								  	<option value="1">Hiện</option>
								</select>
					    	</div>
					    </div>
					    <div class="item">
							<label>Ảnh đại diện (16x9):</label>
							<div class="upload-img">
	                            <div class="input-group up-img">
	                                <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
	                                <label class="button bg-green custom-upload">
	                                    <input type="file" id="ipt-img" class="form-control" name="imgs" onchange="readURL(this,1);" accept="image/*">Upload
	                                </label>
	                                <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
	                            </div>
	                        </div>
	                        <div class="box-img-meta">
	                        	<img src="" alt="hình" id="photo1">
	                        </div>
						</div>
					</div>
				</div>
				<div id="seo-dm" class="group-box-seo tab-content item">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">SEO</a>
					<div class="box-seo">
						<div class="item">
							<label for="slug_vi">Link URL:</label>
							<input autocomplete="off" type="text" name="slug_vi" id="slug_vi" placeholder="Nhập link url vd:danh-muc" value="linh-vuc-hoat-dong" class="form-control">
						</div>
						<div class="item">
							<label for="slug_vi">Tags:</label>
							<input name="vtags" id="vTags" value="" type="hidden">
	                        <input id="Tags" value="collagen,nước uống collagen,collagen dạng bột,tủ lạnh,máy sấy,collagen1,nước uống collagen 1,collagen dạng bột 1,tủ lạnh 1,máy sấy 1" type="hidden">
	                        <ul id="ShowTag"></ul>
						</div>
						<div class="item">
							<label for="meta_web_title">Google:</label>
							<span class="seo-input">
								Title:
								<input autocomplete="off" type="text" name="meta_web_title" id="meta_web_title" placeholder="Nhập title" value="" class="form-control">
							</span>
							<span class="seo-input">
								Keyword:
								<input autocomplete="off" type="text" name="meta_web_keyword" id="meta_web_keyword" placeholder="Nhập keyword" value="" class="form-control">
							</span>
							<span class="seo-input">
								Description:
								<textarea name="meta_web_desc" id="meta_web_desc" class="form-control" rows="3" placeholder="Nhập description"></textarea>
							</span>
						</div>
						<div class="item">
							<label for="meta_web_title">Social:</label>
							<span class="seo-input">
								Title:
								<input autocomplete="off" type="text" name="og_title" id="og_title" placeholder="Meta O.g title..." value="" class="form-control">
							</span>
							<span class="seo-input">
								Description:
								<textarea name="og_desc" id="og_desc" class="form-control" rows="3" placeholder="Meta O.g descriptions..."></textarea>
							</span>
							<span class="seo-input">
								Image (16x9):
								<div class="upload-img">
		                            <div class="input-group up-img">
		                                <input autocomplete="off" type="text" id="ipt-img" class="form-control" placeholder="Chưa có hình ảnh nào được chọn" readonly="">
		                                <label class="button bg-green custom-upload">
		                                    <input type="file" class="form-control" name="icon" id="icon" onchange="readURL(this,2);" accept="image/*">Upload
		                                </label>
		                                <button type="button" class="button bg-red custom-upload delete-img">Xóa</button>
		                            </div>
		                        </div>
							</span>
							<div class="box-img-meta">
	                        	<img src="" alt="image" id="photo2">
	                        </div>
						</div>
					</div>
				</div>
				<div id="note-dm" class="tab-content item">
					<a class="content-product dropdown-collapse bg-black" href="javascript:void(0);">Nội dung <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div class="box-ckeditor">
						<label>Mô tả ngắn:</label>
						<textarea rows="5" class="form-control" id="notes" name="notes"></textarea>
						<label>Nội dung:</label>
						<textarea class="ckeditor" id="elm1" name="desc_vi"></textarea>
					</div>
				</div>
				<div id="listsp-dm" class="tab-content item">
					<a class="dropdown-collapse bg-black" href="javascript:void(0);">Danh sách tin tức</a>
					<div class="box-quick-search">
						<div class="item">
							<form name="quick_search" id="frm" action="" method="post" class="search1">
			                   	<input name="search" value="" type="text" class="form-control custom-ipt" placeholder="Tìm ID, tên bài viết...">
			                   	<button type="submit" class="button bg-black">Tìm kiếm</button>
			                </form>
						</div>
						<div class="item">
							<form name="status_search" id="frm" action="" method="post" class="search2">
		                        <div class="custom-dropdown">
							    	<select class="form-control" id="status_cart" name="status_cart">
							    		<option value="-1">Chọn trạng thái</option>
									  	<option value="0">Lọc</option>
									  	<option value="1">Được xem nhiều</option>
									  	<option value="2">Ngày Publish</option>
									</select>
								</div>
								<div class="box-time">
		                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-order" id="date-order" class="form-control ipt-date" placeholder="Chọn ngày...">
		                            <i class="fa fa-calendar icon-time"></i>
		                        </div>
		                        <button type="submit" class="button bg-black">Tìm kiếm</button>
							</form>
						</div>
					</div>
					<div class="box-table">
						<table class="table table-custom table-responsive">
						    <thead>
						        <tr>
						            <th class="black-custom bold center-custom">STT</th>
						            <th class="black-custom bold center-custom">ID</th>
						            <th class="black-custom bold">Tên bài viết</th>
						            <th class="black-custom bold center-custom">Hình ảnh</th>
						            <th class="black-custom bold">Ngày Publish</th>
						            <th class="black-custom bold">Publish</th>
						            <th class="black-custom bold center-custom">Lượt xem</th>
						            <th class="black-custom bold center-custom">Vị trí</th>
						            <th class="black-custom bold center-custom">Ghim</th>
						            <th class="black-custom bold">Time limit</th>
						            <th class="black-custom bold center-custom">Tác vụ</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td data-title="STT" class="center-custom">1</td>
						            <td data-title="ID" class="center-custom">192</td>
						            <td data-title="Tên bài viết">
						            	Team Building Trường Thịnh Group 9/2017 - Chuyến đi sự gắn kết
						            </td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="http://truongthinhsteel.com.vn/uploads/news/1558347161-1550136933-company-trip-2.jpeg" alt="hình ảnh">
						            </td>
						            <td data-title="Ngày Publish">14-05-2019 | 18:05</td>
						            <td data-title="Publish">UnPublish</td>
						            <td data-title="Lượt xem" class="center-custom">521</td>
						            <td data-title="Vị trí" class="center-custom">
							            <input type="number" class="form-control" value="1" id="sort_cate_news_1">
							        </td>
						            <td data-title="Ghim" class="center-custom">
						            	<label class="checkbox-custom">
						                  <input value="" id="checked_cate_1" type="checkbox" checked="checked">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="Time limit">
							            <div class="box-time">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="time_limit_1" class="form-control ipt-date" placeholder="Chọn ngày...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
							        </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
						            		<i class="fa fa-save"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">2</td>
						            <td data-title="ID" class="center-custom">192</td>
						            <td data-title="Tên bài viết">
						            	Team Building Trường Thịnh Group 9/2017 - Chuyến đi sự gắn kết
						            </td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="http://truongthinhsteel.com.vn/uploads/news/1558347161-1550136933-company-trip-2.jpeg" alt="hình ảnh">
						            </td>
						            <td data-title="Ngày Publish">14-05-2019 | 18:05</td>
						            <td data-title="Publish">UnPublish</td>
						            <td data-title="Lượt xem" class="center-custom">521</td>
						            <td data-title="Vị trí" class="center-custom">
							            <input type="number" class="form-control" value="1" id="sort_cate_news_2">
							        </td>
						            <td data-title="Ghim" class="center-custom">
						            	<label class="checkbox-custom">
						                  <input value="" id="checked_cate_2" type="checkbox" checked="checked">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="Time limit">
							            <div class="box-time">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="time_limit_2" class="form-control ipt-date" placeholder="Chọn ngày...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
							        </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
						            		<i class="fa fa-save"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">3</td>
						            <td data-title="ID" class="center-custom">192</td>
						            <td data-title="Tên bài viết">
						            	Team Building Trường Thịnh Group 9/2017 - Chuyến đi sự gắn kết
						            </td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="http://truongthinhsteel.com.vn/uploads/news/1558347161-1550136933-company-trip-2.jpeg" alt="hình ảnh">
						            </td>
						            <td data-title="Ngày Publish">14-05-2019 | 18:05</td>
						            <td data-title="Publish">UnPublish</td>
						            <td data-title="Lượt xem" class="center-custom">521</td>
						            <td data-title="Vị trí" class="center-custom">
							            <input type="number" class="form-control" value="1" id="sort_cate_news_3">
							        </td>
						            <td data-title="Ghim" class="center-custom">
						            	<label class="checkbox-custom">
						                  <input value="" id="checked_cate_3" type="checkbox" checked="checked">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="Time limit">
							            <div class="box-time">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="time_limit_3" class="form-control ipt-date" placeholder="Chọn ngày...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
							        </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
						            		<i class="fa fa-save"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">4</td>
						            <td data-title="ID" class="center-custom">192</td>
						            <td data-title="Tên bài viết">
						            	Team Building Trường Thịnh Group 9/2017 - Chuyến đi sự gắn kết
						            </td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="http://truongthinhsteel.com.vn/uploads/news/1558347161-1550136933-company-trip-2.jpeg" alt="hình ảnh">
						            </td>
						            <td data-title="Ngày Publish">14-05-2019 | 18:05</td>
						            <td data-title="Publish">UnPublish</td>
						            <td data-title="Lượt xem" class="center-custom">521</td>
						            <td data-title="Vị trí" class="center-custom">
							            <input type="number" class="form-control" value="1" id="sort_cate_news_4">
							        </td>
						            <td data-title="Ghim" class="center-custom">
						            	<label class="checkbox-custom">
						                  <input value="" id="checked_cate_4" type="checkbox" checked="checked">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="Time limit">
							            <div class="box-time">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="time_limit_4" class="form-control ipt-date" placeholder="Chọn ngày...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
							        </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
						            		<i class="fa fa-save"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">5</td>
						            <td data-title="ID" class="center-custom">192</td>
						            <td data-title="Tên bài viết">
						            	Team Building Trường Thịnh Group 9/2017 - Chuyến đi sự gắn kết
						            </td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="http://truongthinhsteel.com.vn/uploads/news/1558347161-1550136933-company-trip-2.jpeg" alt="hình ảnh">
						            </td>
						            <td data-title="Ngày Publish">14-05-2019 | 18:05</td>
						            <td data-title="Publish">UnPublish</td>
						            <td data-title="Lượt xem" class="center-custom">521</td>
						            <td data-title="Vị trí" class="center-custom">
							            <input type="number" class="form-control" value="1" id="sort_cate_news_5">
							        </td>
						            <td data-title="Ghim" class="center-custom">
						            	<label class="checkbox-custom">
						                  <input value="" id="checked_cate_5" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="Time limit">
							            <div class="box-time">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="time_limit_5" class="form-control ipt-date" placeholder="Chọn ngày...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
							        </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
						            		<i class="fa fa-save"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">6</td>
						            <td data-title="ID" class="center-custom">192</td>
						            <td data-title="Tên bài viết">
						            	Team Building Trường Thịnh Group 9/2017 - Chuyến đi sự gắn kết
						            </td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="http://truongthinhsteel.com.vn/uploads/news/1558347161-1550136933-company-trip-2.jpeg" alt="hình ảnh">
						            </td>
						            <td data-title="Ngày Publish">14-05-2019 | 18:05</td>
						            <td data-title="Publish">Publish</td>
						            <td data-title="Lượt xem" class="center-custom">521</td>
						            <td data-title="Vị trí" class="center-custom">
							            <input type="number" class="form-control" value="1" id="sort_cate_news_6">
							        </td>
						            <td data-title="Ghim" class="center-custom">
						            	<label class="checkbox-custom">
						                  <input value="" id="checked_cate_6" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="Time limit">
							            <div class="box-time">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="time_limit_6" class="form-control ipt-date" placeholder="Chọn ngày...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
							        </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
						            		<i class="fa fa-save"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">7</td>
						            <td data-title="ID" class="center-custom">192</td>
						            <td data-title="Tên bài viết">
						            	Team Building Trường Thịnh Group 9/2017 - Chuyến đi sự gắn kết
						            </td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="http://truongthinhsteel.com.vn/uploads/news/1558347161-1550136933-company-trip-2.jpeg" alt="hình ảnh">
						            </td>
						            <td data-title="Ngày Publish">14-05-2019 | 18:05</td>
						            <td data-title="Publish">Publish</td>
						            <td data-title="Lượt xem" class="center-custom">521</td>
						            <td data-title="Vị trí" class="center-custom">
							            <input type="number" class="form-control" value="1" id="sort_cate_news_7">
							        </td>
						            <td data-title="Ghim" class="center-custom">
						            	<label class="checkbox-custom">
						                  <input value="" id="checked_cate_7" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="Time limit">
							            <div class="box-time">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="time_limit_7" class="form-control ipt-date" placeholder="Chọn ngày...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
							        </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
						            		<i class="fa fa-save"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">8</td>
						            <td data-title="ID" class="center-custom">192</td>
						            <td data-title="Tên bài viết">
						            	Team Building Trường Thịnh Group 9/2017 - Chuyến đi sự gắn kết
						            </td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="http://truongthinhsteel.com.vn/uploads/news/1558347161-1550136933-company-trip-2.jpeg" alt="hình ảnh">
						            </td>
						            <td data-title="Ngày Publish">14-05-2019 | 18:05</td>
						            <td data-title="Publish">Publish</td>
						            <td data-title="Lượt xem" class="center-custom">521</td>
						            <td data-title="Vị trí" class="center-custom">
							            <input type="number" class="form-control" value="1" id="sort_cate_news_8">
							        </td>
						            <td data-title="Ghim" class="center-custom">
						            	<label class="checkbox-custom">
						                  <input value="" id="checked_cate_8" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="Time limit">
							            <div class="box-time">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="time_limit_8" class="form-control ipt-date" placeholder="Chọn ngày...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
							        </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
						            		<i class="fa fa-save"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">9</td>
						            <td data-title="ID" class="center-custom">192</td>
						            <td data-title="Tên bài viết">
						            	Team Building Trường Thịnh Group 9/2017 - Chuyến đi sự gắn kết
						            </td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="http://truongthinhsteel.com.vn/uploads/news/1558347161-1550136933-company-trip-2.jpeg" alt="hình ảnh">
						            </td>
						            <td data-title="Ngày Publish">14-05-2019 | 18:05</td>
						            <td data-title="Publish">Publish</td>
						            <td data-title="Lượt xem" class="center-custom">521</td>
						            <td data-title="Vị trí" class="center-custom">
							            <input type="number" class="form-control" value="9" id="sort_cate_news_9">
							        </td>
						            <td data-title="Ghim" class="center-custom">
						            	<label class="checkbox-custom">
						                  <input value="" id="checked_cate_9" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="Time limit">
							            <div class="box-time">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="time_limit_9" class="form-control ipt-date" placeholder="Chọn ngày...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
							        </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
						            		<i class="fa fa-save"></i>
						            	</a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">10</td>
						            <td data-title="ID" class="center-custom">192</td>
						            <td data-title="Tên bài viết">
						            	Team Building Trường Thịnh Group 9/2017 - Chuyến đi sự gắn kết
						            </td>
						            <td data-title="Hình ảnh" class="center-custom">
						            	<img src="http://truongthinhsteel.com.vn/uploads/news/1558347161-1550136933-company-trip-2.jpeg" alt="hình ảnh">
						            </td>
						            <td data-title="Ngày Publish">14-05-2019 | 18:05</td>
						            <td data-title="Publish">Publish</td>
						            <td data-title="Lượt xem" class="center-custom">521</td>
						            <td data-title="Vị trí" class="center-custom">
							            <input type="number" class="form-control" value="10" id="sort_cate_news_10">
							        </td>
						            <td data-title="Ghim" class="center-custom">
						            	<label class="checkbox-custom">
						                  <input value="" id="checked_cate_10" type="checkbox">
						                  <span class="checkmark"></span>
						                </label>
						            </td>
						            <td data-title="Time limit">
							            <div class="box-time">
				                            <input autocomplete="off" onkeypress="return false;" type="text" name="date-news" id="time_limit_10" class="form-control ipt-date" placeholder="Chọn ngày...">
				                            <i class="fa fa-calendar icon-time"></i>
				                        </div>
							        </td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:void(0);" class="link-custom black-custom" title="Lưu">
						            		<i class="fa fa-save"></i>
						            	</a>
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
					<?php include('include/pagination.php')?>
				</div>
			</div>
		</div>
	</article>
</main>
<?php include('include/categorynews/add.php'); ?>
<script>
	function showChild(id){
        var show = jQuery("#show-"+id).val();
        if(show == 0){
            jQuery(".show-child-"+id).show();
            jQuery("#show-"+id).next().addClass('active');
            jQuery("#show-"+id).val(1);
            jQuery('#show-'+id).next().next().html('<i class="fa fa-angle-down" aria-hidden="true"></i>');
        }else{
            jQuery(".show-all-"+id).hide();
            jQuery(".show-all-"+id).val(0);
            jQuery('#show-'+id).next().next().html('<i class="fa fa-angle-right" aria-hidden="true"></i>');
            jQuery("#show-"+id).val(0);
            jQuery("#show-"+id).next().removeClass('active');
        }
    }
    function readURL(input,id) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	        	jQuery('#photo'+id).parent().css('display','block');
	          	jQuery('#photo'+id).attr('src', e.target.result);
	          	if(jQuery('.group-box-seo').height()>jQuery('.group-custom-cate').height()){
					jQuery('.group-category').height(jQuery('.group-box-seo').height()-2);
				}
				else{
					jQuery('.group-category').height(jQuery('.group-custom-cate').height());
				}
	        };
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	jQuery(function(){
		var formatDate = {
	      	format: 'DD/MM/YYYY',
	      	daysOfWeek: [
	            "CN",
	            "T2",
	            "T3",
	            "T4",
	            "T5",
	            "T6",
	            "T7"
	        ],
	        monthNames: [
	            "Tháng 1",
	            "Tháng 2",
	            "Tháng 3",
	            "Tháng 4",
	            "Tháng 5",
	            "Tháng 6",
	            "Tháng 7",
	            "Tháng 8",
	            "Tháng 9",
	            "Tháng 10",
	            "Tháng 11",
	            "Tháng 12"
	        ],
	    }
	    if(window.innerWidth > 576) {
	    	var dropDate = 'down';
	    }
	    else{
	    	var dropDate = 'up';
	    }
	    jQuery('input[name="date-order"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		},function(start,end) {
			jQuery('#start-date3').val(start.format('DD-MM-YYYY'));
			jQuery('#end-date3').val(end.format('DD-MM-YYYY'));
		});

		jQuery('input[name="date-news"]').daterangepicker({
			opens: 'left',
			drops: dropDate,
			autoApply: true,
			maxDate:new Date(),
			locale: formatDate
		});

		CKEDITOR.instances["elm1"];
		new PerfectScrollbar('.group-category');
		
		jQuery('#id_parent1').select2();
		var sampleTags = jQuery("#vTags").val();
        var sampleTags2 = jQuery("#Tags").val();
        sampleTags = sampleTags.split(",");
        sampleTags2 = sampleTags2.split(",");
        jQuery('#ShowTag').tagit({
          availableTags: sampleTags2,
          singleField: true,
          allowSpaces: true,
          allowDuplicates: false,
          singleFieldNode: jQuery('#vTags'),
          beforeTagAdded: function(event, ui) {
                if ($.inArray(ui.tagLabel, sampleTags2) == -1) {
                    alert(ui.tagLabel + ' không phải là tag khả dụng.');
                    jQuery('.tagit-new input').val('');
                    return false;
                }
            }
        });
        jQuery('#ShowTag2').tagit({
          availableTags: sampleTags2,
          singleField: true,
          allowSpaces: true,
          allowDuplicates: false,
          singleFieldNode: jQuery('#vTags2'),
          beforeTagAdded: function(event, ui) {
                if ($.inArray(ui.tagLabel, sampleTags2) == -1) {
                    alert(ui.tagLabel + ' không phải là tag khả dụng.');
                    jQuery('.tagit-new input').val('');
                    return false;
                }
            }
        });
		jQuery('.content-product').click(function(){
	    	if(jQuery('.box-ckeditor').css('display')=='none'){
	    		jQuery('.box-ckeditor').css('display','inline-block');
	    		jQuery(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
	    		jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
	    	}
	    	else{
	    		jQuery('.box-ckeditor').css('display','none');
	    		jQuery(this).find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
	    		jQuery('.nav-primary').css('height',jQuery('.content-sidebar-wrap').outerHeight());
	    	}
	    });

		jQuery(document).on('change', ':file', function() {
		    var input = jQuery(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		});
		jQuery(':file').on('fileselect', function(event, numFiles, label) {
          	var input = jQuery(this).parents('.up-img').find(':text'),
              	log = numFiles > 1 ? numFiles + ' files selected' : label;
          	if( input.length ) {
              	input.val(log);
              	if(input.val(log)){
                	jQuery(this).parents('.up-img').find('.custom-upload').css('display','none');
                	jQuery(this).parents('.up-img').find('.delete-img').css('display','block');
              	}
          	} else {
              	if(log) ;
          	}
      	});
      	jQuery('.delete-img').on('click', function(e){
		    jQuery(this).parent().find('input[type=file]').val('');
		    jQuery(this).parent().find('input[type=text]').val('');
		    jQuery(this).parent().find('.custom-upload').css('display','flex');
		    jQuery(this).parent().parent().next().css('display','none');
		    jQuery(this).css('display','none');
		    jQuery('.group-category').height(jQuery('.group-custom-cate').height());
		    if(jQuery('.group-box-seo').height()>jQuery('.group-custom-cate').height()){
				jQuery('.group-category').height(jQuery('.group-box-seo').height()-2);
			}
			else{
				jQuery('.group-category').height(jQuery('.group-custom-cate').height());
			}
		});
      	jQuery('.categorynews .tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.categorynews .tab-content').not('#' + data).removeClass('show-inline');
	    	jQuery(this).parent().addClass('active');
	    	jQuery('.categorynews .tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    	if(window.innerWidth < 576){
				var heightPagination = jQuery('.pagination-custom').outerHeight();
				jQuery('#listsp-dm .box-table').css('margin-bottom',heightPagination);
			}
	    });

	    jQuery('.show-info-order').click(function(){
	    	if(jQuery('.group-category').css('display')=='none'){
	    		jQuery('.group-category').css('display','block');
	    		showBackgroundPopup();
	    	}
	    	else{
	    		jQuery('.group-category').css('display','none');
	    		deleteBackgroundPopup();
	    	}
	    });
	    jQuery('.pos-cate').click(function(e){
		    jQuery(".dropdown-table").toggle();
		});
		if(window.innerWidth > 768){
			if(jQuery('.group-box-seo').height()>jQuery('.group-custom-cate').height()){
				jQuery('.group-category').height(jQuery('.group-box-seo').height()-2);
			}
			else{
				jQuery('.group-category').height(jQuery('.group-custom-cate').height());
			}
		}
	});
</script>