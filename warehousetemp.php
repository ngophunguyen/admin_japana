<style>
	.box-quick-search{
		display: inline-block;
		width: 100%;
		margin-top: 15px;
	}
	.box-quick-search .item{
		display: inline-block;
		width: 50%;
		float: left;
	}
	.box-quick-search .item:first-child input{
		width: 50%;
		float: left;
	}
	.box-quick-search .item:first-child button{
		float: left;
		margin-left: 15px;
	}	
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}

	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	}
	.table-custom thead tr th:last-child{
		padding-right: 30px;
	}
	.custom-dropdown{
		display: inline-block;
		width: 50%;
		float: right;
	}
	.custom-dropdown:after{
		padding-right: 15px;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item{
			width: 100%;
		}
		.box-quick-search .item:first-child input{
			width: 70%;
		}
		.box-quick-search .item:last-child{
			margin-top: 15px;
		}
		.custom-dropdown{
			width: 100%;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
		.table-custom > tbody > tr > td:last-child{
			justify-content: inherit;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {

	}
	@media (min-width: 768px) and (max-width: 991.98px) {

	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="warehousetemp content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Danh sách kho tạm</h1>
			<ul>
				<li>
					<a href="#" class="link-custom black-custom" title="Xuất Excel">
						<i class="fa fa-file-excel-o" aria-hidden="true"></i> <label>Xuất Excel</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Reset">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Reset</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="container-fluid">
				<div class="box-quick-search">
					<div class="item">
						<form name="frm" id="frm" action="" method="post" class="search1">
	                       <input autocomplete="off" name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm kiếm SKU hoặc Tên sản phẩm">
	                       <button type="submit" class="button bg-black">Tìm kiếm</button>
	                    </form>
					</div>
					<div class="item">
						<form id="FormOderBywarehousetemp" action="" method="post" class="search2">
	                        <div class="custom-dropdown">
						    	<select onchange="onchangeSubmit()" class="form-control" id="loc_sp" name="loc_sp">
						    		<option value="-1">Tất cả</option>
								  	<option value="0">Còn hàng</option>
								  	<option value="1">Hết hàng</option>
								</select>
							</div>
						</form>
					</div>
				</div>
				<div class="box-table">
					<table class="table table-custom table-striped table-responsive">
					    <thead class="bg-black">
					        <tr class="bg-black">
					            <th class="bg-black center-custom">STT</th>
					            <th class="bg-black center-custom">SKU</th>
					            <th class="bg-black">Tên sản phẩm</th>
					            <th class="bg-black center-custom">SL trong kho</th>
					            <th class="bg-black center-custom">SL đã xác nhận</th>
					            <th class="bg-black center-custom">SL chờ duyệt</th>
					            <th class="bg-black center-custom">SL còn lại</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr>
					            <td data-title="STT" class="center-custom">1</td>
					            <td data-title="SKU" class="center-custom">001AA18</td>
					            <td data-title="Tên sản phẩm">Bột Shiseido The Collagen Nhật Bản 126g</td>
					            <td data-title="SL trong kho" class="center-custom">0</td>
					            <td data-title="SL đã xác nhận" class="center-custom">2</td>
					            <td data-title="SL chờ duyệt" class="center-custom">0</td>
					            <td data-title="SL còn lại" class="center-custom"><span class="red-custom">-2</span></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">2</td>
					            <td data-title="SKU" class="center-custom">001AA20</td>
					            <td data-title="Tên sản phẩm">Nước uống Collagen Shiseido Ex (Hộp 10 chai x 50ml)</td>
					            <td data-title="SL trong kho" class="center-custom">0</td>
					            <td data-title="SL đã xác nhận" class="center-custom">4</td>
					            <td data-title="SL chờ duyệt" class="center-custom">0</td>
					            <td data-title="SL còn lại" class="center-custom"><span class="red-custom">-4</span></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">3</td>
					            <td data-title="SKU" class="center-custom">001DEK35</td>
					            <td data-title="Tên sản phẩm">Viên uống hỗ trợ nở ngực Orihiro BBB Best Body Beauty 300 viên</td>
					            <td data-title="SL trong kho" class="center-custom">4</td>
					            <td data-title="SL đã xác nhận" class="center-custom">1</td>
					            <td data-title="SL chờ duyệt" class="center-custom">0</td>
					            <td data-title="SL còn lại" class="center-custom">3</td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">4</td>
					            <td data-title="SKU" class="center-custom">001CD112</td>
					            <td data-title="Tên sản phẩm">Bột lúa mạch Grass Barloy Nhật Bản 44 gói</td>
					            <td data-title="SL trong kho" class="center-custom">0</td>
					            <td data-title="SL đã xác nhận" class="center-custom">1</td>
					            <td data-title="SL chờ duyệt" class="center-custom">0</td>
					            <td data-title="SL còn lại" class="center-custom"><span class="red-custom">-1</span></td>
					        </tr>
					        <tr>
					            <td data-title="STT" class="center-custom">5</td>
					            <td data-title="SKU" class="center-custom">001FAF80</td>
					            <td data-title="Tên sản phẩm">Giấy ướt khử mùi Gatsby Ice Nhật Bản 30 miếng</td>
					            <td data-title="SL trong kho" class="center-custom">7</td>
					            <td data-title="SL đã xác nhận" class="center-custom">10</td>
					            <td data-title="SL chờ duyệt" class="center-custom">0</td>
					            <td data-title="SL còn lại" class="center-custom"><span class="red-custom">-3</span></td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
			
		</div>
	</article>
</main>
<script>
	function onchangeSubmit() {
        document.getElementById("FormOderBywarehousetemp").submit();
    }
	jQuery(function(){
		if(window.innerWidth < 576){
			jQuery('.entry-content').css('margin-bottom','15px')
		}
	})
</script>