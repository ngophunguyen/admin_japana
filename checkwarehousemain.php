<style>
	.checkwarehousemain h4{
		font-size: 16px;
		margin: 15px 0;
		display: inline-block;
		width: 100%;
	}
	.box-quick-search, .box-check-item{
		display: inline-block;
		width: 100%;
	}
	.box-quick-search .item, .box-check-item .item{
		display: inline-block;
		width: 50%;
		float: left;
	}
	.box-quick-search .item:first-child input, .box-check-item input{
		width: 50%;
		float: left;
	}
	.box-quick-search .item:first-child button, .box-check-item input:last-child{
		float: left;
		margin-left: 15px;
	}
	.box-check-item input:last-child{
		width: 100px;
	}
	.search1{
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	    -webkit-box-align: center;
	        -ms-flex-align: center;
	            align-items: center;
	    -webkit-box-pack: start;
	        -ms-flex-pack: start;
	            justify-content: flex-start;
		width: 100%;
	}

	.box-table{
		width: auto;
	    height: 100%;
	    overflow-x: scroll;
	    cursor: -webkit-grab;
	    cursor: grab;
	    margin-bottom: 15px;
	    position: relative;
	}
	.table-custom thead tr th:last-child{
		padding-right: 30px;
	}
	.custom-dropdown{
		display: inline-block;
		width: 50%;
		float: right;
	}
	.custom-dropdown:after{
		padding-right: 15px;
	}
	@media (max-width: 575.98px) {
		.box-quick-search .item, .box-check-item .item{
			width: 100%;
		}
		.box-quick-search .item:first-child input, .box-check-item .item input{
			width: 70%;
		}
		.box-check-item input:last-child{
			width: 30%;
		}
		.box-quick-search .item:last-child{
			margin-top: 15px;
		}
		.custom-dropdown{
			width: 100%;
		}
		#checking-wh, #checked-wh{
			display: none;
			width: 100%;
		}
		.table-custom > tbody > tr > td:first-child{
			display: none;
		}
		.table-custom > tbody > tr > td:last-child{
			justify-content: inherit;
		}
	}
	@media (min-width: 576px) and (max-width: 767.98px) {

	}
	@media (min-width: 768px) and (max-width: 991.98px) {

	}
	@media (min-width: 992px) and (max-width: 1199.98px) {
		
	}
	@media (min-width: 1200px) {
		
	}
</style>
<main class="checkwarehousemain content">
	<article class="entry">
		<header class="entry-header">
			<h1 class="entry-title">Kiểm kho</h1>
			<ul>
				<li>
					<a href="#" class="link-custom black-custom" title="In mã kho">
						<i class="fa fa-file-pdf-o" aria-hidden="true"></i> <label>In mã kho</label>
					</a>
				</li>
				<li>
					<a href="#" class="link-custom black-custom" title="Xuất Excel kiểm kho">
						<i class="fa fa-file-excel-o" aria-hidden="true"></i> <label>Xuất Excel kiểm kho</label>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" class="link-custom black-custom" title="Kiểm lại kho">
						<i class="fa fa-undo" aria-hidden="true"></i> <label>Kiểm lại kho</label>
					</a>
				</li>
				<?php include('include/pc-user.php'); ?>
			</ul>
		</header>
		<div class="entry-content">
			<div class="tab-custom bg-black">
				<div class="item active">
					<a href="javascript:void(0)" data-id="checking-wh" title="Sản phẩm đang kiểm kho">
						<i class="fa fa-search" aria-hidden="true"></i>
						<label>Đang kiểm</label>
					</a>
				</div>
				<div class="item">
					<a href="javascript:void(0)" data-id="checked-wh" title="Sản phẩm đã kiểm kho">
						<i class="fa fa-check-square-o" aria-hidden="true"></i>
						<label>Đã kiểm</label>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<div id="checking-wh" class="tab-content item show-inline">
					<h4 class="red-custom">Sản phẩm đang kiểm kho</h4>
					<div class="box-check-item">
						<div class="item">
							<form name="frm" id="FormOderBycheckwarehousemain" action="" method="post" class="search1">
		                       <input autocomplete="off" id="code_mavach" name="id_product" value="" type="text" class="form-control custom-ipt" placeholder="Nhập mã kho">
		                       <input value="1" readonly="readonly" id="code_sl" onkeypress="checkXuatKho(event)" name="keyword" type="text" class="form-control custom-ipt" placeholder="Số lượng">
		                    </form>
						</div>
					</div>
					<div class="box-table">
						<table class="table table-custom table-striped table-responsive">
						    <thead class="bg-black">
						        <tr class="bg-black">
						            <th class="bg-black center-custom">STT</th>
						            <th class="bg-black">SKU</th>
						            <th class="bg-black center-custom">ID sản phẩm</th>
						            <th class="bg-black">Tên sản phẩm</th>
						            <th class="bg-black center-custom">SL đã kiểm</th>
						            <th class="bg-black center-custom">SL admin</th>
						            <th class="bg-black center-custom">Tác vụ</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td data-title="STT" class="center-custom">1</td>
						            <td data-title="SKU">KMNCC-037GDA04</td>
						            <td data-title="ID sản phẩm" class="center-custom">6388</td>
						            <td data-title="Tên sản phẩm">Đồ chơi lắp ráp thông minh</td>
						            <td data-title="SL đã kiểm" class="center-custom"><span class="red-custom">5</span></td>
						            <td data-title="SL admin" class="center-custom">4</td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:capNhatSL('6388');" class="link-custom black-custom" title="Kiểm lại sản phẩm này">
	                                        <i class="fa fa-refresh" aria-hidden="true"></i> Cập nhật
	                                    </a>
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
				</div>
				<div id="checked-wh" class="tab-content item">
					<h4 class="red-custom">Sản phẩm đã kiểm kho</h4>
					<div class="box-quick-search">
						<div class="item">
							<form name="frm" id="frm" action="" method="post" class="search1">
		                       <input autocomplete="off" name="value" value="" type="text" class="form-control custom-ipt" placeholder="Tìm kiếm SKU hoặc Tên sản phẩm">
		                       <button type="submit" class="button bg-black">Tìm kiếm</button>
		                    </form>
						</div>
						<div class="item">
							<form id="from-submit" action="" method="post" class="search2">
		                        <div class="custom-dropdown">
							    	<select class="form-control" id="id_status" name="id_status">
							    		<option value="-1">Trạng thái</option>
									  	<option value="0">Đã kiểm</option>
									  	<option value="1">Chưa kiểm</option>
									  	<option value="2">Đang thiếu</option>
									  	<option value="3">Đang thừa</option>
									  	<option value="4">Đang đủ</option>
									</select>
								</div>
							</form>
						</div>
					</div>
					<div class="box-table">
						<table class="table table-custom table-striped table-responsive">
						    <thead class="bg-black">
						        <tr class="bg-black">
						            <th class="bg-black center-custom">STT</th>
						            <th class="bg-black">SKU</th>
						            <th class="bg-black center-custom">ID sản phẩm</th>
						            <th class="bg-black">Tên sản phẩm</th>
						            <th class="bg-black center-custom">SL đã kiểm</th>
						            <th class="bg-black center-custom">SL admin</th>
						            <th class="bg-black center-custom">Tác vụ</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr>
						            <td data-title="STT" class="center-custom">1</td>
						            <td data-title="SKU">KMNCC-037GDA04</td>
						            <td data-title="ID sản phẩm" class="center-custom">6388</td>
						            <td data-title="Tên sản phẩm">Đồ chơi lắp ráp thông minh</td>
						            <td data-title="SL đã kiểm" class="center-custom"><span class="red-custom">5</span></td>
						            <td data-title="SL admin" class="center-custom">4</td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:capNhatSL('6388');" class="link-custom black-custom" title="Kiểm lại sản phẩm này">
	                                        <i class="fa fa-refresh" aria-hidden="true"></i> Cập nhật
	                                    </a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">2</td>
						            <td data-title="SKU">020EG15</td>
						            <td data-title="ID sản phẩm" class="center-custom">1070</td>
						            <td data-title="Tên sản phẩm">Sữa rửa mặt Sakura Sensitive Gentle Cleansing Foam</td>
						            <td data-title="SL đã kiểm" class="center-custom"><span class="red-custom">2</span></td>
						            <td data-title="SL admin" class="center-custom">3</td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:capNhatSL('1070');" class="link-custom black-custom" title="Kiểm lại sản phẩm này">
	                                        <i class="fa fa-refresh" aria-hidden="true"></i> Cập nhật
	                                    </a>
						            </td>
						        </tr>
						        <tr>
						            <td data-title="STT" class="center-custom">3</td>
						            <td data-title="SKU">001ECI430</td>
						            <td data-title="ID sản phẩm" class="center-custom">1108</td>
						            <td data-title="Tên sản phẩm">Gel tẩy trang SK-II Facial Treatment Gentle Cleansing 15g</td>
						            <td data-title="SL đã kiểm" class="center-custom"><span class="red-custom">7</span></td>
						            <td data-title="SL admin" class="center-custom">8</td>
						            <td data-title="Tác vụ" class="center-custom">
						            	<a href="javascript:capNhatSL('1108');" class="link-custom black-custom" title="Kiểm lại sản phẩm này">
	                                        <i class="fa fa-refresh" aria-hidden="true"></i> Cập nhật
	                                    </a>
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
					<?php include('include/pagination.php')?>
				</div>
			</div>
		</div>
	</article>
</main>
<script>
	jQuery(function(){
		var heightBottomPagination = jQuery('.pagination-custom').outerHeight();
		if(window.innerWidth < 576){
			jQuery('.entry-content').css('margin-bottom','15px');
			jQuery('.box-table').css('padding-bottom',heightBottomPagination);
		}
		jQuery('.tab-custom .item a').click(function(){
	    	var data = jQuery(this).data('id');
	    	jQuery('.tab-content').not('#' + data).removeClass('show-inline');
	    	jQuery(this).parent().addClass('active');
	    	jQuery('.tab-custom .item a').not(this).parent().removeClass('active');
	    	jQuery('#'+data).addClass('show-inline');
	    });
	})
</script>